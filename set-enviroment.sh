#!/usr/bin/env bash

if [ "$1" == "dev" ] || [ "$1" == "prod" ] || [ "$1" == "local" ];
then
  ENV=$1;

  PATH_ENV=""
  if [ "$1" == "local" ];
  then
    API_PATH="http://localhost:8000/api/v1";
    KEY_PUSH_NOTIFICATION="AAAAfAZXfvg:APA91bEUwoTU8wwhscAybUCJgw0mtKxFyAYL8CTDuQpjsSJfl5rxMc_eHBIYjiHRguOjIAQ4_H2w0nDFYAvz3CAkMD-D4iRP-ny1uocruHBO7l3DQ4pNWpqvvwsD7-foXnT-cXIalmNP";
  fi
  if [ "$1" == "dev" ];
  then
    API_PATH="https://kidsbook-backend.herokuapp.com/api/v1";
    KEY_PUSH_NOTIFICATION="AAAAfAZXfvg:APA91bEUwoTU8wwhscAybUCJgw0mtKxFyAYL8CTDuQpjsSJfl5rxMc_eHBIYjiHRguOjIAQ4_H2w0nDFYAvz3CAkMD-D4iRP-ny1uocruHBO7l3DQ4pNWpqvvwsD7-foXnT-cXIalmNP";
  fi
  if [ "$1" == "prod" ];
  then
    API_PATH="https://kidsbook.mobi/panel/api/v1";
    KEY_PUSH_NOTIFICATION="AAAAX7fUIqw:APA91bGQWrJJ5CXVQeyWMbvLMGzHbzyY4Uf8O5JwhTSbZ3AZVi1BgMRXG48lXRUIU6XReJah4dsv_ItnOS0d1nV6GZBNaeyypslna8N6juTx-RbD1kythfncpLVYjZyjYjDV0_ZrqSRu";
  fi

  echo "Switching to Firebase to $ENV"
   #cp -rf "firebaseEnviroments/android/$ENV/google-services.json" android/app/
   #cp -rf "firebaseEnviroments/android/$ENV/pd-merchant.keystore" android/app/
   #cp -rf "firebaseEnviroments/ios/$ENV/GoogleService-Info.plist" ios/pidoylistomerchant/
   echo "export default { env: '$ENV', path: '$API_PATH', keyPushNotification: '$KEY_PUSH_NOTIFICATION' };" > env.js
  echo "Finish"
  else
    echo "Missing paramenter, please execute sh set-enviroment.sh dev"
fi
