import { createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';

import Menu from '@modules/menu/menu';
import Authenticator from '@auth/screens/authenticator';
import Login from '@auth/screens/sign-in/sign-in-controller';
import Dashboard from '@dashboard/screens/main-page/dashboard';
import Children from '@dashboard/screens/children/children-controller';
import ChildrenDetail from '@dashboard/screens/detail-children/detail-children-controller';
import Parents from '@dashboard/screens/parents/parents-controller';
import DetailParent from '@dashboard/screens/detail-parent/detail-parent-controller';
import Teachers from '@dashboard/screens/teachers/teachers-controller';
import DetailTeacher from '@dashboard/screens/detail-teacher/detail-teacher-controller';
import Classroom from '@dashboard/screens/classroom/classroom-controller';
import AddEditClassroom from '@dashboard/screens/add-edit-classroom/add-edit-classroom-controller';
import Reports from '@dashboard/screens/reports/reports-controller';
import SocialNetwork from '@dashboard/screens/social-network/social-network-controller';
import CreateSocialNetwork from '@dashboard/screens/create-social-network/create-social-network-controller';
import DetailSocialNetwork from '@dashboard/screens/detail-social-network/detail-social-network-controller';
import Calendar from '@dashboard/screens/calendar/calendar-controller';
import Settings from '@dashboard/screens/settings/settings-controller';
import AddParentsToChild from '@dashboard/screens/add-parents-to-child/add-parents-to-child-controller';
import AddParent from '@dashboard/screens/add-parent/add-parent-controller';
import Language from '@modules/language/language-controller';
import ForgotPassword from '@auth/screens/forgot-password/forgot-password-controller';
import NotificationPage from '@auth/screens/notification-page/notification-page-controller';
import RequestAccess from '@auth/screens/request-access/request-access-controller';
import VerificateClient from '@dashboard/screens/verificate-client/verificate-client-controller';
import VerificationCenter from '@dashboard/screens/verification-center/verification-center-controller';
import SearchVerifyClient from '@dashboard/screens/search-verify-client/search-verify-client-controller';
import ListGlobal from '@dashboard/screens/list-global/list-global-controller';
import AddTeacher from '@dashboard/screens/add-teacher/add-teacher-controller';
import Messages from '@dashboard/screens/messages/messages-controller';
import MessageDetail from '@modules/dashboard/screens/message-detail/message-detail-controller';
import CreateMessage from '@dashboard/screens/create-message/create-message-controller';
import ClassroomTeacher from '@dashboard/screens/classroom-teacher/classroom-teacher-controller';
import EventRepeat from '@dashboard/screens/event-repeat/event-repeat-controller';
import ReportDetail from '@dashboard/screens/report-detail/report-detail-controller';
import CreateFastAnswer from '@dashboard/screens/create-fast-answer/create-fast-answer-controller';
import GlobalListMessage from '@dashboard/screens/global-list-message/global-list-message-controller';
import OpenFiles from '@modules/open-files/open-files-controller';
import Activities from '@dashboard/screens/activities/activities-controller';
import CreateActivity from '@dashboard/screens/create-activity/create-activity-controller';
import ActivityDetails from '@dashboard/screens/activity-details/activity-details-controller';
import Attendance from '@dashboard/screens/attendance/attendance-controller';
import CreateReport from '@dashboard/screens/create-report/create-report-controller';
import Birthday from '@dashboard/screens/birthday/birthday-controller';
import SchoolOptions from '@dashboard/screens/school-options/school-options-controller';
import DesignReports from '@dashboard/screens/design-reports/design-report-controller';
import ManageDesignReport from '@dashboard/screens/manage-design-report/manage-design-report-controller';

const SchoolOptionsStack = createStackNavigator(
  {
    SchoolOptions,
    DesignReports,
    ManageDesignReport,
  },
  {
    initialRouteName: 'SchoolOptions',
    headerMode: 'none',
  }
);

const ActivitiesStack = createStackNavigator(
  {
    Activities,
    CreateActivity,
    EventRepeat,
    ListGlobal,
    ActivityDetails,
  },
  {
    initialRouteName: 'Activities',
    headerMode: 'none',
  }
);

const MessagesStack = createStackNavigator(
  {
    Messages,
    MessageDetail,
    CreateMessage,
    EventRepeat,
    ListGlobal,
    GlobalListMessage,
    CreateFastAnswer,
    OpenFiles,
  },
  {
    initialRouteName: 'Messages',
    headerMode: 'none',
  }
);

const VerificationStack = createStackNavigator(
  {
    VerificationCenter,
    SearchVerifyClient,
    VerificateClient,
  },
  {
    initialRouteName: 'VerificationCenter',
    headerMode: 'none',
  }
);

const CalendarStack = createStackNavigator(
  {
    Calendar,
    CreateMessage,
    CreateActivity,
    ListGlobal,
  },
  {
    initialRouteName: 'Calendar',
    headerMode: 'none',
  }
);

const ReportsStack = createStackNavigator(
  {
    Reports,
    ReportDetail,
    ListGlobal,
    CreateReport,
  },
  {
    initialRouteName: 'Reports',
    headerMode: 'none',
  }
);

const SocialNetworkStack = createStackNavigator(
  {
    SocialNetwork,
    CreateSocialNetwork,
    DetailSocialNetwork,
  },
  {
    initialRouteName: 'SocialNetwork',
    headerMode: 'none',
  }
);

const ClassroomStack = createStackNavigator(
  {
    Classroom,
    AddEditClassroom,
    ListGlobal,
    ClassroomTeacher,
  },
  {
    initialRouteName: 'Classroom',
    headerMode: 'none',
  }
);

const TeachersStack = createStackNavigator(
  {
    Teachers,
    VerificateClient,
    AddTeacher,
    DetailTeacher,
  },
  {
    initialRouteName: 'Teachers',
    headerMode: 'none',
  }
);

const ParentsStack = createStackNavigator(
  {
    Parents,
    AddParent,
    DetailParent,
    VerificateClient,
    ListGlobal,
  },
  {
    initialRouteName: 'Parents',
    headerMode: 'none',
  }
);

const ChildrenStack = createStackNavigator(
  {
    Children,
    ChildrenDetail,
    AddParentsToChild,
    AddParent,
    ListGlobal,
    Attendance,
  },
  {
    initialRouteName: 'Children',
    headerMode: 'none',
  }
);

const DashboardStack = createStackNavigator(
  {
    Dashboard,
    Birthday,
    CreateMessage,
  },
  {
    initialRouteName: 'Dashboard',
    headerMode: 'none',
  }
);

const Drawer = createDrawerNavigator(
  {
    Dashboard: DashboardStack,
    Children: ChildrenStack,
    Parents: ParentsStack,
    Teachers: TeachersStack,
    Classroom: ClassroomStack,
    SocialNetwork: SocialNetworkStack,
    Settings,
    Reports: ReportsStack,
    Calendar: CalendarStack,
    VerificationCenter: VerificationStack,
    Messages: MessagesStack,
    Activities: ActivitiesStack,
    SchoolOptions: SchoolOptionsStack,
  },
  {
    unmountInactiveRoutes: false,
    headerMode: 'none',
    contentComponent: Menu,
    drawerBackgroundColor: 'transparent',
    drawerType: 'slide',
  }
);

const LoginStack = createStackNavigator(
  {
    Login,
    Dashboard: Drawer,
    Language,
    ForgotPassword,
    NotificationPage,
    RequestAccess,
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  }
);

export default createSwitchNavigator(
  {
    Authenticator,
    Login: LoginStack,
    Drawer,
  },
  {
    initialRouteName: 'Authenticator',
  }
);
