import { StyleSheet, Dimensions, Platform, PixelRatio } from 'react-native';
import responsiveSize from 'react-native-normalize';
import { Colors } from './colors';

const { width, height } = Dimensions.get('window');

// shoutem styles
const containerStyle = {
  container: {
    backgroundColor: 'transparent',
    elevation: 10000,
    height: 44,
  },
  statusBar: {
    height: 0,
    backgroundColor: 'white',
  },
  leftComponent: {
    flexGrow: 1,
  },
  centerComponent: {
    flexGrow: 1,
  },
  rightComponent: {
    flexGrow: 1,
  },
};

const styles = StyleSheet.create({
  flexible: { flex: 1 },
  topBatNavigation: {
    backgroundColor: 'transparent',
    elevation: 10000,
    height: 44,
    marginBottom: 16,
    justifyContent: 'center',
  },
  welcomeKoala: { width: responsiveSize(180), height: responsiveSize(190) },
  center: { justifyContent: 'center', alignItems: 'center' },
  row: { flexDirection: 'row', alignItems: 'center' },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  activityIndicator: {
    marginRight: 15,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export { styles as Styles, containerStyle };
