import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import Colors from '@assets/colors';
import Header from '@components/Header';
import FastImage from '@components/FastImage';
import GrassImage from '@images/grass.png';
import Icon from 'react-native-vector-icons/Feather';
import Search from '@components/Search';
import TemplateItem from '@components/TemplateItem';
import CheckBox from '@components/CheckBox';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentHeader: {
    height: 40,
    backgroundColor: Colors.GREEN,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  contentSearch: { paddingHorizontal: '5%', marginTop: 15 },
  emptyList: { height: 70, justifyContent: 'center', alignItems: 'center' },
  contentItem: {
    marginTop: 10,
  },

  contentEdit: { paddingHorizontal: '5%', paddingTop: '5%' },
  contentActionsEdit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonTrash: {
    width: 30,
    height: 30,
    borderRadius: 5,
    backgroundColor: Colors.RED,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '2%',
  },
  contentSelectedAll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '3.5%',
    marginTop: '7%',
    alignItems: 'center',
  },
  contentButtons: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonMove: {
    height: 30,
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: Colors.BLUEDARK,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
  },
});

const MultiList = ({
  list = [],
  title,
  setState,
  handleDelete,
  handleAddMultiList,
  onChangeEdit = () => {},
  handleMove = () => {},
  handleShowChilds = false,
  isChildTemplate = true,
  attrName = '',
  absence = false,
}) => {
  const [editMultiple, setEditMultiple] = useState(false);
  const [selectAll, setSelectAll] = useState(false);
  const [data, setData] = useState(list);

  useEffect(() => {
    setData(list);
  }, [list]);

  const handleEditMultiList = () => {
    setEditMultiple(!editMultiple);
    onChangeEdit(!editMultiple);
  };

  const handleSelectAll = () => {
    setSelectAll(!selectAll);
    const newData = data.map((item2) => {
      return { ...item2, checked: !selectAll };
    });
    setData(newData);
  };

  const handleSelectedItem = (item) => {
    const newData = data.map((item2) => {
      if (item2.id === item.id) {
        return { ...item2, checked: !item2.checked };
      }
      return { ...item2 };
    });
    setData(newData);
  };

  const handleManageDelete = (item = null) => {
    setState(attrName, data);
    setTimeout(() => {
      handleDelete(item);
    }, 150);
  };

  const handleManageMove = () => {
    setState(attrName, data);
    handleMove();
  };
  const renderItem = ({ item, index }) => {
    return (
      <View key={index}>
        <TemplateItem
          item={item}
          index={index}
          childTemplate={isChildTemplate}
          handleDelete={handleManageDelete}
          editList={editMultiple}
          handleSelectedItem={handleSelectedItem}
          absence={absence}
          handleShowChilds={handleShowChilds}
        />
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={title}
        min
        absence={absence}
        height={40}
        edit={data.length > 0 && !absence}
        add={!absence}
        onPressEdit={handleEditMultiList}
        onPressAdd={handleAddMultiList}
      />
      {data.length > 0 && (
        <>
          <View style={styles.contentSearch}>
            <Search />
          </View>
          {editMultiple && !absence && (
            <View style={styles.contentEdit}>
              <View style={styles.contentActionsEdit}>
                <Text color={Colors.GRAY}>{I18n.t('actions')}:</Text>
                <View style={styles.contentButtons}>
                  {handleMove && (
                    <TouchableOpacity style={styles.buttonMove} onPress={() => handleManageMove()}>
                      <Text color={Colors.WHITE}>Mover</Text>
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity style={styles.buttonTrash} onPress={() => handleManageDelete()}>
                    <Icon name="trash" size={15} color={Colors.WHITE} />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.contentSelectedAll}>
                <Text>{I18n.t('selectAll')}</Text>
                <CheckBox onPress={handleSelectAll} check={selectAll} />
              </View>
            </View>
          )}
          <View style={styles.contentItem}>
            {data.map((item, index) => {
              return renderItem({ item, index });
            })}
          </View>
        </>
      )}

      {list.length === 0 && (
        <View style={styles.emptyList}>
          <Text>Lista vacia</Text>
        </View>
      )}
    </View>
  );
};

export default MultiList;
