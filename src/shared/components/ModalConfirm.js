import React, { useState } from 'react';
import { connect } from 'react-redux';

import { View, Modal, TouchableOpacity, StyleSheet, Dimensions, FlatList } from 'react-native';
import hiddenModalAction from '@store/actions/modal-confirm.action';
import Colors from '@assets/colors';
import I18n from 'react-native-redux-i18n';
import Text from './Text';
import CheckBox from './CheckBox';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    width: width * 0.7,
    paddingTop: 15,
    paddingBottom: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  line: { height: 1, backgroundColor: 'rgba(0,0,0,0.2)' },
  contentTitle: { paddingHorizontal: 15, marginBottom: 15, marginTop: 9 },
  contentButtons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingHorizontal: 15,
    paddingTop: 15,
  },
  textUppercase: {
    textTransform: 'uppercase',
  },
});

const ModalConfirm = ({ modalConfirm, _hiddenModalAction }) => {
  const [itemSelected, setItemSelected] = useState(null);
  const [data, setData] = useState([]);

  React.useEffect(() => {
    setData(modalConfirm?.data);
  }, [modalConfirm?.data]);

  const onPressCancel = () => {
    if (modalConfirm.onPressCancel) {
      modalConfirm.onPressCancel();
    }
    _hiddenModalAction({ visible: false });
    setItemSelected(null);
  };
  const onPressAccept = () => {
    if (modalConfirm.onPressAccept) {
      const selections = modalConfirm?.multiSelection
        ? data.filter((x) => x.checked)
        : itemSelected;
      modalConfirm.onPressAccept(selections);
    }
    _hiddenModalAction({ visible: false });
    setItemSelected(null);
  };

  const handleMultipleSelection = (item) => {
    const indexData = data.findIndex((x) => x.value === item?.value);
    if (indexData < 0) {
      return;
    }
    let itemData = data[indexData];
    itemData = { ...itemData, checked: !item.checked };
    data[indexData] = itemData;
    setData([...data]);
  };

  const handleSimpleSelection = (item) => {
    setItemSelected(item);
  };

  React.useEffect(() => {
    if (itemSelected && !modalConfirm?.multiSelection) {
      onPressAccept();
    }
  }, [itemSelected]);

  const renderItem = ({ item }) => {
    const { checked } = item;
    return (
      <TouchableOpacity
        style={{ height: 40, flexDirection: 'row', alignItems: 'center', paddingHorizontal: '8%' }}
        onPress={() =>
          modalConfirm?.multiSelection ? handleMultipleSelection(item) : handleSimpleSelection(item)
        }
      >
        <View
          style={{
            marginRight: 15,
          }}
        >
          <CheckBox
            onPress={() =>
              modalConfirm?.multiSelection
                ? handleMultipleSelection(item)
                : handleSimpleSelection(item)
            }
            check={modalConfirm?.multiSelection ? checked : item === itemSelected}
          />
        </View>
        <View>
          <Text>{item?.text}</Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <Modal animationType="fade" transparent visible={modalConfirm.visible}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <View
            style={[styles.contentTitle, { marginBottom: modalConfirm?.data.length > 0 ? 5 : 15 }]}
          >
            <Text align="left" fontSize={modalConfirm?.data.length > 0 ? 18 : 15} weight="Medium">
              {modalConfirm?.title}
            </Text>
          </View>
          {modalConfirm?.subTitle && modalConfirm?.subTitle !== undefined ? (
            <View style={{ paddingHorizontal: 15, marginBottom: 5 }}>
              <Text align="left" fontSize={13} color="#8D8D8D">
                {modalConfirm.subTitle}
              </Text>
            </View>
          ) : null}
          {data && data.length > 0 && (
            <View>
              <View style={[styles.line, { marginBottom: 5 }]} />
              <FlatList
                data={data}
                extraData={data}
                renderItem={renderItem}
                style={{
                  maxHeight: width * 0.7,
                }}
                keyExtractor={(item, index) => item + index}
              />
            </View>
          )}
          <View style={styles.line} />
          <View style={styles.contentButtons}>
            <TouchableOpacity onPress={onPressCancel} style={{ marginRight: '10%' }}>
              <Text color="#C8574E" style={styles.textUppercase}>
                {I18n.t('buttons.cancel')}
              </Text>
            </TouchableOpacity>
            {(modalConfirm?.multiSelection || modalConfirm?.data?.length === 0) && (
              <TouchableOpacity onPress={onPressAccept}>
                <Text color={Colors.GREEN} weight="Medium" style={styles.textUppercase}>
                  {I18n.t('buttons.accept')}
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
};

const mapStateToProps = ({ modalConfirm, i18n }) => ({
  modalConfirm,
  i18n,
});

export default connect(mapStateToProps, { _hiddenModalAction: hiddenModalAction })(ModalConfirm);
