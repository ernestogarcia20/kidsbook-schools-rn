import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  FlatList,
  Animated,
  createRef,
} from 'react-native';
import PropTypes from 'prop-types';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Feather';
import moment from 'moment';

const { width } = Dimensions.get('screen');

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.GREEN,
    height: 40,
    borderRadius: 10,
  },
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentDays: {
    flex: 1,
    // Highlight header
    height: 0,
    alignItems: 'center',
  },
  buttonDay: {
    borderRadius: 100,
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
  },
  rowDays: {
    flex: 1,
    flexDirection: 'row',
    padding: 15,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  controls: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '20%',
    marginRight: 15,
  },
  contentHeaderMonth: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
  },
  calendar: {
    width: width * 0.9,
    paddingTop: 20,
    paddingBottom: 40,
    paddingHorizontal: 20,
    borderRadius: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,0.9)',
  },
  point: { width: 5, height: 5, borderRadius: 10, backgroundColor: Colors.RED },
  renderItemEvent: {
    height: 60,
    width: width * 0.8,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  main: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: 'hsla(0, 0%, 0%, 1)',
  },
  pane: {
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 50,
    borderTopColor: 'transparent',
    backgroundColor: 'hsla(38, 100%, 73%, 1)',
  },
});

class CalendarMin extends Component {
  months = I18n.t('monthNames');

  weekDays = I18n.t('dayNamesShort');

  nDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  ref = null;

  constructor(props) {
    super(props);
    this.state = {
      activeDate: moment(),
      today: moment(),
      matrix: [],
      indexPressing: 1,
      dateSelected: null,
      eventSelected: null,
      loaded: false,
      fadeAnim: new Animated.Value(0),
      lastMonth: moment().month(),
    };
  }

  componentDidMount() {
    this.generateMatrix();
  }

  changeRef = (ref) => {
    if (!ref) {
      return;
    }
    this.ref = ref;
  };

  loaded = async () => {
    const { activeDate, fadeAnim } = this.state;
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 900,
      useNativeDriver: false,
    }).start();
    // await this.ref.scrollToIndex({ animated: true, index: activeDate.month() });
    this.setState({ loaded: true });
  };

  changeMonth = (n) => {
    const { activeDate } = this.state;
    this.setState(() => {
      activeDate.setMonth(activeDate.getMonth() + n);
    });
  };

  handleSelectedDay = (item) => {
    this.setState({ dateSelected: item.date, eventSelected: item.events });
  };

  generateRender = () => {
    const { matrix, dateSelected } = this.state;
    let rows = [];
    rows = matrix.map((row, rowIndex) => {
      const rowItems = row.map((item, colIndex) => {
        let isSelected = false;
        if (dateSelected && dateSelected === item.date) {
          isSelected = true;
        } else if (item.selected && !dateSelected) {
          isSelected = true;
        } else isSelected = false;
        return (
          <View
            key={`${item?.key === undefined ? colIndex : item?.key}`}
            style={styles.contentDays}
          >
            <TouchableOpacity
              onPress={() => this.handleSelectedDay(item, rowIndex, colIndex)}
              style={[
                styles.buttonDay,
                {
                  backgroundColor: isSelected ? '#BCD8F1' : 'transparent',
                },
              ]}
            >
              <Text
                color={rowIndex === 1 ? Colors.BLACK : Colors.BLACKOPACITY}
                weight={isSelected ? 'Bold' : 'Regular'}
              >
                {rowIndex === 1 ? (item.day !== -1 ? item.day : '') : item}
              </Text>
            </TouchableOpacity>
            {rowIndex === 1 && item.events && <View style={styles.point} />}
          </View>
        );
      });
      return (
        <View key={`${rowIndex}`} style={styles.rowDays}>
          {rowItems}
        </View>
      );
    });
    return rows;
  };

  handleArrow = (next = false, isPress = false) => {
    this.generateMatrix(next, isPress, true);
  };

  generateMatrix(next = false, isPress = false, scrolled = false) {
    let { indexPressing, lastMonth } = this.state;
    const { activeDate, today } = this.state;
    const { events } = this.props;
    const matrix = [];
    let maxDays = activeDate.daysInMonth();

    if (isPress) {
      if (next) {
        // counter > maxDays
        activeDate.add(7, 'day');
        indexPressing += 1;
        this.setState({ indexPressing });
      } else {
        indexPressing -= 1;
        if (indexPressing <= 0) {
          return;
        }
        activeDate.subtract(7, 'day');
        this.setState({ indexPressing: indexPressing < 0 ? 0 : indexPressing });
      }
    }

    // Create header
    matrix[0] = this.weekDays;
    const d = moment();
    const weekday = d.day();
    const year = activeDate.year();
    const month = activeDate.month();

    if (month === 1) {
      // February
      if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
        maxDays += 1;
      }
    }
    const newM = moment(activeDate);
    let counter = newM.subtract(weekday, 'days').date();

    for (let row = 1; row < 2; row += 1) {
      matrix[row] = [];
      for (let col = 0; col < 7; col += 1) {
        const date = `${year}-${String(month + 1).padStart(2, 0)}-${String(counter).padStart(
          2,
          0
        )}`;
        const findEvents = events[date] || null;
        const dateInfo = {
          day: counter,
          date,
          selected: today.format('YYYY-MM-DD') === date,
          key: counter + year,
          events: findEvents,
        };
        // const diff = today.format('YYYY-MM-DD');
        // dateInfo.selected = diff === dateInfo.date;
        matrix[row][col] = dateInfo;
        counter += 1;
        if (row === 1 && counter > maxDays) {
          counter = 1;
        }
      }
    }
    if (scrolled && lastMonth !== month) {
      this.setState({ lastMonth: month });
      //  this.ref.scrollToIndex({ animated: true, index: activeDate.month() });
    }
    this.setState({ matrix });
  }

  renderItemEvents = ({ item }) => {
    const { date, subject, time, is_event: isEvent } = item;
    return (
      <View
        style={[
          styles.renderItemEvent,
          {
            backgroundColor: isEvent === 1 ? '#FAE6E2' : '#E2FAEB',
          },
        ]}
      >
        <View>
          <Text align="left" color={Colors.BLACK} weight="Medium">
            {moment(date, 'YYYY-MM-DD').format('MMM DD')}
          </Text>
          <Text align="left" fontSize={12}>
            {moment(`${date} ${time}`).format('h:mm A')}
          </Text>
        </View>
        <View style={{ flex: 0.6 }}>
          <Text fontSize={13} align="left">
            {subject}
          </Text>
        </View>
        <TouchableOpacity>
          <Icon name="chevron-right" size={24} color={Colors.BLACK} />
        </TouchableOpacity>
      </View>
    );
  };

  renderMonth = ({ item, index }) => {
    const { activeDate, loaded } = this.state;
    if (this.months.length === index + 1 && !loaded) {
      this.loaded();
    }
    return (
      <View
        style={{
          width: width * 0.65,
          justifyContent: 'center',
          alignItems: 'flex-start',
        }}
      >
        <Text fontSize={16}>
          {item}, &nbsp;
          {activeDate.year()}
        </Text>
      </View>
    );
  };

  render() {
    const { eventSelected, fadeAnim, activeDate } = this.state;
    return (
      <Animated.View style={[styles.calendar]}>
        <View style={styles.contentHeaderMonth}>
          <Text fontSize={16}>
            {this.months[activeDate.month()]}, &nbsp;
            {activeDate.year()}
          </Text>
          <View style={styles.controls}>
            <TouchableOpacity
              onPress={() => this.handleArrow(false, true)}
              hitSlop={{ left: 30, right: 5, top: 15, bottom: 15 }}
            >
              <Icon name="chevron-left" size={24} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handleArrow(true, true)}
              hitSlop={{ left: 5, right: 30, top: 15, bottom: 15 }}
            >
              <Icon name="chevron-right" size={24} />
            </TouchableOpacity>
          </View>
        </View>
        {this.generateRender()}
        <FlatList
          data={eventSelected}
          style={{ marginTop: eventSelected ? '15%' : 0, flex: 1 }}
          renderItem={this.renderItemEvents}
          keyExtractor={(item, index) => item + index}
        />
      </Animated.View>
    );
  }
}

CalendarMin.propTypes = {
  events: PropTypes.objectOf(PropTypes.array),
};

CalendarMin.defaultProps = {
  events: {},
};

export default CalendarMin;
