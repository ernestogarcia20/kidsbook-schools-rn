import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Text from '@components/Text';
import PropTypes from 'prop-types';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import FastImage from '@components/FastImage';
import CustomIcon from '@components/Icon';
import moment from 'moment';
import { getImageById } from '@utils/functions/globals';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabs: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tabActive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3,
    borderBottomColor: Colors.GREEN,
    flexDirection: 'row',
  },
  tabInactive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

  contentSearch: { padding: '7%' },
  unreadMessage: {
    width: 23,
    height: 23,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.DANGER,
    position: 'absolute',
    right: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightIcon: {
    marginRight: 10,
  },
  buttonViews: {
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 2,
    backgroundColor: Colors.WHITE,
  },
});

const ItemMessageAnswer = ({ item, user, onPressReport = () => {} }) => {
  const {
    user: { avatar },
  } = item;
  const urlImage = getImageById(avatar, user?.authData?.apiToken);
  return (
    <View
      style={{
        marginHorizontal: 15,
        paddingBottom: 15,
        backgroundColor: Colors.WHITE,
        marginBottom: 15,
        borderBottomWidth: 2,
        borderColor: 'transparent',
        shadowColor: Colors.BLACK,
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        borderRadius: 10,
        borderBottomEndRadius: item.sent ? 0 : 10,
        marginLeft: item.sent ? '8%' : 15,
        marginRight: !item.sent ? '8%' : 15,
      }}
    >
      <View
        style={[
          styles.rowLarge,
          {
            height: 55,
            borderTopLeftRadius: !item.sent ? 0 : 10,
            borderTopRightRadius: 10,
            backgroundColor: item.sent ? '#56ACDE' : '#f86707',
            flex: 1,
            paddingHorizontal: 10,
            paddingLeft: '6%',
          },
        ]}
      >
        <View style={[styles.row]}>
          <FastImage
            source={{ uri: urlImage }}
            style={{
              width: 40,
              height: 40,
              borderRadius: 100,
              backgroundColor: '#DADADA',
              marginRight: 15,
            }}
          />
          <Text weight="Medium" fontSize={13} color={Colors.WHITE}>
            {item.sent ? I18n.t('you') : item.user.name}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: 20,
            flexDirection: 'row',
          }}
        >
          {item.attachments != null && item.attachments !== '[]' && (
            <View style={styles.rightIcon}>
              <CustomIcon name="attach" width={25} height={25} />
            </View>
          )}
          {item.report && (
            <TouchableOpacity style={styles.rightIcon} onPress={() => onPressReport(item.report)}>
              <CustomIcon name="kb_reports" width={25} height={25} />
            </TouchableOpacity>
          )}
          {!item.sent && !item.new && (
            <CustomIcon name="kidsbook-message-received-readed" width={20} height={20} />
          )}
          {!item.sent && item.new && (
            <CustomIcon name="kidsbook-message-received" width={20} height={20} />
          )}
          {item.sent && item.readed_by.length > 0 && (
            <CustomIcon name="kidsbook-message-sent-readed" width={20} height={20} />
          )}
          {item.sent && item.readed_by.length === 0 && (
            <CustomIcon name="kidsbook-message-sent" width={25} height={25} />
          )}
        </View>
      </View>
      <View style={{ paddingHorizontal: '7%', paddingTop: '5%' }}>
        <Text formatter align="auto" fontSize={13} color={Colors.BLACK}>
          {item.content}
        </Text>
      </View>
      <View style={{ marginTop: 5, marginRight: '6%' }}>
        <Text align="right" color="#868686" weight="Medium" fontSize={11}>
          {moment(item.created_at).format('DD/MM/YYYY HH:mm').toString()}
        </Text>
      </View>
    </View>
  );
};

ItemMessageAnswer.propTypes = {
  item: PropTypes.object,
  user: PropTypes.object,
  onPressReport: PropTypes.func,
};

ItemMessageAnswer.defaultProps = {
  item: {},
  user: {},
  onPressReport: () => {},
};

export default ItemMessageAnswer;
