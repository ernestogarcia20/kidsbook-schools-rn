import React from 'react';
import { StyleSheet, Text, ViewPropTypes, Linking, Platform, TextInput } from 'react-native';
import Colors from '@assets/colors';
import PropTypes from 'prop-types';
import ParsedText from 'react-native-parsed-text';
import { fontMaker } from './typografy';

const urlRegex = /((https|http)?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
const mailRegex = /([A-z]+\.)?([A-z]+)+@([A-z]+\.)?([A-z]+)\.([A-z]{2,})/gm;

const TextComponent = ({
  children,
  fontSize,
  color,
  weight,
  align,
  style,
  formatter,
  selectable = false,
}) => {
  const styles = StyleSheet.create({
    text: {
      fontSize,
      color,
      textAlign: align,
      ...fontMaker(weight),
    },
    url: {
      color: '#56ACDE',
    },
    mail: {
      color: '#56ACDE',
    },
  });

  const onPressUrl = (url) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      }
    });
  };

  const onPressMail = (email) => {
    const formatEmail = `mailto:${email}`;
    Linking.canOpenURL(formatEmail).then((supported) => {
      if (supported) {
        Linking.openURL(formatEmail);
      }
    });
  };

  const parseTextArray = [
    {
      pattern: urlRegex,
      style: styles.url,
      onPress: onPressUrl,
    },
    {
      pattern: mailRegex,
      style: styles.mail,
      onPress: onPressMail,
    },
  ];
  if (formatter) {
    return (
      <ParsedText
        style={[styles.text, { ...style }]}
        selectable={selectable}
        parse={parseTextArray}
      >
        {children}
      </ParsedText>
    );
  }
  return selectable && Platform.OS === 'ios' ? (
    <TextInput style={[styles.text, { ...style }]} value={children} editable={false} multiline />
  ) : (
    <Text style={[styles.text, { ...style }]} selectable={selectable}>
      {children}
    </Text>
  );
};

TextComponent.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  style: ViewPropTypes.style,
  fontSize: PropTypes.number,
  color: PropTypes.string,
  weight: PropTypes.string,
  align: PropTypes.oneOf(['auto', 'left', 'right', 'center', 'justify']),
  formatter: PropTypes.bool,
  onPress: PropTypes.func,
  selectable: PropTypes.bool,
};

TextComponent.defaultProps = {
  style: ViewPropTypes.style,
  color: Colors.BLACK,
  fontSize: 14,
  weight: 'Regular',
  align: 'center',
  formatter: false,
  onPress: () => {},
  selectable: false,
};

export default TextComponent;
