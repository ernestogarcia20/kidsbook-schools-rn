import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import CustomIcon from '@components/Icon';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({
  button: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 6,
    position: 'absolute',
    bottom: 15,
    right: 10,
  },
});

const Layout = ({
  onPress,
  style,
  color = '#F0F0F0',
  iconName = 'kb_messages',
  iconWidth = 35,
  iconHeight = 35,
  iconLib = false,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.9}
      style={[styles.button, style, { backgroundColor: color }]}
      onPress={onPress}
    >
      {iconLib ? (
        <Icon
          style={{ marginLeft: iconName === 'md-send' ? 5 : 0 }}
          name={iconName}
          size={26}
          color={Colors.WHITE}
        />
      ) : (
        <CustomIcon name={iconName} width={iconWidth} heigth={iconHeight} />
      )}
    </TouchableOpacity>
  );
};

export default Layout;
