import React from 'react';
import { View, StyleSheet, FlatList, Dimensions, Animated, TouchableOpacity } from 'react-native';

import FastImage from '@components/FastImage';
import { getImagePost, getImageActivity, getImageById } from '@utils/functions/globals';
import Colors from '@assets/colors';
import PropTypes from 'prop-types';

const { width: widthD } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  contentDot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },
  flatList: {
    backgroundColor: Colors.BLACK,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  flatListActivity: {
    backgroundColor: Colors.BLACK,
    width: 264,
  },
});

const Layout = ({
  data,
  user,
  activity = false,
  width = widthD * 0.925,
  height = 180,
  onPressAttach = () => {},
}) => {
  const scrollX = new Animated.Value(0);
  const position = Animated.divide(scrollX, width);
  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => {
          onPressAttach({
            url: activity ? getImageActivity(item, user.token) : getImagePost(item, user.token),
            attachment: item,
          });
        }}
        activeOpacity={0.8}
      >
        <FastImage
          resizeMode="cover"
          source={{
            uri: activity ? getImageActivity(item, user.token) : getImagePost(item, user.token),
          }}
          style={{
            flex: 1,
            width,
            height,
            backgroundColor: 'white',
            borderTopLeftRadius: activity ? 0 : 15,
            borderTopRightRadius: activity ? 0 : 15,
          }}
        />
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        horizontal
        snapToAlignment="center"
        scrollEventThrottle={16}
        decelerationRate="fast"
        renderItem={renderItem}
        style={activity ? { ...styles.flatListActivity, width } : styles.flatList}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => item + index}
        onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
          useNativeDriver: false,
        })}
      />
      <View style={styles.contentDot}>
        {data.map((item, index) => {
          const color = position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: ['#C4C4C4', '#1E9ECA', '#C4C4C4'],
            extrapolate: 'clamp',
          });
          return (
            <Animated.View
              key={index}
              style={{
                width: 6,
                height: 6,
                backgroundColor: color,
                borderRadius: 100,
                marginRight: 5,
              }}
            />
          );
        })}
      </View>
    </View>
  );
};

Layout.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  user: PropTypes.objectOf(PropTypes.object),
};

Layout.defaultProps = {
  data: [],
  user: {},
};

export default Layout;
