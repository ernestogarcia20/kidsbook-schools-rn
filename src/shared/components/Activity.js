import React from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Search from '@components/Search';
import Text from '@components/Text';
import CarouseImage from '@components/CarouseImage';
import Colors from '@assets/colors';
import FastImage from '@components/FastImage';
import { getImageById } from '@utils/functions/globals';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Feather';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabActive: {
    minWidth: width / 3,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREEN,
    borderRadius: 10,
    flexDirection: 'row',
  },
  tabInactive: {
    minWidth: width / 3,
    height: 35,
    backgroundColor: '#EDEDED',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  tabs: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: '3%',
    borderBottomColor: 'rgba(0,0,0,0.1)',
    borderBottomWidth: 0.9,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentActivity: {
    marginTop: 15,
    backgroundColor: Colors.WHITE,
    borderRadius: 10,
    padding: 15,
    marginHorizontal: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
  },
  imgProfile: {
    width: 25,
    height: 25,
    borderRadius: 100,
    backgroundColor: '#EAEAEA',
    marginRight: -7,
  },
  contentLine: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    marginLeft: 10,
    minHeight: 50,
  },
});

const Activity = ({ onPress = () => {}, activity, user }) => {
  const { client, start_datetime } = activity;
  const urlImage = getImageById(client?.avatar, user?.apiToken);

  const Widget = ({ iconName, text, color = '#33495C' }) => {
    return (
      <View
        style={[
          styles.rowLarge,
          { height: 27, borderRadius: 15, paddingRight: 10, backgroundColor: '#EAEAEA' },
        ]}
      >
        <View
          style={[
            {
              height: 27,
              width: 27,
              backgroundColor: color,
              borderRadius: 100,
              marginRight: 6,
            },
            styles.center,
          ]}
        >
          <Icon name={iconName} size={19} color={Colors.WHITE} />
        </View>
        <Text fontSize={12} color="#474747" weight="SemiBold">
          {text}
        </Text>
      </View>
    );
  };
  return (
    <TouchableOpacity
      style={styles.contentActivity}
      activeOpacity={0.8}
      onPress={() => onPress(activity)}
    >
      <View style={styles.row}>
        <FastImage
          source={{ uri: urlImage }}
          style={{ width: 55, height: 55, borderRadius: 50, backgroundColor: 'rgba(0,0,0,0.1)' }}
        />
        <View style={[styles.rowLarge, styles.contentLine]}>
          <View style={{ justifyContent: 'space-between', minHeight: 40 }}>
            <Text align="left" fontSize={13}>
              {client?.name}
            </Text>
            <Text align="left" fontSize={12} color="#929292">
              {moment(start_datetime, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY HH:mm a')}
            </Text>
          </View>
          <View style={{ justifyContent: 'space-between', minHeight: 40 }}>
            <Text fontSize={9} weight="SemiBold">
              #TareasEntregadas
            </Text>
            <View style={[styles.row, styles.center]}>
              <View style={styles.imgProfile} />
              <View style={styles.imgProfile} />
              <View style={styles.imgProfile} />
              <View
                style={[
                  styles.imgProfile,
                  {
                    zIndex: -1,
                    alignItems: 'flex-end',
                    justifyContent: 'center',
                  },
                ]}
              >
                <Text fontSize={10}>10+</Text>
              </View>
            </View>
          </View>
        </View>
      </View>

      <View style={{ paddingHorizontal: '0%', paddingVertical: 15 }}>
        <Text align="justify" weight="Medium" fontSize={12}>
          {String(activity.description).length > 130
            ? `${String(activity.description).slice(0, 130)}...`
            : activity.description}
          {String(activity.description).length > 130 && (
            <Text weight="Medium" fontSize={12} color={Colors.GREEN}>
              Ver mas
            </Text>
          )}
        </Text>
      </View>
      {JSON.parse(activity.attachments).length > 0 && (
        <View>
          <CarouseImage
            user={user}
            data={JSON.parse(activity.attachments)}
            activity
            width={264}
            height={77}
          />
        </View>
      )}
      <View
        style={[
          styles.rowLarge,
          { marginTop: JSON.parse(activity.attachments).length > 0 ? 15 : 0 },
        ]}
      >
        {Widget({ iconName: 'eye', text: '10/20' })}
        <Text weight="Medium" fontSize={12}>
          {activity.title}
        </Text>
        {Widget({ iconName: 'message-circle', text: '20', color: '#1FB925' })}
      </View>
    </TouchableOpacity>
  );
};

export default Activity;
