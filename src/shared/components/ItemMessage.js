import React from 'react';
import { View, StyleSheet, TouchableOpacity, FlatList, Dimensions } from 'react-native';
import Text from '@components/Text';
import PropTypes from 'prop-types';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import FastImage from '@components/FastImage';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import { getImageById, excerpt } from '@utils/functions/globals';
import { Popover, PopoverController } from 'react-native-modal-popover';
import NoAvatar from '@images/no-avatar.jpg';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabs: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tabActive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3,
    borderBottomColor: Colors.GREEN,
    flexDirection: 'row',
  },
  tabInactive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

  contentSearch: { padding: '7%' },
  unreadMessage: {
    width: 23,
    height: 23,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.DANGER,
    position: 'absolute',
    right: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightIcon: {
    marginRight: 10,
  },
  buttonViews: {
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 2,
    backgroundColor: Colors.WHITE,
  },
});

const ItemMessage = ({ item, user, onPress = () => {}, detail = false, children }) => {
  const recipients = item?.recipients || [];
  const readed = (obj) => {
    return item.readed_by.indexOf(obj.id) !== -1;
  };
  const renderItemUser = ({ item: itemUser }) => {
    const urlImage = getImageById(itemUser.avatar, user?.authData?.apiToken, 64);
    let loadImageError = false;
    const onErrorImage = () => {
      loadImageError = true;
    };
    return (
      <View style={[styles.rowLarge, { marginBottom: 10 }]}>
        <View style={[styles.row]}>
          <FastImage
            source={loadImageError ? NoAvatar : { uri: urlImage }}
            style={{
              width: 40,
              height: 40,
              borderRadius: 100,
              backgroundColor: '#DADADA',
              marginRight: 15,
            }}
            onError={onErrorImage}
          />
          <Text weight="Medium" fontSize={13}>
            {itemUser.name}
          </Text>
        </View>
        <View style={{ marginLeft: 10 }}>
          {readed(itemUser) ? (
            <Icon name="done-all" color={Colors.GREEN} size={20} />
          ) : (
            <Icon name="done" size={20} />
          )}
        </View>
      </View>
    );
  };
  const {
    user: { avatar },
  } = item;
  const urlImage = getImageById(avatar, user?.authData?.apiToken);
  return (
    <PopoverController>
      {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
        <>
          <TouchableOpacity
            onPress={() => onPress(item)}
            activeOpacity={detail ? 1 : 0.8}
            style={{
              marginHorizontal: 15,
              paddingBottom: 15,
              backgroundColor: Colors.WHITE,
              marginBottom: 15,
              borderTopWidth: item.new && !detail ? 7 : 0,
              borderTopColor: item.new && !detail ? '#E5AE0E' : 'transparent',
              borderBottomWidth: 2,
              borderColor: 'transparent',
              shadowColor: Colors.BLACK,
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
              borderRadius: 10,
            }}
          >
            <View
              style={[
                styles.rowLarge,
                {
                  height: 55,
                  backgroundColor: '#F4F4F4',
                  flex: 1,
                  paddingHorizontal: 10,
                  paddingLeft: '6%',
                },
              ]}
            >
              <View style={[styles.row]}>
                <FastImage
                  source={{ uri: urlImage }}
                  style={{
                    width: 40,
                    height: 40,
                    borderRadius: 100,
                    backgroundColor: '#DADADA',
                    marginRight: 15,
                  }}
                />
                <Text weight="Medium" fontSize={13}>
                  {item.sent ? I18n.t('you') : item.user.name}
                </Text>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: 20,
                  flexDirection: 'row',
                }}
              >
                {item.sent && (
                  <TouchableOpacity
                    ref={setPopoverAnchor}
                    style={[styles.row, styles.rightIcon, styles.buttonViews]}
                    onPress={openPopover}
                  >
                    <Icon name="visibility" size={15} style={{ marginRight: 5 }} />
                    <Text fontSize={12} weight="Medium">
                      {item.readed_by.length}/{item.recipients.length}
                    </Text>
                  </TouchableOpacity>
                )}
                {item.attachments != null && item.attachments !== '[]' && (
                  <View style={styles.rightIcon}>
                    <CustomIcon name="attach" width={25} height={25} />
                  </View>
                )}
                {item.report_id && (
                  <View style={styles.rightIcon}>
                    <CustomIcon name="kb_reports" width={25} height={25} />
                  </View>
                )}
                {!item.sent && !item.new && (
                  <CustomIcon name="kidsbook-message-received-readed" width={20} height={20} />
                )}
                {!item.sent && item.new && (
                  <CustomIcon name="kidsbook-message-received" width={20} height={20} />
                )}
                {item.sent && item.readed_by.length > 0 && (
                  <CustomIcon name="kidsbook-message-sent-readed" width={20} height={20} />
                )}
                {item.sent && item.readed_by.length === 0 && (
                  <CustomIcon name="kidsbook-message-sent" width={25} height={25} />
                )}
              </View>
            </View>
            <View style={{ paddingHorizontal: '7%', paddingTop: '5%' }}>
              {item.subject && item.subject !== '' && (
                <View>
                  <Text weight="Medium" align="left" fontSize={13} color={Colors.BLACK}>
                    {item.subject}:
                  </Text>
                </View>
              )}
              <Text
                formatter={detail}
                align={detail ? 'auto' : 'justify'}
                fontSize={13}
                color={Colors.BLACK}
              >
                {detail ? item.content : excerpt(item.content)}
              </Text>
            </View>
            <View style={{ marginTop: 10, marginRight: '6%' }}>
              <Text align="right" color="#868686" weight="Medium" fontSize={11}>
                {moment(item.created_at).format('DD/MM/YYYY HH:mm').toString()}
              </Text>
            </View>

            <Popover
              visible={popoverVisible}
              fromRect={popoverAnchorRect}
              onClose={closePopover}
              placement="auto"
              contentStyle={{ maxHeight: width * 0.8 }}
            >
              <FlatList
                data={recipients}
                showsVerticalScrollIndicator={false}
                renderItem={renderItemUser}
                keyExtractor={(ite, index) => ite + index}
              />
            </Popover>
            {children}
          </TouchableOpacity>
        </>
      )}
    </PopoverController>
  );
};

ItemMessage.propTypes = {
  item: PropTypes.object,
  user: PropTypes.object,
  onPress: PropTypes.func,
  detail: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

ItemMessage.defaultProps = {
  item: {},
  user: {},
  onPress: () => {},
  detail: false,
  children: <></>,
};

export default ItemMessage;
