import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Text from '@components/Text';
import FastImage from '@components/FastImage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentWidget: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#DADADA',
    borderRadius: 20,
  },
  contentIconWidget: {
    width: 27,
    height: 27,
    backgroundColor: '#57C1D9',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 8,
  },
  contentText: { marginRight: 12, marginTop: 2 },
  icon: {
    width: 12,
    height: 12,
  },
});

const Layout = ({ image, color, text, iconSize = 13, onPress }) => {
  return (
    <TouchableOpacity style={styles.contentWidget} onPress={onPress} activeOpacity={0.8}>
      <View style={[styles.contentIconWidget, { backgroundColor: color }]}>
        <FastImage source={image} style={[styles.icon, { width: iconSize, height: iconSize }]} />
      </View>
      <View style={styles.contentText}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Layout;
