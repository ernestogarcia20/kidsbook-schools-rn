import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import Colors from '@assets/colors';

const styles = StyleSheet.create({
  container: {
    // paddingTop: Platform.OS === 'android' ? '0%' : 0,
  },
  buttonCheck: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: Colors.BROWN,
    borderRadius: 3,
  },
  buttonChecked: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREEN,
    borderRadius: 3,
  },
});

const CheckBox = ({
  style,
  onPress = () => {},
  iconColor,
  iconSize = 15,
  check = false,
  disable = false,
}) => (
  <TouchableOpacity
    style={[
      check ? styles.buttonChecked : styles.buttonCheck,
      { borderColor: disable && !check ? 'rgba(0,0,0,0.1)' : Colors.BROWN },
    ]}
    onPress={onPress}
    hitSlop={{ top: 5, left: 5, bottom: 5, right: 5 }}
  >
    {check && <Icon size={iconSize} color={check ? Colors.WHITE : Colors.BROWN} name="check" />}
  </TouchableOpacity>
);

export default CheckBox;
