import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, ViewPropTypes, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import GrassImage from '@images/grass.png';
import AbsenceImage from '@images/absence.png';
import FastImage from '@components/FastImage';
import Colors from '@assets/colors';
import { pickTextColorBasedOnBgColorSimple } from '@utils/functions/globals';
import Text from './Text';

const styles = StyleSheet.create({
  header: {
    backgroundColor: Colors.GREEN,
    height: 60,
    zIndex: 2,
  },
  background: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  arrowBack: { justifyContent: 'center', alignItems: 'center', marginLeft: 20 },
  iconBell: { justifyContent: 'center', alignItems: 'center', marginRight: 20 },
  absoluteTitle: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    left: 0,
    right: 0,
    zIndex: -1,
  },
  absoluteTitleLeft: {
    position: 'absolute',
    justifyContent: 'center',
    left: 15,
    zIndex: -1,
  },
  point: {
    width: 10,
    height: 10,
    borderRadius: 100,
    backgroundColor: '#FF0000',
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
  },
  paddingIcon: { paddingHorizontal: 10 },
  contentActions: { flexDirection: 'row', alignItems: 'center', paddingRight: '3%' },
});

const Header = ({
  title,
  color,
  style,
  onPress,
  absence = false,
  menu,
  notify,
  min = false,
  height = 60,
  add,
  edit,
  detail,
  onPressDetail = () => {},
  onPressNotify,
  onPressAdd,
  onPressEdit,
  hiddenBackground = false,
}) => {
  const [changeEditList, setChageEditList] = useState(false);

  const handleEdit = () => {
    setChageEditList(!changeEditList);
    onPressEdit();
  };
  return (
    <View style={[style, styles.header, { backgroundColor: color, height }]}>
      <FastImage source={hiddenBackground ? undefined : GrassImage} style={styles.background}>
        {!min ? (
          <TouchableOpacity onPress={onPress} style={styles.arrowBack}>
            <Icon name={menu ? 'menu' : 'arrow-left'} size={30} color={Colors.WHITE} />
          </TouchableOpacity>
        ) : (
          <View />
        )}
        <View style={[min ? styles.absoluteTitleLeft : styles.absoluteTitle]}>
          <Text color={pickTextColorBasedOnBgColorSimple(color)} fontSize={min ? 14 : 18}>
            {title}
          </Text>
        </View>
        <View style={styles.contentActions}>
          {notify && (
            <TouchableOpacity style={styles.iconBell} onPress={onPressNotify}>
              <View style={styles.point} />
              <Icon name="bell" size={24} color={Colors.WHITE} />
            </TouchableOpacity>
          )}
          {edit && (
            <TouchableOpacity style={styles.paddingIcon} onPress={handleEdit}>
              <Icon
                name={changeEditList ? 'file-text' : 'check-square'}
                size={min ? 22 : 24}
                color={Colors.WHITE}
              />
            </TouchableOpacity>
          )}
          {detail && (
            <TouchableOpacity style={styles.paddingIcon} onPress={onPressDetail}>
              <Icon name="settings" size={20} color={Colors.WHITE} />
            </TouchableOpacity>
          )}
          {absence && (
            <TouchableOpacity style={styles.paddingIcon} onPress={handleEdit}>
              <FastImage source={AbsenceImage} style={{ width: 30, height: 30 }} />
            </TouchableOpacity>
          )}
          {add && (
            <TouchableOpacity style={styles.paddingIcon} onPress={onPressAdd}>
              <Icon name="plus" size={min ? 25 : 28} color={Colors.WHITE} />
            </TouchableOpacity>
          )}
        </View>
      </FastImage>
    </View>
  );
};

Header.propTypes = {
  title: PropTypes.string,
  style: ViewPropTypes.style,
  color: PropTypes.string,
  onPress: PropTypes.func,
  onPressNotify: PropTypes.func,
  onPressAdd: PropTypes.func,
  onPressEdit: PropTypes.func,
  menu: PropTypes.bool,
  notify: PropTypes.bool,
  add: PropTypes.bool,
  edit: PropTypes.bool,
};

Header.defaultProps = {
  title: '',
  style: ViewPropTypes.style,
  color: Colors.GREEN,
  onPress: () => {},
  onPressNotify: () => {},
  onPressAdd: () => {},
  onPressEdit: () => {},
  menu: false,
  notify: false,
  add: false,
  edit: false,
};

export default Header;
