import React, { Component } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  DatePickerAndroid,
  TouchableOpacity,
  Platform,
  Animated,
} from 'react-native';
import PropTypes from 'prop-types';
import Text from '@components/Text';
import DatePicker from '@components/DatePicker';
import moment from 'moment';

import Underline from './Underline';
import FloatingLabel from './FloatingLabel';

export default class TextField extends Component {
  constructor(props: Object, context: Object) {
    super(props, context);
    this.state = {
      isFocused: false,
      text: props.value,
      showModalPicker: false,
      dateSelected: null,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      const { value } = this.props;
      if (!value) {
        return;
      }
      this.refs.floatingLabel.floatLabel();
      this.refs.underline.expandLine();
    }, 50);
  }

  componentDidUpdate(prevProps) {
    const { value } = this.props;
    if (prevProps.value !== value && !prevProps.value) {
      this.refs.floatingLabel.floatLabel();
      this.refs.underline.expandLine();
    }
  }

  focus() {
    try {
      this.input.focus();
    } catch (e) {}
  }

  blur() {
    try {
      this.input.blur();
    } catch (e) {}
  }

  isFocused() {
    return this.state.isFocused;
  }

  handleDatePicker = async () => {
    this.setState({ isFocused: true, showModalPicker: true });
    this.refs.floatingLabel.floatLabel();
    this.refs.underline.expandLine();
  };

  onChangeDatePicker = (date) => {
    const newDate = String(moment(date).format('YYYY/MM/DD'));
    this.setState({ showModalPicker: false, isFocused: false, dateSelected: date });
    this.onChangeText(newDate);
    !newDate.length && this.refs.floatingLabel.sinkLabel();
    this.refs.underline.shrinkLine();
  };

  onClosePicker = () => {
    this.setState({ isFocused: false });
    const { dateSelected } = this.props;
    if (dateSelected) {
      this.refs.underline.shrinkLine();
      return;
    }
    String(this.state.dateSelected || '').length < 1 && this.refs.floatingLabel.sinkLabel();
    this.refs.underline.shrinkLine();
  };

  onChangeText = (text) => {
    const { onChangeText = () => {} } = this.props;
    this.setState({ text }, () => onChangeText(text));
  };

  customOnPress = () => {
    const { onPress } = this.props;
    this.refs.floatingLabel.floatLabel();
    this.refs.underline.expandLine();
    onPress(() => {
      const { value } = this.props;
      !value.length && this.refs.floatingLabel.sinkLabel();
      this.refs.underline.shrinkLine();
    });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const {
      label,
      highlightColor,
      duration,
      labelColor,
      borderColor,
      datePicker,
      textColor,
      textFocusColor,
      textBlurColor,
      onFocus,
      onBlur,
      onChangeText,
      value,
      dense,
      inputStyle,
      wrapperStyle,
      labelStyle,
      dateSelected,
      onPress = null,
      refInput = () => {},
      maximumDate = new Date(),
      minimumDate = null,
      ...props
    } = this.props;
    return (
      <View
        style={[
          dense ? styles.denseWrapper : value ? styles.wrapper : styles.wrapperMin,
          wrapperStyle,
        ]}
        ref="wrapper"
      >
        {datePicker || onPress ? (
          <TouchableOpacity
            onPress={onPress ? this.customOnPress : this.handleDatePicker}
            style={[
              dense ? styles.densePickerStyle : { height: 42, justifyContent: 'center' },
              {
                color: textColor,
              },
              this.state.isFocused && textFocusColor
                ? {
                    color: textFocusColor,
                  }
                : {},
              !this.state.isFocused && textBlurColor
                ? {
                    color: textBlurColor,
                  }
                : { paddingHorizontal: 5 },
            ]}
          >
            <Text align="left">{String(value)}</Text>
          </TouchableOpacity>
        ) : (
          <TextInput
            style={[
              dense ? styles.denseTextInput : styles.textInput,
              {
                color: textColor,
              },
              this.state.isFocused && textFocusColor
                ? {
                    color: textFocusColor,
                  }
                : {},
              !this.state.isFocused && textBlurColor
                ? {
                    color: textBlurColor,
                  }
                : {},
            ]}
            onFocus={() => {
              this.setState({ isFocused: true });
              this.refs.floatingLabel.floatLabel();
              this.refs.underline.expandLine();
              onFocus && onFocus();
            }}
            onBlur={() => {
              this.setState({ isFocused: false });
              !this.state.text.length && this.refs.floatingLabel.sinkLabel();
              this.refs.underline.shrinkLine();
              onBlur && onBlur();
            }}
            onChangeText={this.onChangeText}
            ref={(ref) => {
              refInput(ref);
              this.input = ref;
            }}
            value={this.state.text}
            {...props}
          />
        )}
        <FloatingLabel
          isFocused={this.state.isFocused}
          ref="floatingLabel"
          focusHandler={datePicker ? () => {} : this.focus}
          label={label}
          labelColor={labelColor}
          highlightColor={highlightColor}
          duration={duration}
          dense={dense}
          hasValue={this.state.text.length}
          style={[{ marginHorizontal: 5, zIndex: -1 }, labelStyle]}
        />
        <Underline
          ref="underline"
          highlightColor={highlightColor}
          duration={duration}
          borderColor={borderColor}
        />
        <DatePicker
          showModal={this.state.showModalPicker}
          onChange={this.onChangeDatePicker}
          attrName="showModalPicker"
          date={this.state.dateSelected ? this.state.dateSelected : dateSelected || new Date()}
          updateMasterState={this.handleChange}
          maximumDate={maximumDate}
          minimumDate={minimumDate}
          onClose={this.onClosePicker}
        />
      </View>
    );
  }
}

TextField.propTypes = {
  duration: PropTypes.number,
  label: PropTypes.string,
  highlightColor: PropTypes.string,
  labelColor: PropTypes.string,
  borderColor: PropTypes.string,
  textColor: PropTypes.string,
  dateSelected: PropTypes.string,
  textFocusColor: PropTypes.string,
  textBlurColor: PropTypes.string,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChangeText: PropTypes.func,
  value: PropTypes.string,
  dense: PropTypes.bool,
  phone: PropTypes.bool,
  inputStyle: PropTypes.object,
  wrapperStyle: PropTypes.object,
  labelStyle: PropTypes.object,
  datePicker: PropTypes.bool,
};

TextField.defaultProps = {
  duration: 200,
  labelColor: '#9E9E9E',
  borderColor: '#E0E0E0',
  textColor: '#000',
  phone: false,
  value: '',
  dense: false,
  dateSelected: new Date(),
  underlineColorAndroid: 'rgba(0,0,0,0)',
  datePicker: false,
};

const styles = StyleSheet.create({
  wrapper: {
    height: 72,
    paddingTop: 30,
    position: 'relative',
  },
  wrapperMin: {
    height: 72,
    paddingTop: 30,
    position: 'relative',
  },
  denseWrapper: {
    height: 60,
    paddingTop: 28,
    paddingBottom: 4,
    position: 'relative',
  },
  textInput: {
    fontSize: 16,
    position: 'relative',
    paddingTop: Platform.OS === 'ios' ? 7 : undefined,
    paddingBottom: Platform.OS === 'ios' ? 10 : undefined,
  },
  denseTextInput: {
    fontSize: 13,
    paddingBottom: 3,
  },
  pickerStyle: {
    height: 45,
    justifyContent: 'center',
  },
  densePickerStyle: {
    height: 45,
    justifyContent: 'center',
  },
});
