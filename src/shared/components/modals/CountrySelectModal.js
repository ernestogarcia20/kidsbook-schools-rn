import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Modal,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import Container from '@components/Container';
import Header from '@components/Header';
import Search from '@components/Search';
import Text from '@components/Text';

/**
 * Country select modal
 * @augments {Component<Props, State>}
 */
class CountrySelectModal extends Component {
  static propTypes = {
    open: PropTypes.bool.isRequired,
    countries: PropTypes.array,
    onClose: PropTypes.func,
  };

  componentWillReceiveProps(nextProps) {
    const { open } = nextProps;
    this.setState({ open });
  }

  componentDidMount() {
    this.setArrayFilter();
  }

  componentDidUpdate(props) {
    const { countries } = this.props;
    if (countries !== props.countries) {
      this.setArrayFilter();
    }
  }

  setArrayFilter = () => {
    const { countries } = this.props;
    this.setState({ countriesFilter: countries });
  };

  state = {
    open: false,
    countriesFilter: [],
  };

  onClose = () => {
    this.props.onClose();
  };

  changeSelect = (country) => {
    this.props.onChange && this.props.onChange(country);
    this.onClose();
  };

  renderItem = ({ item: country }) => {
    return (
      <TouchableOpacity onPress={() => this.changeSelect(country)}>
        <View style={style.countryList}>
          <View style={{ width: '20%', display: 'flex', flexDirection: 'row' }}>
            <Text style={style.flagImage}> {country.flag}</Text>
            <Text style={{ fontSize: 17 }}> {country.dialCode}</Text>
          </View>
          <View style={style.contentRenderItemName}>
            <Text align="left">{country.en}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  separatorComponent = () => <View style={style.separator} />;

  onChangeText = (text) => {
    const { countries } = this.props;
    const newData = countries.filter((item) => {
      const itemData = `${String(item.dialCode).toUpperCase()} ${String(item.en).toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ countriesFilter: newData });
  };

  render() {
    const { open, countriesFilter } = this.state;
    // const {countries} = this.props;
    return (
      <Modal animationType="fade" style={{ height }} visible={open} transparent={false}>
        <Container>
          <Header
            style={style.header}
            onPress={() => this.onClose()}
            title="Busca el codigo de tu País"
          />
          <View style={style.contentSearch}>
            <Search placeholder="Escribe el nombre de tu País" onChangeText={this.onChangeText} />
          </View>
          <FlatList
            data={countriesFilter}
            extraData={countriesFilter}
            style={style.flatList}
            contentContainerStyle={style.contentContainerStyle}
            showsVerticalScrollIndicator={false}
            ItemSeparatorComponent={this.separatorComponent}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => item + index}
          />
        </Container>
      </Modal>
    );
  }
}

const { height } = Dimensions.get('screen');

const style = StyleSheet.create({
  countryList: {
    display: 'flex',
    flexDirection: 'row',
    margin: 10,
    marginHorizontal: '0%',
  },
  contentSearch: { paddingHorizontal: 15, marginTop: '5%' },
  flatList: { flex: 1, marginHorizontal: '3%' },
  contentContainerStyle: {
    paddingTop: '6%',
    paddingHorizontal: 5,
  },
  separator: { marginVertical: 5, borderWidth: 0.7, borderColor: '#eee' },
  flagImage: {
    width: 32,
    height: 23,
  },
  header: {
    paddingTop: 0,
  },
  contentRenderItemName: { marginLeft: '7%', flex: 1, },
});
export default CountrySelectModal;
