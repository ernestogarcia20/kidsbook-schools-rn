import React, { useState, useEffect } from 'react';
import { Modal, StyleSheet, View, TouchableOpacity, Dimensions } from 'react-native';
import Calendar from '@components/Calendar';
import Text from '@components/Text';
import colors from '@assets/colors';
import moment from 'moment';

const { height } = Dimensions.get('window');
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    paddingTop: '10%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
});

const CalendarModal = ({ modalVisible, onClose, onSave, disableMode, startDate, perDayObj }) => {
  const [dateSelected, setDateSelected] = useState('');
  const [monthSelected, setMonthSelected] = useState(startDate || moment().format('YYYY-MM-DD'));
  const [arrayStringSelected, setArrayStringSelected] = useState([]);

  useEffect(() => {
    if (!modalVisible) {
      setDateSelected('');
      setMonthSelected(startDate || moment().format('YYYY-MM-DD'));
      setArrayStringSelected([]);
    }
  }, [modalVisible]);

  const setState = (key, value) => {
    setMonthSelected(value);
  };
  const onDayPress = (day) => {
    setDateSelected(day.date);
    setArrayStringSelected(day.arrayDates);
  };
  const handleSaveDate = () => {
    onSave({ date: dateSelected, arrayDates: arrayStringSelected });
    onClose();
  };
  return (
    <Modal animationType="fade" transparent visible={modalVisible}>
      <View style={styles.centeredView}>
        {modalVisible && (
          <Calendar
            setState={setState}
            onDayPress={onDayPress}
            dateSelected={dateSelected}
            monthSelected={monthSelected}
            disableMode={disableMode}
            startDisableDateMode={startDate}
            resetData={!modalVisible}
            perDayObj={perDayObj}
          >
            <View
              style={{
                justifyContent: 'flex-end',
                alignItems: 'center',
                flexDirection: 'row',
                marginHorizontal: '8%',
                marginTop: 15,
              }}
            >
              <TouchableOpacity onPress={onClose}>
                <Text color={colors.DANGER}>Cancelar</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ marginLeft: '10%' }} onPress={handleSaveDate}>
                <Text color={colors.GREEN} weight="SemiBold" fontSize={16}>
                  Aceptar
                </Text>
              </TouchableOpacity>
            </View>
          </Calendar>
        )}
      </View>
    </Modal>
  );
};

export default CalendarModal;
