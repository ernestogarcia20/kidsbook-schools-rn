import React, { useState } from 'react';
import { View, StyleSheet, Switch } from 'react-native';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import { EVENT_REPEAT } from '@utils/constants/navigates';
import DateTimeInput from '@components/DateTimeInput';

export const navigationRef = React.createRef();

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentEvent: {},
  contentHeader: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 10,
    marginBottom: 5,
    paddingTop: 5,
  },
});

const Layout = ({
  title = '¿Es un Evento?',
  updateMasterState,
  attrName,
  value,
  i18n,
  navigation,
  onSave = () => {},
}) => {
  const [repeatObj, setRepeatObj] = useState({ text: I18n.t('eventOptions.never') });
  const [startDate, setStartDate] = useState({ text: I18n.t('eventOptions.never') });

  const handleSelection = (result) => {
    setRepeatObj(result);
    onSave({ startDate, repeatObj: result });
  };

  const onPressRepeat = () => {
    navigation?.navigate(EVENT_REPEAT, { handleSelection, startDate });
  };

  const onPressStartDate = (date) => {
    setStartDate(date);
    onSave({ startDate: date, ...repeatObj });
  };

  return (
    <View style={styles.contentEvent}>
      <View style={[styles.rowLarge, styles.contentHeader]}>
        <Text weight="Medium">{title}</Text>
        <View>
          <Switch
            trackColor={{ false: '#767577', true: 'rgba(0, 105, 107, 0.3)' }}
            thumbColor={value ? '#00696B' : '#f4f3f4'}
            style={{ marginRight: 10 }}
            onValueChange={() => updateMasterState(attrName, !value)}
            value={value}
          />
        </View>
      </View>
      {value && (
        <View>
          <DateTimeInput title={I18n.t('eventOptions.starts')} onSave={onPressStartDate} />
          <DateTimeInput
            title={I18n.t('eventOptions.repeat')}
            onPress={onPressRepeat}
            colorValue="#838383"
            customText={repeatObj?.text}
          />
        </View>
      )}
    </View>
  );
};

export default Layout;
