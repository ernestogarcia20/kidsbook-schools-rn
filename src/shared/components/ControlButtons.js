import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import CheckBox from '@components/CheckBox';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Feather';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentEdit: { paddingHorizontal: '5%', paddingTop: '5%' },
  contentActionsEdit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  contentButtons: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonTrash: {
    width: 30,
    height: 30,
    borderRadius: 5,
    backgroundColor: Colors.RED,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '2%',
  },
  contentSelectedAll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '3.5%',
    marginTop: '7%',
    alignItems: 'center',
  },
  buttonMove: {
    height: 30,
    paddingHorizontal: 10,
    borderRadius: 5,
    backgroundColor: Colors.BLUEDARK,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
  },
});

const Layout = ({
  handleManageMove = null,
  handleManageDelete = null,
  handleSelectAll = () => {},
  selectAll,
}) => {
  return (
    <View style={styles.contentEdit}>
      <View style={styles.contentActionsEdit}>
        <Text color={Colors.GRAY}>{I18n.t('actions')}:</Text>
        <View style={styles.contentButtons}>
          {handleManageMove && (
            <TouchableOpacity style={styles.buttonMove} onPress={() => handleManageMove()}>
              <Text color={Colors.WHITE}>Mover</Text>
            </TouchableOpacity>
          )}
          {handleManageDelete && (
            <TouchableOpacity style={styles.buttonTrash} onPress={() => handleManageDelete()}>
              <Icon name="trash" size={15} color={Colors.WHITE} />
            </TouchableOpacity>
          )}
        </View>
      </View>
      <View style={styles.contentSelectedAll}>
        <Text>{I18n.t('selectAll')}</Text>
        <CheckBox onPress={handleSelectAll} check={selectAll} />
      </View>
    </View>
  );
};

export default Layout;
