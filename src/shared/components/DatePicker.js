import React, { useState } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const Layout = ({
  i18n,
  modePicker = 'date',
  minimumDate,
  maximumDate,
  showModal = false,
  onChange = () => {},
  onClose = () => {},
  updateMasterState,
  attrName,
  date = new Date(),
}) => {
  const handleConfirmPicker = (result) => {
    onChange(result);
  };

  const hideDatePicker = () => {
    updateMasterState(attrName, false);
    onClose();
  };
  return (
    <View style={styles.container}>
      <DateTimePickerModal
        isVisible={showModal}
        mode={modePicker}
        isDarkModeEnabled={false}
        textColor={Colors.BLACK}
        locale={i18n.locale}
        headerTextIOS=""
        date={date}
        cancelTextIOS="Cancelar"
        confirmTextIOS="Confirmar"
        minimumDate={minimumDate}
        maximumDate={maximumDate}
        onConfirm={handleConfirmPicker}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

const mapStateToProps = ({ i18n }) => ({
  i18n,
});

export default connect(mapStateToProps)(Layout);
