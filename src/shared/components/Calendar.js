import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { connect } from 'react-redux';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Feather';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import moment from 'moment';
import GrassImage from '@images/grass.png';
import FastImage from '@components/FastImage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  controls: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    width: '20%',
    marginRight: 15,
  },
  contentControls: {
    height: 45,
    backgroundColor: Colors.GREEN,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    paddingHorizontal: '2%',
    paddingLeft: '8%',
  },
  contentEvent: {
    height: 70,
    backgroundColor: Colors.WHITE,
    marginHorizontal: 5,
    borderRadius: 10,
    marginBottom: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
    borderLeftWidth: 17,
    borderLeftColor: '#BBD7EC',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  contentEventSubject: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 0.7,
  },
  eventTrash: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 25,
    height: 25,
    backgroundColor: '#E43956',
    borderRadius: 5,
  },
});

const months = I18n.t('monthNames');

let lastDateBetween = [];

const Layout = ({
  onDayPress,
  setState,
  i18n: i18nProps,
  handleDeleteEvent,
  monthSelected,
  dateSelected,
  newEvents,
  eventSeleted = [],
  disableMode = null,
  startDisableDateMode = null,
  children,
  perDayObj = null,
  resetData = false,
}) => {
  const [refCalendar, setRefCalendar] = useState(null);
  const monthDay = parseInt(moment(monthSelected).format('MM').toString());
  const year = moment(monthSelected).format('YYYY').toString();
  LocaleConfig.defaultLocale = i18nProps.locale;
  useEffect(() => {
    lastDateBetween = [];
  }, []);

  const onPressArrowLeft = () => {
    const mothStartDisableDateMode = startDisableDateMode
      ? parseInt(moment(startDisableDateMode).format('MM').toString())
      : null;
    if (!refCalendar) {
      return;
    }

    if (mothStartDisableDateMode && mothStartDisableDateMode === monthDay) {
      return;
    }

    refCalendar.header.onPressLeft();
  };

  const onPressArrowRight = () => {
    if (!refCalendar) {
      return;
    }

    refCalendar.header.onPressRight();
  };

  const handleProcessDay = (day) => {
    if (!disableMode) {
      return onDayPress(day);
    }
    const indexD = lastDateBetween.findIndex((d) => d === day.dateString);
    if (indexD < 0) {
      return null;
    }
    if (disableMode) {
      return onDayPress({ date: day.dateString, arrayDates: lastDateBetween });
    }
    return onDayPress(day);
  };

  const disabledDays = (mode = 'month', initialDate = '2021-02-11') => {
    const totalDaysInMooth = moment(monthSelected, 'YYYY-MM-DD').daysInMonth();
    const dayDisabled = moment(initialDate, 'YYYY-MM-DD').date();
    if (mode === 'none' || !disableMode) {
      return { undefined };
    }
    if (mode === 'month') {
      let obj = {};
      for (let i = 1; i <= totalDaysInMooth; i += 1) {
        const date = `${year}-${String(monthDay).padStart(2, '0')}-${String(i).padStart(2, '0')}`;
        if (dayDisabled === i) {
          const indexD = lastDateBetween.findIndex((d) => d === date);
          if (indexD < 0) {
            lastDateBetween.push(date);
          }
        }
        obj = { ...obj, [date]: { disabled: dayDisabled !== i } };
      }
      return obj;
    }
    if (mode === 'twoWeeks') {
      //  lastDateBetween = lastDa;
      let obj = {};

      const fortnightAway = new Date(+new Date(initialDate) + 12096e5);
      const resultFormatter = moment(fortnightAway).format('YYYY-MM-DD');

      const indexD = lastDateBetween.findIndex((d) => d === resultFormatter);
      if (indexD === -1) lastDateBetween.push(resultFormatter);
      for (let i = 1; i <= totalDaysInMooth; i += 1) {
        const date = `${year}-${String(monthDay).padStart(2, '0')}-${String(i).padStart(2, '0')}`;
        const indexD2 = lastDateBetween.findIndex((d) => d === date);

        if (indexD2 > -1) {
          const fortnightAway2 = new Date(+new Date(`${lastDateBetween[indexD2]}`) + 12096e5);
          const resultFormatter2 = moment(fortnightAway2).format('YYYY-MM-DD');
          lastDateBetween.push(resultFormatter2);
          obj = { ...obj, [date]: { disabled: date !== lastDateBetween[indexD2] } };
        } else {
          obj = { ...obj, [date]: { disabled: date !== resultFormatter } };
        }
      }
      return obj;
    }
    if (mode === 'weeks') {
      let obj = {};

      const fortnightAway = new Date(+new Date(initialDate) + 6048e5);
      const resultFormatter = moment(fortnightAway).format('YYYY-MM-DD');

      const indexD = lastDateBetween.findIndex((d) => d === resultFormatter);
      if (indexD === -1) lastDateBetween.push(resultFormatter);
      for (let i = 1; i <= totalDaysInMooth; i += 1) {
        const date = `${year}-${String(monthDay).padStart(2, '0')}-${String(i).padStart(2, '0')}`;
        const indexD2 = lastDateBetween.findIndex((d) => d === date);

        if (indexD2 > -1) {
          const fortnightAway2 = new Date(+new Date(`${lastDateBetween[indexD2]}`) + 6048e5);
          const resultFormatter2 = moment(fortnightAway2).format('YYYY-MM-DD');
          lastDateBetween.push(resultFormatter2);
          obj = { ...obj, [date]: { disabled: date !== lastDateBetween[indexD2] } };
        } else {
          obj = { ...obj, [date]: { disabled: date !== resultFormatter } };
        }
      }
      return obj;
    }
    if (mode === 'perDay') {
      let obj = {};

      const stringWeekDays = perDayObj.allList.map((x) => x.weekDay).join(', ');
      for (let i = 1; i <= totalDaysInMooth; i += 1) {
        const date = `${year}-${String(monthDay).padStart(2, '0')}-${String(i).padStart(2, '0')}`;
        const weekDay = moment(date, 'YYYY-MM-DD').weekday();
        if (startDisableDateMode < date && stringWeekDays.match(String(weekDay))) {
          lastDateBetween.push(date);
        }
        obj = {
          ...obj,
          [date]: {
            disabled: startDisableDateMode > date ? true : !stringWeekDays.match(String(weekDay)),
          },
        };
      }
      return obj;
    }
    return {};
  };

  return (
    <View style={styles.container}>
      <View style={{ marginTop: '8%', marginHorizontal: 10 }}>
        <View style={{ backgroundColor: Colors.WHITE, borderRadius: 10, paddingBottom: 15 }}>
          <FastImage source={GrassImage} style={[styles.contentControls]}>
            <Text color={Colors.WHITE} fontSize={16}>
              {months[monthDay - 1]}, {year}
            </Text>
            <View style={styles.controls}>
              <TouchableOpacity
                hitSlop={{ left: 30, right: 5, top: 15, bottom: 15 }}
                onPress={onPressArrowLeft}
              >
                <Icon name="chevron-left" size={24} color={Colors.WHITE} />
              </TouchableOpacity>
              <TouchableOpacity
                hitSlop={{ left: 5, right: 30, top: 15, bottom: 15 }}
                onPress={onPressArrowRight}
              >
                <Icon name="chevron-right" size={24} color={Colors.WHITE} />
              </TouchableOpacity>
            </View>
          </FastImage>
          <Calendar
            // testID={testIDs.calendars.FIRST}
            ref={(ref) => {
              setRefCalendar(ref);
            }}
            markingType="multi-dot"
            current={startDisableDateMode || moment().format('YYYY-MM-DD')}
            minDate={startDisableDateMode || moment().format('YYYY-MM-DD')}
            headerStyle={{ marginTop: 0, marginHorizontal: 0 }}
            renderHeader={() => <View />}
            onDayPress={handleProcessDay}
            onMonthChange={(month) => setState('monthSelected', month.dateString)}
            markedDates={{
              ...newEvents,
              ...disabledDays(disableMode, startDisableDateMode),
              [dateSelected]: {
                selected: true,
                disableTouchEvent: false,
                selectedColor: '#BCD8F1',
                selectedTextColor: Colors.BLACK,
              },
            }}
            theme={{
              textDayFontSize: 14,
              textDayFontFamily:
                Platform.OS === 'android' ? `${'Poppins'}-${'Regular'}` : 'Poppins',
              textSectionTitleColor: 'rgba(0,0,0,0.6)',
            }}
            hideArrows
          />
          {eventSeleted.length > 0 && (
            <View
              style={{
                marginTop: 10,
                marginHorizontal: 10,
                paddingTop: 15,
                borderTopWidth: 1,
                borderTopColor: 'rgba(0,0,0,0.2)',
              }}
            >
              {eventSeleted.map((event, index) => {
                return (
                  <View key={`${index}`} style={styles.contentEvent}>
                    <View>
                      <Text align="left" weight="Medium">
                        {event.is_event === 1 ? I18n.t('event') : I18n.t('activity')}
                      </Text>
                      <Text align="left" fontSize={12} color="rgba(71, 71, 71, 0.6)">
                        {moment(event.time, 'HH:mm').format('hh:mm A')}
                      </Text>
                    </View>
                    <View style={styles.contentEventSubject}>
                      <Text align="left" fontSize={12} weight="Medium" color="#424242">
                        {event.subject}
                      </Text>
                    </View>
                    <TouchableOpacity
                      style={styles.eventTrash}
                      onPress={() => handleDeleteEvent(event)}
                    >
                      <Icon name="trash" color={Colors.WHITE} size={14} />
                    </TouchableOpacity>
                  </View>
                );
              })}
            </View>
          )}
          {children}
        </View>
      </View>
    </View>
  );
};
const mapStateToProps = ({ i18n }) => ({
  i18n,
});

export default connect(mapStateToProps)(Layout);
