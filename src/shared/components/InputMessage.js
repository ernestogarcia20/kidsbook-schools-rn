import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
  Dimensions,
  Keyboard,
  Platform,
} from 'react-native';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Text from '@components/Text';
import IconMaterial from 'react-native-vector-icons/Ionicons';
import { getEmojis } from '@utils/functions/globals';
import { Popover, PopoverController } from 'react-native-modal-popover';
import { fontMaker } from './typografy';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  contentInput: {
    minHeight: 60,
    paddingHorizontal: 20,
    flexDirection: 'row',
    marginHorizontal: 15,
    backgroundColor: Colors.WHITE,
    marginBottom: 15,
    borderBottomWidth: 0,
    borderColor: 'transparent',
    shadowColor: Colors.BLACK,
    borderTopWidth: 1,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
    borderRadius: 20,
  },
  input: {
    flex: 0.9,
    ...fontMaker('Medium'),
    maxHeight: 90,
    marginRight: 10,
    marginVertical: Platform.OS === 'ios' ? 17 : 5,
  },
});

const InputMessage = ({
  onPressSendMessage,
  onPressAttach,
  showAttach = true,
  showEmoji = true,
  onChangeText = () => {},
  placeholder = 'Nuevo mensaje',
}) => {
  const [text, setText] = useState('');
  const [inputRef, setInputRef] = useState(null);
  const [cursorPosition, setCursorPosition] = useState(null);
  const handleOnChangeText = (text) => {
    onChangeText(text);
    setText(text);
  };

  const handleEmoji = (emoji) => {
    const formatText = cursorPosition
      ? [text.slice(0, cursorPosition), emoji, text.slice(cursorPosition)].join('')
      : text.concat(emoji);
    setText(formatText);
    setCursorPosition(null);
  };

  const renderItemEmoji = ({ item }) => {
    return (
      <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => handleEmoji(item)}>
        <Text fontSize={22}>{item}</Text>
      </TouchableOpacity>
    );
  };

  const onSelectionChange = ({ nativeEvent: { selection } }) => {
    setCursorPosition(selection.start);
  };
  return (
    <>
      <PopoverController>
        {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
          <>
            <View style={styles.contentInput}>
              {showEmoji && (
                <TouchableOpacity
                  style={{
                    marginRight: 10,
                    justifyContent: 'flex-end',
                    marginBottom: Platform.OS === 'ios' ? 16 : 17,
                  }}
                  onPress={() => {
                    Keyboard.dismiss();
                    setTimeout(() => {
                      openPopover();
                    }, 80);
                  }}
                  ref={setPopoverAnchor}
                >
                  <Icon name="laugh-beam" size={24} color={Colors.GREEN} />
                </TouchableOpacity>
              )}
              {showAttach && (
                <TouchableOpacity
                  style={{
                    height: 23,
                    width: 25,
                    marginRight: 15,
                    justifyContent: 'flex-end',
                    marginBottom: 15,
                  }}
                  onPress={onPressAttach}
                >
                  <CustomIcon name="attach" width={30} height={30} />
                </TouchableOpacity>
              )}
              <TextInput
                ref={(ref) => {
                  setInputRef(ref);
                }}
                style={styles.input}
                onChangeText={handleOnChangeText}
                onSelectionChange={onSelectionChange}
                placeholder={placeholder}
                multiline
                value={text}
              />
              <TouchableOpacity
                style={{
                  flex: 0.1,
                  justifyContent: 'flex-end',
                  marginBottom: Platform.OS === 'ios' ? 15 : 17,
                }}
                onPress={() => {
                  onPressSendMessage(text);
                  setText('');
                }}
              >
                <IconMaterial name="md-send" size={23} color={Colors.GREEN} />
              </TouchableOpacity>
            </View>
            <Popover
              visible={popoverVisible}
              fromRect={popoverAnchorRect}
              onClose={closePopover}
              placement="top"
              backgroundStyle={{ backgroundColor: 'transparent' }}
              contentStyle={{ maxHeight: width * 0.35, maxWidth: width / 1.5 }}
            >
              <FlatList
                data={getEmojis()}
                showsVerticalScrollIndicator={false}
                renderItem={renderItemEmoji}
                numColumns={6}
                initialNumToRender={5}
                disableVirtualization
                keyExtractor={(ite, index) => ite + index}
              />
            </Popover>
          </>
        )}
      </PopoverController>
    </>
  );
};

export default InputMessage;
