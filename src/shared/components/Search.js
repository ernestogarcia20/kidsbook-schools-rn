import React, { useState } from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { isEmptyOrSpaces } from '@utils/functions/globals';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Feather';
import FastImage from '@components/FastImage';
import Background from '@images/bg1.png';

const styles = StyleSheet.create({
  contentSearch: {},
  textInput: {
    width: '70%',
    fontSize: 16,
    justifyContent: 'center',
  },
  contentBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  boxInputSearch: {
    flexDirection: 'row',
    backgroundColor: Colors.WHITE,
    alignItems: 'center',
    paddingHorizontal: 15,
    height: 55,
    borderRadius: 10,
    justifyContent: 'space-between',
    // IOS
    shadowColor: Colors.PRIMARYDARKCOLOR,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 5,
  },
  icon: {},
  iconClose: { paddingRight: 5 },
  buttonClean: { paddingHorizontal: 5 },

  contentImageSearch: { padding: '7%' },
});

const Search = ({
  placeholder = 'Buscar',
  onChangeText = () => {},
  handleSearch = () => {},
  onBlur = () => {},
  onFocus = () => {},
  value = null,
  keyboardType = 'default',
  hasBackground = false,
}) => {
  const [text, setText] = useState(value || null);

  const handleOnChangeText = (textChange) => {
    onChangeText(textChange);
    setText(textChange);
  };

  const searchInput = () => {
    const newText = isEmptyOrSpaces(text);
    if (newText) {
      return;
    }
    handleSearch();
  };

  const handleCleanSearch = () => {
    handleOnChangeText(null);
  };

  return (
    <FastImage
      source={hasBackground ? Background : null}
      style={[
        hasBackground
          ? styles.contentImageSearch
          : { width: '100%', paddingHorizontal: 2, height: 60 },
      ]}
    >
      <View style={styles.contentSearch}>
        <View style={styles.boxInputSearch}>
          <TextInput
            value={value || text}
            style={[styles.textInput]}
            underlineColorAndroid="transparent"
            placeholderTextColor={Colors.BLACKLIGHT}
            placeholder={placeholder}
            returnKeyType="search"
            keyboardType={keyboardType}
            onChangeText={handleOnChangeText}
            onSubmitEditing={searchInput}
            onBlur={onBlur}
            onFocus={onFocus}
          />
          <View style={styles.contentBox}>
            <TouchableOpacity style={styles.buttonClean} onPress={text ? handleCleanSearch : null}>
              {text ? (
                <Icon color={Colors.RED} name="x" size={25} style={styles.iconClose} />
              ) : null}
            </TouchableOpacity>
            <TouchableOpacity onPress={handleSearch} style={styles.iconClose}>
              <Icon name="search" color="#7F7F7F" size={30} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </FastImage>
  );
};

export default Search;
