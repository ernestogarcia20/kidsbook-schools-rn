import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import KidsbookTranslation from '@shared/icons/custom-svg/kidsbook_translation';
import KidsbookDashboard from '@shared/icons/custom-svg/kidsbook_dashboard';
import KidsbookBabyBoy from '@shared/icons/custom-svg/kidsbook_baby-boy';
import KidsbookParents from '@shared/icons/custom-svg/kidsbook_parents';
import KidsbookTeachers from '@shared/icons/custom-svg/kidsbook_teachers';
import kidsbookAbc from '@shared/icons/custom-svg/kidsbook_abc';
import kidsbookActividades from '@shared/icons/custom-svg/kidsbok_actividades';
import KidsbookNotas from '@shared/icons/custom-svg/kidsbook_notas';
import KidsbookMessage from '@shared/icons/custom-svg/kidsbook_envelope';
import KidsbookCalendar from '@shared/icons/custom-svg/kidsbook_calendar';
import KidsbookReports from '@shared/icons/custom-svg/kidsbook_reports';
import KidsbookStart from '@shared/icons/custom-svg/kidsbook_star';
import KidsbookSettings from '@shared/icons/custom-svg/kidsbook_settings';
import KidsbookSchoolSettings from '@shared/icons/custom-svg/kidsbook_school-settings';
import KidsbookLike from '@shared/icons/custom-svg/kidsbook_like';
import KidsbookLogout from '@shared/icons/custom-svg/kidsbook_logout';
import KidsbookSchool from '@shared/icons/custom-svg/kidsbook_school';
import KidsbookDiaper from '@shared/icons/custom-svg/kidsbook_training-diaper';
import KidsbookBottle from '@shared/icons/custom-svg/kidsbook_bottle';
import KidsbookBottleNo from '@shared/icons/custom-svg/kidsbook_bottle-no';
import KidsbookCalendarBlank from '@shared/icons/custom-svg/kidsbook_calendar-blank';
import KidsbookDiaperNo from '@shared/icons/custom-svg/kidsbook_diaper-no';
import kidsbookMessageReceivedReaded from '@shared/icons/custom-svg/kidsbook_message-received-readed';
import kidsbookMessageReceived from '@shared/icons/custom-svg/kidsbook_message-received';
import kidsbookMessageSentReaded from '@shared/icons/custom-svg/kidsbook_message-sent-readed';
import kidsbookMessageSent from '@shared/icons/custom-svg/kidsbook_message-sent';
import kidsbookBirthday from '@shared/icons/custom-svg/kidsbook_birthday';
import Attach from '@shared/icons/custom-svg/attach';

const svg = {
  kb_translation: KidsbookTranslation,
  kb_dashboard: KidsbookDashboard,
  kb_babyBoy: KidsbookBabyBoy,
  kb_parents: KidsbookParents,
  kb_teachers: KidsbookTeachers,
  kb_abc: kidsbookAbc,
  kb_actividades: kidsbookActividades,
  kb_notas: KidsbookNotas,
  kb_messages: KidsbookMessage,
  kb_calendar: KidsbookCalendar,
  kb_reports: KidsbookReports,
  kb_star: KidsbookStart,
  kb_settings: KidsbookSettings,
  kb_schols_settings: KidsbookSchoolSettings,
  kb_like: KidsbookLike,
  kb_logout: KidsbookLogout,
  kb_school: KidsbookSchool,
  kb_diaper: KidsbookDiaper,
  kb_diaper_no: KidsbookDiaperNo,
  kb_bottle: KidsbookBottle,
  kb_bottle_no: KidsbookBottleNo,
  kb_calendar_blank: KidsbookCalendarBlank,
  'kidsbook-message-received-readed': kidsbookMessageReceivedReaded,
  'kidsbook-message-received': kidsbookMessageReceived,
  'kidsbook-message-sent-readed': kidsbookMessageSentReaded,
  'kidsbook-message-sent': kidsbookMessageSent,
  kidsbook_birthday: kidsbookBirthday,
  attach: Attach,
};
const Icon = (props) => {
  const { name, width, heigth, newKey } = props;
  const IconSvg = svg[name] ? svg[name] : null;
  if (!IconSvg) {
    return <View />;
  }
  return <IconSvg newKey={newKey} width={width} heigth={heigth} />;
};

Icon.propTypes = {
  newKey: PropTypes.string,
  width: PropTypes.number,
  heigth: PropTypes.number,
  name: PropTypes.string.isRequired,
};

Icon.defaultProps = {
  width: 40,
  heigth: 40,
  newKey: '',
};
export default Icon;
