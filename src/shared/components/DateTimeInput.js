import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import I18nT from 'react-native-redux-i18n';
import Text from '@components/Text';
import moment from 'moment';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Colors from '@assets/colors';
import CalendarModal from '@components/modals/CalendarModal';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentEvent: {},
  buttonOption: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: Colors.GREEN,
    marginHorizontal: 15,
    marginBottom: 10,
  },
  contentHeader: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 10,
    marginBottom: 5,
    paddingTop: 5,
  },
});

const defaultDateTime = {
  date: String(moment().format('DD/MM/YYYY')),
  time: String(moment().format('hh:mm a.')),
};

const DateTimeInput = ({
  title = I18nT.t('eventOptions.ends'),
  onPress = () => {},
  colorValue = Colors.BLACK,
  customText = '',
  i18n,
  onSave = () => {},
  customPicker = false,
  disableMode = '',
  startDate = null,
  perDayObj = null,
}) => {
  const [dateTimeObj, setDateTimeObj] = useState(defaultDateTime);
  const [showCustomCalendar, setShowCustomCalendar] = useState(defaultDateTime);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [modePicker, setModePicker] = useState('date');

  useEffect(() => {
    onSave(customPicker ? null : defaultDateTime.date);
    if (startDate) {
      setDateTimeObj({
        ...defaultDateTime,
        date: moment(startDate, 'YYYY-MM-DD').format('DD/MM/YYYY'),
      });
    }
  }, []);

  const onPressDate = () => {
    setModePicker('date');
    if (customPicker) {
      return setShowCustomCalendar(true);
    }
    return setDatePickerVisibility(true);
  };
  const onPressTime = () => {
    setModePicker('time');
    setDatePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const closeModalPicker = () => {
    setShowCustomCalendar(false);
  };

  const handleConfirmPicker = (result) => {
    if (!result) {
      return;
    }
    const newResult =
      modePicker === 'date'
        ? moment(result).format('DD/MM/YYYY')
        : moment(result).format('hh:mm a.');

    if (modePicker === 'date') {
      if (customPicker) {
        setDateTimeObj({ ...dateTimeObj, date: moment(result.date).format('DD/MM/YYYY') });
      } else setDateTimeObj({ ...dateTimeObj, date: newResult });
      onSave(customPicker ? result : newResult);
    } else {
      setDateTimeObj({ ...dateTimeObj, time: newResult });
    }
    if (modePicker !== 'date') {
      hideDatePicker();
    } else if (customPicker) {
      setModePicker('time');
      setTimeout(() => {
        if (customPicker) {
          setDatePickerVisibility(true);
        }
      }, 250);
    } else {
      hideDatePicker();
      setModePicker('time');
      setDatePickerVisibility(true);
    }
  };

  const handleConfirmPickerDateTime = (result) => {
    if (!result) {
      return;
    }
    const date = moment(result).format('DD/MM/YYYY');
    const time = moment(result).format('hh:mm a.');

    setDateTimeObj({ ...dateTimeObj, date, time });
    hideDatePicker();
  };
  return (
    <>
      <View style={[styles.buttonOption, styles.rowLarge]}>
        <Text>{title}</Text>

        {customText ? (
          <TouchableOpacity onPress={onPress} hitSlop={{ top: 20, right: 20, left: 30 }}>
            <Text color={colorValue}>{customText}</Text>
          </TouchableOpacity>
        ) : (
          <Text color={colorValue}>{customText}</Text>
        )}
        {!customText && (
          <View style={styles.row}>
            <TouchableOpacity onPress={onPressDate} hitSlop={{ top: 20, left: 30 }}>
              <Text color={colorValue}>
                {dateTimeObj.date} {'  '}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onPressTime} hitSlop={{ top: 20, right: 15 }}>
              <Text color={colorValue}>{dateTimeObj.time}</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={Platform.OS === 'ios' ? 'datetime' : modePicker}
        isDarkModeEnabled={false}
        textColor={Colors.BLACK}
        locale={i18n.locale}
        headerTextIOS=""
        cancelTextIOS="Cancelar"
        confirmTextIOS="Confirmar"
        minimumDate={new Date()}
        onConfirm={Platform.OS === 'ios' ? handleConfirmPickerDateTime : handleConfirmPicker}
        onCancel={hideDatePicker}
      />
      <CalendarModal
        perDayObj={perDayObj}
        modalVisible={showCustomCalendar}
        onClose={closeModalPicker}
        onSave={handleConfirmPicker}
        disableMode={disableMode}
        startDate={startDate}
      />
    </>
  );
};

const mapStateToProps = ({ i18n }) => ({
  i18n,
});

export default connect(mapStateToProps)(DateTimeInput);
