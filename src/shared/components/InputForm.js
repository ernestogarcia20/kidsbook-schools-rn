import React from 'react';
import { TextInput, View, Platform, StyleSheet } from 'react-native';
import Emoji from '@components/Emoji';
import TextField from '@components/text-input-md';
import Colors from '@assets/colors';
import Text from '@components/Text';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentImages: {
    paddingTop: '5%',
    paddingBottom: '5%',
  },
  content: {
    paddingHorizontal: '6%',
    paddingBottom: '5%',
  },
  paddingHorizontal: {
    paddingHorizontal: 0,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  separator: {
    marginTop: 5,
  },
  contentLine: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)',
  },
  input: { flex: 1, minHeight: 80, maxHeight: 190, marginLeft: 15 },
  inputMultiline: {
    height: null,
    minHeight: 90,
    paddingTop: Platform.OS === 'ios' ? 10 : 5,
    paddingBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  contenButtonCreateActivity: { marginTop: '5%', marginHorizontal: '7%', paddingBottom: '5%' },
  buttonDeleteAttach: {
    position: 'absolute',
    top: 0,
    right: 10,
    width: 20,
    height: 20,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.DANGER,
  },
});

const Input = ({ multiline = false, title, placeholder, value, attrName, updateMasterState }) => {
  return (
    <View style={[styles.paddingHorizontal]}>
      <View style={[styles.row, styles.separator, styles.contentLine]}>
        <Text>{title}: </Text>
        {!multiline && (
          <TextInput
            style={styles.input}
            multiline={multiline}
            placeholder={placeholder}
            value={value}
            onChangeText={(text) => updateMasterState(attrName, text)}
            placeholderTextColor="#909090"
          />
        )}
      </View>
      {multiline && (
        <View style={[styles.contentLine, styles.inputMultiline]}>
          <Emoji >
            <TextInput
              textAlignVertical="top"
              style={styles.input}
              multiline={multiline}
              /*onSelectionChange={onSelectionChange}
              onFocus={scrolled}*/
              placeholder={placeholder}
              placeholderTextColor="#909090"
              value={value}
              onChangeText={(text) => updateMasterState(attrName, text)}
            />
          </Emoji>
        </View>
      )}
    </View>
  );
};

export default Input;
