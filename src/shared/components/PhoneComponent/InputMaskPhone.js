import React from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, Image } from 'react-native';
import PropTypes from 'prop-types';
import CountrySelectModal from '@modals/CountrySelectModal';
import Underline from '@components/text-input-md/lib/Underline';
import colors from '@assets/colors';
import Text from '@components/Text';
import * as RNLocalize from 'react-native-localize';
import CountryData from './Countries';
import { fontMaker } from '../typografy';

export default class IntlPhoneInput extends React.Component {
  constructor(props) {
    super(props);
    const data = CountryData;
    const defaultCountry = data.filter((obj) => obj.code === RNLocalize.getCountry())[0];
    this.state = {
      defaultCountry,
      flag: defaultCountry.flag, // `https://www.countryflags.io/${defaultCountry.ISO2}/flat/64.png`,
      modalVisible: false,
      dialCode: defaultCountry.dialCode,
      phoneNumber: props.defaultValue,
      editableText: props.editableText,
      mask: defaultCountry.mask,
      countryData: data,
    };
  }

  onChangePropText = (unmaskedPhoneNumber, phoneNumber) => {
    const { dialCode, mask } = this.state;
    const { onChangeText = null } = this.props;
    const countOfNumber = mask.match(/9/g).length;
    if (!onChangeText) {
      return;
    }
    const isVerified = countOfNumber === unmaskedPhoneNumber?.length && phoneNumber?.length > 0;
    onChangeText({
      dialCode,
      unmaskedPhoneNumber,
      phoneNumber,
      isVerified,
    });
  };

  onChangeText = (value) => {
    let unmaskedPhoneNumber = (value.match(/\d+/g) || []).join('');

    if (unmaskedPhoneNumber.length === 0) {
      this.setState({ phoneNumber: '' });
      this.onChangePropText('', '');
      return;
    }

    let phoneNumber = this.state.mask.replace(/9/g, '_');
    for (let index = 0; index < unmaskedPhoneNumber.length; index += 1) {
      phoneNumber = phoneNumber.replace('_', unmaskedPhoneNumber[index]);
    }
    let numberPointer = 0;
    for (let index = phoneNumber.length; index > 0; index -= 1) {
      if (phoneNumber[index] !== ' ' && !isNaN(phoneNumber[index])) {
        numberPointer = index;
        break;
      }
    }
    phoneNumber = phoneNumber.slice(0, numberPointer + 1);
    unmaskedPhoneNumber = (phoneNumber.match(/\d+/g) || []).join('');

    this.onChangePropText(unmaskedPhoneNumber, phoneNumber);
    this.setState({ phoneNumber });
  };

  showModal = () =>
    this.props.disableCountryChange
      ? null
      : this.setState({ modalVisible: this.state.editableText });

  hideModal = () => this.setState({ modalVisible: false });

  onCountryChange = async ({ dialCode }) => {
    const { countryData } = this.state;
    const { onChangeCountryCode = () => {} } = this.props;
    onChangeCountryCode(dialCode);
    try {
      const country = countryData.filter((obj) => obj.dialCode === dialCode)[0];
      this.setState({
        dialCode: country.dialCode,
        flag: country.flag, // `https://www.countryflags.io/${country.ISO2}/flat/64.png`,
        mask: country.mask,
        phoneNumber: '',
      });
      this.hideModal();
    } catch (err) {
      const { defaultCountry } = this.state;
      this.setState({
        dialCode: defaultCountry.dialCode,
        flag: defaultCountry.flag,
        mask: defaultCountry.mask,
        phoneNumber: '',
      });
    }
  };

  focus() {
    this.props.inputRef.current.focus();
  }

  render() {
    const { flag, countryData } = this.state;
    const {
      containerStyle,
      phoneInputStyle,
      dialCodeTextStyle,
      inputProps,
      data,
      placeholder,
      highlightColor = colors.GREEN,
      duration = 200,
      borderColor = '#E0E0E0',
    } = this.props;
    return (
      <>
        <View style={{ ...styles.container, ...containerStyle }}>
          <TouchableOpacity onPress={this.showModal}>
            <View style={styles.openDialogView}>
              <Text fontSize={16} style={{ marginRight: 5 }}>
                {flag}
              </Text>
              <Text style={[styles.dialCodeTextStyle, dialCodeTextStyle]}>
                {this.state.dialCode}
              </Text>
            </View>
          </TouchableOpacity>
          {/* this.renderModal() */}
          <CountrySelectModal
            open={this.state.modalVisible}
            countries={countryData}
            onClose={() => this.hideModal()}
            onChange={this.onCountryChange}
          />
          <TextInput
            {...inputProps}
            style={[styles.phoneInputStyle, phoneInputStyle]}
            placeholder={placeholder || this.state.mask}
            editable={this.state.editableText}
            autoCorrect={false}
            keyboardType="number-pad"
            secureTextEntry={false}
            value={this.state.phoneNumber}
            onFocus={() => {
              this.refs.underline.expandLine();
            }}
            onBlur={() => {
              this.refs.underline.shrinkLine();
            }}
            shake
            onChangeText={this.onChangeText}
          />
        </View>
        <Underline
          ref="underline"
          highlightColor={colors.GREEN}
          duration={200}
          borderColor="#E0E0E0"
        />
      </>
    );
  }
}

IntlPhoneInput.propTypes = {
  lang: PropTypes.string,
  defaultCountry: PropTypes.string,
  onChangeText: PropTypes.func,
  customModal: PropTypes.func,
  onChangeCountryCode: PropTypes.func,
  phoneInputStyle: PropTypes.object, // {}
  containerStyle: PropTypes.object, // {}
  dialCodeTextStyle: PropTypes.object, // {}
  flagStyle: PropTypes.object, // {}
  modalContainer: PropTypes.object, // {}
  filterInputStyle: PropTypes.object, // {}
  closeButtonStyle: PropTypes.object, // {}
  modalCountryItemCountryNameStyle: PropTypes.object, // {}
  filterText: PropTypes.string,
  closeText: PropTypes.string,
  searchIconStyle: PropTypes.object,
  disableCountryChange: PropTypes.bool,
  inputRef: PropTypes.object,
};

const styles = StyleSheet.create({
  closeTextStyle: {
    padding: 5,
    fontSize: 20,
    color: 'black',
    fontWeight: 'bold',
  },
  modalCountryItemCountryDialCodeStyle: {
    fontSize: 15,
  },
  modalCountryItemCountryNameStyle: {
    flex: 1,
    fontSize: 15,
  },
  modalCountryItemContainer: {
    flex: 1,
    paddingLeft: 5,
    flexDirection: 'row',
  },
  modalFlagStyle: {
    fontSize: 25,
  },
  modalContainer: {
    paddingTop: 15,
    paddingLeft: 25,
    paddingRight: 25,
    flex: 10,
    backgroundColor: 'white',
  },
  flagStyle: {
    fontSize: 35,
  },
  dialCodeTextStyle: {
    fontSize: 17,
  },
  countryModalStyle: {
    flex: 1,
    borderColor: 'black',
    borderTopWidth: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  openDialogView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageFlag: { width: 30, height: 35 },
  filterInputStyle: {
    flex: 1,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#fff',
    color: '#424242',
  },
  searchIcon: {
    padding: 10,
  },
  filterInputStyleContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  phoneInputStyle: {
    marginLeft: 5,
    flex: 1,
    ...fontMaker('Regular'),
    fontSize: 14,
  },
  container: {
    flexDirection: 'row',
    paddingHorizontal: 12,
    padding: 5,
    borderRadius: 10,
    alignItems: 'center',
    marginBottom: -10,
  },
  searchIconStyle: {
    color: 'black',
    fontSize: 15,
    marginLeft: 15,
  },
  buttonStyle: {
    alignItems: 'center',
    padding: 14,
    marginBottom: 10,
    borderRadius: 3,
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  countryStyle: {
    flex: 1,
    borderColor: 'black',
    borderTopWidth: 1,
    padding: 12,
  },
  closeButtonStyle: {
    padding: 12,
    alignItems: 'center',
  },
});
