import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, TextInput, StyleSheet, ViewPropTypes, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '@assets/colors';

const styles = StyleSheet.create({
  content: {
    height: 40,
    borderWidth: 1,
    borderRadius: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  contentColor: {
    height: 40,
    backgroundColor: Colors.BLUEINPUT,
    borderRadius: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  input: {
    flex: 1,
    paddingHorizontal: 15,
  },
  inputFocus: {
    flex: 1,
    paddingHorizontal: 15,
  },
  buttonEye: { paddingRight: 15 },
});
const InputAuth = ({
  value,
  isPassword,
  placeholder,
  styleInput,
  style,
  keyboardType,
  updateMasterState,
  attrName,
  onChangeText,
  onBlur,
  onFocus,
}) => {
  const [valueState, setValue] = useState(value);
  const [changeColor, setChangeColor] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const thatOnChangeText = (text) => {
    setValue(text);
    onChangeText(text);
    updateMasterState(attrName, text);
  };

  const thatOnFocus = () => {
    onFocus();
    if (!valueState) {
      return;
    }
    setChangeColor(false);
  };

  const thatOnBlur = () => {
    onBlur();
    if (!valueState) {
      return;
    }
    setChangeColor(true);
  };

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  return (
    <View style={[style, changeColor ? styles.contentColor : styles.content]}>
      <TextInput
        value={valueState}
        placeholder={placeholder}
        secureTextEntry={showPassword ? false : isPassword}
        keyboardType={keyboardType}
        style={[styleInput, changeColor ? styles.inputFocus : styles.input]}
        onChangeText={thatOnChangeText}
        placeholderTextColor={Colors.GRAY}
        onFocus={thatOnFocus}
        onBlur={thatOnBlur}
      />
      {isPassword && (
        <TouchableOpacity onPress={handleShowPassword} style={styles.buttonEye}>
          <Icon name={showPassword ? 'eye' : 'eye-slash'} size={20} />
        </TouchableOpacity>
      )}
    </View>
  );
};

InputAuth.propTypes = {
  value: PropTypes.string,
  isPassword: PropTypes.bool,
  placeholder: PropTypes.string,
  keyboardType: PropTypes.string,
  styleInput: ViewPropTypes.style,
  style: ViewPropTypes.style,
  updateMasterState: PropTypes.func,
  onChangeText: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  attrName: PropTypes.string.isRequired,
};

InputAuth.defaultProps = {
  styleInput: ViewPropTypes.style,
  style: ViewPropTypes.style,
  value: '',
  keyboardType: 'default',
  placeholder: '',
  isPassword: false,
  updateMasterState: () => {},
  onChangeText: () => {},
  onFocus: () => {},
  onBlur: () => {},
};

export default InputAuth;
