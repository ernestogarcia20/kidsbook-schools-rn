import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Keyboard,
  Platform,
  Dimensions,
} from 'react-native';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { getEmojis } from '@utils/functions/globals';
import { Popover, PopoverController } from 'react-native-modal-popover';
import Text from '@components/Text';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentChildren: { paddingRight: '12%', flex: 1 },
});

const Layout = ({ onEmoji = () => {}, children, position }) => {
  const handleEmoji = (emoji) => {
    onEmoji(emoji);
  };
  const renderItemEmoji = ({ item }) => {
    return (
      <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={() => handleEmoji(item)}>
        <Text fontSize={22}>{item}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <PopoverController>
      {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
        <>
          <TouchableOpacity
            style={{
              position: 'absolute',
              top: 15,
              right: 15,
              zIndex: 1,
            }}
            onPress={() => {
              Keyboard.dismiss();
              setTimeout(() => {
                openPopover();
              }, 80);
            }}
            ref={setPopoverAnchor}
          >
            <Icon name="laugh-beam" size={24} color={Colors.GREEN} />
          </TouchableOpacity>
          <View style={styles.contentChildren}>{children}</View>
          <Popover
            visible={popoverVisible}
            fromRect={popoverAnchorRect}
            onClose={closePopover}
            placement="top"
            backgroundStyle={{ backgroundColor: 'transparent' }}
            contentStyle={{ maxHeight: width * 0.35, maxWidth: width / 1.5 }}
          >
            <FlatList
              data={getEmojis()}
              showsVerticalScrollIndicator={false}
              renderItem={renderItemEmoji}
              numColumns={6}
              initialNumToRender={5}
              disableVirtualization
              keyExtractor={(ite, index) => ite + index}
            />
          </Popover>
        </>
      )}
    </PopoverController>
  );
};

export default Layout;
