import React, { useState, useEffect } from 'react';

import FastImage, {FastImageProps, OnLoadEvent} from 'react-native-fast-image';
import NoAvatar from '@images/no-avatar.jpg';

const FastImageComponent = (props: FastImageProps) => {
const [onLoadError, setOnLoadError] = useState<boolean>(false);
const [source, setSource] = useState<number>(false);

useEffect(() => {
    if(source !== props.source){
        setOnLoadError(false);
        setSource(props.source)
    }
}, [props.source]);

  const onError = () => {
    if(props?.onError){
      props?.onError();
    }
    setOnLoadError(true);
  };

  const onLoad = (event: OnLoadEvent) => {
    if(props?.onLoad){
     props?.onLoad(event);
    }
  };

  return <FastImage {...props} source={onLoadError ? NoAvatar : source} onError={onError} onLoad={onLoad} />;
};

export default FastImageComponent;
