import React, { Component } from 'react';
import { LANGUAGE } from '@utils/constants/navigates';
import { connect } from 'react-redux';
import showModalConfirmAction from '@store/actions/modal-confirm.action';
import ModalConfirmProps from '@store/models/ModalConfirmModel';

export default class BaseReact extends Component {
  goBack = () => {
    const {
      navigation: { goBack },
    } = this.props;
    goBack();
  };

  showModalConfirm = (confirmProps: ModalConfirmProps, dispatch) => {
    if(!confirmProps.subTitle){
      confirmProps.subTitle = null;
    }
    if(!confirmProps.data){
      confirmProps.data = [];
    }
    if(!confirmProps.onPressAccept){
      confirmProps.onPressAccept = () => {};
    }
    if(!confirmProps.onPressCancel){
      confirmProps.onPressCancel = () => {};
    }
    dispatch(showModalConfirmAction(confirmProps));
  };

  openDrawer = () => {
    const {
      navigation: { openDrawer },
    } = this.props;

    openDrawer();
  };

  handleCloseDrawer = () => {
    const {
      navigation: { closeDrawer },
    } = this.props;
    closeDrawer();
  };

  handleGoToTranslate = () => {
    this.navigate(LANGUAGE);
  };

  navigate = (name, params) => {
    const {
      navigation: { navigate },
    } = this.props;
    navigate(name, params);
  };
}
