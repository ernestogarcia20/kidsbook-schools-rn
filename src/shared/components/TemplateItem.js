import React from 'react';
import { View, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';
import Colors from '@assets/colors';
import Header from '@components/Header';
import { getImageById } from '@utils/functions/globals';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/Feather';
import I18n from 'react-native-redux-i18n';
import ChildsIcon from '@images/childs.png';
import FastImage from '@components/FastImage';
import Search from '@components/Search';
import CheckBox from '@components/CheckBox';
import moment from 'moment';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  paddingIcon: { paddingRight: 7 },
  center: { justifyContent: 'center', alignItems: 'center' },
  dateText: { position: 'absolute', paddingTop: 3 },
  contentIcons: {
    flexDirection: 'row',
    height: 25,
    marginTop: 5,
    alignItems: 'center',
  },
  buttonIconMargin: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.WHITE,
    marginLeft: 10,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  buttonIcon: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.BLUELIGHT,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  contentActionsEdit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonTrash: {
    width: 30,
    height: 30,
    borderRadius: 5,
    backgroundColor: Colors.RED,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '2%',
  },
  contentSelectedAll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '3.5%',
    marginTop: '7%',
    alignItems: 'center',
  },
  contentEdit: { paddingHorizontal: '5%', paddingTop: '5%' },

  renderItem: {
    height: 90,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: '5%',
  },
});

const TemplateItem = ({
  item,
  index,
  user,
  childTemplate = true,
  editList,
  handleSelectedItem,
  handleDelete,
  absence = false,
  disable = false,
  onPressSelected = null,
  isFastAnswer = false,
  handleDetail = () => {},
  handleShowChilds = false,
  isCheckBox = true,
}) => {
  const { name, avatar, use_diaper, use_bottle, birthday, familyIds, checked = false } = item;
  const urlImage = getImageById(avatar, user?.authData?.apiToken);
  return (
    <View
      style={[
        styles.renderItem,
        {
          backgroundColor: index % 2 ? '#EBEBEB' : '',
        },
      ]}
    >
      <View style={styles.contentInfo}>
        {!isFastAnswer ? (
          <FastImage
            source={{ uri: urlImage }}
            style={{ width: 50, height: 50, borderRadius: 100, backgroundColor: '#DADADA' }}
          />
        ) : null}
        <View style={{ marginLeft: 15 }}>
          <Text
            align="left"
            fontSize={12}
            weight="Medium"
            color={disable && !checked ? 'rgba(0,0,0,0.2)' : Colors.BLACK}
          >
            {name}
          </Text>
          {childTemplate && (
            <View style={styles.contentIcons}>
              <>
                <View style={styles.paddingIcon}>
                  <CustomIcon
                    name={use_diaper ? 'kb_diaper' : 'kb_diaper_no'}
                    width={20}
                    heigth={20}
                  />
                </View>
                <View style={styles.paddingIcon}>
                  <CustomIcon
                    name={use_bottle ? 'kb_bottle' : 'kb_bottle_no'}
                    width={20}
                    heigth={20}
                  />
                </View>
                <View style={styles.paddingIcon}>
                  <View style={styles.center}>
                    <CustomIcon name="kb_calendar_blank" width={20} heigth={20} />

                    <Text style={styles.dateText} fontSize={10}>
                      {moment(birthday, 'YYYY-MM-DD').format('DD') || ''}
                    </Text>
                  </View>
                </View>
              </>
            </View>
          )}
        </View>
      </View>
      {editList && (
        <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: '8%' }}>
          {isFastAnswer && (
            <TouchableOpacity
              onPress={() => handleDetail(item)}
              style={[
                styles.center,
                styles.buttonIconMargin,
                { backgroundColor: Colors.BLUELIGHT, marginRight: 20 },
              ]}
            >
              <Icon name="settings" size={19} color={Colors.WHITE} />
            </TouchableOpacity>
          )}
          {isCheckBox ? (
            <CheckBox disable={disable} check={checked} onPress={() => handleSelectedItem(item)} />
          ) : (
            <TouchableOpacity onPress={() => handleSelectedItem(item)} activeOpacity={0.7}>
              <Icon name="chevron-right" size={26} />
            </TouchableOpacity>
          )}
        </View>
      )}
      {isFastAnswer && !editList && (
        <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: '5%' }}>
          {onPressSelected && !editList && (
            <TouchableOpacity
              onPress={() => onPressSelected(item)}
              style={[styles.center, styles.buttonIconMargin]}
            >
              <Icon name="chevron-right" size={28} color={Colors.BLACK} />
            </TouchableOpacity>
          )}
        </View>
      )}
      {!editList && !absence && !handleShowChilds && (
        <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: '5%' }}>
          <TouchableOpacity
            onPress={() => handleDelete(item)}
            style={[styles.center, styles.buttonIconMargin, { backgroundColor: Colors.RED }]}
          >
            <Icon name="trash" size={15} color={Colors.WHITE} />
          </TouchableOpacity>
        </View>
      )}
      {handleShowChilds && !editList && (
        <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: '5%' }}>
          <TouchableOpacity
            onPress={() => handleShowChilds(item)}
            style={[
              styles.center,
              styles.buttonIconMargin,
              { backgroundColor: Colors.WHITE, marginRight: 25 },
            ]}
          >
            <FastImage source={ChildsIcon} style={{ width: 20, height: 20 }} resizeMode="contain" />
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};

const mapStateToProps = ({ user }) => ({
  user,
});

export default connect(mapStateToProps)(TemplateItem);
