import React, { Fragment } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
  View,
  ViewPropTypes,
  TouchableWithoutFeedback,
  Keyboard,
  Modal,
  ActivityIndicator,
  Platform,
} from 'react-native';
import Colors from '@assets/colors';
import PropTypes from 'prop-types';
import Text from '@components/Text';

import I18n from 'react-native-redux-i18n';

import Background from '@images/bg1.png';
import LoadingGif from '@images/eating.gif';
import FastImage from 'react-native-fast-image';

const styles = StyleSheet.create({
  background: { top: 0, left: 0, right: 0, bottom: 0, position: 'absolute' },
  container: { flex: 1, backgroundColor: Colors.WHITE, paddingHorizontal: 10 },
  contentKeyboard: { flex: 0.9 },
  loader: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomSafeArea: { flex: 0, backgroundColor: '#779915' },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  modalView: {
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  activityIndicator: {
    marginRight: 15,
  },
});

export const loader = (propLoading) => {
  const { isLoading, text } = propLoading;
  return (
    <Modal animationType="fade" transparent visible={isLoading}>
      <View style={styles.centeredView}>
        <FastImage source={LoadingGif} style={{ width: 110, height: 110 }} />
        <View style={styles.modalView}>
          <ActivityIndicator size="small" color={Colors.WHITE} style={styles.activityIndicator} />
          <Text style={styles.modalText} color={Colors.WHITE} fontSize={18}>
            {I18n.t(`loading.${text}`)}
          </Text>
        </View>
      </View>
    </Modal>
  );
};

const Container = ({ children, style, propLoading, padding, showBackground }) => {
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <>
        <SafeAreaView style={styles.bottomSafeArea} />

        <SafeAreaView
          // onStartShouldSetResponder={Keyboard.dismiss}
          style={[styles.container, { ...style, paddingHorizontal: padding ? 10 : 0 }]}
        >
          {loader(propLoading)}
          <StatusBar
            backgroundColor={Platform.OS === 'ios' ? Colors.PRIMARYDARKCOLOR : '#779915'}
            barStyle="light-content"
          />
          {showBackground ? (
            <FastImage style={{ flex: 1 }} source={Background}>
              {children}
            </FastImage>
          ) : (
            <View style={{ flex: 1 }}>{children}</View>
          )}
        </SafeAreaView>
      </>
    </TouchableWithoutFeedback>
  );
};

Container.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  style: ViewPropTypes.style,
  propLoading: PropTypes.object,
  padding: PropTypes.bool,
  showBackground: PropTypes.bool,
};

Container.defaultProps = {
  style: ViewPropTypes.style,
  propLoading: {
    isLoading: false,
    text: 'loading',
  },
  padding: false,
  showBackground: false,
};

export default Container;
