import React, { useState, useEffect } from 'react';
import { KeyboardAvoidingView, StyleSheet, Platform, NativeModules } from 'react-native';

const { StatusBarManager } = NativeModules;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const Layout = ({ children, behavior = "padding" }) => {
  const [statusBarHeight, setStatusBarHeight] = useState(0);
  useEffect(() => {
    if (Platform.OS === 'ios') {
      StatusBarManager.getHeight((statusBarFrameData) => {
        setStatusBarHeight(statusBarFrameData.height + 10);
      });
    }
  }, []);
  return (
    <KeyboardAvoidingView
      style={styles.container}
      enabled
      behavior={Platform.OS === 'ios' ? behavior : undefined}
      keyboardVerticalOffset={behavior === "position" ? 0 : statusBarHeight + 44}
    >
      {children}
    </KeyboardAvoidingView>
  );
};

export default Layout;
