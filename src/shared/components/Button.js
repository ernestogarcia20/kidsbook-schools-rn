import React from 'react';
import { StyleSheet, TouchableOpacity, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import Text from '@components/Text';
import GrassImage from '@images/grass.png';
import FastImage from '@components/FastImage';
import Colors from '@assets/colors';
import { pickTextColorBasedOnBgColorSimple } from '@utils/functions/globals';

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.GREEN,
    height: 40,
    borderRadius: 10,
  },
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Button = ({
  title,
  color,
  textColor = null,
  style,
  showGrass = true,
  onPress,
  activeOpacity = 0.5,
}) => {
  return (
    <TouchableOpacity
      style={[styles.button, { backgroundColor: color }, style]}
      onPress={onPress}
      activeOpacity={activeOpacity}
    >
      <FastImage source={showGrass ? GrassImage : null} style={styles.background}>
        <Text color={textColor || pickTextColorBasedOnBgColorSimple(color)}>{title}</Text>
      </FastImage>
    </TouchableOpacity>
  );
};

Button.propTypes = {
  title: PropTypes.string,
  style: ViewPropTypes.style,
  color: PropTypes.string,
  onPress: PropTypes.func,
};

Button.defaultProps = {
  title: '',
  style: ViewPropTypes.style,
  color: Colors.GREEN,
  onPress: () => {},
};

export default Button;
