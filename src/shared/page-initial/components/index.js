import React from 'react';
import { View, StyleSheet } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const Layout = ({ goBack }) => {
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.qualify')} onPress={goBack} />
    </View>
  );
};

export default Layout;
