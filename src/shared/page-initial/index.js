import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import MessagesLayout from './components/messages-component';

class MessagesController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <MessagesLayout goBack={this.goBack} setState={this.handleChange} state={this.state} />
      </Container>
    );
  }
}

const mapStateToProps = ({user}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps)(MessagesController);
