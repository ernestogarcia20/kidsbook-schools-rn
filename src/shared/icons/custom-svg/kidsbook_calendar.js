import React from 'react';
import { Svg, Path, G, Text } from 'react-native-svg';

const KidsbookCalendar = ({ key = '', width = 500, height = 500 }) => {
  return (
    <Svg
      key={key}
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={width}
      height={height}
      viewBox="0 0 500 500"
      enable-background="new 0 0 500 500"
    >
      <G>
        <Path
          fill="#E4E6E6"
          d="M31.252,0H468.75C486,0,500,13.984,500,31.25v437.498c0,17.25-14,31.252-31.25,31.252H31.252
		C14.002,499.998,0,485.998,0,468.748V31.25C0,13.984,14.002,0,31.252,0z"
        />
        <Path
          fill="#C8574E"
          d="M468.75,0H31.252C14.002,0,0,13.984,0,31.25v78.125h499.998V31.25C499.998,13.984,486,0,468.75,0z"
        />
        <Path fill="#B43B2E" d="M0,93.748h499.998V125H0V93.748z" />
        <Path
          fill="#B43B2E"
          d="M250,31.25c8.641,0,15.625,6.998,15.625,15.625S258.641,62.5,250,62.5s-15.625-6.998-15.625-15.625
		S241.359,31.25,250,31.25z"
        />
        <Path
          fill="#C2C5C5"
          d="M468.75,468.748H31.252c-17.25,0-31.252-14-31.252-31.25v31.25C0,485.998,14.002,500,31.252,500H468.75
		C486,500,500,485.998,500,468.748v-31.25C499.998,454.748,486,468.748,468.75,468.748z"
        />
      </G>
      <Text transform="matrix(1 0 0 1 91.8252 397.7002)" fill="#505354" fontSize="298.2885">
        31
      </Text>
    </Svg>
  );
};

export default KidsbookCalendar;
