import React from 'react';
import { Svg, Path, G, Defs, ClipPath, Rect } from 'react-native-svg';

const kidsbookActividades = ({ key = '', width = 512, height = 512 }) => {
  return (
    <Svg
      key={key}
      width={width}
      height={height}
      viewBox="0 0 512 512"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <G clip-Path="url(#clip0)">
        <Path
          d="M39.7251 476.689V441.379V150.069C39.7251 125.696 59.4901 105.931 83.8631 105.931H110.346H154.484H366.346V370.759V406.069V441.379"
          fill="#289F87"
        />
        <Path
          d="M366.345 423.724H172.138C147.765 423.724 128 403.959 128 379.586V105.931H366.345V423.724Z"
          fill="#208673"
        />
        <Path
          d="M39.7251 476.69H101.518V441.38V105.931H83.8631C59.4901 105.931 39.7251 125.696 39.7251 150.069V476.69Z"
          fill="#208673"
        />
        <Path
          d="M366.346 512.001H75.0351C55.5351 512.001 39.7251 496.191 39.7251 476.691C39.7251 457.191 55.5351 441.381 75.0351 441.381H366.345V512.001H366.346Z"
          fill="#B9B9B9"
        />
        <Path
          d="M154.481 370.759V335.449V44.138C154.481 19.765 174.246 0 198.619 0H225.102H472.274V335.448"
          fill="#EA9D2D"
        />
        <Path d="M436.967 35.3093H251.587V141.239H436.967V35.3093Z" fill="#B47923" />
        <Path d="M410.478 176.55H278.068V194.205H410.478V176.55Z" fill="#B47923" />
        <Path d="M410.478 211.859H278.068V229.514H410.478V211.859Z" fill="#B47923" />
        <Path d="M410.49 291.309H366.35V308.964H410.49V291.309Z" fill="#B47923" />
        <Path
          d="M216.274 0V335.448V370.758H154.481V105.931V44.138C154.481 19.765 174.246 0 198.619 0H216.274Z"
          fill="#D68F29"
        />
        <Path
          d="M366.343 406.07H189.791C170.291 406.07 154.481 390.26 154.481 370.76C154.481 351.26 170.291 335.45 189.791 335.45H472.274V406.071H436.964"
          fill="#DBDBDB"
        />
        <Path
          d="M189.791 406.069H366.343H436.964H472.274V370.759H154.481C154.481 390.259 170.291 406.069 189.791 406.069Z"
          fill="#B9B9B9"
        />
        <Path
          d="M366.344 370.759H436.965V476.69L401.654 441.379L366.344 476.69V370.759Z"
          fill="#DC4D41"
        />
        <Path d="M436.97 370.76H366.35V397.243H436.97V370.76Z" fill="#C7463B" />
      </G>
      <Defs>
        <ClipPath id="clip0">
          <Rect width="512" height="512" fill="white" />
        </ClipPath>
      </Defs>
    </Svg>
  );
};

export default kidsbookActividades;
