import React from 'react';
import { Svg, Path, Polygon, G } from 'react-native-svg';

const KidsbookMRReaded = ({ key = '', width = 512, height = 512 }) => {
  return (
    <Svg
      key={key}
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={width}
      height={height}
      viewBox="0 0 512 512"
      enableBackground="new 0 0 512 512"
      xmlSpace="preserve"
    >
      <G id="XMLID_30_">
        <G>
          <Path
            fill="#222222"
            d="M478.978,211.9v287.97H45.018V211.9L255.558,37.13L478.978,211.9z M467.078,490.65V219.4l-211.17-164.63
			L56.918,219.4v271.25H467.078z"
          />
          <Polygon
            fill="#EBC680"
            points="304.318,386.12 299.897,383.28 467.078,219.4 467.078,490.65"
          />
          <Polygon
            fill="#BD9B62"
            points="467.078,219.4 299.897,383.28 255.908,355.03 214.448,383.28 56.918,219.4"
          />
          <Polygon fill="#E5E5E3" points="467.078,219.4 56.918,219.4 255.908,54.771" />
          <Polygon
            fill="#DA9345"
            points="467.078,490.65 56.918,490.65 210.298,386.12 304.318,386.12"
          />
          <Polygon
            fill="#BD9B62"
            points="304.318,386.12 210.298,386.12 214.448,383.28 255.908,355.03 299.897,383.28"
          />
          <Polygon
            fill="#EBC680"
            points="214.448,383.28 210.298,386.12 56.918,490.65 56.918,219.4"
          />
        </G>
        <G>
          <Polygon
            fill="none"
            stroke="#000000"
            strokeWidth="0.5"
            strokeLinecap="round"
            strokeLinejoin="round"
            points="45.018,211.9 
			255.558,37.13 478.978,211.9 478.978,499.87 45.018,499.87"
          />
        </G>
      </G>
      <Path
        fill="#439941"
        stroke="#000000"
        strokeWidth="11"
        strokeMiterlimit="10"
        d="M359.452,63.969l-110.025,99.002l2.242-42.502
	c0.348-6.607-4.726-12.246-11.332-12.594l-28.709-1.514c-6.605-0.348-12.244,4.725-12.593,11.331l-4.833,91.651l-1.514,28.709l0,0
	c-0.35,6.607,4.724,12.244,11.33,12.592l120.36,6.349c6.606,0.349,12.245-4.724,12.593-11.331l1.514-28.709
	c0.348-6.605-4.725-12.244-11.331-12.593l-42.442-2.239l109.996-98.973c7.667-6.898,10.296-16.479,5.871-21.397l-19.23-21.37
	C376.926,55.463,367.12,57.069,359.452,63.969z"
      />
    </Svg>
  );
};

export default KidsbookMRReaded;
