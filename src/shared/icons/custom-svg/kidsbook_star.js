import React from 'react';
import { Svg, Polygon } from 'react-native-svg';

const KidsbookStar = ({ key = '', width = 512, height = 512 }) => {
  return (
    <Svg
      key={key}
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={width}
      height={height}
      viewBox="0 0 512 512"
      enable-background="new 0 0 512 512"
    >
      <Polygon
        fill="#D6891D"
        points="495.997,202.113 375.997,319.083 404.327,484.254 255.997,406.273 107.667,484.254 
			112.487,456.174 436.387,193.453 		"
      />
      <Polygon
        fill="#E0B020"
        points="436.387,193.453 112.487,456.174 135.997,319.083 106.687,290.514 299.717,116.323 
			330.167,178.023 		"
      />
      <Polygon
        fill="#EDCB72"
        points="255.997,27.743 299.717,116.323 106.687,290.514 15.997,202.113 181.837,178.023 		"
      />

      <Polygon
        fill="none"
        stroke="#CD8424"
        strokeWidth="11"
        strokeMiterlimit="10"
        points="106.687,290.514 15.997,202.113 
			181.837,178.023 255.997,27.743 299.717,116.323 330.167,178.023 436.387,193.453 495.997,202.113 375.997,319.083 
			404.327,484.254 255.997,406.273 107.667,484.254 112.487,456.174 135.997,319.083 		"
      />
    </Svg>
  );
};

export default KidsbookStar;
