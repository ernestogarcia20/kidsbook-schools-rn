import React from 'react';
import { Svg, Path, G, Ellipse, Line, Text, TSpan } from 'react-native-svg';

const kidsbookAbc = ({ key = '', width = 512, height = 512 }) => {
  return (
    <Svg
      key={key}
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 512 512"
      enable-background="new 0 0 512 512"
    >
      <G>
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          fill="#A7761D"
          d="M494.013,450.271c0,23.35-17.768,42.277-39.684,42.277H57.699
		c-21.917,0-39.685-18.928-39.685-42.277V186.82c0-23.348,17.768-42.276,39.685-42.276h396.63c21.916,0,39.684,18.929,39.684,42.276
		V450.271z"
        />
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          fill="#DB9600"
          d="M494.013,443.829c0,22.857-17.32,41.387-38.684,41.387H68.698
		c-21.365,0-38.685-18.529-38.685-41.387V185.926c0-22.856,17.32-41.387,38.685-41.387h386.631c21.363,0,38.684,18.53,38.684,41.387
		V443.829z"
        />
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          fill="#336535"
          d="M464.681,431.453c0,20.015-15.603,36.238-34.848,36.238H81.53
		c-19.247,0-34.85-16.224-34.85-36.238V205.639c0-20.014,15.603-36.237,34.85-36.237h348.303c19.245,0,34.848,16.224,34.848,36.237
		V431.453z"
        />
        <Text transform="matrix(1 0 0 1 66.8164 363.4785)">
          <TSpan
            x="0"
            y="0"
            fill="#BF3376"
            stroke="#FFFFFF"
            strokeWidth="6"
            strokeMiterlimit="10"
            fontSize="152.6578"
          >
            A
          </TSpan>
          <TSpan
            x="140.135"
            y="0"
            fill="#76AADB"
            stroke="#FFFFFF"
            strokeWidth="6"
            strokeMiterlimit="10"
            fontSize="152.6578"
          >
            B
          </TSpan>
          <TSpan
            x="264.244"
            y="0"
            fill="#E6B54A"
            stroke="#FFFFFF"
            strokeWidth="6"
            strokeMiterlimit="10"
            fontSize="152.6578"
          >
            C
          </TSpan>
        </Text>
      </G>
      <Line
        fill="none"
        stroke="#DB9600"
        strokeWidth="6"
        strokeMiterlimit="10"
        x1="103.602"
        y1="150.567"
        x2="225.096"
        y2="69.627"
      />
      <Line
        fill="none"
        stroke="#DB9600"
        strokeWidth="6"
        strokeMiterlimit="10"
        x1="424.586"
        y1="150.567"
        x2="303.092"
        y2="69.627"
      />
      <Ellipse
        fill="none"
        stroke="#828282"
        strokeWidth="20"
        strokeMiterlimit="10"
        cx="264.094"
        cy="70.377"
        rx="38.998"
        ry="45.748"
      />
    </Svg>
  );
};

export default kidsbookAbc;
