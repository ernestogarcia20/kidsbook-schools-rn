import React from 'react';
import { Svg, G, Polygon, Path } from 'react-native-svg';

const KidsbookMessage = ({ key = '', width = 512, height = 512 }) => {
  return (
    <Svg
      key={key}
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={width}
      height={height}
      viewBox="0 0 512 512"
      enable-background="new 0 0 512 512"
    >
      <G id="XMLID_12_">
        <G>
          <Polygon fill="#EBC680" points="305.111,15.082 496.918,206.889 314.574,244.057 		" />
          <Polygon fill="#BD9B62" points="305.111,15.082 314.574,244.057 294.828,248.1 		" />
          <Polygon
            fill="#E5E5E3"
            points="305.111,15.082 294.828,248.1 256,256 249.135,289.789 15.082,305.111 		"
          />
          <Polygon
            fill="#DA9345"
            points="496.918,206.889 206.889,496.918 245.053,309.652 317.338,311.078 314.574,244.057 		"
          />
          <Polygon
            fill="#B97B35"
            points="314.574,244.057 317.338,311.078 245.053,309.652 249.135,289.789 293.139,286.879 294.828,248.1 
					"
          />
          <Polygon fill="#E5E5E3" points="256,256 294.828,248.1 293.139,286.879 249.135,289.789 		" />
          <Polygon fill="#BD9B62" points="15.082,305.111 249.135,289.789 245.053,309.652 		" />
          <Polygon fill="#EBC680" points="15.082,305.111 245.053,309.652 206.889,496.918 		" />
        </G>
        <G>
          <Path
            fill="#222222"
            d="M206.889,506L6,305.111L305.111,6L506,206.889L206.889,506z M24.164,305.111l182.725,182.725
			l280.947-280.947L305.111,24.164L24.164,305.111z"
          />
        </G>
      </G>
    </Svg>
  );
};

export default KidsbookMessage;
