import React from 'react';
import { Svg, Path, Polygon, G } from 'react-native-svg';

const KidsbookMSReaded = ({ key = '', width = 512, height = 512 }) => {
  return (
    <Svg
      key={key}
      version="1.1"
      id="Capa_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={width}
      height={height}
      viewBox="0 0 434.46 463.24"
      enableBackground="new 0 0 434.46 463.24"
      xmlSpace="preserve"
    >
      <G id="XMLID_11_">
        <G>
          <Polygon
            fill="#BD9B62"
            points="211.141,318.15 255.13,346.4 259.551,349.24 165.53,349.24 169.681,346.4 		"
          />
          <Polygon fill="#E5E5E3" points="422.311,182.521 12.15,182.521 211.141,17.891 		" />
          <Polygon
            fill="#BD9B62"
            points="12.15,182.521 422.311,182.521 255.13,346.4 211.141,318.15 169.681,346.4 		"
          />
          <Polygon
            fill="#DA9345"
            points="165.53,349.24 259.551,349.24 422.311,453.771 12.15,453.771 		"
          />
          <Path
            fill="#222222"
            d="M0.25,175.021L210.79,0.25l223.42,174.771v287.97H0.25V175.021z M12.15,182.521v271.25h410.16v-271.25
			l-211.17-164.63L12.15,182.521z"
          />
          <Polygon
            fill="#EBC680"
            points="255.13,346.4 422.311,182.521 422.311,453.771 259.551,349.24 		"
          />
          <Polygon
            fill="#EBC680"
            points="12.15,453.771 12.15,182.521 169.681,346.4 165.53,349.24 		"
          />
        </G>
        <G>
          <Polygon
            fill="none"
            stroke="#000000"
            strokeWidth="0.5"
            strokeLinecap="round"
            strokeLinejoin="round"
            points="0.25,175.021 
			210.79,0.25 434.21,175.021 434.21,462.99 0.25,462.99 		"
          />
        </G>
      </G>
      <Path
        fill="#439941"
        stroke="#000000"
        strokeWidth="11"
        strokeMiterlimit="10"
        d="M220.9,216.185l110.025-99.002l-2.241,42.502
	c-0.348,6.607,4.726,12.246,11.332,12.594l28.708,1.514c6.605,0.348,12.244-4.725,12.593-11.33l4.834-91.652l1.513-28.708l0,0
	c0.35-6.608-4.724-12.245-11.33-12.593l-120.36-6.348c-6.605-0.35-12.244,4.723-12.592,11.33L241.868,63.2
	c-0.349,6.605,4.725,12.244,11.33,12.594l42.443,2.238l-109.996,98.973c-7.667,6.898-10.297,16.479-5.873,21.398l19.231,21.369
	C203.428,224.69,213.232,223.085,220.9,216.185z"
      />
    </Svg>
  );
};

export default KidsbookMSReaded;
