import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { HTTP_SERVER_UNAVAILABLE, BEARER_TOKEN_NAME } from '@utils/constants/api';
import ENV from '../../env';
import validationService from './validation-services';

export const BASE_API = ENV.path;
export const VERSION = 'v1/';
export const BEARER_TOKEN = '';

class Api {
  async get(uri, data = null) {
    const url = `${BASE_API}${uri}`;
    try {
      const headers = await this.getHeaders();
      console.log('-------------Get Url----------', JSON.stringify(url));
      const result = await axios.get(url, { headers, params: data, timeout: 50000 });
      console.log('-------------Response----------');
      return result.data;
    } catch (e) {
      console.log(e);
      if (e.response?.status === 401) {
        try {
          // await this.postToken(null, true);
          return this.get(uri);
        } catch (ex) {
          return Promise.reject(ex);
        }
      }
      return validationService(e.response?.status || null, e);
    }
  }

  async delete(uri) {
    const url = `${BASE_API}${uri}`;
    try {
      const headers = await this.getHeaders();
      const result = await axios.delete(url, { headers, timeout: 50000 });
      return result.data;
    } catch (e) {
      if (e.response?.status === 401) {
        try {
          // await this.postToken(null, true);
          return this.get(uri);
        } catch (ex) {
          return Promise.reject(ex);
        }
      }
      return validationService(e.response?.status || null, e);
    }
  }

  async put(uri, body, version = VERSION, returnRequest = false) {
    const headers = await this.getHeaders();
    try {
      const request = await axios.put(`${BASE_API}${VERSION}${uri}`, body, {
        headers,
        timeout: 50000
      });
      if (returnRequest) {
        return request;
      }
      try {
        if (request.status === 500) {
          return Promise.reject({ code: HTTP_SERVER_UNAVAILABLE });
        }
        const response = await request.data;
        /* const path =
          response.path === undefined ? response.errorKey : response.path;
        const errorKey = validation(request.status, path);
        if (errorKey !== '') {
          if (response.id_token !== undefined || response.id_token !== null) {
            return Promise.reject({code: errorKey, token: response.id_token});
          }
          return Promise.reject({code: errorKey});
        } */
        return response;
      } catch (e) {
        return request;
      }
    } catch (e) {
      if (e.response?.status === 401) {
        try {
          // await this.postToken(null, true);
          return this.get(uri);
        } catch (ex) {
          return Promise.reject(ex);
        }
      }
      return validationService(e.response?.status || null, e);
    }
  }

  async patch(uri, body, version = VERSION, returnRequest = false) {
    const headers = await this.getHeaders();
    try {
      const request = await axios.patch(`${BASE_API}${uri}`, body, {
        headers,
        timeout: 50000
      });
      if (returnRequest) {
        return request;
      }
      try {
        if (request.status === 500) {
          return Promise.reject({ code: HTTP_SERVER_UNAVAILABLE });
        }
        const response = await request.data;
        /* const path =
          response.path === undefined ? response.errorKey : response.path;
        const errorKey = validation(request.status, path);
        if (errorKey !== '') {
          if (response.id_token !== undefined || response.id_token !== null) {
            return Promise.reject({code: errorKey, token: response.id_token});
          }
          return Promise.reject({code: errorKey});
        } */
        return response;
      } catch (e) {
        return request;
      }
    } catch (e) {
      if (e.response?.status === 401) {
        try {
          // await this.postToken(null, true);
          return this.get(uri);
        } catch (ex) {
          return Promise.reject(ex);
        }
      }
      return validationService(e.response?.status || null, e);
    }
  }

  async post(uri, body, version = VERSION, returnRequest = false) {
    const headers = await this.getHeaders();
    const url = `${BASE_API}${uri}`;
    try {
      console.log('-------------Post Url----------', url);
      const request = await axios.post(`${BASE_API}${uri}`, body, {
        headers,
        timeout: 50000,
      });
      if (returnRequest) {
        return request;
      }
      console.log(headers);
      try {
        if (request.status === 500) {
          return Promise.reject({ code: HTTP_SERVER_UNAVAILABLE });
        }
        const response = await request.data;
        /* const path =
          response.path === undefined ? response.errorKey : response.path;
        const errorKey = validation(request.status, path);
        if (errorKey !== '') {
          if (response.id_token !== undefined || response.id_token !== null) {
            return Promise.reject({code: errorKey, token: response.id_token});
          }
          return Promise.reject({code: errorKey});
        } */
        return response;
      } catch (e) {
        return request;
      }
    } catch (e) {
      console.log('-------------Post ERROR----------', JSON.stringify(e));
      if (e.response?.status === 401) {
        try {
          // await this.postToken(null, true);
          return this.post(uri, body);
        } catch (ex) {
          return Promise.reject(ex);
        }
      }
      return validationService(e.response?.status || null, e);
    }
  }

  isJson = (str) => {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  };

  setAuthorization = (authorization) => {
    try {
      AsyncStorage.setItem(BEARER_TOKEN_NAME, authorization);
    } catch (e) {}
  };

  getAuthorization = () => {
    try {
      return AsyncStorage.getItem(BEARER_TOKEN_NAME);
    } catch (e) {}

    return null;
  };

  async getHeaders() {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'API-Token': '',
    };

    const authorization = await this.getAuthorization();
    if (authorization !== null) {
      headers['API-Token'] = `${authorization}`;
    }

    return headers;
  }
}

export default new Api();
