export default (code, ex) => {
  if (ex.message && String(ex.message).toLowerCase().includes('network')) {
    return Promise.reject({ code: 'error/internet', log: ex });
  }
  if (code === 400) {
    if (
      ex.response.data &&
      String(ex.response.data.data.message).toLowerCase().includes('client already added')
    ) {
      return Promise.reject({ code: 'account/alreadyInvited', log: ex });
    }
    return Promise.reject({ code: 'auth/wrong', log: ex });
  }
  if (code === 422) {
    return Promise.reject({ code: 'auth/email', log: ex });
  }
  return Promise.reject({ code: 'error/server', log: ex });
};
