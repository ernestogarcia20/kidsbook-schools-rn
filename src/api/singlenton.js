export default class CommonDataManager {
  static myInstance = null;

  dispatch = null;
  context = null;
  cartList = [];
  countCart = 0;

  /**
   * @returns {CommonDataManager}
   */
  static getInstance() {
    if (CommonDataManager.myInstance == null) {
      CommonDataManager.myInstance = new CommonDataManager();
    }

    return this.myInstance;
  }

  getContext() {
    return this.context;
  }

  setContext(res) {
    this.context = res;
  }

  getDispatch() {
    return this.dispatch;
  }

  setDispatch(res) {
    this.dispatch = res;
  }

  getCartList() {
    return this.cartList;
  }

  setCartList(res) {
    this.cartList = res;
  }

  getCountCart() {
    return this.countCart;
  }

  setCountCart(res) {
    this.countCart = res;
  }
}
