import I18n from 'react-native-redux-i18n';
// import * as RNLocalize from 'react-native-localize';

import { LocaleConfig } from 'react-native-calendars';
import en from './locale/en.json';
import es from './locale/es.json';

// const locales = RNLocalize.getLocales();

/* if (Array.isArray(locales)) {
  I18n.locale = 'es';
  // I18n.locale = locales[0].languageTag;
} */
LocaleConfig.locales.es = es;
LocaleConfig.locales.en = en;
I18n.fallbacks = true;
I18n.translations = {
  en,
  es,
  _version: '1.0',
};

export default I18n;
