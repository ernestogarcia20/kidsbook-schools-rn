import { applyMiddleware, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import { createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';
import i18n from '@translation';

import thunk from 'redux-thunk';
import { AsyncStorage } from 'react-native';
import reducer from './reducers';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['navigation'],
};

const middleware = createReactNavigationReduxMiddleware((state) => state.navigation, 'root');

const store = createStore(
  persistReducer(persistConfig, reducer),
  applyMiddleware(middleware, thunk)
);

const persistor = persistStore(store);

export { store, persistor, i18n };
