import { SET_SELECTED_LANGUAGE, SET_LANGUAGE_LIST } from '@utils/constants/reducers';
import { LocaleConfig } from 'react-native-calendars';

import I18n, { setLocale } from 'react-native-redux-i18n';

export const setStore = (ACCION, data) => {
  return { type: ACCION, payload: data };
};

export const setLanguageAction = (locale) => {
  return async (dispatch) => {
    dispatch(setLocale(locale));
    // LocaleConfig.defaultLocale = 'en';
    const languageList = I18n.t('languageList');
    dispatch(setStore(SET_LANGUAGE_LIST, languageList || []));
    dispatch(setStore(SET_SELECTED_LANGUAGE, languageList.find((x) => x.selected) || null));
  };
};
