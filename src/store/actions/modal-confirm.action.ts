import { SET_MODAL_CONFIRM } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import ModalConfirmProps from '@store/models/ModalConfirmModel';



export default (propsModal: ModalConfirmProps) => {
  return async (dispatch) => {
    dispatch(setStore(SET_MODAL_CONFIRM, propsModal));
    /*
    dispatch(setStore(SET_LANGUAGE_LIST, languageList || []));
    dispatch(setStore(SET_SELECTED_LANGUAGE, languageList.find((x) => x.selected) || null)); */
  };
};
