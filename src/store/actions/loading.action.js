import { SET_LOADING } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';

export default (isLoading = false, text) => {
  return async (dispatch) => {
    dispatch(setStore(SET_LOADING, { isLoading, text }));
    /*
    dispatch(setStore(SET_LANGUAGE_LIST, languageList || []));
    dispatch(setStore(SET_SELECTED_LANGUAGE, languageList.find((x) => x.selected) || null)); */
  };
};
