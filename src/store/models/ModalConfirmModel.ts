type onPressAccept = () => any;
type onPressCancel = () => any;

export default interface ModalConfirmProps {
    visible: boolean;
    title: string;
    description: string;
    subTitle: string,
    multiSelection: boolean;
    data: [],
    onPressAccept: onPressAccept;
    onPressCancel: onPressCancel;
}