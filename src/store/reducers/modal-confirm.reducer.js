import { SET_MODAL_CONFIRM, REMOVE_ALL } from '@utils/constants/reducers';

const INITIAL_STATE = {
  visible: false,
  title: '',
  subTitle: '',
  description: '',
  multiSelection: false,
  data: [],
  onPressAccept: () => {},
  onPressCancel: () => {},
};

function modalConfirmReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_MODAL_CONFIRM:
      return { ...state, ...action.payload };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return { ...state };
  }
}

export default modalConfirmReducer;
