import { combineReducers } from 'redux';
import { reducer as i18n } from 'react-native-redux-i18n';

import children from '@dashboard/reducers/children.reducer';
import calendar from '@dashboard/reducers/calendar.reducer';
import teacher from '@dashboard/reducers/teachers.reducer';
import parents from '@dashboard/reducers/parents.reducer';
import classroom from '@dashboard/reducers/classroom.reducer';
import socialNetwork from '@dashboard/reducers/social-network.reducer';
import reports from '@dashboard/reducers/report.reducer';
import fastAnswers from '@dashboard/reducers/fast-answers.reducer';
import activities from '@dashboard/reducers/activities.reducer';
import user from '../../modules/auth/reducers/user';
import navigation from './navigation';
import app from './app';
import language from './language.reducer';
import loading from './loading.reducer';
import modalConfirm from './modal-confirm.reducer';

export default combineReducers({
  app,
  navigation,
  user,
  i18n,
  language,
  loading,
  children,
  calendar,
  teacher,
  fastAnswers,
  parents,
  classroom,
  socialNetwork,
  reports,
  modalConfirm,
  activities,
});
