import { SET_SELECTED_LANGUAGE, SET_LANGUAGE_LIST } from '@utils/constants/reducers';

const INITIAL_STATE = {
  selectedLanguage: null,
  languageList: [],
};

function languageReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_LANGUAGE_LIST:
      return { ...state, languageList: action.payload };
    case SET_SELECTED_LANGUAGE:
      return { ...state, selectedLanguage: action.payload };
    default:
      return state;
  }
}

export default languageReducer;
