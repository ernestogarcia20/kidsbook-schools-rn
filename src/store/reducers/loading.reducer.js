import { REMOVE_ALL, SET_LOADING } from '@utils/constants/reducers';

const INITIAL_STATE = {
  propLoading: {
    isLoading: false,
    text: 'loading',
  },
};

function languageReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_LOADING:
      const { text } = action.payload;
      return { ...state, propLoading: { ...action.payload, text: text || 'loading' } };
    case REMOVE_ALL:
      return INITIAL_STATE;
    default:
      return { ...state };
  }
}

export default languageReducer;
