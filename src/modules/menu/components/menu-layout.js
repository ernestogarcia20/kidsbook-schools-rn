import React from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import IconSvg from '@components/Icon';
import FastImage from '@components/FastImage';
import Colors from '@assets/colors';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FBFD',
  },
  profileImg: {
    width: 30,
    height: 30,
    borderRadius: 40,
    marginTop: 20,
  },
  listItem: {
    height: 65,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.WHITE,
    paddingHorizontal: '5%',
    marginTop: 5,
  },
  circleImage: {
    width: 30,
    height: 30,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EEE',
  },
  contentImage: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 10,
    marginRight: 10,
  },
  flatList: {},
  sidebarDivider: {
    height: 1,
    backgroundColor: 'lightgray',
    marginVertical: 10,
  },
  contentIcon: { zIndex: -1, height: 50, width: 50, justifyContent: 'center', alignItems: 'center' },
  contentText: { marginLeft: 10 },
  contentHeader: {
    height: 60,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: Colors.GREEN,
  },
  contentUser: { justifyContent: 'center', flexDirection: 'row', alignItems: 'center' },
  closeDrawer: { marginRight: 15 },
  contentIconText: { flexDirection: 'row', alignItems: 'center' },
});

const Layout = ({ user, routes, onPressItem, handleLogOut, handleCloseDrawer, i18n }) => {
  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.listItem}
        onPress={() => (item.logOut ? handleLogOut() : onPressItem(item))}
      >
        <View style={styles.contentIconText}>
          <View style={styles.contentIcon}>
            <IconSvg name={item.icon} width={45} heigth={45} />
          </View>
          <View style={styles.contentText}>
            <Text align="left" color={Colors.BLACK} weight="Medium">
              {I18n.t(item.name)}
            </Text>
          </View>
        </View>
        <TouchableOpacity>
          <Icon name="chevron-right" color={Colors.BLACK} size={24} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <View style={styles.contentHeader}>
        <View style={styles.contentUser}>
          <View style={styles.contentImage}>
            <View style={styles.circleImage}>
              <FastImage style={styles.circleImage} source={{ uri: user?.imageProfile }} />
            </View>
          </View>
          <Text color={Colors.WHITE} weight="bold" fontSize={14}>
            {user?.authData?.name || ''}
          </Text>
        </View>
        <TouchableOpacity style={styles.closeDrawer} onPress={handleCloseDrawer}>
          <Icon name="x" color={Colors.WHITE} size={24} />
        </TouchableOpacity>
      </View>
      <FlatList
        data={routes}
        extraData={i18n}
        renderItem={renderItem}
        keyExtractor={(item, index) => item + index}
      />
    </View>
  );
};
export default Layout;
