import React from 'react';
import { connect } from 'react-redux';
import {
  DASHBOARD,
  SIGN_IN,
  CHILDREN,
  TEACHERS,
  PARENTS,
  CLASSROOM,
  SOCIAL_NETWORK,
  SETTINGS,
  REPORTS,
  CALENDAR,
  MESSAGES,
  ACTIVITIES,
  SCHOOL_OPTIONS,
  VERIFICATION_CENTER,
} from '@utils/constants/navigates';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { logOut } from '@auth/actions/auth.action';
import I18n from 'react-native-redux-i18n';
import MenuLayout from './components/menu-layout';

class Menu extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      routes: [
        {
          name: 'menu_list.mainPage',
          navigate: DASHBOARD,
          icon: 'kb_dashboard',
          isTeacher: true,
        },
        {
          name: 'menu_list.children',
          navigate: CHILDREN,
          icon: 'kb_babyBoy',
          isTeacher: true,
        },
        {
          name: 'menu_list.parents',
          navigate: PARENTS,
          icon: 'kb_parents',
          isTeacher: false,
        },
        {
          name: 'menu_list.teachers',
          navigate: TEACHERS,
          icon: 'kb_teachers',
          isTeacher: false,
        },
        {
          name: 'menu_list.classrooms',
          navigate: CLASSROOM,
          icon: 'kb_abc',
          isTeacher: true,
        },
        {
          name: 'menu_list.activities',
          navigate: ACTIVITIES,
          icon: 'kb_actividades',
          isTeacher: true,
        },
        /* {
          name: 'menu_list.notes',
          navigate: null,
          icon: 'kb_notas',
          isTeacher: true,
        }, */
        {
          name: 'menu_list.messages',
          navigate: MESSAGES,
          icon: 'kb_messages',
          isTeacher: true,
        },
        {
          name: 'menu_list.calendar',
          navigate: CALENDAR,
          icon: 'kb_calendar',
          isTeacher: true,
        },
        {
          name: 'menu_list.reports',
          navigate: REPORTS,
          icon: 'kb_reports',
          isTeacher: true,
        },
        {
          name: 'menu_list.verification',
          navigate: VERIFICATION_CENTER,
          icon: 'kb_star',
          isTeacher: false,
        },
        {
          name: 'menu_list.setting',
          navigate: SETTINGS,
          icon: 'kb_settings',
          isTeacher: true,
        },
        {
          name: 'menu_list.schoolOptions',
          navigate: SCHOOL_OPTIONS,
          icon: 'kb_schols_settings',
          isTeacher: false,
        },
        {
          name: 'menu_list.socialNetwork',
          navigate: SOCIAL_NETWORK,
          icon: 'kb_like',
          isTeacher: true,
        },
        {
          name: 'menu_list.logout',
          navigate: null,
          icon: 'kb_logout',
          logOut: true,
          isTeacher: true,
        },
      ],
    };
  }

  componentDidMount() {
    this.filterRoutes();
  }

  filterRoutes = () => {
    const { user } = this.props;
    const { routes } = this.state;
    if (user.isTeacher) {
      const newRoutes = routes.filter((x) => x.isTeacher);
      this.setState({ routes: newRoutes });
    }
  };

  handleLogOut = () => {
    const { _logOut } = this.props;
    _logOut();
    this.navigate(SIGN_IN);
  };

  onPressItem = (item) => {
    const {
      navigation: { navigate, closeDrawer },
    } = this.props;
    if (!item.navigate) {
      return;
    }
    navigate(item.navigate);
    closeDrawer();
  };

  render() {
    const { user, i18n } = this.props;
    const { routes } = this.state;
    return (
      <Container padding={false}>
        <MenuLayout
          user={user}
          i18n={i18n}
          onPressItem={this.onPressItem}
          handleLogOut={this.handleLogOut}
          handleCloseDrawer={this.handleCloseDrawer}
          routes={routes}
          {...this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, i18n }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  i18n,
});

export default connect(mapStateToProps, { _logOut: logOut })(Menu);
