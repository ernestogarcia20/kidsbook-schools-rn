import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { setLanguageAction } from '@store/actions/language.action';
import LanguageLayout from './components/language-layout';

class LanguageController extends BaseComponent {
  constructor(props) {
    super(props);

    this.state = {
      languageList: props.languageList,
      locale: props.selectedLanguage?.locale || null,
    };
  }

  componentDidUpdate(prevProps) {
    const { languageList } = this.props;
    if (prevProps.languageList !== languageList) {
      this.changeLanguageState();
    }
  }

  changeLanguageState = () => {
    const { languageList } = this.props;
    this.setState({ languageList });
  };

  handleChangeLanguage = (locale) => {
    const { selectedLanguage } = this.props;
    const { languageList } = this.state;

    if (selectedLanguage?.locale === locale) {
      return;
    }

    const newLanguageList = languageList.map((lng) => {
      if (lng.locale === locale) {
        return { ...lng, selected: true };
      }
      return { ...lng, selected: false };
    });

    this.setState({ languageList: newLanguageList, locale });
  };

  saveChangeLanguage = () => {
    const { locale } = this.state;
    const { _setLanguageAction } = this.props;
    _setLanguageAction(locale);
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container showBackground>
        <LanguageLayout
          goBack={this.goBack}
          handleChangeLanguage={this.handleChangeLanguage}
          saveChangeLanguage={this.saveChangeLanguage}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ language: { languageList, selectedLanguage }, i18n }, store) => ({
  dispatch: store.navigation.dispatch,
  languageList,
  selectedLanguage,
  i18n,
});

export default connect(mapStateToProps, { _setLanguageAction: setLanguageAction })(
  LanguageController
);
