import React from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import Button from '@components/Button';
import Icon from 'react-native-vector-icons/Feather';
import Colors from '@assets/colors';
import I18n from 'react-native-redux-i18n';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
  },
  contentListTranslate: {
    flex: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  renderItem: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
    marginHorizontal: '10%',
  },
  box: {
    backgroundColor: Colors.WHITE,
    borderRadius: 10,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
  },
  flatList: {},
  line: { height: 2, backgroundColor: Colors.GRAYDARK },
  contentSubtitleBox: { width: width / 1.4, paddingTop: 10 },
  contentTitleBox: { paddingTop: 15, paddingBottom: 10 },
  contentButton: { paddingHorizontal: '5%', paddingBottom: '5%' },
});

const Layout = ({ goBack, state, handleChangeLanguage, saveChangeLanguage }) => {
  const { languageList } = state;

  // alert(JSON.stringify(languageList))

  const renderItem = (item, i) => {
    const { label, selected, locale } = item;
    return (
      <TouchableOpacity
        key={`${i}`}
        style={styles.renderItem}
        onPress={() => handleChangeLanguage(locale)}
      >
        <Text align="left">{label}</Text>
        {selected && <Icon name="check" size={24} color={Colors.GREEN} />}
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('languages')} onPress={goBack} />
      <View style={styles.contentListTranslate}>
        <View style={styles.box}>
          <View style={styles.contentTitleBox}>
            <Text color={Colors.GRAY} fontSize={13}>
              {I18n.t('selectLanguage')}
            </Text>
          </View>
          <View style={styles.line} />
          <View style={styles.contentSubtitleBox}>
            <Text fontSize={13}>{I18n.t('pselectLanguage')}</Text>
          </View>
          <View style={{ paddingBottom: 15 }}>
            {languageList &&
              languageList.map((item, index) => {
                return renderItem(item, index);
              })}
          </View>
          <View style={styles.contentButton}>
            <Button title={I18n.t('changeLanguage')} onPress={saveChangeLanguage} />
          </View>
        </View>
      </View>
    </View>
  );
};

export default Layout;
