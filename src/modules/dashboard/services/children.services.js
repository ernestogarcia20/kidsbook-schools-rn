/* eslint-disable prettier/prettier */
import API from '@api/services';


export const getchildrenForSchool = async (schoolId) => {
    return API.get(`/views/childrenForSchool/${schoolId}`);
}

export const createRealtionship = async (data) => {
    return API.post(`/children/createRelationship`, data);
} 

export const deleteChildrens = async (data) => {
    return API.post(`/models/childrens/multidelete`, data);
}

export const unAssignedParent = async (id) => {
    return API.get(`/models/clientchildrens/${id}/delete`);
}

export const saveAttendance = async (data) => {
    return API.post(`/attendance/saveAttendance`, data);
}

export const editChildren = async (id, data) => {
    return API.post(`/models/childrens/update/${id}`, data);
}

export const addChildren = async (data) => {
    return API.post(`/models/childrens`, data);
}