import API from '@api/services';

export const getParents = async () => {
  return API.get(`/views/parents`);
};

export const findParent = async (schoolId, data) => {
  return API.post(`/clients/findByEmail/${schoolId}/Parent`, data);
};

export const inviteParent = async (data) => {
  return API.post(`/clients/inviteParent`, data);
};

export const inviteExistingParent = async (data) => {
  return API.post(`/clients/inviteClient`, data);
};

export const deleteParents = async (ids) => {
  return API.post(`/models/clientschools/multidelete`, { ids });
};
