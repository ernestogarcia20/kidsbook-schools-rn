/* eslint-disable prettier/prettier */
import API from '@api/services';

export const getFastAnswers = (schoolId) => {
    return API.get(`/fastAnswer/list/${schoolId}`);
}

export const updateFastAnswers = (data) => {
    return API.post(`/fastAnswer/update`, data);
}

export const createFastAnswer = (data) => {
    return API.post(`/fastAnswer/create`, data);
}

export const deleteFastAnswer = (fastAnswerId) => {
    return API.get(`/fastAnswer/delete/${fastAnswerId}`);
}