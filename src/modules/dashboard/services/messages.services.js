/* eslint-disable prettier/prettier */
import API from '@api/services';

export const getMessages = (schoolId, type, state, page) => {
    return API.get(`/messages/paginatedforSchool/${schoolId}/${type}/${state}?page=${page}`);
}

export const readMessage = (messageId) => {
    return API.get(`/messages/read/${messageId}`);
}

export const answerMessage = (schoolId, data) => {
    return API.post(`/messages/answer/${schoolId}`, data);
}

export const searchMessages = (schoolId, type) => {
    return API.get(`/messages/search/${schoolId}/${type}`);
}

export const sendMessage = (data) => {
    return API.post(`/messages/send`, data);
}
export const sendFileForMessage = (data) => {
    return API.post(`/messages/addAttachment`, data);
}