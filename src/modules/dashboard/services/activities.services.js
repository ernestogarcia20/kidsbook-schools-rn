/* eslint-disable prettier/prettier */
import API from '@api/services';
import moment from 'moment';

export const getAllActivities = (schoolId, page = 1, dateNow = moment().format('YYYY-MM-DD'),) => {
    return API.get(`/activities/list/${schoolId}/${dateNow}?page=${page}`);
}

export const getTeacherActivities = (schoolId, page = 1, dateNow = moment().format('YYYY-MM-DD'),) => {
    return API.get(`/activities/listTeacher/${schoolId}/${dateNow}?page=${page}`);
}

export const createActivity = (data) => {
    return API.post(`/activities/createActivity`, data);
}

export const updateActivity = (data) => {
    return API.post(`/activities/updateActivity`, data);
}

export const deleteActivity = (activityId) => {
    return API.get(`/activities/delete/${activityId}`);
}

export const sendFileForActivity = (data) => {
    return API.post(`/activities/addAttachment`, data);
}