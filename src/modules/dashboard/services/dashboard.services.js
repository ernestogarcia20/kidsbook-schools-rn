import API from '@api/services';
import moment from 'moment';

export const getDashboard = (schoolId) => {
  return API.get(`/views/dashboard/${schoolId}`);
};

export const getEventCalendar = (schoolId) => {
  return API.get(`/messages/events/${schoolId}`);
};

export const updateSchool = (schoolId, data) => {
  return API.post(`/models/schools/update/${schoolId}`, data);
};

export const getBirthday = (schoolId) => {
  return API.get(`/views/birthday/${schoolId}/${moment().format('YYYY-MM-DD')}`);
};
