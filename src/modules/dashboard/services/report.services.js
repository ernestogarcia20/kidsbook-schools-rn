import API from '@api/services';

export const getReports = async (schoolId, page = 1) => {
  return API.get(`/views/reports/${schoolId}?page=${page}`);
};

export const getReportDetail = async (reportId) => {
  return API.get(`/views/reportDetails/${reportId}`);
};

export const getReportFieldsByChild = async (schoolId, childId) => {
  return API.get(`/views/reportContent/${schoolId}/${childId}`);
};

export const getReportFields = async () => {
  return API.get(`/views/reportFields`);
};

export const sendReport = async (data) => {
  return API.post(`/reports/send`, data);
};

export const getReportTemplates = async (schoolId) => {
  return API.get(`/views/reportTemplates/${schoolId}`);
};

export const saveReportTemplate = async (schoolId, data) => {
  return API.post(`/views/saveReportTemplate/${schoolId}`, data);
};

export const getNamedReportFields = async (schoolId) => {
  return API.get(`/views/namedReportFields/${schoolId}`);
};

export const answerReport = async (data) => {
  return API.post('/reports/answer', data);
};

export const deleteReports = async (ids) => {
  return API.post(`/models/clientschools/multidelete`, { ids });
};
