/* eslint-disable prettier/prettier */
import API from '@api/services';

export const getEventCalendar = (schoolId) => {
    return API.get(`/messages/events/${schoolId}`);
}

export const deleteEventCalendar = (eventId) => {
    return API.get(`/models/messages/${eventId}/delete`);
}