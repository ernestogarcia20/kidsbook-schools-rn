import API from '@api/services';

export const getClassroom = async () => {
  return API.get(`/views/classrooms`);
};

export const createClassroom = async (data) => {
  return API.post(`/models/classrooms`, data);
};

export const getDetailClassroom = async (classroomId) => {
  return API.get(`/views/manageClassroomData/${classroomId}`);
};

export const updateClassroom = async (classroomId, data) => {
  return API.post(`/models/classrooms/update/${classroomId}`, data);
};

export const deleteClassroom = async (ids) => {
  return API.post(`/models/classrooms/multidelete`, { ids });
};
