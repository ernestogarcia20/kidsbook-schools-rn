import API from '@api/services';

export const rateClient = async (data) => {
  return API.post(`/clients/rate`, data);
};

export const getOwnRates = async (schoolId) => {
  return API.get(`/clients/ownRates/${schoolId}`);
};

export const searchClients = async (schoolId, text) => {
  return API.get(`/clients/searchAll/${schoolId}/${text}`);
};

export const deleteTeacher = async (ids) => {
  return API.post(`/models/clientschools/multidelete`, { ids });
};
