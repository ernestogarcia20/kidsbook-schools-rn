import API from '@api/services';

export const getFeed = async (schoolId, paginate = 10, page = 1) => {
  return API.get(`/social/feed/${schoolId}/${paginate}?page=${page}`);
};

export const newPost = async (data) => {
  return API.post(`/models/posts`, data);
};

export const likePostComment = async (schoolId) => {
  return API.get(`/social/likeComment/${schoolId}`);
};

export const getComments = async (postId) => {
  return API.get(`/social/comments/${postId}`);
};

export const likePost = async (postId) => {
  return API.get(`/social/like/${postId}`);
};

export const deletePost = async (postId) => {
  return API.get(`/models/posts/${postId}/delete`);
};

export const updatePost = async (postId, data) => {
  return API.post(`/models/posts/update/${postId}`, data);
};

export const sendComment = async (data) => {
  return API.post(`/models/comments`, data);
};

export const uploadImagePost = async (data) => {
  return API.post(`/social/uploadImage`, data);
};

export const deleteParents = async (ids) => {
  return API.post(`/models/clientschools/multidelete`, { ids });
};
