import API from '@api/services';

export const getTeachers = async () => {
  return API.get(`/views/teachers`);
};

export const findTeacher = async (schoolId, data) => {
  return API.post(`/clients/findByEmail/${schoolId}/Teacher`, data);
};

export const inviteTeacher = async (data) => {
  return API.post(`/clients/inviteTeacher`, data);
};

export const inviteExistingTeacher = async (data) => {
  return API.post(`/clients/inviteClient`, data);
};

export const deleteTeacher = async (ids) => {
  return API.post(`/models/clientschools/multidelete`, { ids });
};
