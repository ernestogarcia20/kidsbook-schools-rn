import { SET_CLASSROOM_LIST } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import {
  getClassroom,
  deleteClassroom,
  createClassroom,
  getDetailClassroom,
  updateClassroom,
} from '@dashboard/services/classroom.services';

export const getClassroomAction = (id, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getClassroom();
      const findData = data[`school_${id}`];
      dispatch(loadingAction(false));
      dispatch(setStore(SET_CLASSROOM_LIST, findData));
      return Promise.resolve(findData);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getDetailClassroomAction = (classroomId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getDetailClassroom(classroomId);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const createClassroomAction = (obj, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'creatingClassroom'));
    try {
      const { data } = await createClassroom(obj);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/classroom`),
        type: 'success',
      });
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const updateClassroomAction = (
  classroomId,
  obj,
  loading = true,
  showMessageUpdate = false
) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await updateClassroom(classroomId, obj);
      dispatch(loadingAction(false));
      if (showMessageUpdate) {
        showMessage({
          message: I18n.t('alert.success'),
          description: I18n.t(`validation.success/updateClassroom`),
          type: 'success',
        });
      }
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteClassroomAction = (ids) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      const response = await deleteClassroom(ids);
      dispatch(loadingAction(false));
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
