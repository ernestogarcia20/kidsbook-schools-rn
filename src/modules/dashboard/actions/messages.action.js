import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import {
  getMessages,
  readMessage,
  answerMessage,
  sendMessage,
  sendFileForMessage,
} from '@dashboard/services/messages.services';
import { BASE_API } from '@api/services';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS from 'react-native-fs';
import { Platform } from 'react-native';

const extention = (filename) => {
  return /[.]/.exec(filename) ? /[^.]+$/.exec(filename) : undefined;
};

export const getMessagesAction = (schoolId, type, state, page, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getMessages(schoolId, type, state, page);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const readMessagesAction = (messageId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await readMessage(messageId);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const answerMessageAction = (schoolId, obj, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'answerMessage'));
    try {
      const data = await answerMessage(schoolId, obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const sendMessageAction = (obj, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'sendMessage'));
    try {
      const data = await sendMessage(obj);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/messageSent`),
        type: 'success',
      });
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const sendFileForMessageAction = (obj, loading = true) => {
  return async (dispatch) => {
    try {
      const data = await sendFileForMessage(obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const downloadFileAction = (token, attachment, loading = true) => {
  return async (dispatch) => {
    const PictureDir = `${RNFS.DocumentDirectoryPath}/${attachment}`;

    // const decUrl = decodeURI(attach.fileData.uri);
    try {
      try {
        const content = await RNFS.exists(PictureDir);
        if (!content) {
          throw content;
        }
        return { PictureDir };
      } catch (e) {
        dispatch(loadingAction(loading, 'downloadFile'));
        const url = `${BASE_API}/messages/attachment/${attachment}?api-key=${token}`;
        const options = {
          fromUrl: url,
          toFile: PictureDir,
        };

        const data = await RNFS.downloadFile(options).promise;
        if (data.statusCode !== 200) {
          throw data;
        }
        dispatch(loadingAction(false));
        /* showMessage({
          message: I18n.t('alert.success'),
          description: I18n.t(`validation.success/download`),
          type: 'success',
        }); */
        return { PictureDir };
      }
    } catch (e) {
      console.log(e);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.failed/download`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteTeacherAction = (ids) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      const response = await deleteTeacher(ids);
      dispatch(loadingAction(false));
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
