import {
  SET_EVENT_CALENDAR,
  REMOVE_ALL,
  SET_BIRTHDAY_LIST,
  SET_USER,
} from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import I18n from 'react-native-redux-i18n';
import {
  getDashboard,
  getEventCalendar,
  getBirthday,
  updateSchool,
} from '@dashboard/services/dashboard.services';
import loadingAction from '@store/actions/loading.action';

export const getDashboardAction = (schoolId) => {
  return async () => {
    // dispatch(loadingAction(true, 'logIn'));
    try {
      const { data } = await getDashboard(schoolId);
      return Promise.resolve(data);
    } catch (e) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getEventCalendarAction = (schoolId) => {
  return async (dispatch) => {
    // dispatch(loadingAction(true, 'logIn'));
    try {
      const data = await getEventCalendar(schoolId);
      dispatch(setStore(SET_EVENT_CALENDAR, data));
      return Promise.resolve(data);
    } catch (e) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getBirthdayListAction = (schoolId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading));
    try {
      const { data } = await getBirthday(schoolId);
      dispatch(setStore(SET_BIRTHDAY_LIST, data));

      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const updateSchoolAction = (schoolId, obj, allUser, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading));
    try {
      const { data } = await updateSchool(schoolId, obj);
      dispatch(setStore(SET_USER, { ...allUser, school: data }));
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/update`),
        type: 'success',
      });
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const forgotPasswordAction = (data) => {
  return async (dispatch) => {
    /* dispatch(setLocale(locale));
      const languageList = I18n.t('languageList');
      dispatch(setStore(SET_LANGUAGE_LIST, languageList || []));
      dispatch(setStore(SET_SELECTED_LANGUAGE, languageList.find((x) => x.selected) || null)); */
  };
};
