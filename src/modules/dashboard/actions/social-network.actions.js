import { SET_POSTS, LIKE_POST, REMOVE_POST } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import {
  getFeed,
  newPost,
  uploadImagePost,
  likePost,
  deletePost,
  updatePost,
} from '@dashboard/services/social-network.services';
import { fontMaker } from '@components/typografy';

export const getFeedAction = (schoolId, paginate, page, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getFeed(schoolId, paginate, page);
      dispatch(loadingAction(false));
      dispatch(setStore(SET_POSTS, data));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const newPostAction = (obj, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'creatingPost'));
    try {
      const { data } = await newPost(obj);
      // dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const updatePostAction = (obj, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'updatingPost'));
    try {
      const { data } = await updatePost(obj);
      // dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const likePostAction = (postId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const data = await likePost(postId);
      if (data?.message) {
        showMessage({
          message: data.message,
          type: 'default',
          titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
        });
      }
      if (data?.liked) {
        dispatch(setStore(LIKE_POST, postId));
      }
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const uploadImagePostAction = (obj, loading = true) => {
  return async (dispatch) => {
    try {
      const { data } = await uploadImagePost(obj);
      // dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deletePostAction = (postId) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      const response = await deletePost(postId);
      dispatch(setStore(REMOVE_POST, postId));
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/deletedPost`),
        type: 'success',
      });
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
