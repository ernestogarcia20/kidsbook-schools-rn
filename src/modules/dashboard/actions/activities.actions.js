import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import {
  getTeacherActivities,
  getAllActivities,
  updateActivity,
  sendFileForActivity,
  deleteActivity,
  createActivity,
} from '@dashboard/services/activities.services';
import { SET_ACTIVITIES, UPDATE_ACTIVITY, REMOVE_ACTIVITY } from '@utils/constants/reducers';
import RNFS from 'react-native-fs';

export const getAllActivitiesAction = (schoolId, page, loader = false) => {
  return async (dispatch) => {
    dispatch(loadingAction(loader ? page === 1 : loader));
    try {
      const { data } = await getAllActivities(schoolId, page);
      dispatch(setStore(SET_ACTIVITIES, data));
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getTeacherActivitiesAction = (schoolId, page) => {
  return async (dispatch) => {
    dispatch(loadingAction(page === 1));
    try {
      const { data } = await getTeacherActivities(schoolId, page);
      dispatch(setStore(SET_ACTIVITIES, data));
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const downloadFileActivityAction = (url, attachment, loading = true) => {
  return async (dispatch) => {
    const PictureDir = `${RNFS.DocumentDirectoryPath}/${attachment}`;

    // const decUrl = decodeURI(attach.fileData.uri);
    try {
      try {
        const content = await RNFS.exists(PictureDir);
        if (!content) {
          throw content;
        }
        return { PictureDir };
      } catch (e) {
        dispatch(loadingAction(loading, 'downloadFile'));
        const options = {
          fromUrl: url,
          toFile: PictureDir,
        };

        const data = await RNFS.downloadFile(options).promise;
        if (data.statusCode !== 200) {
          throw data;
        }
        dispatch(loadingAction(false));
        /* showMessage({
          message: I18n.t('alert.success'),
          description: I18n.t(`validation.success/download`),
          type: 'success',
        }); */
        return { PictureDir };
      }
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.failed/download`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const updateActivityAction = (obj) => {
  return async (dispatch) => {
    try {
      const { data } = await updateActivity(obj);
      dispatch(setStore(UPDATE_ACTIVITY, obj));
      return Promise.resolve(data);
    } catch (e) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const createActiviyAction = (obj, loading = true) => {
  return async (dispatch) => {
    // dispatch(loadingAction(loading, 'creatingActivity'));
    try {
      const { data } = await createActivity(obj);
      // dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const insertActivityAction = (activity) => {
  return async (dispatch) => {
    dispatch(setStore(UPDATE_ACTIVITY, activity));
  };
};

export const sendFileForActivityAction = (obj, loading = true) => {
  return async (dispatch) => {
    try {
      const data = await sendFileForActivity(obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteActivityAction = (activityId) => {
  return async (dispatch) => {
    dispatch(loadingAction(true));
    try {
      await deleteActivity(activityId);
      dispatch(setStore(REMOVE_ACTIVITY, { id: activityId }));
      dispatch(loadingAction(false));
      return Promise.resolve(activityId);
    } catch (e) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
