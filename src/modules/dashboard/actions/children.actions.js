import { SET_CHILDREN_LIST, SET_CHILDREN_PENDING_LIST } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import {
  getchildrenForSchool,
  deleteChildrens,
  editChildren,
  addChildren,
  createRealtionship,
  unAssignedParent,
  saveAttendance,
} from '@dashboard/services/children.services';

export const getChildrenForSchoolAction = (schoolId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getchildrenForSchool(schoolId);

      dispatch(loadingAction(false, 'loading'));
      dispatch(setStore(SET_CHILDREN_LIST, data));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false, 'loading'));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const updateLocalListAction = (list) => {
  return async (dispatch) => {
    dispatch(setStore(SET_CHILDREN_LIST, list));
  };
};

export const createRealtionshipAction = (data) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'creatingRelationship'));
    try {
      const response = await createRealtionship(data);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/relationship`),
        type: 'success',
      });
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const unAssignedParentAction = (id) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'deletingRelationship'));
    try {
      const response = await unAssignedParent(id);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/relationshipDeleted`),
        type: 'success',
      });
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const saveAttendanceAction = (obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'savingAttendance'));
    try {
      const { data } = await saveAttendance(obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const editChildrenAction = (id, data) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'editInfo'));
    try {
      const response = await editChildren(id, data);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/update`),
        type: 'success',
      });
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const addChildrenAction = (data) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'creatingChild'));
    try {
      const response = await addChildren(data);
      dispatch(loadingAction(false));
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteChildrensAction = (ids = []) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      const response = await deleteChildrens({ ids });
      dispatch(loadingAction(false, 'loading'));
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false, 'loading'));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
