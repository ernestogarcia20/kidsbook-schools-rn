import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import { rateClient, getOwnRates, searchClients } from '@dashboard/services/verification.services';

export const getOwnRatesAction = (schoolId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const data = await getOwnRates(schoolId);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const searchClientsAction = (schoolId, text) => {
  return async (dispatch) => {
    try {
      const { data } = await searchClients(schoolId, text);
      return Promise.resolve(data);
    } catch (e) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const rateClientAction = (obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'loading'));
    try {
      const { data } = await rateClient(obj);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/ratedUser`),
        type: 'success',
      });
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteTeacherAction = (ids) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      const response = await deleteTeacher(ids);
      dispatch(loadingAction(false));
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
