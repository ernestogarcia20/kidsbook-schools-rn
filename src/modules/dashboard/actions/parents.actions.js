import { SET_PARENT_LIST } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import {
  getParents,
  deleteParents,
  findParent,
  inviteParent,
  inviteExistingParent,
} from '@dashboard/services/parents.services';

export const getParentsAction = (id, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getParents();
      const findData = data[`school_${id}`];
      dispatch(loadingAction(false));
      dispatch(setStore(SET_PARENT_LIST, findData));
      return Promise.resolve(findData);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const findParentAction = (schoolId, obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'searching'));
    try {
      const { data } = await findParent(schoolId, obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      if (e.code === 'account/alreadyInvited') {
        showMessage({
          message: I18n.t('alert.info'),
          description: I18n.t(`validation.${e.code}`),
          type: 'default',
        });
      } else {
        showMessage({
          message: I18n.t('alert.error'),
          description: I18n.t(`validation.${e.code}`),
          type: 'danger',
        });
      }
      throw e;
    }
  };
};

export const inviteParentAction = (obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'sendInvitation'));
    try {
      const data = await inviteParent(obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const inviteExistingParentAction = (obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'sendInvitation'));
    try {
      const data = await inviteExistingParent(obj);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/sentInvitation`),
        type: 'success',
      });
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteParentsAction = (ids) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      const response = await deleteParents(ids);
      dispatch(loadingAction(false));
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
