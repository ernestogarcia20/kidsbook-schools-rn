import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import { deleteEventCalendar, getEventCalendar } from '@dashboard/services/calendar.services';
import { SET_EVENT_CALENDAR } from '@utils/constants/reducers';

export const getEventCalendarAction = (schoolId) => {
  return async (dispatch) => {
    // dispatch(loadingAction(true, 'logIn'));
    try {
      const data = await getEventCalendar(schoolId);
      dispatch(setStore(SET_EVENT_CALENDAR, data));
      return Promise.resolve(data);
    } catch (e) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteEventAction = (eventId) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      const response = await deleteEventCalendar(eventId);
      dispatch(loadingAction(false));
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
