import { SET_TEACHER_LIST } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import {
  getTeachers,
  deleteTeacher,
  findTeacher,
  inviteExistingTeacher,
  inviteTeacher,
} from '@dashboard/services/teachers.services';

export const getTeachersAction = (id, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getTeachers();
      const findData = data[`school_${id}`];
      dispatch(loadingAction(false));
      dispatch(setStore(SET_TEACHER_LIST, findData));
      return Promise.resolve(findData);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const findTeacherAction = (schoolId, obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'searching'));
    try {
      const { data } = await findTeacher(schoolId, obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      if (e.code === 'account/alreadyInvited') {
        showMessage({
          message: I18n.t('alert.info'),
          description: I18n.t(`validation.account/alreadyTeacherInvited`),
          type: 'default',
        });
      } else {
        showMessage({
          message: I18n.t('alert.error'),
          description: I18n.t(`validation.${e.code}`),
          type: 'danger',
        });
      }
      throw e;
    }
  };
};

export const inviteTeacherAction = (obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'sendInvitation'));
    try {
      const data = await inviteTeacher(obj);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/sentInvitation`),
        type: 'success',
      });
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const inviteExistingTeacherAction = (obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'sendInvitation'));
    try {
      const data = await inviteExistingTeacher(obj);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.success/sentInvitation`),
        type: 'success',
      });
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteTeacherAction = (ids) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      const response = await deleteTeacher(ids);
      dispatch(loadingAction(false));
      return Promise.resolve(response);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
