import { SET_FAST_ANSWER, REMOVE_FAST_ANSWER,  } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import I18n from 'react-native-redux-i18n';
import {
  getFastAnswers,
  updateFastAnswers,
  createFastAnswer,
  deleteFastAnswer,
} from '@dashboard/services/fast-answer.services';
import loadingAction from '@store/actions/loading.action';

export const getFastAnswerAction = (schoolId) => {
  return async (dispatch) => {
    // dispatch(loadingAction(true, 'logIn'));
    try {
      const { data } = await getFastAnswers(schoolId);
      dispatch(setStore(SET_FAST_ANSWER, data));
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const createFastAnswerAction = (obj) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'loading'));
    try {
      const { data } = await createFastAnswer(obj);
      dispatch(setStore(SET_FAST_ANSWER, data));
      dispatch(loadingAction(false));
      return Promise.resolve(true);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const editFastAnswerAction = (obj, fastAnswerList = []) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'editInfo'));
    try {
      await updateFastAnswers({ ...obj, id_fast_answer: obj.id });

      const newFastAnswerList = fastAnswerList;
      const index = fastAnswerList.findIndex((x) => x.id === obj.id);
      newFastAnswerList[index] = { ...fastAnswerList[index], ...obj };

      dispatch(setStore(SET_FAST_ANSWER, [...newFastAnswerList]));

      dispatch(loadingAction(false));
      return Promise.resolve(true);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteFastAnswerAction = (idFastAnswer) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      await deleteFastAnswer(idFastAnswer);
      dispatch(setStore(REMOVE_FAST_ANSWER, idFastAnswer));
      dispatch(loadingAction(false));
      return Promise.resolve(true);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
