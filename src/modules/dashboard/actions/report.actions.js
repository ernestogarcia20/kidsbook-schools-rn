import { SET_REPORT_LIST, SET_FIELDS, SET_USER } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import {
  getReports,
  getReportDetail,
  getReportFieldsByChild,
  getNamedReportFields,
  answerReport,
  sendReport,
  getReportTemplates,
  getReportFields,
  saveReportTemplate,
} from '@dashboard/services/report.services';

export const getReportAction = (schoolId, page, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getReports(schoolId, page);
      dispatch(loadingAction(false));
      dispatch(setStore(SET_REPORT_LIST, data));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getReportDetailAction = (reportId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getReportDetail(reportId);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getNamedReportFieldsAction = (schoolId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getNamedReportFields(schoolId);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const answerReportAction = (data, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'answerMessage'));
    try {
      await answerReport(data);
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.success'),
        description: I18n.t(`validation.messageSent`),
        type: 'default',
      });
      return Promise.resolve(true);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const saveReportTemplateAction = (schoolId, obj, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'saving'));
    try {
      const data = await saveReportTemplate(schoolId, obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getReportFieldsAction = (loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getReportFields();
      dispatch(loadingAction(false));
      dispatch(setStore(SET_FIELDS, data?.fields || []));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getReportFieldsByChildAction = (schoolId, childId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading, 'loading'));
    try {
      const { data } = await getReportFieldsByChild(schoolId, childId);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const sendReportAction = (obj, send = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, send ? 'sendingReport' : 'savingReport'));
    try {
      const { data } = await sendReport(obj);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const getReportTemplatesAction = (schoolId, loading = true) => {
  return async (dispatch) => {
    dispatch(loadingAction(loading));
    try {
      const data = await getReportTemplates(schoolId);
      dispatch(loadingAction(false));
      return Promise.resolve(data);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const deleteReportAction = (ids) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'remove'));
    try {
      /* const response = await deleteTeacher(ids);
      dispatch(loadingAction(false));
      return Promise.resolve(response); */
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};
