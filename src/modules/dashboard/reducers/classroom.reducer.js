import { SET_CLASSROOM_LIST, REMOVE_ALL } from '@utils/constants/reducers';

const INITIAL_STATE = {
  classroomList: [],
};

function classromReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_CLASSROOM_LIST:
      return { ...state, classroomList: action.payload };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default classromReducer;
