import { SET_FAST_ANSWER, REMOVE_FAST_ANSWER, REMOVE_ALL } from '@utils/constants/reducers';

const INITIAL_STATE = {
  fastAnswerList: [],
};

function classromReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_FAST_ANSWER:
      return { ...state, fastAnswerList: action.payload };
    case REMOVE_FAST_ANSWER:
      const idFastAnswer = action.payload;
      const fastAnswerList = state.fastAnswerList;
      const index = fastAnswerList.findIndex((x) => x.id === idFastAnswer);
      const newFastAnswerList = fastAnswerList;
      newFastAnswerList.splice(index, 1);
      return { ...state, fastAnswerList: [...newFastAnswerList] };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default classromReducer;
