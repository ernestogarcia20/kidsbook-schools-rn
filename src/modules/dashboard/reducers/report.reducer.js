import { SET_REPORT_LIST, SET_FIELDS,  REMOVE_ALL } from '@utils/constants/reducers';

const INITIAL_STATE = {
  reportList: [],
  fields: [],
};

function reportReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_REPORT_LIST:
      return { ...state, reportList: action.payload };
    case SET_FIELDS:
      return { ...state, fields: action.payload };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default reportReducer;
