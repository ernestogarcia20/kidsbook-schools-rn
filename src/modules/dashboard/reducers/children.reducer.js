import {
  SET_CHILDREN_LIST,
  SET_CHILDREN_PENDING_LIST,
  REMOVE_ALL,
} from '@utils/constants/reducers';

const INITIAL_STATE = {
  childrenList: [],
  childrenListPending: [],
};

function childrenReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_CHILDREN_LIST:
      return { ...state, childrenList: action.payload };
    case SET_CHILDREN_PENDING_LIST:
      return { ...state, childrenListPending: action.payload };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default childrenReducer;
