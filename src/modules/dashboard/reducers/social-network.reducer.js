import { SET_POSTS, LIKE_POST, REMOVE_POST, REMOVE_ALL } from '@utils/constants/reducers';

const INITIAL_STATE = {
  posts: [],
};

function socialNetworkReducer(state = INITIAL_STATE, action) {
  const postId = action.payload;
  let postState = state.posts;
  const indexPost = postState.findIndex((x) => x.id === postId);
  switch (action.type) {
    case SET_POSTS:
      return { ...state, posts: action.payload };
    case LIKE_POST:
      if (indexPost > -1) {
        const post = postState[indexPost];
        postState[indexPost] = { ...post, likes: post.likes + 1 };
      }
      return { ...state, posts: [...postState] };
    case REMOVE_POST:
      if (indexPost > -1) {
        postState = postState
          .slice(0, indexPost)
          .concat(postState.slice(indexPost + 1, postState.length));
      }
      return { ...state, posts: [...postState] };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default socialNetworkReducer;
