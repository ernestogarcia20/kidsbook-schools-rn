import {
  SET_ACTIVITIES,
  UPDATE_ACTIVITY,
  REMOVE_ACTIVITY,
  REMOVE_ALL,
} from '@utils/constants/reducers';

const INITIAL_STATE = {
  activitiesList: [],
};

function activitiesReducer(state = INITIAL_STATE, action) {
  let activity = null;
  let indexActivity = null;
  if (action.type === REMOVE_ACTIVITY || action.type === UPDATE_ACTIVITY) {
    activity = action.payload;
    indexActivity = state.activitiesList.indexOf((x) => x.id === activity.id);
  }
  const { activitiesList } = state;
  switch (action.type) {
    case SET_ACTIVITIES:
      return { ...state, activitiesList: action.payload };
    case REMOVE_ACTIVITY:
      if (indexActivity > -1) {
        activitiesList.splice(indexActivity, 1);
      }
      return { ...state, activitiesList: [...activitiesList] };
    case UPDATE_ACTIVITY:
      if (indexActivity > -1) {
        activitiesList[indexActivity] = { ...activitiesList[indexActivity], ...activity };
      } else activitiesList.push(activity);
      return { ...state, activitiesList: [...activitiesList] };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default activitiesReducer;
