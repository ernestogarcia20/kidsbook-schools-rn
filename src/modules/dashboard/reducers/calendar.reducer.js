import { SET_EVENT_CALENDAR, SET_BIRTHDAY_LIST, REMOVE_ALL } from '@utils/constants/reducers';

const INITIAL_STATE = {
  events: {},
  birthdayList: [],
};

function calendarReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_EVENT_CALENDAR:
      return { ...state, events: action.payload };
    case SET_BIRTHDAY_LIST:
      return { ...state, birthdayList: action.payload };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default calendarReducer;
