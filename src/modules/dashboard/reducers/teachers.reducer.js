import { SET_TEACHER_LIST, REMOVE_ALL } from '@utils/constants/reducers';

const INITIAL_STATE = {
  teacherList: [],
};

function teachersReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_TEACHER_LIST:
      return { ...state, teacherList: action.payload };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default teachersReducer;
