import { SET_PARENT_LIST, SET_PARENT_PENDING_LIST, REMOVE_ALL } from '@utils/constants/reducers';

const INITIAL_STATE = {
  parentList: [],
  parentListPending: [],
};

function parentReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_PARENT_LIST:
      return { ...state, parentList: action.payload };
    case SET_PARENT_PENDING_LIST:
      return { ...state, parentListPending: action.payload };
    case REMOVE_ALL: {
      return INITIAL_STATE;
    }
    default:
      return state;
  }
}

export default parentReducer;
