import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import Header from '@components/Header';
import Search from '@components/Search';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import colors from '@assets/colors';
import { Styles } from '@assets/Styles';
import StarRating from 'react-native-star-rating';

const validateRelationship = {
  Father: 'Father',
  Mother: 'Mother',
  Uncle: 'Uncle',
  Brother: 'Brother',
  Sister: 'Sister',
  Other: 'Other',
  Teacher: 'Teacher',
  Parent: 'Parent',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentRelationship: {
    borderRadius: 3,
    borderColor: colors.GREEN,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 15,
    minWidth: 65,
    marginLeft: 5,
    marginBottom: 5,
  },
  contentInfo: {
    paddingTop: '7%',
    paddingBottom: '3%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    marginHorizontal: '5%',
  },
  paddingLeft: {
    paddingLeft: 10,
  },
});

const Layout = ({ openDrawer, handleNewVerification, state }) => {
  const renderItem = ({ item }) => {
    return (
      <View style={styles.contentInfo}>
        <View>
          <View style={Styles.row}>
            <View style={styles.contentRelationship}>
              <Text
                color={colors.GREEN}
                fontSize={11}
                weight="SemiBold"
                style={{ textTransform: 'uppercase' }}
              >
                {item && !validateRelationship[item.reported_as || 'Other']
                  ? item?.reported_as
                  : I18n.t(`listRelationship.${item.reported_as ? item.reported_as : 'Other'}`)}
              </Text>
            </View>
          </View>
          <View style={[styles.paddingLeft, { flex: 0.7, justifyContent: 'flex-start' }]}>
            <Text align="left" color="#757575">
              {item?.client?.name}
            </Text>
            <Text align="left" color="#757575" fontSize={12}>
              {item?.client?.email}
            </Text>
            <Text align="left" color="#757575" style={{marginTop: 2}}>
              {item?.reason}
            </Text>
            <View style={Styles.row}>
              <StarRating
                disabled
                maxStars={5}
                starSize={23}
                starStyle={{ marginRight: 5 }}
                rating={item.rate}
                fullStarColor="#FFC107"
              />
            </View>
          </View>
        </View>
      </View>
    );
  };

  const { rates } = state;
  return (
    <View style={styles.container}>
      <Header
        title={I18n.t('menu_list.verificationCenter')}
        menu
        add
        onPressAdd={handleNewVerification}
        onPress={openDrawer}
      />
      <Search hasBackground />
      <FlatList
        data={rates || []}
        renderItem={renderItem}
        keyExtractor={(item, index) => item + index}
      />
    </View>
  );
};

export default Layout;
