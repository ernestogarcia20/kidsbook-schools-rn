import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { getOwnRatesAction } from '@modules/dashboard/actions/verification.actions';
import { SEARCH_VERIFY_CLIENT } from '@utils/constants/navigates';
import VarificationCenterLayout from './components/verification-center-layout';

class VarificationCenterController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      rates: [],
    };
  }

  componentDidMount() {
    this.init();
  }

  refeshScreen = () => {
    this.init(false);
  };

  init = async (loading = true) => {
    const { _getOwnRatesAction, user } = this.props;

    try {
      const ownRates = await _getOwnRatesAction(user.schoolId, loading);
      this.setState({ rates: ownRates.rates });
    } catch (e) {}
  };

  handleNewVerification = () => {
    this.navigate(SEARCH_VERIFY_CLIENT, { refeshScreen: this.refeshScreen });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <VarificationCenterLayout
          openDrawer={this.openDrawer}
          setState={this.handleChange}
          handleNewVerification={this.handleNewVerification}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, { _getOwnRatesAction: getOwnRatesAction })(
  VarificationCenterController
);
