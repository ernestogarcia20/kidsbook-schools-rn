import React from 'react';
import { View, StyleSheet, Dimensions, ScrollView } from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import Icon from '@components/Icon';
import I18n from 'react-native-redux-i18n';
import { getImageById } from '@utils/functions/globals';
import FastImage from '@components/FastImage';
import Colors from '@assets/colors';
import NoAvatar from '@images/no-avatar.jpg';
import { Styles } from '@assets/Styles';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    marginBottom: '5%',
  },
  avatar: {
    width: width / 2,
    height: width / 2,
    zIndex: -1,
    borderWidth: 2,
    borderColor: Colors.GREEN,
    borderRadius: 200,
  },
  contentFormProfile: {
    justifyContent: 'flex-start',
    marginTop: 15,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 20,
  },
  marginHorizontal: {
    marginHorizontal: '8%',
  },
  itemFamily: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    marginTop: 0,
    borderColor: 'rgba(0,0,0,0.2)',
  },
});

const Layout = ({ goBack, state, user }) => {
  const { teacher } = state;
  return (
    <View style={styles.container}>
      <Header title={I18n.t('profile')} onPress={goBack} />
      <ScrollView>
        <View style={styles.contentAvatar}>
          <FastImage
            source={
              teacher?.avatar ? { uri: getImageById(teacher?.avatar, user?.token) } : NoAvatar
            }
            style={styles.avatar}
          />
        </View>
        <View style={styles.marginHorizontal}>
          <View style={[Styles.row, styles.contentFormProfile]}>
            <View style={{ marginRight: 10 }}>
              <Text weight="SemiBold">{I18n.t('firstName')}: </Text>
            </View>
            <Text>{teacher?.name}</Text>
          </View>
          <View style={[Styles.row, styles.contentFormProfile]}>
            <View style={{ marginRight: 10 }}>
              <Text weight="SemiBold">{I18n.t('phone')}: </Text>
            </View>
            <Text>{teacher?.telephone}</Text>
          </View>
          <View style={[Styles.row, styles.contentFormProfile]}>
            <View style={{ marginRight: 10 }}>
              <Text weight="SemiBold">{I18n.t('email')}: </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text align="right" fontSize={14}>
                {teacher?.email}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.marginHorizontal}>
          {teacher?.classrooms?.length > 0 && (
            <View
              style={{
                marginTop: '8%',
                height: 35,
                borderBottomWidth: 1,
                borderColor: 'rgba(0,0,0,0.2)',
                paddingHorizontal: 15
              }}
            >
              <Text align="left">{I18n.t('menu_list.classrooms')}</Text>
            </View>
          )}
          {teacher?.classrooms?.map((classroom, index) => {
            return (
              <View style={styles.itemFamily} key={index}>
                <View
                  style={[
                    Styles.row,
                    styles.paddingLeft,
                    { flex: 0.7, justifyContent: 'flex-start' },
                  ]}
                >
                  <View style={[Styles.center, { width: 25, height: 25, marginRight: 20 }]}>
                    <Icon name="kb_abc" width={25} heigth={25} />
                  </View>
                  <Text align="left" weight="Medium" fontSize={12}>
                    {classroom?.name}
                  </Text>
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default Layout;
