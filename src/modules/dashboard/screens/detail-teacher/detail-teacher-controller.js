import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import DetailTeacherLayout from './components/detail-teacher-layout';

class DetailTeacherController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const teacher = getParam('teacher', {});
    this.state = {
      teacher,
    };
  }

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <DetailTeacherLayout
          goBack={this.goBack}
          user={this.props.user}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps)(DetailTeacherController);
