import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import { getClassroomAction, deleteClassroomAction } from '@dashboard/actions/classroom.actions';
import BaseComponent from '@components/BaseComponent';
import { CHILDREN_DETAIL, ADD_EDIT_CLASSROOM, CLASSROOM_TEACHER } from '@utils/constants/navigates';
import I18n from 'react-native-redux-i18n';
import ClassroomLayout from './components/classroom-layout';

class ClassroomController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      editList: false,
      selectAll: false,
      classroomList: props.classroomList,
    };
  }

  componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const { classroomList } = this.props;
    if (prevProps.classroomList !== classroomList) {
      this.setListInState();
    }
  }

  setListInState = () => {
    const { classroomList } = this.props;
    classroomList.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
    this.setState({ classroomList });
  };

  init = async () => {
    const { _getClassroomAction, classroomList, user } = this.props;
    try {
      const response = await _getClassroomAction(
        user.schoolData.schools[0].id,
        classroomList.length === 0
      );
      console.log(JSON.stringify(response));
    } catch (e) {}
  };

  updateListClassroom = async () => {
    const { _getClassroomAction, user } = this.props;
    try {
      await _getClassroomAction(user.schoolId, false);
    } catch (e) {}
  };

  onPressAdd = () => {
    this.navigate(ADD_EDIT_CLASSROOM, {
      updateList: this.updateListClassroom,
    });
    /* this.navigate(CHILDREN_DETAIL, {
      edit: false,
      updateListChild: this.updateListChild,
    }); */
  };

  onPressEdit = () => {
    const { editList } = this.state;
    this.setState({ editList: !editList });
  };

  handleSelectAll = () => {
    const { selectAll, classroomList } = this.state;
    const newList = classroomList.map((child) => {
      return { ...child, checked: !selectAll };
    });
    this.setState({ selectAll: !selectAll, classroomList: newList });
  };

  handleSelectedItem = (item) => {
    const { classroomList } = this.state;
    const index = classroomList.findIndex((it) => it === item);
    if (index < 0) {
      return;
    }
    let child = classroomList[index];
    child = { ...child, checked: !child.checked };
    classroomList[index] = child;
    this.setState({ classroomList });
  };

  handleDelete = async (classroom = null) => {
    const { dispatch } = this.props;

    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t(
          classroom ? 'modalsText.deleteClassroom' : 'modalsText.deleteMultipleClassroom'
        ),
        onPressAccept: () => this.deleteClassroom(classroom),
      },
      dispatch
    );
  };

  deleteClassroom = async (classroom = null) => {
    const { classroomList } = this.state;
    const { _deleteClassroomAction } = this.props;
    if (classroomList.length === 0) {
      return;
    }
    const filter = classroomList.filter((ch) => ch.checked);
    const ids = [];
    if (classroom) {
      ids.push(classroom.id);
    }
    const deleteIds = classroom ? ids : filter.map((ch) => ch.id);
    try {
      await _deleteClassroomAction(deleteIds);
      const newclassroomList = classroomList.filter((c) => deleteIds.indexOf(c.id) === -1);
      this.setState({ classroomList: newclassroomList });
    } catch (e) {}
  };

  handleDetailClassroom = (item) => {
    const { user } = this.props;
    if (user.isTeacher) {
      this.navigate(CLASSROOM_TEACHER, {
        classroom: item,
      });
      return;
    }
    this.navigate(ADD_EDIT_CLASSROOM, {
      edit: true,
      classroom: item,
      updateList: this.updateListClassroom,
    });
    /* this.navigate(CHILDREN_DETAIL, {
      edit: true,
      child: item,
      updateListChild: this.updateListChild,
    }); */
  };

  render() {
    const { classroomList, user } = this.props;
    return (
      <Container>
        <ClassroomLayout
          openDrawer={this.openDrawer}
          user={user}
          classroomList={classroomList}
          state={this.state}
          onPressAdd={this.onPressAdd}
          onPressEdit={this.onPressEdit}
          handleDelete={this.handleDelete}
          handleDetailClassroom={this.handleDetailClassroom}
          handleSelectedItem={this.handleSelectedItem}
          handleSelectAll={this.handleSelectAll}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, classroom: { classroomList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  classroomList,
});

export default connect(mapStateToProps, {
  _getClassroomAction: getClassroomAction,
  _deleteClassroomAction: deleteClassroomAction,
})(ClassroomController);
