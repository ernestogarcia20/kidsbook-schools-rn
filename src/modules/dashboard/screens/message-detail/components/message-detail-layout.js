import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  Platform,
  Animated,
  TouchableOpacity,
  KeyboardAvoidingView,
  NativeModules,
  Keyboard,
} from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import ItemMessage from '@components/ItemMessage';
import ItemMessageAnswer from '@components/ItemMessageAnswer';
import InputMessage from '@components/InputMessage';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';

const { StatusBarManager } = NativeModules;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainerStyle: {
    paddingTop: 20,
  },
  contentSeparatorResponse: {
    marginHorizontal: 20,
    height: 45,
    justifyContent: 'flex-end',
    marginBottom: 23,
    paddingBottom: 5,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    borderBottomWidth: 1,
  },
  contentAttach: { marginHorizontal: 15, marginTop: 5 },
  attachItem: {
    height: 50,
    marginTop: 0,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 15,
    flexWrap: 'wrap',
  },
});

const keyboardPadding = new Animated.Value(0);
let keyboardWillShowListener = null;
let keyboardWillHideListener = null;
const Layout = ({
  goBack,
  user,
  state,
  onPressReport,
  handleSendMessage,
  handleDownloadAttach,
}) => {
  const { message, answers, messageResponse, attachments } = state;
  const [statusBarHeight, setStatusBarHeight] = useState(0);
  const [scrollView, setScrollView] = useState(null);

  const scrolled = () => {
    if (!scrollView) {
      return;
    }
    setTimeout(() => {
      scrollView.scrollToEnd({ animated: true });
    }, 5);
  };

  const keyboardWillShow = () => {
    Animated.timing(keyboardPadding, { toValue: 1, duration: 0, useNativeDriver: false }).start();
  };

  const keyboardWillHide = () => {
    Animated.timing(keyboardPadding, { toValue: 0, duration: 0, useNativeDriver: false }).start();
  };

  useEffect(() => {
    if (Platform.OS === 'ios') {
      StatusBarManager.getHeight((statusBarFrameData) => {
        setStatusBarHeight(statusBarFrameData.height + 10);
      });
    }
    keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', keyboardWillShow);
    keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', keyboardWillHide);
    return () => {
      keyboardWillShowListener.remove();
      keyboardWillHideListener.remove();
    };
  }, []);

  const messageObj = messageResponse || message;

  const listHeaderComponent = () => {
    return (
      <View style={{ marginTop: 20 }}>
        <ItemMessage detail item={messageObj} user={user}>
          {attachments.length > 0 && (
            <>
              <View style={styles.contentAttach}>
                <Text align="left">{I18n.t('attachments')}</Text>
              </View>
              {attachments.map((attach) => {
                return (
                  <TouchableOpacity
                    style={styles.attachItem}
                    onPress={() => handleDownloadAttach(attach)}
                  >
                    <CustomIcon name="attach" width={35} height={35} />
                    <Text align="left">{attach}</Text>
                  </TouchableOpacity>
                );
              })}
            </>
          )}
        </ItemMessage>
        {answers.length > 0 && (
          <View style={styles.contentSeparatorResponse}>
            <Text align="left" color="#9A9A9A">
              {I18n.t('answers')}
            </Text>
          </View>
        )}
      </View>
    );
  };

  const renderItem = ({ item }) => {
    return (
      <View>
        <ItemMessageAnswer onPressReport={onPressReport} item={item} user={user} />
      </View>
    );
  };
  const scrollStyles = (_keyboardPadding) => {
    return { paddingBottom: _keyboardPadding };
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.messageDetail')} onPress={goBack} />
      <View style={{ flex: 1 }}>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          enabled
          behavior={Platform.OS === 'ios' ? 'padding' : undefined}
          keyboardVerticalOffset={statusBarHeight + 44}
        >
          <Animated.FlatList
            ref={(ref) => {
              setScrollView(ref);
            }}
            data={answers}
            renderItem={renderItem}
            keyboardShouldPersistTaps="handled"
            onContentSizeChange={scrolled}
            onLayout={scrolled}
            style={scrollStyles(keyboardPadding)}
            ListHeaderComponent={listHeaderComponent}
            keyExtractor={(item, index) => item + index}
          />

          {(!messageObj.sent || messageObj.recipients.length === 1) && (
            <InputMessage showAttach={false} onPressSendMessage={handleSendMessage} />
          )}
        </KeyboardAvoidingView>
      </View>
    </View>
  );
};

export default Layout;
