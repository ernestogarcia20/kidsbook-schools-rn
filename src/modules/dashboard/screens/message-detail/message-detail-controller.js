import React from 'react';
import { connect } from 'react-redux';
import {
  readMessagesAction,
  answerMessageAction,
  downloadFileAction,
} from '@dashboard/actions/messages.action';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import FileViewer from 'react-native-file-viewer';
import MessageDetailLayout from './components/message-detail-layout';

class MessageDetailController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const message = getParam('message', null);
    this.state = {
      message,
      messageResponse: null,
      answers: [],
      attachments: message?.attachments ? JSON.parse(message?.attachments) : [],
    };
  }

  componentDidMount() {
    this.readMessage();
  }

  readMessage = async () => {
    const { message } = this.state;
    const {
      _readMessagesAction,
      navigation: { state },
    } = this.props;

    try {
      const data = await _readMessagesAction(message.id);
      await state.params.updateMessageItem(message);

      this.setState({
        answers: data?.answers ? data?.answers : [],
        messageResponse: data,
        attachments: data?.attachments ? JSON.parse(data?.attachments) : [],
      });
    } catch (e) {}
  };

  handleSendMessage = async (text) => {
    const { _answerMessageAction, user } = this.props;
    const { answers, message } = this.state;
    if (!text) {
      return;
    }
    try {
      const data = await _answerMessageAction(user.schoolId, {
        parent_id: message.id,
        content: text,
      });
      this.setState({ messageResponse: data, answers: data?.answers ? data?.answers : answers });
    } catch (e) {}
  };

  onPressReport = (report) => {};

  handleDownloadAttach = async (attach) => {
    const { _downloadFileAction, user } = this.props;
    try {
      const { PictureDir } = await _downloadFileAction(user.token, attach);

      setTimeout(() => {
        FileViewer.open(PictureDir);
      }, 250);
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <MessageDetailLayout
          setState={this.handleChange}
          onPressReport={this.onPressReport}
          handleDownloadAttach={this.handleDownloadAttach}
          handleSendMessage={this.handleSendMessage}
          user={user}
          goBack={this.goBack}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _readMessagesAction: readMessagesAction,
  _answerMessageAction: answerMessageAction,
  _downloadFileAction: downloadFileAction,
})(MessageDetailController);
