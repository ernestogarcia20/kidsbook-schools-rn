import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import {
  getChildrenForSchoolAction,
  deleteChildrensAction,
} from '@dashboard/actions/children.actions';
import BaseComponent from '@components/BaseComponent';
import I18n from 'react-native-redux-i18n';
import { CHILDREN_DETAIL, ADD_PARENTS_TO_CHILD, ATTENDANCE } from '@utils/constants/navigates';
import ChildrenLayout from './components/children-layout';

class ChildrenController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      editList: false,
      selectAll: false,
      childrenList: [],
    };
  }

  componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const { childrenList } = this.props;
    if (JSON.stringify(prevProps.childrenList) !== JSON.stringify(childrenList)) {
      this.setListInState();
    }
  }

  setListInState = () => {
    const { childrenList } = this.props;
    const list = childrenList;
    const indexPage = childrenList.length;
    const pageMax = childrenList.length + 10;

    for (let i = indexPage; i < childrenList.length; i += 1) {
      if (i === pageMax) break;
      list.push(childrenList[i]);
    }
    list.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
    childrenList.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });

    this.setState({ childrenList: [...list], childrenListComplete: [...childrenList] });
  };

  init = async () => {
    const { _getChildrenForSchoolAction, user } = this.props;
    try {
      await _getChildrenForSchoolAction(user.schoolId, true);
      this.setListInState();
    } catch (e) {}
  };

  loadMoreChildren = () => {
    const { endLoadMessage, loadingMore, deleting } = this.state;
    if (endLoadMessage || loadingMore || deleting) {
      this.setState({ deleting: false });
      return;
    }
    this.setListInState();
  };

  updateListChild = async () => {
    const { _getChildrenForSchoolAction, user } = this.props;
    try {
      await _getChildrenForSchoolAction(user.schoolId, false);
      this.setListInState();
    } catch (e) {}
  };

  onPressAdd = () => {
    this.navigate(CHILDREN_DETAIL, {
      edit: false,
      updateListChild: () => this.updateListChild(),
      updateParentChild: this.setListInState,
    });
  };

  onPressEdit = () => {
    const { editList } = this.state;
    this.setState({ editList: !editList });
  };

  handleSelectAll = () => {
    const { selectAll, childrenList } = this.state;
    const newList = childrenList.map((child) => {
      return { ...child, checked: !selectAll };
    });
    this.setState({ selectAll: !selectAll, childrenList: newList });
  };

  handleSelectedItem = (item) => {
    const { childrenList } = this.state;
    const index = childrenList.findIndex((it) => it === item);
    if (index < 0) {
      return;
    }
    let child = childrenList[index];
    child = { ...child, checked: !child.checked };
    childrenList[index] = child;
    this.setState({ childrenList });
  };

  handleDelete = async (child = null) => {
    const { dispatch } = this.props;

    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('modalsText.deleteChild'),
        onPressAccept: () => this.deleteChildren(child),
      },
      dispatch
    );
  };

  deleteChildren = async (child) => {
    const { childrenList } = this.state;
    const { _deleteChildrensAction } = this.props;
    if (childrenList.length === 0) {
      return;
    }
    const filter = childrenList.filter((ch) => ch.checked);
    const ids = [];
    if (child) {
      ids.push(child.id);
    }
    const deleteIds = child ? ids : filter.map((ch) => ch.id);
    try {
      await _deleteChildrensAction(deleteIds);
      const newChildrenList = childrenList.filter((c) => deleteIds.indexOf(c.id) === -1);
      this.setState({ childrenList: newChildrenList, deleting: true });
    } catch (e) {}
  };

  handleDetailChildren = (item) => {
    this.navigate(CHILDREN_DETAIL, {
      edit: true,
      child: item,
      updateListChild: this.updateListChild,
    });
  };

  handleAttendance = (child, isExit = false) => {
    this.navigate(ATTENDANCE, { child, updateListChild: this.updateListChild, isExit });
  };

  handleParents = (child) => {
    this.navigate(ADD_PARENTS_TO_CHILD, { child, updateListChild: this.setListInState });
  };

  render() {
    const { user } = this.props;
    return (
      <Container>
        <ChildrenLayout
          openDrawer={this.openDrawer}
          user={user}
          state={this.state}
          onPressAdd={this.onPressAdd}
          onPressEdit={this.onPressEdit}
          handleDelete={this.handleDelete}
          loadMoreChildren={this.loadMoreChildren}
          handleDetailChildren={this.handleDetailChildren}
          handleSelectedItem={this.handleSelectedItem}
          handleAttendance={this.handleAttendance}
          handleSelectAll={this.handleSelectAll}
          handleParents={this.handleParents}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, children: { childrenList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  childrenList,
});

export default connect(mapStateToProps, {
  _getChildrenForSchoolAction: getChildrenForSchoolAction,
  _deleteChildrensAction: deleteChildrensAction,
})(ChildrenController);
