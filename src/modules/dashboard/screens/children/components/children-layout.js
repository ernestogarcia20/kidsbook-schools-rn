import React from 'react';
import { View, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import Colors from '@assets/colors';
import Header from '@components/Header';
import { getImageById, calculeAge } from '@utils/functions/globals';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/Feather';
import I18n from 'react-native-redux-i18n';
import Background from '@images/bg1.png';
import IconProfile from '@images/kb_profile.png';
import FastImage from '@components/FastImage';
import Search from '@components/Search';
import CheckBox from '@components/CheckBox';
import moment from 'moment';
import NoAvatar from '@images/no-avatar.jpg';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
  },
  tabs: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tabActive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3,
    borderBottomColor: Colors.GREEN,
  },
  tabInactive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentSearch: { padding: '7%' },
  paddingIcon: { paddingRight: 7 },
  center: { justifyContent: 'center', alignItems: 'center' },
  dateText: { position: 'absolute', paddingTop: 3 },
  contentIcons: {
    flexDirection: 'row',
    height: 25,
    marginTop: 5,
    alignItems: 'center',
  },
  buttonIconMargin: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.WHITE,
    marginLeft: 10,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  buttonIcon: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.BLUELIGHT,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  contentActionsEdit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonTrash: {
    width: 30,
    height: 30,
    borderRadius: 5,
    backgroundColor: Colors.RED,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '2%',
  },
  contentSelectedAll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '3.5%',
    marginTop: '7%',
    alignItems: 'center',
  },
  contentEdit: { paddingHorizontal: '5%', paddingTop: '5%' },

  renderItem: {
    height: 90,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentInfo: {
    flex: 0.7,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: '5%',
  },
});

const Layout = ({
  openDrawer,
  user,
  onPressEdit,
  onPressAdd,
  state,
  handleSelectAll,
  handleSelectedItem,
  handleDelete,
  handleDetailChildren,
  handleParents,
  loadMoreChildren,
  handleAttendance,
}) => {
  const { editList, selectAll, childrenList } = state;
  const renderItem = ({ item, index }) => {
    let loadImageError = false;
    const { name, avatar, use_diaper, use_bottle, birthday, familyIds, checked = false } = item;
    const urlImage = getImageById(avatar, user?.authData?.apiToken);
    const attendanceIn = item?.attendance?.find((x) => x.is_exit === 0);
    const attendanceExit = item?.attendance?.find((x) => x.is_exit === 1);
    const onErrorImage = () => {
      loadImageError = true;
    };
    return (
      <View
        style={[
          styles.renderItem,
          {
            backgroundColor: index % 2 ? '#EBEBEB' : '',
          },
        ]}
      >
        <View style={styles.contentInfo}>
          <TouchableOpacity onPress={() => handleDetailChildren(item)}>
            <FastImage
              source={loadImageError ? NoAvatar : { uri: urlImage }}
              style={{
                width: 50,
                height: 50,
                borderRadius: 100,
                backgroundColor: 'rgba(0,0,0,0.1)',
              }}

              onError={onErrorImage}
            />
          </TouchableOpacity>
          <View style={{ marginLeft: 15 }}>
            <Text align="left" fontSize={12} weight="Medium">
              {name}
            </Text>
            <View style={styles.contentIcons}>
              <View style={styles.paddingIcon}>
                <CustomIcon
                  name={use_diaper ? 'kb_diaper' : 'kb_diaper_no'}
                  width={20}
                  heigth={20}
                />
              </View>
              <View style={styles.paddingIcon}>
                <CustomIcon
                  name={use_bottle ? 'kb_bottle' : 'kb_bottle_no'}
                  width={20}
                  heigth={20}
                />
              </View>
              <View style={styles.paddingIcon}>
                <View style={styles.center}>
                  <CustomIcon name="kb_calendar_blank" width={20} heigth={20} />

                  <Text style={styles.dateText} fontSize={10}>
                    {calculeAge(birthday)}
                  </Text>
                </View>
              </View>
              <View style={styles.paddingIcon}>
                <View
                  style={[
                    styles.center,
                    {
                      paddingHorizontal: 20,
                      flexDirection: 'row',
                      backgroundColor: index % 2 ? Colors.WHITE : '#F2F1F1',
                      paddingVertical: 12,
                      borderRadius: 5,
                    },
                  ]}
                >
                  <View style={{ position: 'absolute' }}>
                    <CustomIcon name="kb_parents" width={15} heigth={15} />
                  </View>
                  <Text
                    style={{ position: 'absolute', right: 4, bottom: 0 }}
                    fontSize={11}
                    weight="SemiBold"
                  >
                    {familyIds.length}
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        {editList ? (
          <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: '8%' }}>
            <CheckBox check={checked} onPress={() => handleSelectedItem(item)} />
          </View>
        ) : (
          <View style={{ flexDirection: 'row', alignItems: 'center', paddingRight: '5%' }}>
            {!user.isTeacher && (
              <TouchableOpacity
                style={[styles.center, styles.buttonIcon]}
                onPress={() => handleDetailChildren(item)}
              >
                <Icon name="settings" size={15} color={Colors.WHITE} />
              </TouchableOpacity>
            )}
            {!user.isTeacher && (
              <TouchableOpacity
                onPress={() => handleDelete(item)}
                style={[styles.center, styles.buttonIconMargin, { backgroundColor: Colors.RED }]}
              >
                <Icon name="trash" size={15} color={Colors.WHITE} />
              </TouchableOpacity>
            )}
            {user.isTeacher && (
              <TouchableOpacity
                onPress={() => (!attendanceIn ? handleAttendance(item) : null)}
                style={[
                  styles.center,
                  styles.buttonIconMargin,
                  { width: 30, height: 30, backgroundColor: 'white', marginRight: 10 },
                ]}
              >
                {!attendanceIn ? (
                  <Icon name="upload" size={23} color={Colors.BLUEDARK} />
                ) : (
                  <View
                    style={[
                      { width: 25, height: 25, backgroundColor: Colors.GREEN, borderRadius: 5 },
                      styles.center,
                    ]}
                  >
                    <Icon name="check" size={18} color="white" />
                  </View>
                )}
              </TouchableOpacity>
            )}
            {user.isTeacher && attendanceIn && (
              <TouchableOpacity
                onPress={() => (!attendanceExit ? handleAttendance(item, true) : null)}
                style={[
                  styles.center,
                  styles.buttonIconMargin,
                  { width: 30, height: 30, backgroundColor: 'white', marginRight: 10 },
                ]}
              >
                {!attendanceExit ? (
                  <Icon name="log-out" size={23} color={Colors.ORANGE} />
                ) : (
                  <View
                    style={[
                      { width: 25, height: 25, backgroundColor: Colors.GREEN, borderRadius: 5 },
                      styles.center,
                    ]}
                  >
                    <Icon name="check" size={18} color="white" />
                  </View>
                )}
              </TouchableOpacity>
            )}
            {!user.isTeacher ? (
              <TouchableOpacity
                style={[styles.center, styles.buttonIconMargin]}
                onPress={() => handleParents(item)}
              >
                <CustomIcon name="kb_parents" width={15} heigth={15} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={[styles.center, { marginTop: 8 }]}
                onPress={() => handleParents(item)}
              >
                <FastImage
                  source={IconProfile}
                  style={{ width: 35, height: 35 }}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            )}
          </View>
        )}
      </View>
    );
  };
  const ListHeaderComponent = () => {
    return (
      <View>
        <Search hasBackground />
        {editList && (
          <View style={styles.contentEdit}>
            <View style={styles.contentActionsEdit}>
              <Text color={Colors.GRAY}>{I18n.t('actions')}:</Text>
              <TouchableOpacity style={styles.buttonTrash} onPress={() => handleDelete()}>
                <Icon name="trash" size={15} color={Colors.WHITE} />
              </TouchableOpacity>
            </View>
            <View style={styles.contentSelectedAll}>
              <Text>{I18n.t('selectAll')}</Text>
              <CheckBox onPress={handleSelectAll} check={selectAll} />
            </View>
          </View>
        )}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={I18n.t('menu_list.children')}
        onPress={openDrawer}
        menu
        edit={!user.isTeacher}
        add={!user.isTeacher}
        onPressEdit={onPressEdit}
        onPressAdd={onPressAdd}
      />
      {!user.isTeacher && (
        <View style={styles.tabs}>
          <TouchableOpacity style={styles.tabActive}>
            <Text color={Colors.GREEN}>Verificados</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabInactive}>
            <Text color={Colors.GRAY}>Por Verificar</Text>
          </TouchableOpacity>
        </View>
      )}
      <FlatList
        data={childrenList}
        extraData={childrenList}
        renderItem={renderItem}
        initialNumToRender={4}
        ListHeaderComponent={ListHeaderComponent}
        onEndReachedThreshold={3}
        onEndReached={loadMoreChildren}
        disableVirtualization
        keyExtractor={(item, index) => item + index}
      />
    </View>
  );
};

export default Layout;
