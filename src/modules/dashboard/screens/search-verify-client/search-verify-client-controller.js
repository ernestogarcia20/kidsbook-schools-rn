import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { searchClientsAction } from '@modules/dashboard/actions/verification.actions';
import { VERIFICATE_CLIENT } from '@utils/constants/navigates';
import SearchVerifyClientLayout from './components/search-verify-client-layout';

class SearchVerifyClientController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      rates: [],
    };
  }

  handleGoToVerifyClient = (client) => {
    const {
      navigation: { getParam },
    } = this.props;
    const refeshScreen = getParam('refeshScreen', () => {});
    this.navigate(VERIFICATE_CLIENT, { client, routeCenter: true, refeshScreen });
  };

  onChangeSearch = (text) => {
    const { _searchClientsAction, user } = this.props;

    if (!text) {
      this.setState({ rates: [] });
      return;
    }
    if (this.timeoutText) {
      clearTimeout(this.timeoutText);
      this.timeoutText = null;
      return;
    }

    this.timeoutText = setTimeout(async () => {
      try {
        const data = await _searchClientsAction(user.schoolId, text);
        this.setState({ rates: data.results });
      } catch (e) {}
    }, 250);
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <SearchVerifyClientLayout
          goBack={this.goBack}
          setState={this.handleChange}
          onChangeSearch={this.onChangeSearch}
          handleGoToVerifyClient={this.handleGoToVerifyClient}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, { _searchClientsAction: searchClientsAction })(
  SearchVerifyClientController
);
