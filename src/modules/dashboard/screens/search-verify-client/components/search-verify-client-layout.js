import React from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Header from '@components/Header';
import Search from '@components/Search';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';
import colors from '@assets/colors';
import { Styles } from '@assets/Styles';
import { validateRelationship } from '@utils/functions/globals';
import StarRating from 'react-native-star-rating';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentSearch: {
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  contentRelationship: {
    borderRadius: 3,
    borderColor: colors.GREEN,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 15,
    minWidth: 65,
    marginLeft: 5,
    marginBottom: 5,
  },
  contentInfo: {
    paddingTop: '7%',
    paddingBottom: '3%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    marginHorizontal: '5%',
  },
  paddingLeft: {
    paddingLeft: 10,
  },
});

const Layout = ({ goBack, state, onChangeSearch, handleGoToVerifyClient = () => {} }) => {
  const { rates } = state;
  const renderItem = ({ item }) => {
    const onPress = () => {
      handleGoToVerifyClient(item);
    };
    return (
      <TouchableOpacity style={styles.contentInfo} onPress={onPress} activeOpacity={0.7}>
        <View>
          <View style={[styles.paddingLeft, { flex: 0.7, justifyContent: 'flex-start' }]}>
            <Text align="left" color="#757575">
              {item?.name}
            </Text>
            <Text align="left" color="#757575" fontSize={12}>
              {item?.email}
            </Text>
            <View style={Styles.row}>
              <StarRating
                disabled
                maxStars={5}
                starSize={23}
                starStyle={{ marginRight: 5 }}
                fullStarColor="#FFC107"
              />
            </View>
          </View>
        </View>

        <View style={Styles.row}>
          <View style={styles.contentRelationship}>
            <Text
              color={colors.GREEN}
              fontSize={11}
              weight="SemiBold"
              style={{ textTransform: 'uppercase' }}
            >
              {item && !validateRelationship[item?.pivot?.type || 'Other']
                ? item?.pivot?.type
                : I18n.t(`listRelationship.${item?.pivot?.type ? item?.pivot?.type : 'Other'}`)}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.verifyClient')} onPress={goBack} />
      <View style={styles.contentSearch}>
        <Search onChangeText={onChangeSearch} />
      </View>
      <FlatList
        data={rates || []}
        renderItem={renderItem}
        keyExtractor={(item, index) => item + index}
      />
    </View>
  );
};

export default Layout;
