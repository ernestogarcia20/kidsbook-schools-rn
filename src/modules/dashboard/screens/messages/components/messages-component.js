import React from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import FastImage from '@components/FastImage';
import Search from '@components/Search';
import FloatingButton from '@components/FloatingButton';
import Icon from 'react-native-vector-icons/Feather';
import Background from '@images/bg1.png';
import ItemMessage from '@components/ItemMessage';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabs: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tabActive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3,
    borderBottomColor: Colors.GREEN,
    flexDirection: 'row',
  },
  tabInactive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

  contentSearch: { padding: '7%' },
  unreadMessage: {
    width: 23,
    height: 23,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.DANGER,
    position: 'absolute',
    right: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightIcon: {
    marginRight: 10,
  },
  buttonViews: {
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 2,
    backgroundColor: Colors.WHITE,
  },
  contentFilter: {
    height: 40,
    marginTop: 13,
    marginHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0, 0.2)',
  },
});

const Layout = ({
  openDrawer,
  user,
  state,
  setState,
  showModalSelection,
  handleTabFilter,
  loadMoreMessages,
  onChangeSearch,
  handlePressMessage,
  handleNewMessage,
}) => {
  const { messages, buttonFilter, messageUnread, listButtonTabs, filterMessage } = state;

  const renderItem = ({ item }) => {
    return <ItemMessage item={item} user={user} onPress={handlePressMessage} />;
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.messages')} menu onPress={openDrawer} />

      <View style={styles.tabs}>
        {listButtonTabs.map((tab, index) => {
          const countUnread = messageUnread ? messageUnread[tab.key] : null;
          return (
            <TouchableOpacity
              key={index}
              style={buttonFilter === tab.key ? styles.tabActive : styles.tabInactive}
              onPress={() => {
                handleTabFilter(tab.key);
              }}
            >
              <Text
                color={Colors.GREEN}
                fontSize={16}
                style={{ marginRight: countUnread < 1 ? 0 : 5 }}
              >
                {tab.label}
              </Text>
              {countUnread && countUnread !== 0 ? (
                <View style={styles.unreadMessage}>
                  <Text fontSize={11} color={Colors.WHITE}>
                    {countUnread}
                  </Text>
                </View>
              ) : null}
            </TouchableOpacity>
          );
        })}
      </View>
      <Search onChangeText={onChangeSearch} hasBackground />
      <View style={[styles.row, styles.contentFilter]}>
        <View>
          <Text fontSize={15} weight="Medium">
            {I18n.t('buttons.filter')}
          </Text>
        </View>
        <View>
          <TouchableOpacity
            style={[
              styles.row,
              {
                marginLeft: 10,
                marginBottom: 0,
              },
            ]}
            onPress={() => showModalSelection('relationship')}
          >
            <Text align="left">{filterMessage}</Text>
            <Icon
              name="triangle"
              size={12}
              style={{ transform: [{ rotate: '60deg' }], marginLeft: 10, marginBottom: 7 }}
            />
          </TouchableOpacity>
        </View>
      </View>
      <FlatList
        data={messages}
        extraData={messages}
        renderItem={renderItem}
        contentContainerStyle={{ paddingBottom: 20, paddingTop: 17 }}
        keyExtractor={(item, index) => item + index}
        disableVirtualization
        onEndReachedThreshold={6}
        onEndReached={loadMoreMessages}
        nestedScrollEnabled
      />
      <FloatingButton onPress={handleNewMessage} />
    </View>
  );
};

export default Layout;
