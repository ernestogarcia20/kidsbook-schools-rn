import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { getMessagesAction } from '@dashboard/actions/messages.action';
import { getFastAnswerAction } from '@dashboard/actions/fast-answers.actions';
import {
  MESSAGE_PARENTS,
  MESSAGE_INTERNAL,
  MESSAGE_ALL,
  MESSAGE_RECEIVED,
} from '@utils/constants/globals';
import { MESSAGE_DETAIL, CREATE_MESSAGE } from '@utils/constants/navigates';
import I18n from 'react-native-redux-i18n';
import MessagesLayout from './components/messages-component';

class MessagesController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      buttonFilter: MESSAGE_PARENTS,
      messages: [],
      filterInternal: MESSAGE_RECEIVED,
      filterMessage: I18n.t(`listMessageFilter.${MESSAGE_RECEIVED}`),
      messageUnread: null,
      loadingMore: false,
      endLoadMessage: false,
      page: 1,
      listButtonTabs: [
        { key: MESSAGE_PARENTS, label: I18n.t(`tabMessage.message`) },
        { key: MESSAGE_INTERNAL, label: I18n.t(`tabMessage.internal`) },
        { key: MESSAGE_ALL, label: I18n.t(`tabMessage.all`) },
      ],
    };
  }

  timeOut = null;

  componentDidMount() {
    this.loadFastAnswerAction();
    this.loadMessages();
  }

  loadFastAnswerAction = async () => {
    const { user, _getFastAnswerAction } = this.props;
    try {
      _getFastAnswerAction(user.schoolId);
    } catch (e) {}
  };

  loadMessages = async () => {
    const { _getMessagesAction, user } = this.props;
    const { buttonFilter, filterInternal, page, messages: messagesState } = this.state;

    this.setState({ loadingMore: page > 0 });
    try {
      const { newData, messages } = await _getMessagesAction(
        user.schoolId,
        buttonFilter,
        filterInternal,
        page,
        page === 1
      );
      let paginate = page;
      let endPaginate = false;
      if (messages.current_page !== messages.last_page) {
        paginate = messages.current_page === page ? page + 1 : messages.current_page;
      } else {
        paginate = messages.last_page - 1;
        endPaginate = true;
      }
      this.setState({
        messageUnread: newData,
        messages: messagesState.concat(messages.data),
        page: paginate,
        endLoadMessage: endPaginate,
        loadingMore: false,
      });
    } catch (e) {}
  };

  showModalSelection = () => {
    const { dispatch, user } = this.props;
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('modalsText.selectedFilter'),
        data: I18n.t(user.isTeacher ? 'listMessageFilterStringTeacher' : 'listMessageFilterString'),
        multiSelection: false,
        onPressAccept: (item) => this.filterMessage(item),
      },
      dispatch
    );
  };

  loadMoreMessages = () => {
    const { endLoadMessage, loadingMore } = this.state;
    if (endLoadMessage || loadingMore) {
      return;
    }
    this.loadMessages();
  };

  filterMessage = (item) => {
    this.setState(
      {
        filterInternal: item.value,
        filterMessage: I18n.t(`listMessageFilter.${item.value}`),
        messages: [],
        page: 1,
        loadingMore: false,
        endLoadMessage: false,
      },
      this.loadMessages
    );
  };

  handleTabFilter = (key) => {
    const buttonFilter = key;
    if (buttonFilter === this.state.buttonFilter) {
      return;
    }
    this.setState(
      { buttonFilter, page: 1, loadingMore: false, endLoadMessage: false, messages: [] },
      this.loadMessages
    );
  };

  onChangeSearch = (text) => {
    if (this.timeOut) {
      clearTimeout(this.timeOut);
    }
    /* this.timeOut = setTimeout(() => {
      alert(`buscar: ${text}`);
    }, 850); */
  };

  updateMessageItem = (message) => {
    const { messages, messageUnread, buttonFilter } = this.state;
    const indexMessage = messages.findIndex((x) => x.id === message.id);
    if (indexMessage < 0) {
      return;
    }

    let count = messageUnread[buttonFilter];
    if (message.new && count > 0) count -= 1;

    messageUnread[buttonFilter] = count;
    messages[indexMessage] = { ...message, new: false };
    this.setState({ messages, messageUnread });
  };

  loadNewMessage = () => {
    this.setState(
      { page: 1, loadingMore: false, endLoadMessage: false, messages: [] },
      this.loadMessages
    );
  };

  handleNewMessage = () => {
    this.navigate(CREATE_MESSAGE, { loadMessages: this.loadNewMessage });
  };

  handlePressMessage = (message) => {
    this.navigate(MESSAGE_DETAIL, { message, updateMessageItem: this.updateMessageItem });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <MessagesLayout
          setState={this.handleChange}
          openDrawer={this.openDrawer}
          showModalSelection={this.showModalSelection}
          handleTabFilter={this.handleTabFilter}
          handleNewMessage={this.handleNewMessage}
          handlePressMessage={this.handlePressMessage}
          loadMoreMessages={this.loadMoreMessages}
          onChangeSearch={this.onChangeSearch}
          user={user}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _getMessagesAction: getMessagesAction,
  _getFastAnswerAction: getFastAnswerAction,
})(MessagesController);
