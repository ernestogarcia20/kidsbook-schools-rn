import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Calendar from '@components/Calendar';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const Layout = ({
  openDrawer,
  state,
  onDayPress,
  setState,
  i18n: i18nProps,
  handleDeleteEvent,
  handleOnPressAdd,
}) => {
  const { dateSelected, monthSelected, newEvents, eventSeleted } = state;

  return (
    <View style={styles.container}>
      <Header
        title={I18n.t('menu_list.calendar')}
        onPress={openDrawer}
        menu
        add
        onPressAdd={handleOnPressAdd}
      />
      <ScrollView>
        <Calendar
          setState={setState}
          onDayPress={onDayPress}
          dateSelected={dateSelected}
          handleDeleteEvent={handleDeleteEvent}
          monthSelected={monthSelected}
          newEvents={newEvents}
          eventSeleted={eventSeleted}
        />
      </ScrollView>
    </View>
  );
};

export default Layout;
