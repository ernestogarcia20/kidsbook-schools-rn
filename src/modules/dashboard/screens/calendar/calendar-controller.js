import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { deleteEventAction, getEventCalendarAction } from '@dashboard/actions/calendar.actions';

import I18nT from 'react-native-redux-i18n';

import moment from 'moment';
import { CREATE_MESSAGE, CREATE_ACTIVITY } from '@utils/constants/navigates';
import CalendarLayout from './components/calendar-layout';

class CalendarController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      dateSelected: moment().format('YYYY-MM-DD'),
      monthSelected: moment().format('YYYY-MM-DD'),
      newEvents: {},
      eventSeleted: [],
    };
  }

  componentDidUpdate(prevProps) {
    const { events } = this.props;
    if (JSON.stringify(prevProps.events) !== JSON.stringify(events)) {
      this.formatEvents();
    }
  }

  componentDidMount() {
    this.formatEvents();
    this.init();
  }

  init = async () => {
    const { _getEventCalendarAction, user } = this.props;

    try {
      await _getEventCalendarAction(user.schoolId);
      this.formatEvents();
    } catch (e) {}
  };

  formatEvents = () => {
    const { events } = this.props;
    if (!events) {
      return;
    }
    let newEvents = Object.keys(events).map((date) => {
      return {
        [date]: {
          dots: events[date].map((event) => {
            if (event.is_event === 1) {
              return { ...event, color: '#57C1D9' };
            }
            return { ...event, color: 'red' };
          }),
        },
      };
    });
    newEvents = Object.assign({}, ...newEvents);
    this.setState({ newEvents });
  };

  onDayPress = (day) => {
    const { newEvents } = this.state;
    const eventSeleted = newEvents[day.dateString] || null;
    this.setState({ dateSelected: day.dateString, eventSeleted: eventSeleted?.dots || [] });
  };

  handleDeleteEvent = (event) => {
    const { dispatch } = this.props;

    this.showModalConfirm(
      {
        visible: true,
        title: I18nT.t('modalsText.deleteEvent'),
        onPressAccept: () => this.deleteEvent(event),
      },
      dispatch
    );
  };

  deleteEvent = async (event) => {
    const { _deleteEventAction } = this.props;
    const { newEvents } = this.state;

    try {
      await _deleteEventAction(event.id);
      const events = newEvents[event.date].dots;
      const index = events.findIndex((e) => e.id === event.id);
      if (index > -1) {
        events.splice(index, 1);
      }
      console.log('EVENTOSS', events);
      newEvents[event.date] = events;
      const eventSeleted = newEvents[event.date] || null;
      this.setState({ newEvents: { ...newEvents }, eventSeleted }, this.init);
      // const eventSeleted = childrenList.filter((c) => deleteIds.indexOf(c.id) === -1);
    } catch (e) {}
  };

  addEventOrActivity = (option) => {
    this.navigate(option.value === 'event' ? CREATE_MESSAGE : CREATE_ACTIVITY);
  };

  handleOnPressAdd = () => {
    const { dispatch } = this.props;

    this.showModalConfirm(
      {
        visible: true,
        title: I18nT.t('modalsText.createEventActivity'),
        subTitle: I18nT.t('optionCamera.selectOption'),
        data: I18nT.t('listCreateCalendar'),
        onPressAccept: (result) => this.addEventOrActivity(result),
      },
      dispatch
    );
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user, events, i18n } = this.props;
    return (
      <Container showBackground>
        <CalendarLayout
          openDrawer={this.openDrawer}
          setState={this.handleChange}
          onDayPress={this.onDayPress}
          handleDeleteEvent={this.handleDeleteEvent}
          handleOnPressAdd={this.handleOnPressAdd}
          i18n={i18n}
          events={events}
          user={user}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, calendar: { events }, i18n }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  events,
  i18n,
});

export default connect(mapStateToProps, {
  _getEventCalendarAction: getEventCalendarAction,
  _deleteEventAction: deleteEventAction,
})(CalendarController);
