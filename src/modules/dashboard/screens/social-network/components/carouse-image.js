import React from 'react';
import { View, StyleSheet, FlatList, Dimensions, Animated } from 'react-native';

import FastImage from '@components/FastImage';
import { getImagePost } from '@utils/functions/globals';
import Colors from '@assets/colors';
import PropTypes from 'prop-types';
import normalize from 'react-native-normalize';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  contentDot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },
  flatList: {
    backgroundColor: Colors.BLACK,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
});

const Layout = ({ data, user, height = 180 }) => {
  const scrollX = new Animated.Value(0);
  const position = Animated.divide(scrollX, width * 0.925);
  const renderItem = ({ item }) => {
    return (
      <FastImage
        source={{ uri: getImagePost(item, user.token) }}
        style={{
          flex: 1,
          width: width * 0.947,
          height: normalize(height),
          backgroundColor: 'black',
          borderTopLeftRadius: 15,
          borderTopRightRadius: 15,
        }}
      />
    );
  };
  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        horizontal
        snapToAlignment="center"
        scrollEventThrottle={16}
        decelerationRate="fast"
        renderItem={renderItem}
        style={styles.flatList}
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => item + index}
        onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
          useNativeDriver: false,
        })}
      />
      <View style={styles.contentDot}>
        {data.map((item, index) => {
          const color = position.interpolate({
            inputRange: [index - 1, index, index + 1],
            outputRange: ['#C4C4C4', '#1E9ECA', '#C4C4C4'],
            extrapolate: 'clamp',
          });
          return (
            <Animated.View
              style={{
                width: 8,
                height: 8,
                backgroundColor: color,
                borderRadius: 100,
                marginRight: 5,
              }}
            />
          );
        })}
      </View>
    </View>
  );
};

Layout.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  user: PropTypes.objectOf(PropTypes.object),
  height: PropTypes.number,
};

Layout.defaultProps = {
  data: [],
  user: {},
  height: 180,
};

export default Layout;
