import React from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import FastImage from '@components/FastImage';
import { getImagePost, getImageById } from '@utils/functions/globals';
import Colors from '@assets/colors';
import Text from '@components/Text';
import Icon from 'react-native-vector-icons/Feather';
import ActionWidget from '@components/ActionWidget';
import FloatingButton from '@components/FloatingButton';
import LikeIcon from '@images/megusta.png';
import CommentIcon from '@images/charla.png';
import { Popover, PopoverController } from 'react-native-modal-popover';
import { Styles } from '@assets/Styles';
import CarouselImage from './carouse-image';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentDot: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
  contentItem: {
    paddingBottom: '7%',
    marginVertical: 10,
    backgroundColor: Colors.WHITE,
    marginHorizontal: 10,
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
  },
  contentPost: { marginTop: 15, marginHorizontal: '7%' },
  contentHorizontal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentImage: { flexDirection: 'row', alignItems: 'center' },
  imageProfile: {
    width: 50,
    height: 50,
    borderRadius: 100,
    backgroundColor: '#DADADA',
    marginRight: 15,
  },
  contentWidget: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#DADADA',
    borderRadius: 20,
  },
  contentIconWidget: {
    width: 27,
    height: 27,
    backgroundColor: '#57C1D9',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  spacingWidget: { marginLeft: 10 },
  contentControls: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
  contentWidgetControls: { flexDirection: 'row', alignItems: 'center' },
});

const Layout = ({
  openDrawer,
  posts,
  user,
  handleNewPost,
  handleLikePost,
  handleDetailPost,
  handleDeletePost,
  handleEditPost,
}) => {
  const renderItem = ({ item }) => {
    const { img_urls, client, content, created_at, likes, comments } = item;
    const imgParse = JSON.parse(img_urls);
    const urlImage = getImageById(client?.avatar, user?.authData?.apiToken);

    const onPressLike = () => {
      handleLikePost(item);
    };
    const onPressDetail = () => {
      handleDetailPost(item);
    };
    const onPressEditPost = () => {
      handleEditPost(item);
    };
    const onPressDeletePost = () => {
      handleDeletePost(item);
    };
    return (
      <PopoverController>
        {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
          <>
            <View style={styles.contentItem}>
              <CarouselImage data={imgParse} user={user} height={380} />

              <View style={styles.contentPost}>
                <View style={styles.contentHorizontal}>
                  <View style={styles.contentImage}>
                    <FastImage source={{ uri: urlImage }} style={styles.imageProfile} />
                    <Text weight="Medium" fontSize={13}>
                      {client.name}
                    </Text>
                  </View>
                  <TouchableOpacity ref={setPopoverAnchor} onPress={openPopover}>
                    <Icon name="more-vertical" size={25} />
                  </TouchableOpacity>
                </View>
                <View style={[{ marginVertical: '5%' }, Styles.row]}>
                  <Text fontSize={13} align="left">
                    {content}
                  </Text>
                  <TouchableOpacity
                    onPress={onPressDetail}
                    hitSlop={{ top: 30, bottom: 20, left: 10, right: 30 }}
                  >
                    <Text color={Colors.GREEN} weight="Medium">
                      {' '}
                      ...Ver mas
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.contentControls}>
                  <View style={styles.contentWidgetControls}>
                    <ActionWidget
                      image={LikeIcon}
                      color="#57C1D9"
                      text={likes}
                      onPress={onPressLike}
                    />
                    <View style={styles.spacingWidget}>
                      <ActionWidget
                        image={CommentIcon}
                        iconSize={15}
                        color="#CF742E"
                        text={comments}
                        onPress={onPressDetail}
                      />
                    </View>
                  </View>
                  <Text fontSize={13}>{created_at}</Text>
                </View>
              </View>
              <Popover
                visible={popoverVisible}
                fromRect={popoverAnchorRect}
                onClose={closePopover}
                placement="auto"
                contentStyle={[{ justifyContent: 'center' }]}
              >
                <TouchableOpacity
                  style={{ height: 50, justifyContent: 'center', paddingHorizontal: 20 }}
                  onPress={() => {
                    closePopover();
                    setTimeout(() => {
                      onPressDeletePost();
                    }, 600);
                  }}
                >
                  <Text color={Colors.DANGER} weight="SemiBold">
                    Eliminar
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ height: 50, justifyContent: 'center' }}
                  onPress={() => {
                    closePopover();
                    onPressEditPost();
                  }}
                >
                  <Text color={Colors.GREEN} weight="SemiBold" fontSize={16}>
                    Editar
                  </Text>
                </TouchableOpacity>
              </Popover>
            </View>
          </>
        )}
      </PopoverController>
    );
  };

  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.socialNetwork')} onPress={openDrawer} menu />
      <FlatList data={posts} renderItem={renderItem} keyExtractor={(item, index) => item + index} />
      <FloatingButton color="#CF742E" iconName="md-send" iconLib onPress={handleNewPost} />
    </View>
  );
};

export default Layout;
