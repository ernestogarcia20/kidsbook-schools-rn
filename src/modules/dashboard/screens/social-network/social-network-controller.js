import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {
  getFeedAction,
  likePostAction,
  deletePostAction,
} from '@dashboard/actions/social-network.actions';
import { CREATE_SOCIAL_NETWORK, DETAIL_SOCIAL_NETWORK } from '@utils/constants/navigates';
import SocialNetworkLayout from './components/social-network-layout';

class SocialNetworkController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.init();
  }

  handleNewPost = () => {
    this.navigate(CREATE_SOCIAL_NETWORK, { updatedList: this.init });
  };

  init = async () => {
    const { _getFeedAction, user } = this.props;

    try {
      await _getFeedAction(user.schoolId);
    } catch (e) {}
  };

  handleLikePost = async (post) => {
    const { _likePostAction, user } = this.props;

    try {
      const data = await _likePostAction(post.id);
    } catch (e) {}
  };

  handleDeletePost = async (post) => {
    const { _deletePostAction } = this.props;

    try {
      await _deletePostAction(post.id);
    } catch (e) {}
  };

  handleEditPost = (post) => {
    this.navigate(CREATE_SOCIAL_NETWORK, { edit: true, post, updatedList: this.init });
  };

  handleDetailPost = (post) => {
    this.navigate(DETAIL_SOCIAL_NETWORK, { post });
  };

  render() {
    const { user, posts } = this.props;
    return (
      <Container showBackground>
        <SocialNetworkLayout
          openDrawer={this.openDrawer}
          handleNewPost={this.handleNewPost}
          handleLikePost={this.handleLikePost}
          handleDetailPost={this.handleDetailPost}
          handleDeletePost={this.handleDeletePost}
          handleEditPost={this.handleEditPost}
          posts={posts}
          user={user}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, socialNetwork: { posts } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  posts,
});

export default connect(mapStateToProps, {
  _getFeedAction: getFeedAction,
  _likePostAction: likePostAction,
  _deletePostAction: deletePostAction,
})(SocialNetworkController);
