import React from 'react';
import {
  View,
  StyleSheet,
  Switch,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import Button from '@components/Button';
import { FEMALE_GENDER, MALE_GENDER } from '@utils/constants/globals';
import Colors from '@assets/colors';
import FastImage from '@components/FastImage';
import NoAvatar from '@images/no-avatar.jpg';
import Icon from 'react-native-vector-icons/Feather';
import CustomIcon from '@components/Icon';
import TextField from '@components/text-input-md';
import { getImageById } from '@utils/functions/globals';
import I18n from 'react-native-redux-i18n';
import { Styles } from '@assets/Styles';

const { width } = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    width: width / 2,
    height: width / 2,
    zIndex: -1,
    borderWidth: 2,
    borderColor: Colors.GREEN,
    borderRadius: 200,
  },
  borderAvatar: {
    position: 'absolute',
    width: width / 1.9,
    height: width / 1.9,
    zIndex: 1,
  },
  contentAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    marginBottom: '5%',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  marginText: { marginRight: 5 },
  contentInput: { paddingHorizontal: '5%' },
  contentDatePicker: { paddingHorizontal: '5%', marginBottom: '4%', marginTop: -5 },
  contentSwitch: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: '5%',
    paddingTop: '5%',
    borderBottomWidth: 0.5,
    paddingBottom: 13,
  },
  contentInfo: {
    height: 40,
    backgroundColor: '#515151',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: '5%',
    marginTop: '5%',
  },
  contentContainerStyle: { paddingBottom: 20 },
  contentButtonEdit: { paddingTop: '7%', paddingHorizontal: '6%' },
});

const Layout = ({
  goBack,
  edit,
  child,
  setState,
  useDiaper,
  useBottle,
  gender,
  fullName,
  birthdate,
  knownAllergies,
  emergencyNumber,
  authorizedPersons,
  personDocument,
  updateImage,
  user,
  handleEditChildren,
  handleAddChildren,
  handleChangeImageProfile,
}) => {
  return (
    <View style={styles.container}>
      <Header
        title={
          edit
            ? I18n.t(`menu_list.${child?.gender === FEMALE_GENDER ? 'editGirl' : 'editChild'}`)
            : I18n.t('buttons.addChild')
        }
        onPress={goBack}
      />
      <KeyboardAvoidingView
        style={Styles.flexible}
        behavior={Platform.OS === 'android' ? undefined : 'position'}
        enabled
      >
        <ScrollView
          contentContainerStyle={styles.contentContainerStyle}
          keyboardShouldPersistTaps="handled"
        >
          <TouchableOpacity style={styles.contentAvatar} onPress={handleChangeImageProfile}>
            <FastImage
              source={{ uri: getImageById(updateImage || child?.avatar, user.token) }}
              style={styles.avatar}
            />
          </TouchableOpacity>
          <View style={styles.row}>
            <Text style={styles.marginText}>{I18n.t('pressToChange')}</Text>
            <Icon name="arrow-up" size={19} color={Colors.BLACK} />
          </View>

          <View
            style={[
              styles.row,
              { justifyContent: 'space-between', alignItems: 'center', height: 40, marginTop: 15 },
            ]}
          >
            <TouchableOpacity
              onPress={() => setState('gender', MALE_GENDER)}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 40,
                flex: 1,
                borderWidth: 1,
                borderColor: gender === FEMALE_GENDER ? '#D95786' : Colors.BLUELIGHT,
                backgroundColor: gender === FEMALE_GENDER ? Colors.WHITE : Colors.BLUELIGHT,
              }}
            >
              <CustomIcon name="kb_babyBoy" width={30} heigth={30} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setState('gender', FEMALE_GENDER)}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                height: 40,
                flex: 1,
                borderWidth: 1,
                borderColor: gender === FEMALE_GENDER ? '#D95786' : Colors.BLUELIGHT,
                backgroundColor: gender === FEMALE_GENDER ? '#D95786' : Colors.WHITE,
              }}
            >
              <CustomIcon name="kb_babyBoy" width={30} heigth={30} />
            </TouchableOpacity>
          </View>
          <View style={styles.contentInput}>
            <TextField
              label={I18n.t('fullName')}
              highlightColor={Colors.GREEN}
              value={fullName}
              onChangeText={(text) => setState('fullName', text)}
            />
          </View>
          <View style={styles.contentDatePicker}>
            <TextField
              label={I18n.t('birthday')}
              highlightColor={Colors.GREEN}
              value={birthdate}
              datePicker
              onChangeText={(text) => setState('birthdate', text)}
            />
          </View>
          <View style={styles.contentSwitch}>
            <Text weight="SemiBold">{I18n.t('useDiaper')}: </Text>
            <Switch
              trackColor={{ false: '#767577', true: 'rgba(0, 105, 107, 0.4)' }}
              thumbColor={useDiaper ? '#00696B' : '#f4f3f4'}
              style={{ marginRight: 10 }}
              onValueChange={() => setState('useDiaper', !useDiaper)}
              value={useDiaper}
            />
          </View>
          <View style={styles.contentSwitch}>
            <Text weight="SemiBold">{I18n.t('useBottle')}: </Text>
            <Switch
              trackColor={{ false: '#767577', true: 'rgba(0, 105, 107, 0.4)' }}
              thumbColor={useBottle ? '#00696B' : '#f4f3f4'}
              style={{ marginRight: 10 }}
              onValueChange={() => setState('useBottle', !useBottle)}
              value={useBottle}
            />
          </View>
          <View style={styles.contentInfo}>
            <Text weight="SemiBold" color={Colors.WHITE}>
              {I18n.t('addInfo')}
            </Text>
          </View>
          <View style={styles.contentInput}>
            <TextField
              label={I18n.t('knownAllergies')}
              highlightColor={Colors.GREEN}
              value={knownAllergies}
              onChangeText={(text) => setState('knownAllergies', text)}
            />
          </View>
          <View style={styles.contentInput}>
            <TextField
              label={I18n.t('emergencyNumber')}
              highlightColor={Colors.GREEN}
              value={emergencyNumber}
              onChangeText={(text) => setState('emergencyNumber', text)}
            />
          </View>
          <View style={styles.contentInput}>
            <TextField
              label={I18n.t('authorizedPersons')}
              highlightColor={Colors.GREEN}
              value={authorizedPersons}
              onChangeText={(text) => setState('authorizedPersons', text)}
            />
          </View>
          <View style={styles.contentInput}>
            <TextField
              label={I18n.t('personDocument')}
              highlightColor={Colors.GREEN}
              value={personDocument}
              onChangeText={(text) => setState('personDocument', text)}
            />
          </View>
          <View style={styles.contentButtonEdit}>
            <Button
              title={I18n.t(`buttons.${edit ? 'edit' : 'addChild'}`)}
              onPress={() => (edit ? handleEditChildren() : handleAddChildren())}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

export default Layout;
