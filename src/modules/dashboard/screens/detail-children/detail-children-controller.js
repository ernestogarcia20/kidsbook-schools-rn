import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { editChildrenAction, addChildrenAction } from '@dashboard/actions/children.actions';
import ImagePicker from 'react-native-image-picker';
import I18n from 'react-native-redux-i18n';
import { MALE_GENDER } from '@utils/constants/globals';
import { ADD_PARENTS_TO_CHILD } from '@utils/constants/navigates';
import ChildrenDetailLayout from './components/detail-children-layout';

class ChildrenDetailController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const edit = getParam('edit', false);
    const child = getParam('child', null);
    console.log(child);
    this.state = {
      edit,
      child,
      useDiaper: child ? child?.use_diaper : false,
      useBottle: child ? child?.use_bottle : false,
      gender: child ? child?.gender : MALE_GENDER,
      fullName: child?.name,
      birthdate: child ? child?.birthday : '',
      knownAllergies: '',
      emergencyNumber: '',
      authorizedPersons: '',
      personDocument: '',
      updateImage: null,
    };
  }

  handleAddChildren = async () => {
    const {
      _addChildrenAction,
      navigation: { state },
      dispatch,
      user,
    } = this.props;
    const { useDiaper, useBottle, fullName, birthdate, gender, updateImage } = this.state;

    if (!fullName || !birthdate || !gender) {
      return;
    }

    const obj = {
      use_bottle: useBottle,
      use_diaper: useDiaper,
      birthday: birthdate,
      name: fullName,
      gender,
      illnesses: '',
      allergies: '',
      avatar: updateImage,
      school_id: user.schoolId,
    };
    try {
      const { data } = await _addChildrenAction(obj);
      this.showModalConfirm(
        {
          visible: true,
          title: I18n.t('modalsText.addParentsToChild'),
          onPressAccept: () => this.goToAddParent(data),
          onPressCancel: () => this.goBack(),
        },
        dispatch
      );
      await state.params.updateListChild();
    } catch (e) {}
  };

  goToAddParent = (child) => {
    const {
      navigation: { state },
    } = this.props;
    // console.log(JSON.stringify(child));
    this.navigate(ADD_PARENTS_TO_CHILD, {
      child: {
        ...child,
        familyIds: [],
        classrooms: [],
        reportedToday: [],
        attendance: [],
        relationships: [],
      },
      updateParentChild: state.params.updateParentChild,
    });
  };

  handleChangeImageProfile = () => {
    const options = {
      title: I18n.t('optionCamera.selectOption'),
      cancelButtonTitle: I18n.t('optionCamera.cancel'),
      customButtons: [{ name: 'camera', title: I18n.t('optionCamera.camera') }],
      takePhotoButtonTitle: '',
      chooseFromLibraryButtonTitle: I18n.t('optionCamera.gallery'),
      quality: 1.0,
      mediaType: 'photo',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: false,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        ImagePicker.launchCamera(
          {
            mediaType: 'photo',
            includeBase64: false,
            maxHeight: 500,
            maxWidth: 500,
          },
          (response2) => {
            this.setState({ updateImage: `data:image/jpg;base64,${response2.data}` });
          }
        );
      }
      if (!response.data) {
        return;
      }
      this.setState({ updateImage: `data:image/jpg;base64,${response.data}` });
    });
  };

  handleEditChildren = async () => {
    const {
      _editChildrenAction,
      navigation: { state },
    } = this.props;
    const {
      useDiaper,
      useBottle,
      fullName,
      birthdate,
      gender,
      child,
      updateImage,
      knownAllergies,
      emergencyNumber,
      authorizedPersons,
      personDocument,
    } = this.state;
    const obj = {
      use_bottle: useBottle ? 1 : 0,
      use_diaper: useDiaper ? 1 : 0,
      birthday: birthdate,
      name: fullName,
      gender,
      illnesses: '',
      allergies: knownAllergies,
      avatar: updateImage,
    };
    try {
      await _editChildrenAction(child.id, { ...child, ...obj });
      this.goBack();
      await state.params.updateListChild();
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <ChildrenDetailLayout
          setState={this.handleChange}
          goBack={this.goBack}
          user={user}
          handleEditChildren={this.handleEditChildren}
          handleAddChildren={this.handleAddChildren}
          handleChangeImageProfile={this.handleChangeImageProfile}
          {...this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _editChildrenAction: editChildrenAction,
  _addChildrenAction: addChildrenAction,
})(ChildrenDetailController);
