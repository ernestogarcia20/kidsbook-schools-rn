import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { showMessage } from 'react-native-flash-message';
import I18n from 'react-native-redux-i18n';
import { saveAttendanceAction } from '@dashboard/actions/children.actions';
import moment from 'moment';
import AttendanceLayout from './component/attendance-layout';

class AttendanceController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const child = getParam('child', {});
    const isExit = getParam('isExit', false);
    this.state = {
      child,
      nameResponsable: '',
      temperatureResponsable: '',
      temperatureChild: '',
      observations: '',
      isExit,
    };
  }

  handleSaveAttendance = async () => {
    const {
      _saveAttendanceAction,
      navigation: { state },
      user,
    } = this.props;
    const {
      nameResponsable,
      temperatureResponsable,
      temperatureChild,
      observations,
      child,
      isExit,
    } = this.state;
    try {
      const data = {
        children_id: child.id,
        school_id: user.schoolId,
        responsible_name: nameResponsable,
        temperature_responsible: temperatureResponsable,
        temperature_children: temperatureChild,
        observations,
        save_at: moment().format('YYYY-MM-DD HH:mm:ss'),
        is_exit: isExit ? 1 : 0,
      };
      await _saveAttendanceAction(data);
      this.goBack();
      if (state.params.updateListChild) {
        state.params.updateListChild();
      }
    } catch (e) {
      console.log(e);
    }
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <AttendanceLayout
          goBack={this.goBack}
          setState={this.handleChange}
          handleSaveAttendance={this.handleSaveAttendance}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _saveAttendanceAction: saveAttendanceAction,
})(AttendanceController);
