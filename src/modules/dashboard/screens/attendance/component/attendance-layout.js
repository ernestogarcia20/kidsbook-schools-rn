import React, { useState } from 'react';
import { View, StyleSheet, TextInput, Platform, ScrollView } from 'react-native';
import I18n from 'react-native-redux-i18n';
import Header from '@components/Header';
import Button from '@components/Button';
import TextField from '@components/text-input-md';
import Emoji from '@components/Emoji';
import Text from '@components/Text';
import colors from '@assets/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    paddingHorizontal: '6%',
    paddingBottom: '5%',
  },
  contentButton: {
    paddingHorizontal: '6%',
    paddingBottom: '5%',
  },
  paddingHorizontal: {
    paddingHorizontal: 0,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  separator: {
    marginTop: 5,
  },
  contentLine: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)',
  },
  input: { flex: 1, minHeight: 80, maxHeight: 190, marginLeft: 15 },
  inputMultiline: {
    height: null,
    minHeight: 90,
    paddingTop: Platform.OS === 'ios' ? 10 : 5,
    paddingBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Layout = ({ goBack, setState, state, handleSaveAttendance }) => {
  const { observations, nameResponsable, temperatureResponsable, temperatureChild, isExit } = state;

  const [scrollView, setScrollView] = useState(null);
  const [cursorPosition, setCursorPosition] = useState(null);
  const scrolled = () => {
    if (!scrollView) {
      return;
    }
    setTimeout(() => {
      scrollView.scrollTo({ animated: true, y: 130 });
    }, 1);
  };
  const onEmoji = (emoji) => {
    const formatText = cursorPosition
      ? [observations.slice(0, cursorPosition), emoji, observations.slice(cursorPosition)].join('')
      : observations.concat(emoji);
    setState('observations', formatText);
    setCursorPosition(null);
  };
  const onSelectionChange = ({ nativeEvent: { selection } }) => {
    setCursorPosition(selection.start);
  };

  const Input = ({
    multiline = false,
    title,
    placeholder,
    numeric = false,
    value,
    attrName,
    updateMasterState,
  }) => {
    return (
      <View style={[styles.paddingHorizontal]}>
        <View style={[styles.row, styles.separator, styles.contentLine]}>
          <Text>{title}: </Text>
          {!multiline && (
            <TextInput
              style={styles.input}
              multiline={multiline}
              placeholder={placeholder}
              value={value}
              keyboardType={numeric ? 'number-pad' : 'default'}
              onChangeText={(text) => updateMasterState(attrName, text)}
              placeholderTextColor="#909090"
            />
          )}
        </View>
        {multiline && (
          <View style={[styles.contentLine, styles.inputMultiline]}>
            <Emoji onEmoji={onEmoji}>
              <TextInput
                textAlignVertical="top"
                style={styles.input}
                multiline={multiline}
                onSelectionChange={onSelectionChange}
                onFocus={scrolled}
                placeholder={placeholder}
                placeholderTextColor="#909090"
                value={value}
                onChangeText={(text) => updateMasterState(attrName, text)}
              />
            </Emoji>
          </View>
        )}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.attendance')} onPress={goBack} />
      <ScrollView contentContainerStyle={styles.content}>
        <View style={styles.contentInput}>
          {!isExit ? (
            <TextField
              label="Acompañado por: "
              highlightColor={colors.GREEN}
              value={nameResponsable}
              onChangeText={(text) => setState('nameResponsable', text)}
            />
          ) : null}
          {!isExit &&
            Input({
              multiline: false,
              title: 'Temperatura del acompañante',
              placeholder: '37.0c°',
              numeric: true,
              value: temperatureResponsable,
              attrName: 'temperatureResponsable',
              updateMasterState: setState,
            })}
          {Input({
            multiline: false,
            title: 'Temperatura del niñ@',
            placeholder: '37.0c°',
            numeric: true,
            value: temperatureChild,
            attrName: 'temperatureChild',
            updateMasterState: setState,
          })}
        </View>
        {Input({
          multiline: true,
          title: 'Observaciones',
          placeholder: 'Escriba las observaciones aqui',
          value: observations,
          attrName: 'observations',
          updateMasterState: setState,
        })}
      </ScrollView>
      <View style={styles.contentButton}>
        <Button title="Guardar asistencia" onPress={handleSaveAttendance} />
      </View>
    </View>
  );
};

export default Layout;
