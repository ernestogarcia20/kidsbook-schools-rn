import React from 'react';
import { View, StyleSheet, FlatList, TextInput } from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';
import CarouseImage from '@components/CarouseImage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentValue: {
    height: 60,
    width: 60,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
    borderRadius: 100,
  },
});

const Layout = ({ goBack, state, user, handleEditActivty, handleAttachmentFile }) => {
  const { activity } = state;

  const ListHeaderComponent = () => {
    return (
      <View style={{}}>
        <View style={styles.rowLarge}>
          <Text color="#3C3C3C" align="left" weight="Medium" fontSize={19}>
            {activity?.title}
          </Text>
          <View style={[styles.contentValue, styles.center]}>
            <Text color="#3C3C3C" weight="Medium" fontSize={19}>
              {activity.value}
            </Text>
          </View>
        </View>
        <View style={{ marginTop: 20 }}>
          <Text align="justify" weight="Light" selectable>
            {activity.description}
          </Text>
        </View>
        <View style={{ marginTop: 20 }}>
          <CarouseImage
            user={user}
            data={JSON.parse(activity.attachments)}
            activity
            width={332}
            height={154}
            onPressAttach={handleAttachmentFile}
          />
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={I18n.t('menu_list.activityDetail')}
        onPress={goBack}
        detail
        onPressDetail={handleEditActivty}
      />

      <FlatList
        ListHeaderComponent={ListHeaderComponent}
        contentContainerStyle={{ paddingHorizontal: 25, paddingTop: '5%' }}
      />
    </View>
  );
};

export default Layout;
