import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { CREATE_ACTIVITY } from '@utils/constants/navigates';
import { downloadFileActivityAction } from '@dashboard/actions/activities.actions';
import FileViewer from 'react-native-file-viewer';
import ActivityDetailsLayout from './components/activity-details-layout';

class ActivityDetailsController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const activity = getParam('activity', {});
    this.state = {
      activity,
    };
  }

  updateActivity = (activity) => {
    const {
      navigation: { getParam },
    } = this.props;
    const loadActivities = getParam('loadActivities', {});
    loadActivities(false);
    this.setState({ activity });
  };

  handleEditActivty = () => {
    const { activity } = this.state;
    const {
      navigation: { getParam },
    } = this.props;
    const loadActivities = getParam('loadActivities', {});
    if (!activity) {
      return;
    }
    this.navigate(CREATE_ACTIVITY, {
      edit: true,
      activity,
      updateActivity: this.updateActivity,
      loadActivities,
    });
  };

  handleAttachmentFile = async (attach) => {
    const { _downloadFileActivityAction } = this.props;
    if (attach?.url) {
      try {
        const { PictureDir } = await _downloadFileActivityAction(attach.url, attach.attachment);

        setTimeout(() => {
          FileViewer.open(PictureDir);
        }, 150);
      } catch (e) {}
      return;
    }
    const decUrl = decodeURI(attach?.fileData?.uri);
    FileViewer.open(decUrl);
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <ActivityDetailsLayout
          user={this.props.user}
          goBack={this.goBack}
          handleAttachmentFile={this.handleAttachmentFile}
          setState={this.handleChange}
          handleEditActivty={this.handleEditActivty}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _downloadFileActivityAction: downloadFileActivityAction,
})(ActivityDetailsController);
