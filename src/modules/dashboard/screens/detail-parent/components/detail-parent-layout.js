import React from 'react';
import { View, StyleSheet, Dimensions, ScrollView } from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';
import { getImageById } from '@utils/functions/globals';
import FastImage from '@components/FastImage';
import Colors from '@assets/colors';
import NoAvatar from '@images/no-avatar.jpg';
import { Styles } from '@assets/Styles';

const { width } = Dimensions.get('window');
const validateRelationship = {
  Father: 'Padre',
  Mother: 'Madre',
  Uncle: 'Tío',
  Brother: 'Hermano',
  Sister: 'Hermana',
  Other: 'Otro',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    marginBottom: '5%',
  },
  avatar: {
    width: width / 2,
    height: width / 2,
    zIndex: -1,
    borderWidth: 2,
    borderColor: Colors.GREEN,
    borderRadius: 200,
  },
  contentFormProfile: {
    justifyContent: 'flex-start',
    marginTop: 15,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 20,
  },
  marginHorizontal: {
    marginHorizontal: '8%',
  },
  itemFamily: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    marginTop: 0,
    borderColor: 'rgba(0,0,0,0.2)',
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 100,
    backgroundColor: '#DADADA',
    marginRight: 15,
  },
  contentRelationship: {
    borderRadius: 3,
    borderColor: '#57C1D9',
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 15,
    minWidth: 65,
    marginLeft: 5,
  },
  paddingLeft: {
    paddingLeft: 10,
  },
});

const Layout = ({ goBack, state, user }) => {
  const { parent } = state;
  return (
    <View style={styles.container}>
      <Header title={I18n.t('profile')} onPress={goBack} />
      <ScrollView>
        <View style={styles.contentAvatar}>
          <FastImage
            source={parent?.avatar ? { uri: getImageById(parent?.avatar, user?.token) } : NoAvatar}
            style={styles.avatar}
          />
        </View>
        <View style={styles.marginHorizontal}>
          <View style={[Styles.row, styles.contentFormProfile]}>
            <View style={{ marginRight: 10 }}>
              <Text weight="SemiBold">{I18n.t('firstName')}: </Text>
            </View>
            <Text>{parent?.name}</Text>
          </View>
          <View style={[Styles.row, styles.contentFormProfile]}>
            <View style={{ marginRight: 10 }}>
              <Text weight="SemiBold">{I18n.t('phone')}: </Text>
            </View>
            <Text>{parent?.telephone}</Text>
          </View>
          <View style={[Styles.row, styles.contentFormProfile]}>
            <View style={{ marginRight: 10 }}>
              <Text weight="SemiBold">{I18n.t('email')}: </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text align="right" fontSize={14}>
                {parent?.email}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.marginHorizontal}>
          {parent?.relationships?.length > 0 && (
            <View
              style={{
                marginTop: '5%',
                height: 40,
                borderBottomWidth: 1,
                borderColor: 'rgba(0,0,0,0.2)',
              }}
            >
              <Text align="left">{I18n.t('relationships')}</Text>
            </View>
          )}
          {parent?.relationships?.map((rel, index) => {
            if (rel.child === null) {
              return <View key={index} />;
            }
            return (
              <View style={styles.itemFamily} key={index}>
                <View
                  style={[
                    Styles.row,
                    styles.paddingLeft,
                    { flex: 0.7, justifyContent: 'flex-start' },
                  ]}
                >
                  <FastImage
                    source={{ uri: getImageById(rel?.avatar, user?.token) }}
                    style={styles.imageProfile}
                  />
                  <Text align="left" weight="Medium" fontSize={12}>
                    {rel?.child?.name}
                  </Text>
                </View>
                <View style={Styles.row}>
                  <View style={styles.contentRelationship}>
                    <Text
                      color="#57C1D9"
                      fontSize={11}
                      weight="SemiBold"
                      style={{ textTransform: 'uppercase' }}
                    >
                      {!validateRelationship[rel.relationship || 'Other']
                        ? rel.relationship
                        : I18n.t(
                            `listRelationship.${rel.relationship ? rel.relationship : 'Other'}`
                          )}
                    </Text>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default Layout;
