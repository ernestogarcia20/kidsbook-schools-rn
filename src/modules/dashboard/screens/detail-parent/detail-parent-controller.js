import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import DetailParentLayout from './components/detail-parent-layout';

class DetailParentController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const parent = getParam('parent', {});
    this.state = {
      parent,
    };
  }

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <DetailParentLayout
          goBack={this.goBack}
          setState={this.handleChange}
          state={this.state}
          user={this.props.user}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps)(DetailParentController);
