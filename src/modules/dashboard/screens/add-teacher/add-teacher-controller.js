import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {
  findTeacherAction,
  inviteTeacherAction,
  inviteExistingTeacherAction,
} from '@dashboard/actions/teachers.actions';
import { validateEmail } from '@utils/functions/globals';
import I18n from 'react-native-redux-i18n';
import { showMessage } from 'react-native-flash-message';
import { fontMaker } from '@components/typografy';
import AddTeacherLayout from './components/add-teacher-layout';

class AddTeacherController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const teacher = getParam('teacher', null);
    this.state = {
      email: '',
      teacher,
      showNewTeacher: false,
      showResendEmail: false,
      name: '',
      lastName: '',
      gender: null,
      relationship: null,
      phone: '',
      country: '',
      city: '',
      address: '',
    };
  }

  handleFindTeacher = async () => {
    const { _findTeacherAction, user } = this.props;
    const { email } = this.state;

    if (!validateEmail(email)) {
      showMessage({
        message: I18n.t(`validation.error/emailInvalid`),
        type: 'default',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
        position: 'top',
      });
      return;
    }

    try {
      const { client } = await _findTeacherAction(user.schoolId, { email });
      if (client) {
        this.setState({ teacher: client, showResendEmail: true });
        return;
      }
      this.setState({ teacher: null, showNewTeacher: true });
    } catch (e) {}
  };

  handleAddTeacher = async () => {
    const {
      user,
      _inviteTeacherAction,
      navigation: { state },
    } = this.props;
    const { name, lastName, phone, address, email } = this.state;
    if (!name) {
      showMessage({
        message: I18n.t(`validation.error/teacherNameRequire`),
        type: 'default',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }

    const obj = {
      school_id: user.schoolId,
      name: `${name} ${lastName}`,
      telephone: phone,
      address,
      email,
    };
    try {
      await _inviteTeacherAction(obj);
      if (state.params.refreshList) {
        state.params.refreshList();
      }
      this.goBack();
    } catch (e) {}
  };

  onChangeTextPhone = ({ dialCode, unmaskedPhoneNumber, phoneNumber, isVerified }) => {
    this.setState({ phone: `${dialCode} ${phoneNumber}`, dialCode });
  };

  showModalSelection = (key) => {
    const { dispatch } = this.props;
    this.setState({ modalSelected: key });
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t(key !== 'relationship' ? 'gender' : 'relationship'),
        data: I18n.t(key !== 'relationship' ? 'listGender' : 'listRelationshipString'),
        onPressAccept: (item) => this.selectionModal(item),
      },
      dispatch
    );
  };

  handleSendInvitation = async () => {
    const {
      user,
      _inviteExistingTeacherAction,
      navigation: { state },
    } = this.props;
    const { teacher } = this.state;

    const obj = {
      school_id: user.schoolId,
      client_id: teacher.id,
      type: 'Teacher',
    };
    try {
      await _inviteExistingTeacherAction(obj);
      if (state.params.refreshList) {
        state.params.refreshList();
      }
      this.goBack();
    } catch (e) {}
  };

  selectionModal = (item) => {
    const { modalSelected } = this.state;

    if (modalSelected === 'relationship') {
      return this.setState({ relationship: item });
    }
    if (modalSelected === 'gender') {
      return this.setState({ gender: item });
    }
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <AddTeacherLayout
          setState={this.handleChange}
          state={this.state}
          user={user}
          handleFindTeacher={this.handleFindTeacher}
          handleAddTeacher={this.handleAddTeacher}
          showModalSelection={this.showModalSelection}
          goBack={this.goBack}
          onChangeTextPhone={this.onChangeTextPhone}
          handleSendInvitation={this.handleSendInvitation}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _findTeacherAction: findTeacherAction,
  _inviteTeacherAction: inviteTeacherAction,
  _inviteExistingTeacherAction: inviteExistingTeacherAction,
})(AddTeacherController);
