import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import DocumentPicker from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';
import { showMessage } from 'react-native-flash-message';
import { fontMaker } from '@components/typografy';
import {
  sendFileForActivityAction,
  createActiviyAction,
  updateActivityAction,
  deleteActivityAction,
  downloadFileActivityAction,
} from '@dashboard/actions/activities.actions';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import RNFS from 'react-native-fs';
import ImagePicker from 'react-native-image-picker';
import { LIST_GLOBAL, ACTIVITIES } from '@utils/constants/navigates';
import moment from 'moment';
import CreateActivityLayout from './components/create-activity-layout';

class CreateActivityController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = this.props;
    const edit = getParam('edit', false);
    const activity = getParam('activity', null);
    this.state = {
      isFutureActivity: false,
      dateSelected: activity?.start_datetime
        ? moment(activity.start_datetime, 'YYYY-MM-DD HH:mm:ss').toDate()
        : '',
      deliveryDate: activity?.start_datetime
        ? moment(activity.start_datetime, 'YYYY-MM-DD HH:mm:ss').format('YYYY/MM/DD')
        : '',
      classroomList: activity?.classrooms || [],
      attachList: activity?.attachments ? JSON.parse(activity?.attachments) : [],
      title: activity?.title || '',
      description: activity?.description || '',
      activityValue: activity?.value || '',
      attachDelete: [],
      edit,
      activity,
    };
  }

  handleClassroomSelected = (classroomList) => {
    const oldClassroomList = this.state.classroomList;
    this.setState({ classroomList: oldClassroomList.concat(classroomList) });
  };

  handleAddMultiListClassroom = () => {
    const { classroomList } = this.state;
    this.navigate(LIST_GLOBAL, {
      key: 'classroom',
      handleSelected: this.handleClassroomSelected,
      ignore: classroomList,
    });
  };

  handleDeleteClassroom = (classroom) => {
    let { classroomList } = this.state;
    const index = classroomList.indexOf(classroom);
    if (!classroom) {
      classroomList = classroomList.filter((x) => !x.checked);
    }
    if (index > -1) {
      classroomList.splice(index, 1);
    }
    this.setState({ classroomList });
  };

  handleCreateActivity = async () => {
    const {
      classroomList,
      attachList,
      title,
      description,
      activityValue,
      deliveryDate,
    } = this.state;
    const {
      _sendFileForActivityAction,
      _createActiviyAction,
      _loadingAction,
      user,
      navigation: { getParam },
    } = this.props;

    if (classroomList.length === 0) {
      showMessage({
        message: I18n.t(`validation.error/classroomList`),
        type: 'danger',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }

    _loadingAction(true, 'creatingActivity');
    const loadActivities = getParam('loadActivities', () => {});
    const classroomsId = JSON.stringify(classroomList.map((x) => x.id));

    try {
      const activity = await _createActiviyAction({
        classrooms_id: classroomsId,
        school_id: user.schoolId,
        matter_id: 0,
        id_knowledge_area: 0,
        title,
        description,
        value: activityValue,
        attachments: '[]',
        start_datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
        end_datetime: moment(deliveryDate, 'YYYY/MM/DD').format('YYYY-MM-DD HH:mm:ss'),
      });
      if (attachList.length > 0) {
        await Promise.all(
          attachList.map(async (file) => {
            const attachment = {
              name: file.fileData.name,
              imageData: null,
              extension: file.fileType,
              size: file.fileData.size,
              file: file.fileData.uriApi,
              modifyAttachment: null,
            };
            await _sendFileForActivityAction({ attachment, activity_id: activity.id });
          })
        );
      }
      _loadingAction(false);
      this.goBack();
      loadActivities();
    } catch (e) {
      // _loadingAction(false);
    }
  };

  handleDateFuture = (date) => {};

  handleFolder = async () => {
    const { attachList } = this.state;
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
        // There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      // Printing the log realted to the file
      const mBytes = Math.round(res.size / (1024 * 1024));
      if (mBytes > 100000) {
        showMessage({
          message: I18n.t(`validation.error/file`),
          type: 'default',
          titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
        });
        // alert(mBytes);
        return;
      }
      const fileExt = res.name.split('.').pop().toLowerCase();
      const filename = res.uri.split('/').pop();
      const name =
        filename.replace(`.${fileExt}`, '').length > 8
          ? filename.replace(`.${fileExt}`, '').substr(0, 8)
          : filename.replace(`.${fileExt}`, '');

      const find = attachList.filter((x) => x.fileNameLarge === filename);
      if (find.length > 0) {
        return;
      }

      const file64 = await RNFS.readFile(decodeURI(res.uri), 'base64');
      if (fileExt === 'png' || fileExt === 'jpg') {
        attachList.push({
          image: res.uri,
          type: 'image',
          fileType: fileExt,
          fileNameLarge: filename,
          fileData: {
            ...res,
            name: `${name}.${fileExt}`,
            uriApi: `data:image/${fileExt};base64,${file64}`,
          },
        });
        this.setState({ attachList });
        return;
      }
      attachList.push({
        image: res.uri,
        type: 'file',
        fileType: fileExt,
        fileNameLarge: filename,
        fileData: {
          ...res,
          name: `${name}.${fileExt}`,
          uriApi: `data:${fileExt};base64,${file64}`,
        },
      });
      this.setState({ attachList });
      // Setting the state to show single file attributes 60mb
      // setSingleFile(res);
    } catch (err) {
      // setSingleFile(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        // alert('Canceled');
      } else {
        // For Unknown Error
        // alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  handleCamera = () => {
    const { attachList } = this.state;
    const options = {
      quality: 1.0,
      mediaType: 'photo',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: false,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      if (!response.data) {
        return;
      }

      const filename = response.uri.split('/').pop();
      const type = filename.split('.').pop();
      const name = filename.replace(`.${type}`, '').substr(0, 8);
      const attach = `data:image/jpg;base64,${response.data}`;
      attachList.push({
        image: attach,
        type: 'image',
        fileType: type,
        fileData: { name: `${name}.${type}`, uri: response.uri, uriApi: attach },
      });
      this.setState({ attachList });
    });
  };

  handleDeleteAttach = (attach) => {
    const { attachList, attachDelete: attachDeleteState, edit } = this.state;
    const attachDelete = attachDeleteState;
    const index = attachList.findIndex((a) => a === attach);
    if (edit) {
      const indexDelete = attachDelete.findIndex((x) => x === attachList[index]);
      if (indexDelete < 0) {
        attachDelete.push(attachList[index]);
      }
    }
    attachList.splice(index, 1);
    this.setState({ attachList, attachDelete });
  };

  handleGoToOpenFile = async (attach) => {
    const { _downloadFileActivityAction } = this.props;
    if (attach?.url) {
      try {
        const { PictureDir } = await _downloadFileActivityAction(attach.url, attach.attachment);

        setTimeout(() => {
          FileViewer.open(PictureDir);
        }, 150);
      } catch (e) {}
      return;
    }
    const decUrl = decodeURI(attach?.fileData?.uri);
    FileViewer.open(decUrl);
  };

  handleShowChilds = (classroom) => {
    const ignore = [];
    const arr = JSON.parse(classroom.children);
    arr.forEach((id) => {
      const indexIgnore = ignore.findIndex((x) => x.id === id);
      if (indexIgnore < 0) {
        ignore.push({ id });
      }
    });
    this.navigate(LIST_GLOBAL, {
      // handleSelected: this.handleClassroomSelected,
      ignoreReverse: ignore,
    });
  };

  handleUpdateActivity = async () => {
    const {
      classroomList,
      attachList,
      attachDelete,
      title,
      description,
      activityValue,
      deliveryDate,
      activity: activityState,
    } = this.state;
    const {
      _sendFileForActivityAction,
      _updateActivityAction,
      _loadingAction,
      user,
      navigation: { getParam },
    } = this.props;

    if (classroomList.length === 0) {
      showMessage({
        message: I18n.t(`validation.error/classroomList`),
        type: 'danger',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }

    _loadingAction(true, 'updatingActivity');
    const updateActivity = getParam('updateActivity', () => {});
    const classroomsId = JSON.stringify(classroomList.map((x) => x.id));

    const newAttach = attachList.filter((x) => x.fileType);

    try {
      if (attachDelete.length > 0) {
        await Promise.all(
          attachDelete.map(async (file) => {
            await _sendFileForActivityAction({
              modifyAttachment: file,
              activity_id: activityState.id,
            });
          })
        );
      }
      if (newAttach.length > 0) {
        await Promise.all(
          newAttach.map(async (file) => {
            const attachment = {
              name: file.fileData.name,
              imageData: null,
              extension: file.fileType,
              size: file.fileData.size,
              file: file.fileData.uriApi,
              modifyAttachment: null,
            };
            await _sendFileForActivityAction({ attachment, activity_id: activityState.id });
          })
        );
      }
      const data = await _updateActivityAction({
        id: activityState.id,
        classrooms_id: classroomsId,
        school_id: user.schoolId,
        matter_id: 0,
        id_knowledge_area: 0,
        title,
        description,
        value: activityValue,
        // start_datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
        end_datetime: moment(deliveryDate, 'YYYY/MM/DD').format('YYYY-MM-DD HH:mm:ss'),
      });
      await _loadingAction(false);
      this.goBack();
      updateActivity(data);
    } catch (e) {
      // _loadingAction(false);
    }
  };

  handleDeleteActivity = async () => {
    const {
      _deleteActivityAction,
      navigation: { getParam },
    } = this.props;
    const { activity } = this.state;

    const loadActivities = getParam('loadActivities', () => {});
    try {
      await _deleteActivityAction(activity.id);
      loadActivities(false);
      this.navigate(ACTIVITIES);
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <CreateActivityLayout
          goBack={this.goBack}
          handleTabFilter={this.handleTabFilter}
          setState={this.handleChange}
          state={this.state}
          handleGoToOpenFile={this.handleGoToOpenFile}
          handleUpdateActivity={this.handleUpdateActivity}
          handleDeleteActivity={this.handleDeleteActivity}
          navigation={this.props.navigation}
          handleCreateActivity={this.handleCreateActivity}
          handleFolder={this.handleFolder}
          handleCamera={this.handleCamera}
          user={this.props.user}
          handleShowChilds={this.handleShowChilds}
          handleDateFuture={this.handleDateFuture}
          handleDeleteAttach={this.handleDeleteAttach}
          handleAddMultiListClassroom={this.handleAddMultiListClassroom}
          handleDeleteClassroom={this.handleDeleteClassroom}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _createActiviyAction: createActiviyAction,
  _sendFileForActivityAction: sendFileForActivityAction,
  _loadingAction: loadingAction,
  _updateActivityAction: updateActivityAction,
  _deleteActivityAction: deleteActivityAction,
  _downloadFileActivityAction: downloadFileActivityAction,
})(CreateActivityController);
