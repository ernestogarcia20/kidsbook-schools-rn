import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TextInput,
  Platform,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Event from '@components/Event';
import KeyboardAvoidingView from '@components/KeyboardAvoidingView';
import Emoji from '@components/Emoji';
import TextField from '@components/text-input-md';
import Colors from '@assets/colors';
import Text from '@components/Text';
import MultiList from '@components/MultiList';
import Button from '@components/Button';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/Feather';
import FastImage from '@components/FastImage';
import { getImageActivity } from '@utils/functions/globals';

const { width } = Dimensions.get('window');

const hitSlop = { top: 25, bottom: 15, right: 25 };
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentImages: {
    paddingTop: '5%',
    paddingBottom: '5%',
  },
  content: {
    paddingHorizontal: '6%',
    paddingBottom: '5%',
  },
  paddingHorizontal: {
    paddingHorizontal: 0,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  separator: {
    marginTop: 5,
  },
  contentLine: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)',
  },
  input: { flex: 1, minHeight: 80, maxHeight: 190, marginLeft: 15 },
  inputMultiline: {
    height: null,
    minHeight: 90,
    paddingTop: Platform.OS === 'ios' ? 10 : 5,
    paddingBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  contenButtonCreateActivity: { marginTop: '5%', marginHorizontal: '7%', paddingBottom: '5%' },
  buttonDeleteAttach: {
    position: 'absolute',
    top: 0,
    right: 10,
    width: 20,
    height: 20,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.DANGER,
  },
});

const Layout = ({
  goBack,
  setState,
  state,
  user,
  navigation,
  handleDeleteClassroom,
  handleAddMultiListClassroom,
  handleCreateActivity,
  handleCamera,
  handleFolder,
  handleDeleteAttach,
  handleGoToOpenFile,
  handleShowChilds,
  handleDateFuture,
  handleUpdateActivity,
  handleDeleteActivity,
}) => {
  const {
    isFutureActivity,
    title,
    description,
    deliveryDate,
    activityValue,
    classroomList,
    attachList,
    dateSelected,
    edit,
  } = state;
  const [scrollView, setScrollView] = useState(null);
  const [cursorPosition, setCursorPosition] = useState(null);
  const scrolled = () => {
    if (!scrollView) {
      return;
    }
    setTimeout(() => {
      scrollView.scrollTo({ animated: true, y: 130 });
    }, 1);
  };
  const onEmoji = (emoji) => {
    const formatText = cursorPosition
      ? [description.slice(0, cursorPosition), emoji, description.slice(cursorPosition)].join('')
      : description.concat(emoji);
    setState('description', formatText);
    setCursorPosition(null);
  };
  const onSelectionChange = ({ nativeEvent: { selection } }) => {
    setCursorPosition(selection.start);
  };
  const input = ({ multiline = false, title, placeholder, value, attrName, updateMasterState }) => {
    return (
      <View style={[styles.paddingHorizontal]}>
        <View style={[styles.row, styles.separator, styles.contentLine]}>
          <Text>{title}: </Text>
          {!multiline && (
            <TextInput
              style={styles.input}
              multiline={multiline}
              placeholder={placeholder}
              value={value}
              onChangeText={(text) => updateMasterState(attrName, text)}
              placeholderTextColor="#909090"
            />
          )}
        </View>
        {multiline && (
          <View style={[styles.contentLine, styles.inputMultiline]}>
            <Emoji onEmoji={onEmoji}>
              <TextInput
                textAlignVertical="top"
                style={styles.input}
                multiline={multiline}
                onSelectionChange={onSelectionChange}
                onFocus={scrolled}
                placeholder={placeholder}
                placeholderTextColor="#909090"
                value={value}
                onChangeText={(text) => updateMasterState(attrName, text)}
              />
            </Emoji>
          </View>
        )}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={edit ? I18n.t('menu_list.editActivity') : I18n.t('menu_list.createActivity')}
        onPress={goBack}
      />
      <KeyboardAvoidingView>
        <ScrollView
          ref={(ref) => {
            setScrollView(ref);
          }}
        >
          <View style={styles.content}>
            <View style={styles.contentImages}>
              <View style={[styles.rowLarge, styles.contentLine]}>
                <View style={styles.row}>
                  <View style={styles.center}>
                    <CustomIcon name="attach" width={25} heigth={25} />
                  </View>
                  <Text>Agregar adjunto</Text>
                </View>
                <View style={[styles.row, { paddingRight: 15 }]}>
                  <TouchableOpacity
                    style={{ marginRight: 15 }}
                    hitSlop={hitSlop}
                    onPress={handleCamera}
                  >
                    <Icon name="instagram" size={24} />
                  </TouchableOpacity>
                  <TouchableOpacity hitSlop={hitSlop} onPress={handleFolder}>
                    <Icon name="folder" size={24} />
                  </TouchableOpacity>
                </View>
              </View>
              <ScrollView horizontal>
                {attachList.length > 0 && (
                  <View
                    style={[
                      styles.contentMessage,
                      styles.paddingHorizontal,
                      styles.row,
                      { flex: 1, marginTop: '4%' },
                    ]}
                  >
                    {attachList.map((attach, index) => {
                      return (
                        <TouchableOpacity
                          key={index}
                          onPress={() =>
                            handleGoToOpenFile(
                              !attach?.fileType
                                ? { url: getImageActivity(attach, user.token), attachment: attach }
                                : attach
                            )
                          }
                          style={{
                            minHeight: 90,
                            paddingRight: 10,
                            marginTop: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                        >
                          {!attach?.fileType && (
                            <>
                              <FastImage
                                source={{ uri: getImageActivity(attach, user.token) }}
                                resizeMode="stretch"
                                style={{
                                  height: 90,
                                  width: 80,
                                  borderRadius: 5,
                                  backgroundColor: '#FAFAFA',
                                }}
                              />
                            </>
                          )}
                          {attach?.type === 'image' && attach?.fileType && (
                            <>
                              <FastImage
                                source={{ uri: attach?.image }}
                                resizeMode="stretch"
                                style={{ height: 90, width: 80, borderRadius: 5 }}
                              />
                            </>
                          )}
                          {attach.type !== 'image' && attach.fileType && (
                            <>
                              <View
                                style={{
                                  height: 90,
                                  width: 80,
                                  borderRadius: 5,
                                  backgroundColor: 'rgba(0,0,0,0.1)',
                                }}
                              />
                            </>
                          )}
                          <TouchableOpacity
                            style={styles.buttonDeleteAttach}
                            onPress={() => handleDeleteAttach(attach)}
                          >
                            <Icon name="x" size={15} color="white" />
                          </TouchableOpacity>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                )}
              </ScrollView>
            </View>
            <Event
              title="¿Actividad a futuro?"
              updateMasterState={setState}
              attrName="isFutureActivity"
              value={isFutureActivity}
              onSave={handleDateFuture}
              navigation={navigation}
            />
            {input({
              multiline: false,
              title: 'Titulo de la actividad',
              placeholder: '¿De que trata?',
              value: title,
              attrName: 'title',
              updateMasterState: setState,
            })}
            {input({
              multiline: true,
              title: 'Descripcion de la actividad',
              placeholder: 'Escriba la descripcion',
              value: description,
              attrName: 'description',
              updateMasterState: setState,
            })}
            {input({
              multiline: false,
              title: 'Valoración',
              placeholder: '100',
              value: activityValue,
              attrName: 'activityValue',
              updateMasterState: setState,
            })}
            <TextField
              label="Fecha de entrega"
              highlightColor={Colors.GREEN}
              value={deliveryDate}
              dateSelected={dateSelected}
              datePicker
              maximumDate={null}
              minimumDate={new Date()}
              onChangeText={(text) => setState('deliveryDate', text)}
            />
          </View>
        
            <MultiList
              title="Salones"
              isChildTemplate={false}
              list={classroomList}
              setState={setState}
              handleMove={false}
              handleShowChilds={handleShowChilds}
              attrName="classroomList"
              handleAddMultiList={handleAddMultiListClassroom}
              handleDelete={(classroom) => handleDeleteClassroom(classroom)}
            />
          

          <View style={styles.contenButtonCreateActivity}>
            {!edit && <Button title="Crear actividad" onPress={handleCreateActivity} />}
            {edit && <Button title="Actualizar actividad" onPress={handleUpdateActivity} />}
            {edit && (
              <Button
                title="Eliminar actividad"
                onPress={handleDeleteActivity}
                style={{ marginTop: 15 }}
                color="#E44C44"
              />
            )}
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

export default Layout;
