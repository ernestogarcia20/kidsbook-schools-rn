import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { getParentsAction } from '@dashboard/actions/parents.actions';
import {
  createRealtionshipAction,
  unAssignedParentAction,
  updateLocalListAction,
} from '@dashboard/actions/children.actions';
import I18n from 'react-native-redux-i18n';
import { ADD_PARENT, CHILDREN } from '@utils/constants/navigates';
import AddParentsToChildLayout from './components/add-parents-to-child-layout';

class AddParentsToChildController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const child = getParam('child', null);
    this.state = {
      child,
      listParents: [],
      listParentsChild: [],
      listParentsAll: [],
    };
  }

  async componentDidMount() {
    const {
      _getParentsAction,
      navigation: { state },
      user,
    } = this.props;
    const { child } = this.state;
    try {
      const list = await _getParentsAction(user.schoolData.schools[0].id, true);
      if (list.length === 0) {
        return;
      }
      const listParents = [];
      const listParentsChild = [];
      list.forEach((item) => {
        const familyMember = item;
        if (child.familyIds.indexOf(familyMember.id) === -1) {
          listParents.push(familyMember);
        } else {
          const relationship = this.relationshipByClientId(familyMember.id);
          familyMember.relationship =
            relationship?.relationship === 'Other'
              ? relationship?.relationship_other
              : relationship?.relationship;
          listParentsChild.push(familyMember);
        }
      });
      if (state?.params?.updateParentChild) {
        state?.params?.updateParentChild();
      }
      const listParentsAll = listParents;
      this.setState({ listParents, listParentsAll, listParentsChild });
    } catch (e) {}
  }

  relationshipByClientId = (clientId) => {
    const { child } = this.state;
    const findClient = child.relationships.find((x) => x.client_id === clientId);
    return findClient || null;
  };

  handleAssign = (parent) => {
    const { listParents } = this.state;
    const newListParents = listParents.map((_parent) => {
      if (parent.id === _parent.id) {
        return { ..._parent, showRelashionship: true };
      }
      return { ..._parent, showRelashionship: false };
    });
    this.setState({ listParents: newListParents });
  };

  refreshRelationship = (clientId = null, relationship = null) => {
    const { child } = this.state;
    if (clientId && relationship) {
      child.familyIds.push(clientId);
      child.relationships.push(relationship);
    }
    this.setState({ child }, this.updateStoreChildList);
  };

  showModalDelete = (parent) => {
    const { dispatch } = this.props;
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('modalsText.removeFamilyMember'),
        onPressAccept: () => this.handleDeleteParentFamily(parent),
      },
      dispatch
    );
  };

  showModalRelationship = () => {
    const { dispatch } = this.props;
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('relationship'),
        data: I18n.t('listRelationshipString'),
        onPressAccept: (selectedKinship) => this.handleSelectedKinship(selectedKinship),
      },
      dispatch
    );
  };

  handleSelectedKinship = (selectedKinship) => {
    const { listParents } = this.state;

    const index = listParents.findIndex((it) => it.showRelashionship);
    if (index < 0) {
      return;
    }
    let parent = listParents[index];
    parent = { ...parent, relationshipSelected: selectedKinship };
    listParents[index] = parent;
    this.setState({ listParents });
  };

  handleDeleteParentFamily = async (member) => {
    const { child } = this.state;
    const { _unAssignedParentAction } = this.props;
    const rel = member.relationships.filter((x) => x.children_id === child.id);

    rel.forEach(async (r) => {
      try {
        await _unAssignedParentAction(r.id);
        child.familyIds = child.familyIds.filter((cID) => cID !== r.client_id);
        child.relationships = child.relationships.filter((_rel) => r.id !== _rel.id);
        this.setState({ child }, this.updateStoreChildList);
      } catch (e) {}
    });
  };

  handleSetAssign = async (parent) => {
    if (!parent.relationshipSelected) {
      return;
    }
    const { child } = this.state;
    const {
      _createRealtionshipAction,
      navigation: { state },
    } = this.props;
    const values = {
      client_id: parent.id,
      children_id: child.id,
      relationship: parent.relationshipSelected.value,
      relationship_other: parent.otherRelationship,
    };
    try {
      const { relationship } = await _createRealtionshipAction(values);
      child.familyIds.push(values.client_id);
      child.relationships.push(relationship);
      if (state?.params?.updateParentChild) {
        state?.params?.updateParentChild();
      }
      this.setState({ child }, this.updateStoreChildList);
    } catch (e) {}
  };

  updateStoreChildList = async () => {
    const { child } = this.state;
    const { _updateLocalListAction, childrenList } = this.props;
    const index = childrenList.findIndex((it) => it.id === child.id);
    if (index < 0) {
      return;
    }
    let _child = childrenList[index];
    _child = { ..._child, ...child };
    childrenList[index] = _child;
    await _updateLocalListAction(childrenList);
    this.componentDidMount();
  };

  handleAddParent = () => {
    const { child } = this.state;
    this.navigate(ADD_PARENT, { child, refreshList: this.refreshRelationship });
  };

  handleSearchParent = (text) => {
    const { listParentsAll } = this.state;
    if (!text) {
      this.setState({ search: text, listParents: listParentsAll });
      return;
    }
    const newData = listParentsAll.filter((item) => {
      const itemData = `${String(item.name).toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ listParents: newData, search: text });
  };

  handleGoBack = () => {
    const {
      navigation: { state },
    } = this.props;
    if (state.params.updateListChild) {
      state.params.updateListChild();
    }
    this.navigate(CHILDREN);
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <AddParentsToChildLayout
          setState={this.handleChange}
          goBack={this.handleGoBack}
          user={user}
          state={this.state}
          handleAssign={this.handleAssign}
          showModalDelete={this.showModalDelete}
          showModalRelationship={this.showModalRelationship}
          handleSetAssign={this.handleSetAssign}
          handleAddParent={this.handleAddParent}
          handleSearchParent={this.handleSearchParent}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, parents: { parentList }, children: { childrenList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  parentList,
  childrenList,
});

export default connect(mapStateToProps, {
  _getParentsAction: getParentsAction,
  _createRealtionshipAction: createRealtionshipAction,
  _unAssignedParentAction: unAssignedParentAction,
  _updateLocalListAction: updateLocalListAction,
})(AddParentsToChildController);
