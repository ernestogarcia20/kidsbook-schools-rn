import React from 'react';
import { View, StyleSheet, FlatList, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import FastImage from '@components/FastImage';
import Text from '@components/Text';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Feather';
import { getImageById } from '@utils/functions/globals';
import NoAvatar from '@images/no-avatar.jpg';
import Search from '@components/Search';
import FloatingButton from '@components/FloatingButton';
import ItemParent from './item-parents';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    width: width / 2,
    height: width / 2,
    zIndex: -1,
    borderWidth: 2,
    borderColor: Colors.GREEN,
    borderRadius: 200,
  },
  contentAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    marginBottom: '5%',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainerStyle: {
    marginHorizontal: '6%',
  },
  itemFamily: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    marginTop: 0,
    borderColor: 'rgba(0,0,0,0.2)',
  },
  contentTitleFamily: {
    height: 50,
    justifyContent: 'center',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    marginTop: 20,
    borderColor: 'rgba(0,0,0,0.2)',
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 100,
    backgroundColor: '#DADADA',
    marginRight: 15,
  },
  contentRelationship: {
    borderRadius: 3,
    borderColor: '#57C1D9',
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 15,
    minWidth: 65,
    marginLeft: 5,
  },
  buttonDelete: {
    backgroundColor: '#E43956',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 4,
    marginLeft: 20,
    marginRight: 10,
  },
  paddingLeft: {
    paddingLeft: 10,
  },
  buttonAssign: {
    borderRadius: 3,
    backgroundColor: '#57C1D9',
    paddingVertical: 4,
    paddingHorizontal: 15,
  },
  contentFormProfile: {
    justifyContent: 'flex-start',
    marginTop: 15,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 20,
  },
});

const validateRelationship = {
  Father: 'Padre',
  Mother: 'Madre',
  Uncle: 'Tío',
  Brother: 'Hermano',
  Sister: 'Hermana',
  Other: 'Otro',
};

const Layout = ({
  goBack,
  state,
  user,
  handleAssign,
  showModalDelete,
  showModalRelationship,
  handleSetAssign,
  handleAddParent,
  handleSearchParent,
}) => {
  const { child, listParents, listParentsChild, search } = state;

  const renderItemParents = ({ item, index }) => {
    return (
      <ItemParent
        item={item}
        user={user}
        key={`${index}`}
        child={child}
        handleAssign={handleAssign}
        showModalRelationship={showModalRelationship}
        handleSetAssign={handleSetAssign}
      />
    );
  };

  const listHeaderComponent = () => {
    return (
      <View>
        {user.isTeacher && <FloatingButton style={{ left: 10, top: 20 }} />}
        <View style={styles.contentAvatar}>
          <FastImage
            source={child?.avatar ? { uri: getImageById(child?.avatar, user.token) } : NoAvatar}
            style={styles.avatar}
          />
        </View>
        {!user.isTeacher && (
          <View style={styles.row}>
            <Text weight="Medium">{child?.name}</Text>
          </View>
        )}

        {user.isTeacher && (
          <>
            <View style={[styles.row, styles.contentFormProfile]}>
              <View style={{ marginRight: 10 }}>
                <Text weight="SemiBold">{I18n.t('firstName')}: </Text>
              </View>
              <Text>{child?.name}</Text>
            </View>
            <View style={[styles.row, styles.contentFormProfile]}>
              <View style={{ marginRight: 10 }}>
                <Text weight="SemiBold">{I18n.t('phone')}: </Text>
              </View>
              <Text>{child?.phone || '-----------'}</Text>
            </View>
            <View style={[styles.row, styles.contentFormProfile]}>
              <View style={{ marginRight: 10 }}>
                <Text weight="SemiBold">{I18n.t('email')}: </Text>
              </View>
              <Text>{child?.email || '-----------'}</Text>
            </View>
          </>
        )}

        <View style={[styles.contentTitleFamily, { borderTopWidth: user.isTeacher ? 0 : 1 }]}>
          <Text align="left" weight="SemiBold" color="#A8A8A8">
            {I18n.t('family')}
          </Text>
        </View>
        {listParentsChild?.map((item) => {
          return (
            <View style={styles.itemFamily}>
              <View
                style={[
                  styles.row,
                  styles.paddingLeft,
                  { flex: 0.7, justifyContent: 'flex-start' },
                ]}
              >
                <FastImage
                  source={{ uri: getImageById(item?.avatar, user.token) }}
                  style={styles.imageProfile}
                />
                <Text align="left" weight="Medium" fontSize={12}>
                  {item.name}
                </Text>
              </View>
              <View style={styles.row}>
                <View style={styles.contentRelationship}>
                  <Text
                    color="#57C1D9"
                    fontSize={11}
                    weight="SemiBold"
                    style={{ textTransform: 'uppercase' }}
                  >
                    {!validateRelationship[item.relationship || 'Other']
                      ? item.relationship
                      : I18n.t(
                          `listRelationship.${item.relationship ? item.relationship : 'Other'}`
                        )}
                  </Text>
                </View>
                {!user.isTeacher && (
                  <TouchableOpacity
                    style={styles.buttonDelete}
                    onPress={() => showModalDelete(item)}
                  >
                    <Icon name="x" size={20} color={Colors.WHITE} />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          );
        })}

        {!user.isTeacher && (
          <View style={{ marginBottom: 15, marginTop: 25, marginHorizontal: 2 }}>
            <Search onChangeText={handleSearchParent} value={search} />
          </View>
        )}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={user.isTeacher ? I18n.t('profile') : I18n.t('menu_list.assignParent')}
        add={!user.isTeacher}
        onPress={goBack}
        onPressAdd={handleAddParent}
      />
      <ScrollView contentContainerStyle={styles.contentContainerStyle}>
        {listHeaderComponent()}
        {!user.isTeacher && (
          <FlatList
            data={listParents}
            extraData={state}
            scrollEnabled={false}
            renderItem={renderItemParents}
            keyExtractor={(item, index) => item + index}
          />
        )}
      </ScrollView>
    </View>
  );
};

export default Layout;
