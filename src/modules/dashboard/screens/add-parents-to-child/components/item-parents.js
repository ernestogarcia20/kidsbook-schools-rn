import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { getImageById } from '@utils/functions/globals';
import I18n from 'react-native-redux-i18n';
import FastImage from '@components/FastImage';
import Text from '@components/Text';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Feather';
import Button from '@components/Button';
import TextInput from '@components/text-input-md';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  itemFamily: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    marginTop: 0,
    borderColor: 'rgba(0,0,0,0.2)',
  },
  paddingLeft: {
    paddingLeft: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonAssign: {
    borderRadius: 3,
    backgroundColor: '#57C1D9',
    paddingVertical: 4,
    paddingHorizontal: 15,
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 100,
    backgroundColor: '#DADADA',
    marginRight: 15,
  },
  contentRow: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5,
  },
  contentRelationship: {
    paddingHorizontal: 20,
    paddingVertical: 17,
    backgroundColor: 'white',
    marginHorizontal: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
    marginBottom: 20,
  },
  contentButton: { paddingHorizontal: '20%', marginTop: 15 },
  contentInput: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: Colors.GREEN,
    borderBottomWidth: 1.5,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
});

const Layout = ({
  item,
  user,
  key,
  handleAssign,
  child,
  showModalRelationship,
  handleSetAssign,
}) => {
  const [otherRelationship, setOtherRelationship] = useState('');
  return (
    <View>
      <View key={`${key}`} style={styles.itemFamily}>
        <View style={[styles.row, styles.paddingLeft, { flex: 0.7, justifyContent: 'flex-start' }]}>
          <FastImage
            source={{ uri: getImageById(item?.avatar, user.token) }}
            style={styles.imageProfile}
          />
          <Text align="left" weight="Medium" fontSize={12}>
            {item.name}
          </Text>
        </View>
        {item.showRelashionship ? (
          <View style={styles.row}>
            <View style={styles.contentRow}>
              <Icon name="arrow-down" size={18} />
            </View>
            <Text weight="Medium" fontSize={12}>
              {I18n.t('buttons.completeToAssign')}
            </Text>
          </View>
        ) : (
          <View style={[styles.row, { flex: 0.3 }]}>
            <TouchableOpacity style={styles.buttonAssign} onPress={() => handleAssign(item)}>
              <Text color={Colors.WHITE} fontSize={11} weight="SemiBold">
                {I18n.t(`buttons.assign`)}
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
      {item.showRelashionship && (
        <View style={styles.contentRelationship}>
          <Text
            fontSize={12}
            weight="SemiBold"
            style={{
              marginTop: 5,
            }}
            align="left"
          >
            {String(I18n.t('assingName'))
              .replace('$name1', item.name)
              .replace('$name2', child.name)}
          </Text>
          <TouchableOpacity style={styles.contentInput} onPress={showModalRelationship}>
            <Text align="left" color="#9D9D9D" weight="Medium">
              {item.relationshipSelected ? item.relationshipSelected.text : I18n.t('relationship')}
            </Text>
            <Icon name="triangle" size={12} style={{ transform: [{ rotate: '60deg' }] }} />
          </TouchableOpacity>
          {item.relationshipSelected &&
            item.relationshipSelected.text &&
            item.relationshipSelected.value === 'Other' && (
              <View style={{ marginBottom: 10 }}>
                <TextInput
                  label={I18n.t('otherRelationship')}
                  highlightColor={Colors.GREEN}
                  value={otherRelationship}
                  onChangeText={(text) => setOtherRelationship(text)}
                />
              </View>
            )}
          <View style={styles.contentButton}>
            <Button
              title={I18n.t('buttons.assign')}
              onPress={() => handleSetAssign({ ...item, otherRelationship })}
            />
          </View>
        </View>
      )}
    </View>
  );
};

export default Layout;
