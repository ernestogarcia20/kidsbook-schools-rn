import React from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Search from '@components/Search';
import Colors from '@assets/colors';
import { getImageById } from '@utils/functions/globals';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import FastImage from '@components/FastImage';
import moment from 'moment';
import Button from '@components/Button';
import CheckBox from '@components/CheckBox';
import TemplateItem from '@components/TemplateItem';
import ControlButtons from '@components/ControlButtons';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentSearch: {
    paddingHorizontal: '5%',
    paddingVertical: '5%',
  },
  paddingIcon: { paddingRight: 7 },
  center: { justifyContent: 'center', alignItems: 'center' },
  dateText: { position: 'absolute', paddingTop: 3 },
  contentIcons: {
    flexDirection: 'row',
    height: 25,
    marginTop: 5,
    alignItems: 'center',
  },
  buttonIconMargin: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.WHITE,
    marginLeft: 10,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  buttonIcon: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.BLUELIGHT,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  contentActionsEdit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonTrash: {
    width: 30,
    height: 30,
    borderRadius: 5,
    backgroundColor: Colors.RED,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '2%',
  },
  contentSelectedAll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '3.5%',
    marginTop: '7%',
    alignItems: 'center',
  },
  contentEdit: { paddingHorizontal: '5%', paddingTop: '5%' },

  renderItem: {
    height: 90,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: '5%',
    paddingRight: '8%',
    flex: 1,
  },
  rowLarge: {
    alignItems: 'center',
    flexDirection: 'row',
  },
});

const Layout = ({
  goBack,
  state,
  user,
  handleSelectedItem,
  handleSearch,
  handleSelectAll,
  onPressSelectedAll,
}) => {
  const { list, multiple, selectAll, onlyItem, key, currentSelection, maxSelection, navigate } = state;

  const title = () => {
    if (key === 'children') {
      return 'menu_list.childrenList';
    }
    if (key === 'teacher') {
      return 'menu_list.teacherList';
    }
    if (key === 'childByclassroom' || key === 'classroom') {
      return 'menu_list.classroomList';
    }
    return 'menu_list.parentList';
  };

  const renderItem = ({ item, index }) => {
    const disable = currentSelection === maxSelection;
    return (
      <TemplateItem
        index={index}
        childTemplate={key === 'children'}
        item={item}
        isCheckBox={!navigate}
        disable={disable}
        handleSelectedItem={() => (disable && !item.checked ? null : handleSelectedItem(item))}
        editList={multiple}
      />
    );
  };
  const listHeaderComponent = () => {
    return (
      <View>
        <ControlButtons handleSelectAll={handleSelectAll} selectAll={selectAll} />
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t(title())} onPress={goBack} />
      <View style={styles.contentSearch}>
        <Search onChangeText={handleSearch} />
      </View>
      <FlatList
        data={list}
        ListHeaderComponent={navigate ? undefined : listHeaderComponent}
        extraData={state}
        initialNumToRender={5}
        renderItem={renderItem}
        keyExtractor={(item, index) => item + index}
      />
      {multiple && !navigate && (
        <View style={{ marginBottom: 15, marginHorizontal: '10%', marginTop: 15 }}>
          <Button title="Seleccionar" onPress={onPressSelectedAll} />
        </View>
      )}
    </View>
  );
};

export default Layout;
