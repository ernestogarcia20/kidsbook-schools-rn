import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { getChildrenForSchoolAction } from '@dashboard/actions/children.actions';
import { getTeachersAction } from '@dashboard/actions/teachers.actions';
import { getClassroomAction } from '@dashboard/actions/classroom.actions';
import { getParentsAction } from '@dashboard/actions/parents.actions';

import { LIST_GLOBAL } from '@utils/constants/navigates';
import ListGlobalLayout from './components/list-global-layout';

class ListGlobalController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const multiple = getParam('multiple', true);
    const key = getParam('key', 'children');
    const maxSelection = getParam('maxSelection', null);
    const navigate = getParam('navigate', null);
    this.state = {
      list: [],
      allList: [],
      multiple,
      selectAll: false,
      key,
      maxSelection,
      currentSelection: 0,
      navigate,
    };
  }

  componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const { childrenList, teacherList, classroomList, parentList } = this.props;
    if (
      childrenList !== prevProps.childrenList ||
      teacherList !== prevProps.teacherList ||
      classroomList !== prevProps.classroomList ||
      parentList !== prevProps.parentList
    ) {
      this.analize();
    }
  }

  init = async () => {
    const {
      user,
      childrenList,
      teacherList,
      classroomList,
      parentList,
      _getChildrenForSchoolAction,
      _getTeachersAction,
      _getClassroomAction,
      _getParentsAction,
    } = this.props;
    const { key } = this.state;
    this.analize();
    try {
      if (key === 'children' && childrenList.length === 0) {
        await _getChildrenForSchoolAction(user.schoolId);
      } else if (key === 'teachers' && (teacherList.length === 0 || !teacherList)) {
        await _getTeachersAction(user.schoolId);
      } else if (
        (key === 'childByclassroom' || key === 'classroom') &&
        classroomList.length === 0
      ) {
        await _getClassroomAction(user.schoolId);
      } else if (key === 'parents' && parentList.length === 0) {
        await _getParentsAction(user.schoolId);
      }
      // await _getChildrenForSchoolAction(user.schoolId);
      // await _getTeachersAction(user.schoolId);
    } catch (e) {}
  };

  analize = () => {
    setTimeout(() => {
      const {
        childrenList,
        teacherList,
        classroomList,
        parentList,
        navigation: { getParam },
      } = this.props;
      const { key } = this.state;
      let list = key === 'children' ? childrenList : teacherList;
      const listIgnore = getParam('ignore', []);
      const ignoreReverse = getParam('ignoreReverse', []);
      if (key === 'children') {
        list = childrenList.filter((c) =>
          typeof c.relationships === 'string'
            ? x.relationships !== '[]'
            : c.relationships.length !== 0
        );
      } else if (key === 'teacher') {
        list = teacherList;
      } else if (key === 'childByclassroom' || key === 'classroom') {
        list = classroomList.filter((x) => x.children !== '[]');
      } else if (key === 'parents') {
        list = parentList;
      }
      const ignoreChildren =
        ignoreReverse.length === 0 ? listIgnore.map((a) => a.id) : ignoreReverse.map((a) => a.id);
      const newList = list.filter((a) =>
        ignoreReverse.length === 0
          ? ignoreChildren.indexOf(a.id) === -1
          : ignoreChildren.indexOf(a.id) > -1
      );
      this.setState({ list: newList, allList: newList });
    }, 100);
  };

  handleSearch = (text) => {
    const { allList } = this.state;
    if (!text) {
      this.setState({ search: text, list: allList });
      return;
    }
    const newData = allList.filter((item) => {
      const itemData = `${String(item.name).toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ list: newData, search: text });
  };

  handleSelectedItem = (item) => {
    const {
      navigation: { getParam },
    } = this.props;
    const { multiple, list, key, allList, navigate } = this.state;

    if (navigate) {
      this.navigate(navigate, { item });
      return;
    }
    if (multiple) {
      const indexList = list.findIndex((x) => x.id === item.id);
      const indexAllList = allList.findIndex((x) => x.id === item.id);
      if (indexList > -1 && indexAllList > -1) {
        const itemList = list[indexList];
        const itemAllList = allList[indexAllList];
        list[indexList] = { ...itemList, checked: !itemList.checked };
        allList[indexAllList] = { ...itemAllList, checked: !itemAllList.checked };
        const currentSelection = String(allList.filter((x) => x.checked).length);
        this.setState({ list, allList, currentSelection });
      }
      return;
    }
    const handleSelected = getParam('handleSelected', () => {});
    handleSelected([item], key);
    this.goBack();
  };

  handleSelectAll = () => {
    const { list, allList, selectAll } = this.state;
    const newList = list.map((item2) => {
      return { ...item2, checked: !item2.checked };
    });
    const newAllList = allList.map((item2) => {
      return { ...item2, checked: !item2.checked };
    });
    this.setState({ list: newList, allList: newAllList, selectAll: !selectAll });
  };

  onPressSelectedAll = () => {
    const {
      navigation: { getParam },
    } = this.props;
    const { allList, key } = this.state;
    const handleSelected = getParam('handleSelected', () => {});
    const ignoreRecipent = getParam('ignore', []);
    if (key === 'childByclassroom') {
      const unCheckList = allList.filter((x) => !x.checked);
      const ignore = [];
      if (unCheckList.length > 0) {
        unCheckList.forEach((classroom, index) => {
          const arr = JSON.parse(classroom.children);
          arr.forEach((id) => {
            const indexIgnore = ignore.findIndex((x) => x.id === id);
            if (indexIgnore < 0) {
              ignore.push({ id });
            }
          });
        });
      }

      this.goBack();
      setTimeout(() => {
        this.navigate(LIST_GLOBAL, { ignore: ignore.concat(ignoreRecipent), handleSelected });
      }, 250);
      return;
    }
    handleSelected(
      allList
        .filter((x) => x.checked)
        .map((it) => {
          return { ...it, checked: false };
        }),
      key
    );
    this.goBack();
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <ListGlobalLayout
          setState={this.handleChange}
          state={this.state}
          goBack={this.goBack}
          user={user}
          handleSelectAll={this.handleSelectAll}
          handleSelectedItem={this.handleSelectedItem}
          onPressSelectedAll={this.onPressSelectedAll}
          handleSearch={this.handleSearch}
        />
      </Container>
    );
  }
}

const mapStateToProps = (
  {
    user,
    children: { childrenList },
    teacher: { teacherList },
    classroom: { classroomList },
    parents: { parentList },
  },
  store
) => ({
  dispatch: store.navigation.dispatch,
  user,
  childrenList,
  teacherList,
  classroomList,
  parentList,
});

export default connect(mapStateToProps, {
  _getChildrenForSchoolAction: getChildrenForSchoolAction,
  _getTeachersAction: getTeachersAction,
  _getClassroomAction: getClassroomAction,
  _getParentsAction: getParentsAction,
})(ListGlobalController);
