import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { CREATE_SOCIAL_NETWORK } from '@utils/constants/navigates';
import {
  getFeedAction,
  likePostAction,
  deletePostAction,
} from '@dashboard/actions/social-network.actions';
import DetailSocialNetworkLayout from './components/detail-social-network-layout';

class DetailSocialNetworkController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const post = getParam('post', {});
    this.state = {
      post,
    };
  }

  handleLikeComment = () => {};

  handleLikePost = async (_post) => {
    const { _likePostAction, user } = this.props;
    const { post } = this.state;

    try {
      const data = await _likePostAction(_post.id);
      if (data?.liked) {
        this.setState({ post: { ...post, likes: post.likes + 1 } });
      }
    } catch (e) {}
  };

  handleDeletePost = async (post) => {
    const { _deletePostAction } = this.props;

    try {
      await _deletePostAction(post.id);
    } catch (e) {}
  };

  handleEditPost = (post) => {
    this.navigate(CREATE_SOCIAL_NETWORK, { edit: true, post, updatedList: this.init });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <DetailSocialNetworkLayout
          goBack={this.goBack}
          user={this.props.user}
          handleLikeComment={this.handleLikeComment}
          setState={this.handleChange}
          handleLikePost={this.handleLikePost}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _likePostAction: likePostAction,
  _deletePostAction: deletePostAction,
})(DetailSocialNetworkController);
