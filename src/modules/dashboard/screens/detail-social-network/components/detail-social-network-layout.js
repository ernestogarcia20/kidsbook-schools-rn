import React from 'react';
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView,
  Platform,
} from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import FastImage from '@components/FastImage';
import { getImagePost, getImageById } from '@utils/functions/globals';
import Colors from '@assets/colors';
import Text from '@components/Text';
import Icon from 'react-native-vector-icons/Feather';
import ActionWidget from '@components/ActionWidget';
import FloatingButton from '@components/FloatingButton';
import LikeIcon from '@images/megusta.png';
import CommentIcon from '@images/charla.png';
import { Popover, PopoverController } from 'react-native-modal-popover';
import { Styles } from '@assets/Styles';
import CarouselImage from '@dashboard/screens/social-network/components/carouse-image';
import InputMessage from '@components/InputMessage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentDot: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center' },
  contentItem: {
    paddingBottom: '7%',
    marginVertical: 10,
    backgroundColor: Colors.WHITE,
    marginHorizontal: 10,
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
  },
  contentPost: { marginTop: 15, marginHorizontal: '7%' },
  contentHorizontal: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentImage: { flexDirection: 'row', alignItems: 'center' },
  imageProfile: {
    width: 50,
    height: 50,
    borderRadius: 100,
    backgroundColor: '#DADADA',
    marginRight: 15,
  },
  contentWidget: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#DADADA',
    borderRadius: 20,
  },
  contentIconWidget: {
    width: 27,
    height: 27,
    backgroundColor: '#57C1D9',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  spacingWidget: { marginLeft: 10 },
  contentControls: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
  contentWidgetControls: { flexDirection: 'row', alignItems: 'center' },
});

const Layout = ({
  goBack,
  handleDetailPost,
  handleLikePost,
  handleEditPost,
  handleDeletePost,
  handleLikeComment,
  state,
  user,
}) => {
  const { post } = state;
  const RenderPost = () => {
    const { img_urls, client, content, created_at, likes, comments } = post;
    const imgParse = JSON.parse(img_urls);
    const urlImage = getImageById(client?.avatar, user?.authData?.apiToken);

    const onPressLike = () => {
      handleLikePost(post);
    };
    const onPressEditPost = () => {
      handleEditPost(post);
    };
    const onPressDeletePost = () => {
      handleDeletePost(post);
    };
    return (
      <PopoverController>
        {({ openPopover, closePopover, popoverVisible, setPopoverAnchor, popoverAnchorRect }) => (
          <>
            <View style={styles.contentItem}>
              <CarouselImage data={imgParse} user={user} height={380} />

              <View style={styles.contentPost}>
                <View style={styles.contentHorizontal}>
                  <View style={styles.contentImage}>
                    <FastImage source={{ uri: urlImage }} style={styles.imageProfile} />
                    <Text weight="Medium" fontSize={13}>
                      {client.name}
                    </Text>
                  </View>
                  <TouchableOpacity ref={setPopoverAnchor} onPress={openPopover}>
                    <Icon name="more-vertical" size={25} />
                  </TouchableOpacity>
                </View>
                <View style={[{ marginVertical: '5%' }, Styles.row]}>
                  <Text fontSize={13} align="left">
                    {content}
                  </Text>
                </View>
                <View style={styles.contentControls}>
                  <View style={styles.contentWidgetControls}>
                    <ActionWidget
                      image={LikeIcon}
                      color="#57C1D9"
                      text={likes}
                      onPress={onPressLike}
                    />
                    <View style={styles.spacingWidget}>
                      <ActionWidget
                        image={CommentIcon}
                        iconSize={15}
                        color="#CF742E"
                        text={comments}
                      />
                    </View>
                  </View>
                  <Text fontSize={13}>{created_at}</Text>
                </View>
              </View>
              <Popover
                visible={popoverVisible}
                fromRect={popoverAnchorRect}
                onClose={closePopover}
                placement="auto"
                contentStyle={[{ justifyContent: 'center' }]}
              >
                <TouchableOpacity
                  style={{ height: 50, justifyContent: 'center', paddingHorizontal: 20 }}
                  onPress={() => {
                    closePopover();
                    setTimeout(() => {
                      onPressDeletePost();
                    }, 600);
                  }}
                >
                  <Text color={Colors.DANGER} weight="SemiBold">
                    Eliminar
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ height: 50, justifyContent: 'center' }}
                  onPress={() => {
                    closePopover();
                    onPressEditPost();
                  }}
                >
                  <Text color={Colors.GREEN} weight="SemiBold" fontSize={16}>
                    Editar
                  </Text>
                </TouchableOpacity>
              </Popover>
            </View>
          </>
        )}
      </PopoverController>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.socialNetwork')} onPress={goBack} />
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        enabled
        behavior={Platform.OS === 'ios' ? 'position' : undefined}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 50 : undefined}
      >
        <ScrollView>
          {RenderPost()}
          <View>
            <View
              style={{
                height: 50,
                paddingHorizontal: 15,
                justifyContent: 'center',
                borderBottomWidth: 1,
                borderColor: 'rgba(0,0,0,0.15)',
              }}
            >
              <Text align="left" color="rgba(0,0,0,0.6)">
                Comentar
              </Text>
            </View>
            <View style={{ marginTop: 20 }}>
              <InputMessage
                showAttach={false}
                showEmoji={false}
                placeholder="Escriba su comentario"
              />
            </View>
            <View
              style={{
                height: 50,
                paddingHorizontal: 15,
                justifyContent: 'center',
                borderBottomWidth: 1,
                borderColor: 'rgba(0,0,0,0.15)',
              }}
            >
              <Text align="left" color="rgba(0,0,0,0.6)">
                Comentarios
              </Text>
            </View>
            <View>
              {post?.comments ? (
                post?.comments?.map((comment, index) => {
                  const onPressLikeComment = () => {
                    handleLikeComment(comment);
                  };
                  return (
                    <View key={index}>
                      <Text>{comment.client.name}</Text>
                      <View style={styles.contentControls}>
                        <View style={styles.contentWidgetControls}>
                          <ActionWidget
                            image={LikeIcon}
                            color="#57C1D9"
                            text={comment.likes}
                            onPress={onPressLikeComment}
                          />
                        </View>
                        <Text fontSize={13}>{created_at}</Text>
                      </View>
                    </View>
                  );
                })
              ) : (
                <View style={[Styles.center, {height: 40}]}>
                  <Text>Sin comentarios</Text>
                </View>
              )}
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

export default Layout;
