import React, { useState } from 'react';
import { View, StyleSheet, TextInput, ScrollView } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import FastImage from '@components/FastImage';
import { getImageById } from '@utils/functions/globals';
import Colors from '@assets/colors';
import StarRating from 'react-native-star-rating';
import Underline from '@components/text-input-md/lib/Underline';
import Button from '@components/Button';
import { fontMaker } from '@components/typografy';

const validateRelationship = {
  Father: 'Father',
  Mother: 'Mother',
  Uncle: 'Uncle',
  Brother: 'Brother',
  Sister: 'Sister',
  Other: 'Other',
  Teacher: 'Teacher',
  Parent: 'Parent',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentRelationship: {
    borderRadius: 3,
    borderColor: Colors.GREEN,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 15,
    minWidth: 65,
    marginLeft: 5,
  },
  contentInfo: {
    paddingTop: '7%',
    paddingBottom: '3%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    marginHorizontal: '5%',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  paddingLeft: {
    paddingLeft: 10,
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 100,
    backgroundColor: '#DADADA',
    marginRight: 15,
  },
  contentRating: {
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',

    marginHorizontal: '5%',
    marginTop: '4%',
    paddingBottom: '4%',
  },
  multiLine: {
    ...fontMaker('Medium'),
    fontSize: 14,
  },
});

const Layout = ({ goBack, state, user, setState, handleSendQualify }) => {
  const { client, rating, key, comment, routeCenter } = state;
  const [refLine, setRefLine] = useState(null);
  return (
    <View style={styles.container}>
      <Header
        title={I18n.t(routeCenter ? 'menu_list.verifyClient' : 'menu_list.qualify')}
        onPress={goBack}
      />
      <ScrollView>
        <View style={styles.contentInfo}>
          <View style={[styles.paddingLeft, { flex: 0.7, justifyContent: 'flex-start' }]}>
            <Text align="left" color="#757575" fontSize={12}>
              {client?.name}
            </Text>
            <Text align="left" color="#757575" fontSize={12}>
              {client?.email}
            </Text>
          </View>
          <View style={styles.contentRelationship}>
            <Text
              color={Colors.GREEN}
              fontSize={11}
              weight="SemiBold"
              style={{ textTransform: 'uppercase' }}
            >
              {client && !validateRelationship[client.pivot.type || 'Other']
                ? client.pivot.type
                : I18n.t(`listRelationship.${client.pivot.type ? client.pivot.type : 'Other'}`)}
            </Text>
          </View>
        </View>
        <View style={[styles.row, styles.contentRating]}>
          <Text style={{ marginRight: 10, marginTop: 2 }}>{I18n.t('assessment')}</Text>
          <StarRating
            disabled={false}
            maxStars={5}
            starSize={23}
            starStyle={{ marginRight: 5 }}
            rating={rating}
            selectedStar={(rt) => setState('rating', rt)}
            fullStarColor="#FFC107"
          />
        </View>
        <View style={{ marginHorizontal: '5%', marginTop: '5%' }}>
          <TextInput
            editable
            maxLength={40}
            multiline
            placeholder="Por favor explica su valoración"
            textAlignVertical="top"
            numberOfLines={4}
            style={styles.multiLine}
            onFocus={() => {
              refLine && refLine.expandLine();
            }}
            onBlur={() => {
              refLine && refLine.shrinkLine();
            }}
            onChangeText={(text) => setState('comment', text)}
            value={comment}
          />
          <Underline
            ref={(ref) => {
              setRefLine(ref);
            }}
            highlightColor={Colors.GREEN}
            duration={200}
            borderColor="#E0E0E0"
          />
        </View>
      </ScrollView>
      <View style={{ paddingBottom: '5%', marginHorizontal: '7%' }}>
        <Button title={I18n.t('buttons.sendQualify')} onPress={handleSendQualify} />
      </View>
    </View>
  );
};

export default Layout;
