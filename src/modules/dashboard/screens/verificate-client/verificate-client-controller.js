import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { rateClientAction } from '@dashboard/actions/verification.actions';
import { VERIFICATION_CENTER } from '@utils/constants/navigates';
import VerificateClientLayout from './components/verificate-client-layout';

class VerificateClientController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const client = getParam('client', null);
    const key = getParam('key', null);
    const routeCenter = getParam('routeCenter', false);
    this.state = {
      client,
      key,
      rating: 4,
      routeCenter,
    };
  }

  handleSendQualify = async () => {
    const { rating, client, comment, routeCenter } = this.state;
    const {
      _rateClientAction,
      user,
      navigation: { getParam },
    } = this.props;

    const rateData = {
      school_name: user?.school?.name,
      school_id: user?.schoolId,
      rate: rating,
      client_id: client.id,
      reason: comment,
      reported_as: client.pivot.type,
    };

    try {
      await _rateClientAction(rateData);
      if (!routeCenter) {
        this.goBack();
      } else {
        const refeshScreen = getParam('refeshScreen', () => {});
        this.backCenter();
        refeshScreen();
      }
    } catch (e) {}
  };

  backCenter = () => {
    this.navigate(VERIFICATION_CENTER, { update: true });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <VerificateClientLayout
          goBack={this.state.routeCenter ? this.backCenter : this.goBack}
          setState={this.handleChange}
          state={this.state}
          handleSendQualify={this.handleSendQualify}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, { _rateClientAction: rateClientAction })(
  VerificateClientController
);
