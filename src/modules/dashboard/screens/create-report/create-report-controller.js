import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { REPORTS } from '@utils/constants/navigates';
import I18n from 'react-native-redux-i18n';
import { getReportFieldsByChildAction, sendReportAction } from '@dashboard/actions/report.actions';
import { showMessage } from 'react-native-flash-message';
import CreateReportLayout from './components/create-report-layout';

class CreateReportController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const children = getParam('item', null);
    this.state = {
      children,
      fieldObs: {},
      classroomSelected: {},
      moods: [],
      fields: [],
      moodSelected: {},
      stickers: [],
      stickersSelected: {},
      koalaStickers: [],
      koalaSelected: {},
      fieldValues: {},
      selectedMood: {},
      observations: '',
      assignments: '',
    };
  }

  async componentDidMount() {
    const { _getReportFieldsByChildAction, user } = this.props;
    const { children } = this.state;

    try {
      const data = await _getReportFieldsByChildAction(user.schoolId, children.id);
      const { fields, koalaStickers, stickers, moods } = data;
      const fieldValues = {};
      fields.forEach((field) => {
        if (field.type === 'Range') fieldValues[field.name] = 0;

        if (field.type === 'Checkbox') fieldValues[field.name] = false;
      });
      const classrooms = data.classrooms.filter((c) => {
        for (const rep of children.reportedToday) {
          if (c.id == rep.clasroom_id) {
            return false;
          }
        }
        return true;
      });

      const assignmentText = data.assignment_text;
      const observationText = data.observation_text;
      this.setState({
        fieldValues,
        fields,
        koalaStickers,
        stickers,
        moods,
        classrooms,
        assignmentText,
        observationText,
        classroomSelected: classrooms[0],
      });
    } catch (e) {}
  }

  handleChangeClassroom = () => {
    const { dispatch } = this.props;
    const { classrooms } = this.state;
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('menu_list.classrooms'),
        data: classrooms.map((item) => {
          return { text: item.name, checked: false, classroom: item };
        }),
        onPressAccept: (item) => this.handleClassroomSelected(item),
      },
      dispatch
    );
  };

  handleClassroomSelected = (data) => {
    this.setState({ classroomSelected: data.classroom });
  };

  handleMoodSelected = (mood) => {
    this.setState({ moodSelected: mood });
  };

  handleStickerSelected = (sticker) => {
    const { stickersSelected } = this.state;
    if (stickersSelected[sticker?.sticker_id]) {
      delete stickersSelected[sticker?.sticker_id];
      this.setState({ ...stickersSelected });
      return;
    }
    this.setState({
      stickersSelected: { ...stickersSelected, [sticker?.sticker_id]: sticker },
    });
  };

  handleKoalaSelected = (koala) => {
    this.setState({ koalaSelected: koala });
  };

  Template = () => {
    const { classroomSelected, classrooms } = this.state;
    return classrooms.find((c) => c.id === classroomSelected.id).template;
  };

  getFieldById = (id) => {
    const { fields } = this.state;
    let fieldSelected = null;
    fields.forEach((field) => {
      if (field.id === id) {
        fieldSelected = field;
        return field;
      }
      return field;
    });
    return fieldSelected;
  };

  checkDependence = (field) => {
    if (field.depends_on === null || field.depends_on === '') return true;

    const dependenceField = this.getFieldById(field.depends_on);

    if (dependenceField === null) {
      return true;
    }
    switch (dependenceField.type) {
      case 'Checkbox':
        return this.getValue(dependenceField);
      case 'Range':
        return this.getValue(dependenceField) > 0;
      default:
        return true;
    }
  };

  getValue = (field) => {
    const { fieldValues } = this.state;
    return fieldValues[field.name];
  };

  checkShow = (field) => {
    const { classroomSelected } = this.state;
    return (
      (classroomSelected?.template?.fields.indexOf(field?.id.toString()) !== -1 ||
        classroomSelected?.template?.fields.indexOf(field?.id) !== -1) &&
      this.checkDependence(field) &&
      this.compliesWithConditions(field)
    );
  };

  compliesWithConditions = (field) => {
    const { children } = this.state;
    if (field.conditions.includes('Doesnt Use Bottle') && children.use_bottle) return false;

    if (field.conditions.includes('Uses Bottle') && !children.use_bottle) return false;

    if (field.conditions.includes('Doesnt Use Diaper') && children.use_diaper) return false;

    if (field.conditions.includes('Uses Diaper') && !children.use_diaper) return false;

    return true;
  };

  onValueChange = (field, value) => {
    const { fieldValues } = this.state;
    fieldValues[field.name] = value;
    this.setState({ fieldValues });
  };

  onValueChangeObs = (field, value) => {
    const { fieldObs } = this.state;
    fieldObs[field.name] = value;
    this.setState({ fieldObs });
  };

  fixedFieldValues = () => {
    const { fields, fieldValues } = this.state;
    const fieldNames = fields.filter((f) => this.checkShow(f)).map((f) => f.name);

    const filter = {};
    for (const key of fieldNames) {
      filter[key] = fieldValues[key];
    }
    return filter;
  };

  customMoodName = () => {
    const { moodSelected } = this.state;
    return moodSelected.customName != null && moodSelected.customName !== ''
      ? moodSelected.customName
      : moodSelected.name;
  };

  handleSendReport = async (send = true) => {
    const { user, _sendReportAction } = this.props;
    const {
      moodSelected,
      children,
      classroomSelected,
      fieldObs,
      koalaSelected,
      stickersSelected,
      report,
      observations,
      assignments,
    } = this.state;
    if (!moodSelected) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.selecting/mood`),
        type: 'default',
      });
      return;
    }

    const data = {
      clasroom_id: classroomSelected.id,
      child_id: children.id,
      mood_id: moodSelected.id,
      mood_text: this.customMoodName(),
      assignments_text: assignments,
      observation_text: observations,
      sticker_ids: stickersSelected.map((c) => c.id),
      data: this.fixedFieldValues(),
      school_id: user.schoolId,
      obs_data: fieldObs || {},
      send,
      koala_sticker_id: koalaSelected?.id || null,
    };

    if (report != null) {
      data.report_id = report.id;
    }

    try {
      await _sendReportAction(data, send);
      this.goBack();
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <CreateReportLayout
          goBack={() => this.navigate(REPORTS)}
          handleChangeClassroom={this.handleChangeClassroom}
          handleMoodSelected={this.handleMoodSelected}
          handleStickerSelected={this.handleStickerSelected}
          handleSaveReport={this.handleSaveReport}
          handleSendReport={this.handleSendReport}
          handleKoalaSelected={this.handleKoalaSelected}
          onValueChangeObs={this.onValueChangeObs}
          onValueChange={this.onValueChange}
          checkShow={this.checkShow}
          user={this.props.user}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _getReportFieldsByChildAction: getReportFieldsByChildAction,
  _sendReportAction: sendReportAction,
})(CreateReportController);
