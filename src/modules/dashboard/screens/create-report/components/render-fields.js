import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, Dimensions, Switch } from 'react-native';
import Slider from '@react-native-community/slider';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Styles } from '@assets/Styles';
import FastImage from '@components/FastImage';
import { getImageById } from '@utils/functions/globals';
import colors from '@assets/colors';
import { SvgCss } from 'react-native-svg';
import loadLocalResource from 'react-native-local-resource';
import { svg } from '@shared/icons/import-svg-file';

const { width } = Dimensions.get('screen');

const renderFileds = ({
  checkShow,
  field,
  fieldValues,
  onValueChange = () => {},
  onValueChangeObs = () => {},
}) => {
  const [newSvg, setNewSvg] = useState(null);

  useEffect(() => {
    loadLocalResource(svg[field.icon_name]).then((result) => {
      setNewSvg(result);
    });
  }, []);

  const breakfastText = () => {
    try {
      return field.values[fieldValues[field.name]];
    } catch (e) {
      return '';
    }
  };

  return (
    <View style={[{ width }]}>
      {checkShow(field) && field.type === 'Range' && (
        <>
          <View style={[Styles.rowLarge, { width }]}>
            <View style={[Styles.center, { width: width * 0.2 }]}>
              {newSvg && <SvgCss xml={newSvg} width={55} height={55} />}
              <Text fontSize={13}>{field.name}</Text>
            </View>
            <View
              style={[
                Styles.rowLarge,
                { borderBottomWidth: 1, height: 95, borderBottomColor: 'rgba(0,0,0,0.15)' },
              ]}
            >
              <View style={{ width: width * 0.6 }}>
                <Slider
                  style={{ width: '100%', height: 50 }}
                  minimumValue={0}
                  step={1}
                  maximumValue={field.values.length - 1}
                  minimumTrackTintColor={colors.GREEN}
                  onValueChange={(value) => onValueChange(field, value)}
                  maximumTrackTintColor="rgba(0,0,0,0.1)"
                />
              </View>

              <View style={{ width: width * 0.15 }}>
                <Text fontSize={13}>{breakfastText()}</Text>
              </View>
            </View>
          </View>
        </>
      )}
      {checkShow(field) && field.type === 'Checkbox' && (
        <View style={[Styles.row, { paddingHorizontal: 10, flex: 1 }]}>
          <View style={[Styles.center, { marginBottom: 15, marginRight: 10 }]}>
            {newSvg && <SvgCss xml={newSvg} width={55} height={55} />}
          </View>
          <View
            style={[
              Styles.rowLarge,
              { flex: 1, borderBottomWidth: 1, height: 95, borderBottomColor: 'rgba(0,0,0,0.15)' },
            ]}
          >
            <Text>{field.name}</Text>
            <Switch
              value={fieldValues[field.name]}
              onValueChange={() => onValueChange(field, !fieldValues[field.name])}
            />
          </View>
        </View>
      )}
      {/* checkShow(field) && field.type === 'Text' && (
        <View>
          <View style={[Styles.row, { paddingHorizontal: 10, flex: 1 }]}>
            <View style={[Styles.center, { marginRight: 10 }]}>
              {newSvg && <SvgCss xml={newSvg} width={55} height={55} />}
            </View>{' '}
            <View
              style={[
                Styles.rowLarge,
                {
                  flex: 1,
                  borderBottomWidth: 1,
                  height: 95,
                  borderBottomColor: 'rgba(0,0,0,0.15)',
                },
              ]}
            >
              <Text>{field.name}</Text>
              
            </View>
          </View>
        </View>
            ) */}
      {checkShow(field) && field.type === 'Number' && (
        <View>
          <View style={[Styles.row, { paddingHorizontal: 10, flex: 1 }]}>
            <View style={[Styles.center, { marginRight: 10 }]}>
              {newSvg && <SvgCss xml={newSvg} width={55} height={55} />}
            </View>
            <View
              style={[
                Styles.rowLarge,
                {
                  flex: 1,
                  borderBottomWidth: 1,
                  height: 95,
                  borderBottomColor: 'rgba(0,0,0,0.15)',
                },
              ]}
            >
              <TextInput
                placeholder={field.name}
                style={{ flex: 1 }}
                keyboardType="number-pad"
                placeholderTextColor="rgba(0,0,0,0.6)"
                onChangeText={(changeText) => onValueChange(field, changeText)}
              />
            </View>
          </View>
        </View>
      )}
      {/* checkShow(field) && field.type === 'Time' && (
        <View>
          <View />
        </View>
      ) */}
      {field.depends_on == null && fieldValues[field.name] ? (
        <View
          style={{
            flex: 1,
            borderBottomWidth: 1,
            height: 85,
            paddingBottom: 10,
            paddingHorizontal: 15,
            borderBottomColor: 'rgba(0,0,0,0.15)',
          }}
        >
          <TextInput
            placeholder={`${field.name} more details`}
            style={{ flex: 1 }}
            placeholderTextColor="rgba(0,0,0,0.6)"
            onChangeText={(changeText) => onValueChangeObs(field, changeText)}
          />
        </View>
      ) : null}
    </View>
  );
};

export default renderFileds;
