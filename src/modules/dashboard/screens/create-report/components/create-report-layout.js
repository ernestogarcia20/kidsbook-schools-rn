import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import Slider from '@react-native-community/slider';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Styles } from '@assets/Styles';
import FastImage from '@components/FastImage';
import { getImageById } from '@utils/functions/globals';
import colors from '@assets/colors';
import { SvgCssUri } from 'react-native-svg';
import Button from '@components/Button';
import RenderFields from './render-fields';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileImage: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: 'rgba(0,0,0,0.1)',
  },
});

const Layout = ({
  goBack,
  state,
  user,
  handleChangeClassroom,
  handleMoodSelected,
  handleStickerSelected,
  handleKoalaSelected,
  checkShow,
  onValueChange,
  onValueChangeObs,
  setState,
  handleSendReport,
}) => {
  const {
    classroomSelected,
    children,
    moods,
    fields,
    moodSelected,
    stickers,
    stickersSelected,
    koalaStickers,
    koalaSelected,
    fieldValues,
    selectedMood,
    observations,
    assignments,
  } = state;
  const urlImage = getImageById(children.avatar, user.token);
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.createReport')} onPress={goBack} />
      <KeyboardAvoidingView style={{ flex: 1 }} enabled behavior="position">
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 20 }}
        >
          <View
            style={[
              Styles.rowLarge,
              {
                marginHorizontal: 15,
                paddingTop: 5,
                borderBottomWidth: 1,
                borderBottomColor: 'rgba(0,0,0,0.2)',
              },
            ]}
          >
            <View style={[Styles.row, { height: 45 }]}>
              <View style={[{ width: 30, height: 30 }, Styles.center]}>
                <CustomIcon name="kb_abc" width={30} heigth={30} />
              </View>
              <View style={{ marginTop: 10, marginLeft: 10 }}>
                <Text>Salón</Text>
              </View>
            </View>
            <TouchableOpacity style={Styles.row} onPress={handleChangeClassroom}>
              <Text>{classroomSelected?.name}</Text>
              <View style={{ marginLeft: 5 }}>
                <Icon name="caret-down" size={15} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[Styles.row, { marginHorizontal: 15, marginVertical: 10 }]}>
            <View style={styles.borderImage}>
              <FastImage style={styles.profileImage} source={{ uri: urlImage }} />
            </View>
            <View style={{ marginLeft: 10 }}>
              <Text weight="Medium" fontSize={14}>
                {children?.name}
              </Text>
            </View>
          </View>
          <View>
            <Header min title="Humor" height={40} hiddenBackground color={colors.BLUE} />
          </View>
          <View style={[Styles.row, { flexWrap: 'wrap', paddingVertical: 10 }, Styles.center]}>
            {moods?.map((mood, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={[
                    {
                      backgroundColor:
                        moodSelected?.image_id === mood.image_id ? '#CECECE' : 'white',
                      marginHorizontal: 4,
                      marginTop: 10,
                      borderRadius: 50,
                      padding: 5,
                    },
                    Styles.shadow,
                  ]}
                  onPress={() => handleMoodSelected(mood)}
                >
                  <FastImage
                    source={{ uri: getImageById(mood.image_id, user.token) }}
                    style={{ width: 45, height: 45 }}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
          {moodSelected && (
            <View style={[Styles.row, { marginHorizontal: 15, marginVertical: 5 }]}>
              <FastImage
                source={{ uri: getImageById(moodSelected?.image_id, user.token) }}
                style={{ width: 45, height: 45, marginRight: 10 }}
              />
              <TextInput
                placeholder={moodSelected.name}
                style={{
                  height: 45,
                  borderBottomWidth: 1,
                  borderBottomColor: 'rgba(0,0,0,0.2)',
                  width: '85%',
                }}
                onChangeText={(changeValue) => {
                  setState('selectedMood', { ...selectedMood, customName: changeValue });
                }}
                placeholderTextColor="rgba(0,0,0,0.3)"
              />
            </View>
          )}
          <View style={[{ flexWrap: 'wrap', paddingVertical: 10 }]}>
            {classroomSelected &&
              fields?.map((field, index) => {
                return (
                  <View key={index}>
                    <RenderFields
                      checkShow={checkShow}
                      field={field}
                      fieldValues={fieldValues}
                      onValueChange={onValueChange}
                      onValueChangeObs={onValueChangeObs}
                    />
                  </View>
                );
              })}
          </View>
          <View>
            <Header min title="Etiquetas" height={40} hiddenBackground color={colors.BLUE} />
          </View>
          <View style={[Styles.row, { flexWrap: 'wrap', paddingVertical: 10 }, Styles.center]}>
            {stickers?.map((sticker, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={[
                    {
                      backgroundColor: stickersSelected[sticker.sticker_id] ? '#CECECE' : 'white',
                      marginHorizontal: 4,
                      marginTop: 10,
                      borderRadius: 50,
                      padding: 5,
                    },
                    Styles.shadow,
                  ]}
                  onPress={() => handleStickerSelected(sticker)}
                >
                  <FastImage
                    source={{ uri: getImageById(sticker.sticker_id, user.token) }}
                    style={{ width: 45, height: 45 }}
                  />
                </TouchableOpacity>
              );
            })}
          </View>
          <View>
            <Header min title="Logros" height={40} hiddenBackground color={colors.BLUE} />
          </View>
          <View style={[Styles.row, { flexWrap: 'wrap', paddingVertical: 10 }, Styles.center]}>
            {koalaStickers?.map((sticker, index) => {
  console.log(getImageById(sticker.sticker_id, user?.token, 100))
              return (
                <TouchableOpacity
                  key={index}
                  style={[
                    {
                      backgroundColor:
                        sticker.sticker_id === koalaSelected?.sticker_id ? '#b2b0b059' : undefined,
                      borderRadius: 10,
                      marginHorizontal: 4,
                      marginTop: 10,
                      padding: 5,
                    },
                    Styles.shadow,
                  ]}
                  onPress={() => handleKoalaSelected(sticker)}
                >
                  <SvgCssUri
                    width={70}
                    height={90}
                    uri={getImageById(sticker.sticker_id, user?.token)}
                  />
                  <Text>{sticker.name}</Text>
                </TouchableOpacity>
              );
            })}
          </View>
          <View style={{ marginTop: 30, paddingHorizontal: 15 }}>
            <TextInput
              placeholder="Observaciones"
              placeholderTextColor="rgba(0,0,0,0.5)"
              multiline
              textAlignVertical="top"
              numberOfLines={3}
              onChangeText={(changeText) => setState('observations', changeText)}
              style={{
                flex: 1,
                borderBottomWidth: 1,
                height: 50,
                paddingBottom: 10,
                paddingHorizontal: 15,
                borderBottomColor: 'rgba(0,0,0,0.15)',
              }}
            />
            <TextInput
              placeholder="Asignaciones"
              placeholderTextColor="rgba(0,0,0,0.5)"
              multiline
              textAlignVertical="top"
              numberOfLines={3}
              onChangeText={(changeText) => setState('assignments', changeText)}
              style={{
                flex: 1,
                borderBottomWidth: 1,
                height: 50,
                marginTop: 25,
                paddingBottom: 10,
                paddingHorizontal: 15,
                borderBottomColor: 'rgba(0,0,0,0.15)',
              }}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <View style={[Styles.rowLarge, { paddingHorizontal: '8%', paddingTop: 15 }]}>
        <Button
          title="Salvar"
          style={{ width: '47%' }}
          showGrass={false}
          color={colors.ORANGE}
          onPress={() => handleSendReport(false)}
        />
        <Button
          title="Enviar"
          style={{ width: '47%' }}
          showGrass={false}
          onPress={handleSendReport}
        />
      </View>
    </View>
  );
};

export default Layout;
