import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Search from '@components/Search';
import Colors from '@assets/colors';
import TemplateItem from '@components/TemplateItem';
import ControlButtons from '@components/ControlButtons';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentSearch: {
    paddingHorizontal: '5%',
    paddingVertical: '5%',
  },
  paddingIcon: { paddingRight: 7 },
  center: { justifyContent: 'center', alignItems: 'center' },
  dateText: { position: 'absolute', paddingTop: 3 },
  contentIcons: {
    flexDirection: 'row',
    height: 25,
    marginTop: 5,
    alignItems: 'center',
  },
  buttonIconMargin: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.WHITE,
    marginLeft: 10,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  buttonIcon: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.BLUELIGHT,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  contentActionsEdit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonTrash: {
    width: 30,
    height: 30,
    borderRadius: 5,
    backgroundColor: Colors.RED,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '2%',
  },
  contentSelectedAll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '3.5%',
    marginTop: '7%',
    alignItems: 'center',
  },
  contentEdit: { paddingHorizontal: '5%', paddingTop: '5%' },

  renderItem: {
    height: 90,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: '5%',
    paddingRight: '8%',
    flex: 1,
  },
  rowLarge: {
    alignItems: 'center',
    flexDirection: 'row',
  },
});

const Layout = ({
  goBack,
  state,
  handleSelectedItem,
  handleSearch,
  handleSelectAll,
  setState,
  handleDetailFastAnswer,
  handleDeleteItemSelected,
}) => {
  const { list, multiple, selectAll, currentSelection, maxSelection, edit, key } = state;

  const title = () => {
    return `menu_list.${key}`;
  };

  const renderItem = ({ item, index }) => {
    const disable = currentSelection === maxSelection;
    return (
      <TemplateItem
        index={index}
        childTemplate={false}
        item={item}
        absence
        isFastAnswer={key === 'fastAnswer'}
        onPressSelected={handleSelectedItem}
        handleDetail={handleDetailFastAnswer}
        disable={disable}
        handleSelectedItem={() => (disable && !item.checked ? null : handleSelectedItem(item))}
        editList={multiple}
      />
    );
  };
  const listHeaderComponent = () => {
    return (
      <View>
        {edit && (
          <ControlButtons
            handleSelectAll={handleSelectAll}
            handleManageDelete={handleDeleteItemSelected}
            selectAll={selectAll}
          />
        )}
      </View>
    );
  };
  const handleEdit = () => {
    setState('edit', !edit);
    setState('multiple', !multiple);
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t(title())} onPress={goBack} edit onPressEdit={handleEdit} />
      <View style={styles.contentSearch}>
        <Search onChangeText={handleSearch} />
      </View>
      <FlatList
        data={list}
        extraData={state}
        ListHeaderComponent={listHeaderComponent}
        initialNumToRender={10}
        renderItem={renderItem}
        keyExtractor={(item, index) => item + index}
      />
    </View>
  );
};

export default Layout;
