import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { CREATE_FAST_ANSWER } from '@utils/constants/navigates';
import I18n from 'react-native-redux-i18n';
import { deleteFastAnswerAction } from '@dashboard/actions/fast-answers.actions';
import ListGlobalLayout from './components/global-list-message-layout';

class ListGlobalController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const key = getParam('key', 'recipents');
    this.state = {
      list: [],
      allList: [],
      multiple: false,
      selectAll: false,
      key,
      currentSelection: 0,
    };
  }

  componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const { fastAnswerList } = this.props;
    if (prevProps.fastAnswerList !== fastAnswerList) {
      this.setState({ list: fastAnswerList, allList: fastAnswerList });
    }
  }

  init = async () => {
    const { key } = this.state;
    this.analize();
  };

  analize = () => {
    setTimeout(() => {
      const {
        navigation: { getParam },
      } = this.props;
      const { key } = this.state;
      const listIgnore = getParam('ignore', []);
      const list = getParam('list', []).map((x) => {
        return { ...x, checked: false };
      });
      const ignoreChildren = listIgnore == null ? [] : listIgnore.map((a) => a.id);
      const newList = list.filter((a) => ignoreChildren.indexOf(a.id) === -1);
      this.setState({ list: newList, allList: newList });
    }, 100);
  };

  handleSearch = (text) => {
    const { allList } = this.state;
    if (!text) {
      this.setState({ search: text, list: allList });
      return;
    }
    const newData = allList.filter((item) => {
      const itemData = `${String(item.name).toUpperCase()}`;

      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ list: newData, search: text });
  };

  handleSelectedItem = (item) => {
    const {
      navigation: { getParam },
    } = this.props;
    const { multiple, list, key } = this.state;
    if (multiple) {
      const indexList = list.findIndex((x) => x.id === item.id);
      if (indexList > -1) {
        const itemList = list[indexList];
        list[indexList] = { ...itemList, checked: !itemList.checked };
        const currentSelection = String(list.filter((x) => x.checked).length);
        this.setState({ list, currentSelection });
      }
      return;
    }
    const handleSelected = getParam('handleSelected', () => {});
    const selectedFastAnswer = getParam('selectedFastAnswer', () => {});
    selectedFastAnswer(item);
    handleSelected([item], key);
    this.goBack();
  };

  handleSelectAll = () => {
    const { list, selectAll } = this.state;
    const newList = list.map((item2) => {
      return { ...item2, checked: !item2.checked };
    });
    this.setState({ list: newList, selectAll: !selectAll });
  };

  handleDetailFastAnswer = (fastAnswer) => {
    this.navigate(CREATE_FAST_ANSWER, {
      edit: true,
      idFastAnswer: fastAnswer.id,
      content: fastAnswer.content,
      subject: fastAnswer.subject,
      aliasName: fastAnswer.name,
    });
  };

  handleDeleteItemSelected = () => {
    const { dispatch } = this.props;
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('modalsText.remove'),
        onPressAccept: this.handleDelete,
      },
      dispatch
    );
  };

  handleDelete = async () => {
    const { list, key } = this.state;
    const {
      _deleteFastAnswerAction,
      navigation: { getParam },
    } = this.props;
    const listFilter = list.filter((x) =>
      key === 'recipients' ? !x.checked || x.checked === undefined : x.checked
    );
    const updateList = getParam('updateList', () => {});
    if (key !== 'fastAnswer') {
      updateList(listFilter);
      this.goBack();
      return;
    }
    try {
      listFilter.forEach(async (item) => {
        await _deleteFastAnswerAction(item.id);
      });
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <ListGlobalLayout
          setState={this.handleChange}
          state={this.state}
          goBack={this.goBack}
          user={user}
          handleDeleteItemSelected={this.handleDeleteItemSelected}
          handleDetailFastAnswer={this.handleDetailFastAnswer}
          handleSelectAll={this.handleSelectAll}
          handleSelectedItem={this.handleSelectedItem}
          handleSearch={this.handleSearch}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, fastAnswers: { fastAnswerList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  fastAnswerList,
});

export default connect(mapStateToProps, { _deleteFastAnswerAction: deleteFastAnswerAction })(
  ListGlobalController
);
