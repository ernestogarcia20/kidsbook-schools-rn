import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import { getTeachersAction, deleteTeacherAction } from '@dashboard/actions/teachers.actions';
import BaseComponent from '@components/BaseComponent';
import {
  CHILDREN_DETAIL,
  VERIFICATE_CLIENT,
  ADD_TEACHER,
  DETAIL_TEACHER,
} from '@utils/constants/navigates';
import I18n from 'react-native-redux-i18n';
import TeachersLayout from './components/teachers-layout';

class TeachersController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      editList: false,
      selectAll: false,
      teacherList: props.teacherList,
    };
  }

  componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const { teacherList } = this.props;
    if (prevProps.teacherList !== teacherList) {
      this.setListInState();
    }
  }

  setListInState = () => {
    const { teacherList } = this.props;
    this.setState({ teacherList });
  };

  init = async () => {
    const { _getTeachersAction, teacherList, user } = this.props;
    try {
      const response = await _getTeachersAction(
        user.schoolData.schools[0].id,
        teacherList.length === 0
      );
      console.log(JSON.stringify(response));
    } catch (e) {}
  };

  updateListTeachers = async () => {
    const { _getTeachersAction, user } = this.props;
    try {
      await _getTeachersAction(user.schoolData.schools[0].id, false);
    } catch (e) {}
  };

  onPressAdd = () => {
    this.navigate(ADD_TEACHER, { refreshList: this.updateListTeachers });
    /* this.navigate(CHILDREN_DETAIL, {
      edit: false,
      updateListChild: this.updateListChild,
    }); */
  };

  onPressEdit = () => {
    const { editList } = this.state;
    this.setState({ editList: !editList });
  };

  handleSelectAll = () => {
    const { selectAll, teacherList } = this.state;
    const newList = teacherList.map((child) => {
      return { ...child, checked: !selectAll };
    });
    this.setState({ selectAll: !selectAll, teacherList: newList });
  };

  handleSelectedItem = (item) => {
    const { teacherList } = this.state;
    const index = teacherList.findIndex((it) => it === item);
    if (index < 0) {
      return;
    }
    let child = teacherList[index];
    child = { ...child, checked: !child.checked };
    teacherList[index] = child;
    this.setState({ teacherList });
  };

  handleDelete = async (teacher = null, multipleDelete = false) => {
    const { dispatch } = this.props;

    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t(
          multipleDelete ? 'modalsText.deleteMultipleTeacher' : 'modalsText.deleteTeacher'
        ),
        onPressAccept: () => this.deleteTeachers(teacher, multipleDelete),
      },
      dispatch
    );
  };

  deleteTeachers = async (teacher = null, multipleDelete = false) => {
    const { teacherList } = this.state;
    const { _deleteTeacherAction, dispatch } = this.props;
    if (teacherList.length === 0) {
      return;
    }
    const filter = teacherList.filter((ch) => ch.checked);
    const ids = [];
    if (teacher) {
      ids.push(teacher.pivot.id);
    }
    const deleteIds = teacher ? ids : filter.map((ch) => ch.pivot.id);
    try {
      await _deleteTeacherAction(deleteIds);
      if (!multipleDelete) {
        this.showModalConfirm(
          {
            visible: true,
            title: I18n.t('modalsText.qualify'),
            onPressAccept: () => this.goToQualify(teacher),
          },
          dispatch
        );
      }
      const newTeacherList = teacherList.filter((c) => deleteIds.indexOf(c.pivot.id) === -1);
      this.setState({ teacherList: newTeacherList });
    } catch (e) {}
  };

  goToQualify = (teacher) => {
    this.navigate(VERIFICATE_CLIENT, { client: teacher });
  };

  handleDetailTeacher = (teacher) => {
    this.navigate(DETAIL_TEACHER, { teacher });
  };

  handleVerifyTeacher = (item) => {
    /* this.navigate(CHILDREN_DETAIL, {
      edit: true,
      child: item,
      updateListChild: this.updateListChild,
    }); */
  };

  render() {
    const { teacherList, user } = this.props;
    return (
      <Container>
        <TeachersLayout
          openDrawer={this.openDrawer}
          user={user}
          teacherList={teacherList}
          state={this.state}
          onPressAdd={this.onPressAdd}
          onPressEdit={this.onPressEdit}
          handleDelete={this.handleDelete}
          handleVerifyTeacher={this.goToQualify}
          handleDetailTeacher={this.handleDetailTeacher}
          handleSelectedItem={this.handleSelectedItem}
          handleSelectAll={this.handleSelectAll}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, teacher: { teacherList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  teacherList,
});

export default connect(mapStateToProps, {
  _getTeachersAction: getTeachersAction,
  _deleteTeacherAction: deleteTeacherAction,
})(TeachersController);
