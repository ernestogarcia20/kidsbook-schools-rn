import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import FastImage from '@components/FastImage';
import Text from '@components/Text';
import Colors from '@assets/colors';
import NoAvatar from '@images/no-avatar.jpg';
import Icon from 'react-native-vector-icons/Feather';
import FloatingButton from '@components/FloatingButton';
import Button from '@components/Button';
import { getImageById } from '@utils/functions/globals';
import MultiList from '@components/MultiList';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    width: width / 2,
    height: width / 2,
    zIndex: -1,
    borderWidth: 2,
    borderColor: Colors.GREEN,
    borderRadius: 200,
  },
  borderAvatar: {
    position: 'absolute',
    width: width / 1.9,
    height: width / 1.9,
    zIndex: 1,
  },
  contentAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    marginBottom: '5%',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  marginText: { marginRight: 5 },
  contentInput: { paddingHorizontal: '5%' },
  contentDatePicker: { paddingHorizontal: '5%', marginBottom: '4%', marginTop: -5 },
  contentSwitch: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: '5%',
    paddingTop: '5%',
    borderBottomWidth: 0.5,
    paddingBottom: 13,
  },
  contentInfo: {
    height: 40,
    backgroundColor: '#515151',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: '5%',
    marginTop: '5%',
  },
  contentContainerStyle: { paddingBottom: 20 },
  contentButtonAdd: { paddingBottom: '4%', paddingHorizontal: '6%' },
  contentButtonEdit: { paddingTop: '10%', paddingHorizontal: '6%', marginBottom: '10%' },
  rowInput: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
  contentFormProfile: {
    justifyContent: 'flex-start',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 15,
    marginBottom: '15%',
    marginHorizontal: 20,
  },
});

const Layout = ({ goBack, state, setState }) => {
  const { classroom, childrenList, editListAbsence } = state;

  return (
    <View style={styles.container}>
      <Header title={I18n.t('classroomProfile')} onPress={goBack} />
      <ScrollView
        contentContainerStyle={styles.contentContainerStyle}
        keyboardShouldPersistTaps="always"
      >
        <FloatingButton iconName="kb_actividades" style={{ right: 20, top: 20 }} />
        <View style={styles.contentAvatar}>
          <FastImage
            source={
              classroom?.avatar ? { uri: getImageById(classroom?.avatar, user.token) } : NoAvatar
            }
            style={styles.avatar}
          />
        </View>
        <View style={[styles.row, styles.contentFormProfile]}>
          <View style={{ marginRight: 10 }}>
            <Text weight="SemiBold">{I18n.t('classroomNameTeacher')}: </Text>
          </View>
          <Text>
            {classroom?.name} {classroom?.level}
          </Text>
        </View>
        <MultiList
          title="Niños"
          isChildTemplate
          list={childrenList}
          setState={setState}
          attrName="childrenList"
          absence
          onChangeEdit={(value) => setState('editListAbsence', value)}
        />
      </ScrollView>

      {editListAbsence && (
        <View style={styles.contentButtonAdd}>
          <Button title={I18n.t(`buttons.saveAbsence`)} onPress={() => {}} />
        </View>
      )}
    </View>
  );
};

export default Layout;
