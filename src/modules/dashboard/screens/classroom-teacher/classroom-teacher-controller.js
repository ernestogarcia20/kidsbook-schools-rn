import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { getDetailClassroomAction } from '@modules/dashboard/actions/classroom.actions';
import ClassroomTeacherLayout from './components/classroom-teacher-layout';

class ClassroomTeacherController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const classroom = getParam('classroom', null);
    this.state = {
      classroom,
      childrenList: [],
      editListAbsence: false,
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    const { _getDetailClassroomAction } = this.props;
    const { classroom } = this.state;
    try {
      const result = await _getDetailClassroomAction(classroom.id, true);
      this.setState({ childrenList: result.children });
    } catch (e) {}
  };

  handleAbsence = () => {};

  handleCreateActivity = () => {};

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <ClassroomTeacherLayout
          setState={this.handleChange}
          handleAbsence={this.handleAbsence}
          handleCreateActivity={this.handleCreateActivity}
          goBack={this.goBack}
          user={user}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _getDetailClassroomAction: getDetailClassroomAction,
})(ClassroomTeacherController);
