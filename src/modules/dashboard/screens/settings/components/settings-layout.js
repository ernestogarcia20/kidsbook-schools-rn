import React from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import FastImage from '@components/FastImage';
import Text from '@components/Text';
import Icon from 'react-native-vector-icons/Feather';
import Colors from '@assets/colors';
import CustomIcon from '@components/Icon';
import TextField from '@components/text-input-md';
import NoAvatar from '@images/no-avatar.jpg';
import Button from '@components/Button';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    width: width / 2,
    height: width / 2,
    zIndex: -1,
    borderWidth: 2,
    borderColor: Colors.GREEN,
    borderRadius: 200,
  },
  borderAvatar: {
    position: 'absolute',
    width: width / 1.9,
    height: width / 1.9,
    zIndex: 1,
  },
  contentAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    marginBottom: '5%',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  marginText: { marginRight: 5 },
  contentInput: { paddingHorizontal: '5%' },
  contentContainerStyle: { paddingBottom: 20 },
  buttonTranslate: {
    justifyContent: 'space-between',
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    marginHorizontal: '5%',
    borderBottomColor: 'rgba(0,0,0,0.2)',
    paddingLeft: 15,
    paddingRight: 10,
    marginTop: 15,
  },
});

const Layout = ({ openDrawer, handleChangeImageProfile, updateImage, user, setState, state, handleGoToTranslate }) => {
  const { address, name, email } = state;

  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.setting')} onPress={openDrawer} menu />
      <ScrollView contentContainerStyle={styles.contentContainerStyle}>
        <TouchableOpacity style={styles.contentAvatar} onPress={handleChangeImageProfile}>
          <FastImage
            source={
              updateImage
                ? { uri: updateImage }
                : user?.avatar
                ? { uri: getImageById(user?.avatar, user.token) }
                : NoAvatar
            }
            style={styles.avatar}
          />
        </TouchableOpacity>
        <View style={styles.row}>
          <Text style={styles.marginText}>{I18n.t('pressToChange')}</Text>
          <Icon name="arrow-up" size={19} color={Colors.BLACK} />
        </View>
        <TouchableOpacity style={styles.buttonTranslate} onPress={handleGoToTranslate}>
          <View style={{ alignItems: 'center', flexDirection: 'row' }}>
            <CustomIcon name="kb_translation" height={40} width={40} />
            <Text style={{ marginLeft: 15 }}>{I18n.t('selectLanguage')}</Text>
          </View>
          <Icon name="chevron-right" size={24} />
        </TouchableOpacity>
        <View style={styles.contentInput}>
          <TextField
            label={I18n.t('fullName')}
            highlightColor={Colors.GREEN}
            value={name}
            onChangeText={(text) => setState('name', text)}
          />
        </View>
        <View style={styles.contentInput}>
          <TextField
            label={I18n.t('email')}
            highlightColor={Colors.GREEN}
            value={email}
            onChangeText={(text) => setState('email', text)}
          />
        </View>
        <View style={styles.contentInput}>
          <TextField
            label={I18n.t('address')}
            highlightColor={Colors.GREEN}
            value={address}
            onChangeText={(text) => setState('address', text)}
          />
        </View>
      </ScrollView>
      <View style={{ paddingHorizontal: '25%', marginBottom: '7%' }}>
        <Button title={I18n.t('buttons.edit')} />
      </View>
    </View>
  );
};

export default Layout;
