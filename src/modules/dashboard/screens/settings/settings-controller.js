import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import SettingsLayout from './components/settings-layout';

class SettingsController extends BaseComponent {
  constructor(props) {
    super(props);
    const { user } = props;
    this.state = {
      name: user?.authData?.name || '',
      email: user?.authData?.email || '',
      address: user?.authData?.address || '',
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    // const { _getFeedAction, user } = this.props;

    try {
      // await _getFeedAction(user.schoolId);
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <SettingsLayout
          openDrawer={this.openDrawer}
          setState={this.handleChange}
          handleGoToTranslate={this.handleGoToTranslate}
          user={user}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, i18n }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  i18n,
});

export default connect(mapStateToProps, {})(SettingsController);
