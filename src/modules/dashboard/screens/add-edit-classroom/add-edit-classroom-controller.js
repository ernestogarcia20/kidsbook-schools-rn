import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { getReportTemplatesAction } from '@modules/dashboard/actions/report.actions';
import I18n from 'react-native-redux-i18n';
import ImagePicker from 'react-native-image-picker';
import * as RNFS from 'react-native-fs';
import { showMessage } from 'react-native-flash-message';
import {
  createClassroomAction,
  getDetailClassroomAction,
  updateClassroomAction,
} from '@modules/dashboard/actions/classroom.actions';
import { LIST_GLOBAL, CLASSROOM } from '@utils/constants/navigates';
import AddEditClassroomLayout from './components/add-edit-classroom-layout';
import { fontMaker } from '@components/typografy';

class AddEditClassroomController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const edit = getParam('edit', false);
    const classroom = getParam('classroom', null);
    this.state = {
      edit,
      reportTemplate: [],
      classroomName: classroom?.name || '',
      level: classroom?.level || '',
      maxKids: String(classroom?.max_children || 0),
      maxTeachers: String(classroom?.max_teachers || 0),
      reportTemplateSelected: null,
      customLevel: false,
      classroom,
      childrenList: [],
      teacherList: [],
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    const { _getReportTemplateAction, _getDetailClassroomAction, user } = this.props;
    const { edit, classroom } = this.state;
    try {
      if (edit) {
        const result = await _getDetailClassroomAction(classroom.id, true);
        this.setState({ childrenList: result.children, teacherList: result.teachers });
      }
      const data = await _getReportTemplateAction(user.schoolId, false);
      const reportTemplate = data?.templates || [];
      let reportTemplateSelected = null;
      if (edit) {
        const index = reportTemplate.findIndex((x) => x.id === classroom.template_id);
        if (index > -1) {
          const rt = reportTemplate[index];
          reportTemplateSelected = { ...rt, text: rt.name, value: rt.id };
        }
      }
      this.setState({ reportTemplate, reportTemplateSelected });
    } catch (e) {
      alert(e);
    }
  };

  handleShowLeves = (callback) => {
    const { user, dispatch } = this.props;
    const levels = user.school.levels || [];
    const newLevels = levels.map((level) => {
      return { ...level, text: level.level, value: level.level };
    });
    newLevels.push({ text: I18n.t('newClassroomLevel'), value: 'new' });
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('optionCamera.selectOption'),
        data: newLevels,
        onPressAccept: (selected) => this.handleLevelSelected(selected, callback),
        onPressCancel: callback,
      },
      dispatch
    );
  };

  handleLevelSelected = (selected, callback) => {
    if (selected.value === 'new') {
      this.setState({ customLevel: true }, callback);
      return;
    }
    this.setState({ level: selected.value }, () => callback());
  };

  handleChangeImageProfile = () => {
    const options = {
      title: I18n.t('optionCamera.selectOption'),
      cancelButtonTitle: I18n.t('optionCamera.cancel'),
      customButtons: [{ name: 'camera', title: I18n.t('optionCamera.camera') }],
      takePhotoButtonTitle: '',
      chooseFromLibraryButtonTitle: I18n.t('optionCamera.gallery'),
      quality: 1.0,
      mediaType: 'photo',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: false,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      if (!response.data) {
        return;
      }
      this.setState({ updateImage: `data:image/jpg;base64,${response.data}` });
    });
  };

  handleShowReportTemplate = (callback) => {
    const { dispatch } = this.props;
    const { reportTemplate } = this.state;
    const newReportTemplate = reportTemplate.map((report) => {
      return { text: report.name, value: report.id };
    });
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('optionCamera.selectOption'),
        data: newReportTemplate,
        onPressAccept: (selected) => this.handleReportTemplateSelected(selected, callback),
        onPressCancel: callback,
      },
      dispatch
    );
  };

  handleReportTemplateSelected = (selected, callback) => {
    if (!selected || !selected?.text) {
      return;
    }
    this.setState({ reportTemplateSelected: selected }, () => callback(selected.text));
  };

  handleAddClassroom = async () => {
    const {
      _createClassroomAction,
      navigation: { state },
      user,
    } = this.props;
    const { classroomName, level, maxKids, maxTeachers, reportTemplateSelected } = this.state;
    if (!classroomName) {
      showMessage({
        message: I18n.t(`validation.error/classroomNameEmpty`),
        type: 'default',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }
    const obj = {
      name: classroomName,
      level,
      max_children: maxKids || 0,
      max_teachers: maxTeachers || 0,
      template_id: reportTemplateSelected?.value || 1,
      school_id: user.schoolId,
      teachers: '[]',
      children: '[]',
    };
    try {
      await _createClassroomAction(obj);
      this.navigate(CLASSROOM);
      await state.params.updateList();
    } catch (e) {}
  };

  handleDelete = async (item = null, key = null) => {
    const { dispatch } = this.props;
    const { classroom } = this.state;

    const child = I18n.t(`modalsText.removeAllChildClassroom`);
    const teacher = I18n.t(`modalsText.removeAllTeacherClassroom`);
    let title = key === 'child' ? child : teacher;

    if (item) {
      title = String(I18n.t(`modalsText.removeItemClassroom`))
        .replace('$1', item.name)
        .replace('$2', classroom.name);
    }

    this.showModalConfirm(
      {
        visible: true,
        title,
        onPressAccept: () => this.deleteItemOrMultiple(item, key),
      },
      dispatch
    );
  };

  deleteItemOrMultiple = async (item = null, key) => {
    const {
      _updateClassroomAction,
      user,
      navigation: { state },
    } = this.props;
    const {
      classroomName,
      level,
      maxKids,
      maxTeachers,
      reportTemplateSelected,
      classroom,
    } = this.state;
    let { childrenList, teacherList } = this.state;

    if (key === 'child' && item) {
      const index = childrenList.indexOf(item);
      if (index > -1) {
        childrenList.splice(index, 1);
      }
    } else if (key === 'teacher' && item) {
      const index = teacherList.indexOf(item);
      if (index > -1) {
        teacherList.splice(index, 1);
      }
    } else {
      teacherList = teacherList.filter((x) => !x.checked);
      childrenList = childrenList.filter((x) => !x.checked);
    }

    const newChildrenList = childrenList.map((x) => x.id);
    const newTeacherList = teacherList.map((x) => x.id);

    const obj = {
      name: classroomName,
      level,
      max_children: maxKids,
      max_teachers: maxTeachers,
      template_id: reportTemplateSelected.value || 0,
      school_id: user.schoolId,
      teachers: JSON.stringify(newTeacherList),
      children: JSON.stringify(newChildrenList),
    };

    try {
      await _updateClassroomAction(classroom.id, obj);
      this.setState({ childrenList, teacherList });
      await state.params.updateList();
    } catch (e) {}
  };

  handleEditClassroom = async (item = [], key, editForm = false) => {
    const {
      user,
      _updateClassroomAction,
      navigation: { state },
    } = this.props;

    const {
      classroomName,
      level,
      maxKids,
      maxTeachers,
      reportTemplateSelected,
      childrenList,
      teacherList,
      classroom,
    } = this.state;

    if (item.length > 0) {
      if (key === 'children') {
        childrenList.push(
          ...item.map((x) => {
            return { ...x, checked: false };
          })
        );
      } else {
        teacherList.push(
          ...item.map((x) => {
            return { ...x, checked: false };
          })
        );
      }
    }

    const newChildrenList = childrenList.map((x) => x.id);
    const newTeacherList = teacherList.map((x) => x.id);
    const obj = {
      name: classroomName,
      level,
      max_children: maxKids,
      max_teachers: maxTeachers,
      template_id: reportTemplateSelected?.value || 0,
      school_id: user.schoolId,
      teachers: JSON.stringify(newTeacherList),
      children: JSON.stringify(newChildrenList),
    };

    try {
      this.setState({ childrenList, teacherList });
      await _updateClassroomAction(classroom.id, obj, true, editForm);
      await state.params.updateList();
    } catch (e) {}
    // this.setState({ child });
  };

  handleGoToListGlobal = (key) => {
    const { childrenList, teacherList, maxKids, maxTeachers } = this.state;
    this.navigate(LIST_GLOBAL, {
      key,
      handleSelected: this.handleEditClassroom,
      ignore: key === 'teachers' ? teacherList : childrenList,
      maxSelection: String(
        key === 'teachers'
          ? parseInt(maxTeachers) - teacherList.length
          : parseInt(maxKids) - childrenList.length
      ),
    });
  };

  handleAddMultiListChild = () => {
    this.handleGoToListGlobal('children');
  };

  handleAddMultiListTeacher = () => {
    this.handleGoToListGlobal('teachers');
  };

  handleGoBack = () => {
    this.goBack();
  };

  validateForm = () => {};

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <AddEditClassroomLayout
          setState={this.handleChange}
          handleEditClassroom={this.handleEditClassroom}
          handleAddClassroom={this.handleAddClassroom}
          handleShowLeves={this.handleShowLeves}
          handleChangeImageProfile={this.handleChangeImageProfile}
          handleShowReportTemplate={this.handleShowReportTemplate}
          handleAddMultiListChild={this.handleAddMultiListChild}
          handleAddMultiListTeacher={this.handleAddMultiListTeacher}
          handleDelete={this.handleDelete}
          goBack={this.handleGoBack}
          user={user}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _getReportTemplateAction: getReportTemplatesAction,
  _createClassroomAction: createClassroomAction,
  _getDetailClassroomAction: getDetailClassroomAction,
  _updateClassroomAction: updateClassroomAction,
})(AddEditClassroomController);
