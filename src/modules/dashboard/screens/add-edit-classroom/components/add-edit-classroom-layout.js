import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions, ScrollView } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import FastImage from '@components/FastImage';
import Text from '@components/Text';
import Colors from '@assets/colors';
import NoAvatar from '@images/no-avatar.jpg';
import Icon from 'react-native-vector-icons/Feather';
import TextField from '@components/text-input-md';
import Button from '@components/Button';
import MultiList from '@components/MultiList';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    width: width / 2,
    height: width / 2,
    zIndex: -1,
    borderWidth: 2,
    borderColor: Colors.GREEN,
    borderRadius: 200,
  },
  borderAvatar: {
    position: 'absolute',
    width: width / 1.9,
    height: width / 1.9,
    zIndex: 1,
  },
  contentAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    marginBottom: '5%',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  marginText: { marginRight: 5 },
  contentInput: { paddingHorizontal: '5%' },
  contentDatePicker: { paddingHorizontal: '5%', marginBottom: '4%', marginTop: -5 },
  contentSwitch: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: '5%',
    paddingTop: '5%',
    borderBottomWidth: 0.5,
    paddingBottom: 13,
  },
  contentInfo: {
    height: 40,
    backgroundColor: '#515151',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: '5%',
    marginTop: '5%',
  },
  contentContainerStyle: { paddingBottom: 20 },
  contentButtonAdd: { paddingBottom: '4%', paddingHorizontal: '6%' },
  contentButtonEdit: { paddingTop: '10%', paddingHorizontal: '6%', marginBottom: '10%' },
  rowInput: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 20,
  },
});

const Layout = ({
  goBack,
  state,
  handleChangeImageProfile,
  setState,
  handleEditClassroom,
  handleAddClassroom,
  handleShowLeves,
  handleShowReportTemplate,
  handleAddMultiListChild,
  handleAddMultiListTeacher,
  handleDelete,
}) => {
  const {
    edit,
    updateImage,
    classroom,
    level,
    classroomName,
    maxKids,
    maxTeachers,
    customLevel,
    reportTemplateSelected,
    childrenList,
    teacherList,
  } = state;

  const [refLevel, setRefLevel] = useState(null);

  useEffect(() => {
    if (!customLevel || !refLevel) {
      return;
    }
    setTimeout(() => {
      refLevel.focus();
    }, 150);
  }, [customLevel, refLevel]);
  return (
    <View style={styles.container}>
      <Header
        title={I18n.t(edit ? 'menu_list.editClassroom' : 'menu_list.addClassroom')}
        onPress={goBack}
      />
      <ScrollView
        contentContainerStyle={styles.contentContainerStyle}
        keyboardShouldPersistTaps="always"
      >
        {/*! edit && (
          <>
            <TouchableOpacity style={styles.contentAvatar} onPress={handleChangeImageProfile}>
              <FastImage
                source={
                  updateImage
                    ? { uri: updateImage }
                    : classroom?.avatar
                    ? { uri: getImageById(classroom?.avatar, user.token) }
                    : NoAvatar
                }
                style={styles.avatar}
              />
            </TouchableOpacity>

            <View style={styles.row}>
              <Text style={styles.marginText}>{I18n.t('pressToChange')}</Text>
              <Icon name="arrow-up" size={19} color={Colors.BLACK} />
            </View>
          </>
            ) */}
        <View style={styles.rowInput}>
          <TextField
            label={I18n.t('classroomName')}
            highlightColor={Colors.GREEN}
            value={classroomName}
            wrapperStyle={{ flex: 1, marginRight: 10 }}
            onChangeText={(text) => setState('classroomName', text)}
          />
          <TextField
            refInput={(ref) => {
              setRefLevel(ref);
            }}
            label={I18n.t('classroomLevel')}
            highlightColor={Colors.GREEN}
            value={level}
            wrapperStyle={{ flex: 1, marginLeft: 10 }}
            onPress={customLevel ? null : handleShowLeves}
            onBlur={() => (!level ? setState('customLevel', false) : null)}
            onChangeText={(text) => setState('level', text)}
          />
        </View>
        <View style={styles.rowInput}>
          <TextField
            label={I18n.t('maxKids')}
            highlightColor={Colors.GREEN}
            keyboardType="phone-pad"
            wrapperStyle={{ flex: 1, marginRight: 10 }}
            value={maxKids}
            onChangeText={(text) => setState('maxKids', text)}
          />
          <TextField
            label={I18n.t('maxTeachers')}
            keyboardType="phone-pad"
            highlightColor={Colors.GREEN}
            wrapperStyle={{ flex: 1, marginLeft: 10 }}
            value={maxTeachers}
            onChangeText={(text) => setState('maxTeachers', text)}
          />
        </View>
        <View style={styles.contentInput}>
          <TextField
            label={I18n.t('reportTemplate')}
            highlightColor={Colors.GREEN}
            value={reportTemplateSelected?.text || ''}
            onPress={handleShowReportTemplate}
          />
        </View>
        {edit && (
          <>
            <View style={styles.contentButtonEdit}>
              <Button
                title={I18n.t(`menu_list.${'editClassroom'}`)}
                onPress={() => handleEditClassroom([], null, true)}
              />
            </View>

            <MultiList
              title="Niños"
              isChildTemplate
              list={childrenList}
              setState={setState}
              attrName="childrenList"
              handleAddMultiList={handleAddMultiListChild}
              handleDelete={(child) => handleDelete(child, 'child')}
            />
            <MultiList
              title="Profesores"
              isChildTemplate={false}
              list={teacherList}
              setState={setState}
              attrName="teacherList"
              handleAddMultiList={handleAddMultiListTeacher}
              handleDelete={(teacher) => handleDelete(teacher, 'teacher')}
            />
          </>
        )}
      </ScrollView>
      {!edit && (
        <View style={styles.contentButtonAdd}>
          <Button
            title={I18n.t(`menu_list.${edit ? 'editClassroom' : 'addClassroom'}`)}
            onPress={handleAddClassroom}
          />
        </View>
      )}
    </View>
  );
};

export default Layout;
