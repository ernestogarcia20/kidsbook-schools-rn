import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import FileViewer from 'react-native-file-viewer';
import DocumentPicker from 'react-native-document-picker';
import ImagePicker from 'react-native-image-picker';
import { showMessage } from 'react-native-flash-message';
import { fontMaker } from '@components/typografy';
import I18n from 'react-native-redux-i18n';
import RNFS from 'react-native-fs';
import {
  newPostAction,
  uploadImagePostAction,
} from '@modules/dashboard/actions/social-network.actions';
import loadingAction from '@store/actions/loading.action';
import CreateSocialNetworkLayout from './components/create-social-network-layout';

class CreateSocialNetworkController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      attachList: [],
      content: '',
    };
  }

  handleGoToOpenFile = (attach) => {
    const decUrl = decodeURI(attach.fileData.uri);
    FileViewer.open(decUrl);
  };

  handleDeleteAttach = (attach) => {
    const { attachList } = this.state;
    const index = attachList.findIndex((a) => a === attach);
    attachList.splice(index, 1);
    this.setState({ attachList });
  };

  handleFolder = async () => {
    const { attachList } = this.state;
    if (attachList.length === 6) {
      showMessage({
        message: I18n.t(`validation.error/max6Images`),
        type: 'default',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
        // There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      // Printing the log realted to the file
      const mBytes = Math.round(res.size / (1024 * 1024));
      if (mBytes > 150000) {
        showMessage({
          message: I18n.t(`validation.error/file`),
          type: 'default',
          titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
        });
        // alert(mBytes);
        return;
      }
      const fileExt = res.name.split('.').pop().toLowerCase();
      const filename = res.uri.split('/').pop();
      const name =
        filename.replace(`.${fileExt}`, '').length > 8
          ? filename.replace(`.${fileExt}`, '').substr(0, 8)
          : filename.replace(`.${fileExt}`, '');

      const find = attachList.filter((x) => x.fileNameLarge === filename);
      if (find.length > 0) {
        return;
      }

      const file64 = await RNFS.readFile(decodeURI(res.uri), 'base64');
      if (fileExt === 'png' || fileExt === 'jpg') {
        attachList.push({
          image: res.uri,
          type: 'image',
          fileType: fileExt,
          fileNameLarge: filename,
          fileData: {
            ...res,
            name: `${name}.${fileExt}`,
            uriApi: `data:image/${fileExt};base64,${file64}`,
          },
        });
        this.setState({ attachList });
        return;
      }
      attachList.push({
        image: res.uri,
        type: 'file',
        fileType: fileExt,
        fileNameLarge: filename,
        fileData: {
          ...res,
          name: `${name}.${fileExt}`,
          uriApi: `data:${fileExt};base64,${file64}`,
        },
      });
      this.setState({ attachList });
      // Setting the state to show single file attributes 60mb
      // setSingleFile(res);
    } catch (err) {
      // setSingleFile(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        // alert('Canceled');
      } else {
        // For Unknown Error
        // alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  handleCamera = () => {
    const { attachList } = this.state;
    if (attachList.length === 6) {
      showMessage({
        message: I18n.t(`validation.error/max6Images`),
        type: 'default',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }
    const options = {
      quality: 1.0,
      mediaType: 'photo',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: false,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      if (!response.data) {
        return;
      }

      const filename = response.uri.split('/').pop();
      const type = filename.split('.').pop();
      const name = filename.replace(`.${type}`, '').substr(0, 8);
      const attach = `data:image/jpg;base64,${response.data}`;
      attachList.push({
        image: attach,
        type: 'image',
        fileData: { name: `${name}.${type}`, uri: response.uri, uriApi: attach },
      });
      this.setState({ attachList });
    });
  };

  handleNewPost = async () => {
    const {
      _newPostAction,
      _loadingAction,
      user,
      navigation: { getParam },
    } = this.props;
    const { content } = this.state;
    const obj = {
      content,
      school_id: user.schoolId,
      client_id: user.id,
      likes: 0,
      comments: 0,
    };
    const updatedList = getParam('updatedList', () => {});
    try {
      const data = await _newPostAction(obj);
      this.setState({ postId: data.id }, async () => {
        try {
          await this.sendImageByIndex(0);
          this.goBack();
          updatedList();
          _loadingAction(false);
        } catch (e) {
          console.log('eee2', e);
          _loadingAction(false);
        }
      });
    } catch (e) {
      console.log('eee3', e);
      _loadingAction(false);
    }
  };

  sendImageByIndex = (index) => {
    return new Promise((solve, reject) => {
      const { attachList } = this.state;
      this.sendImage(attachList[index])
        .then(() => {
          if (index < attachList.length - 1)
            this.sendImageByIndex(index + 1)
              .then(() => solve())
              .catch(() => solve());
          else solve();
        })
        .catch(() => {
          if (index < attachList.length - 1)
            this.sendImageByIndex(index + 1)
              .then(() => solve())
              .catch(() => solve());
          else solve();
        });
    });
  };

  sendImage = ({ fileData }) => {
    const { _uploadImagePostAction } = this.props;
    const { postId } = this.state;
    return new Promise(async (solve, reject) => {
      const obt = { imageData: fileData.uriApi, postId };
      try {
        await _uploadImagePostAction(obt);
        solve();
      } catch (e) {
        reject(e);
      }
    });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <CreateSocialNetworkLayout
          goBack={this.goBack}
          handleGoToOpenFile={this.handleGoToOpenFile}
          setState={this.handleChange}
          handleFolder={this.handleFolder}
          handleCamera={this.handleCamera}
          handleDeleteAttach={this.handleDeleteAttach}
          handleNewPost={this.handleNewPost}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _newPostAction: newPostAction,
  _uploadImagePostAction: uploadImagePostAction,
  _loadingAction: loadingAction,
})(CreateSocialNetworkController);
