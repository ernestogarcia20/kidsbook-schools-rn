import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Button from '@components/Button';
import Underline from '@components/text-input-md/lib/Underline';
import { fontMaker } from '@components/typografy';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@assets/colors';
import { Styles } from '@assets/Styles';
import FastImage from '@components/FastImage';

const { width } = Dimensions.get('window');

const hitSlop = { top: 25, bottom: 15, right: 25 };

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentRelationship: {
    borderRadius: 3,
    borderColor: colors.GREEN,
    borderWidth: 1,
    paddingVertical: 4,
    paddingHorizontal: 15,
    minWidth: 65,
    marginLeft: 5,
  },
  contentInfo: {
    paddingTop: '7%',
    paddingBottom: '3%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    marginHorizontal: '5%',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  paddingLeft: {
    paddingLeft: 10,
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 100,
    backgroundColor: '#DADADA',
    marginRight: 15,
  },
  contentRating: {
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',

    marginHorizontal: '5%',
    marginTop: '4%',
    paddingBottom: '4%',
  },
  multiLine: {
    ...fontMaker('Medium'),
    fontSize: 14,
  },
  contentMessage: {
    marginTop: '5%',
  },
  paddingHorizontal: {
    paddingHorizontal: 20,
  },
  buttonDeleteAttach: {
    position: 'absolute',
    top: -10,
    right: 15,
    width: 20,
    height: 20,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.DANGER,
  },
});

const Layout = ({
  goBack,
  state,
  setState,
  handleCamera,
  handleFolder,
  handleGoToOpenFile,
  handleDeleteAttach,
  handleNewPost
}) => {
  const { content, attachList } = state;
  const [refLine, setRefLine] = useState(null);
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.socialNetwork')} onPress={goBack} />

      <View style={styles.container}>
        <ScrollView>
          <View style={{ marginHorizontal: '5%', marginTop: '5%' }}>
            <TextInput
              editable
              maxLength={40}
              multiline
              placeholder="Contenido"
              textAlignVertical="top"
              numberOfLines={4}
              style={styles.multiLine}
              onFocus={() => {
                refLine && refLine.expandLine();
              }}
              onBlur={() => {
                refLine && refLine.shrinkLine();
              }}
              onChangeText={(text) => setState('content', text)}
              value={content}
            />
            <Underline
              ref={(ref) => {
                setRefLine(ref);
              }}
              highlightColor={colors.GREEN}
              duration={200}
              borderColor="#E0E0E0"
            />
          </View>
          <View style={[styles.contentMessage, styles.paddingHorizontal, { marginTop: '5%' }]}>
            <View style={[Styles.rowLarge, styles.contentLine]}>
              <View style={Styles.row}>
                <View style={Styles.center}>
                  <CustomIcon name="attach" width={25} heigth={25} />
                </View>
                <Text>Agregar imagenes ({attachList.length}/6)</Text>
              </View>
              <View style={[Styles.row, { paddingRight: 15 }]}>
                <TouchableOpacity
                  style={{ marginRight: 15 }}
                  hitSlop={hitSlop}
                  onPress={handleCamera}
                >
                  <Icon name="instagram" size={24} />
                </TouchableOpacity>
                <TouchableOpacity hitSlop={hitSlop} onPress={handleFolder}>
                  <Icon name="folder" size={24} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {attachList.length > 0 && (
            <View
              style={[
                styles.contentMessage,
                styles.paddingHorizontal,
                Styles.row,
                { flex: 1, flexWrap: 'wrap', marginTop: '4%' },
              ]}
            >
              {attachList.map((attach, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() => handleGoToOpenFile(attach)}
                    style={{
                      minHeight: 90,
                      width: width / 3.4,
                      marginTop: 10,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    {attach.type === 'image' ? (
                      <>
                        <FastImage
                          source={{ uri: attach.image }}
                          resizeMode="stretch"
                          style={{ height: 90, width: 70, borderRadius: 5 }}
                        />
                        <Text>{attach.fileData.name}</Text>
                      </>
                    ) : (
                      <>
                        <View
                          style={{ height: 90, width: 70, backgroundColor: 'rgba(0,0,0,0.1)' }}
                        />
                        <Text>{attach.fileData.name}</Text>
                      </>
                    )}
                    <TouchableOpacity
                      style={styles.buttonDeleteAttach}
                      onPress={() => handleDeleteAttach(attach)}
                    >
                      <Icon name="x" size={15} color="white" />
                    </TouchableOpacity>
                  </TouchableOpacity>
                );
              })}
            </View>
          )}
        </ScrollView>
      </View>
      <View style={{ padding: 15 }}>
        <Button title={I18n.t('buttons.send')} onPress={handleNewPost} />
      </View>
    </View>
  );
};

export default Layout;
