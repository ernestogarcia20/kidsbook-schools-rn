import React, { useState } from 'react';
import { View, StyleSheet, ScrollView, TextInput, Platform } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import Colors from '@assets/colors';
import Emoji from '@components/Emoji';
import Button from '@components/Button';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentRecipients: {
    paddingBottom: 10,
  },
  contentContainerStyle: {
    paddingVertical: '5%',
  },
  recipientButton: {
    height: 30,
    width: '48%',
    backgroundColor: Colors.GREEN,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5,
    marginBottom: 10,
  },
  contentButtons: {
    flex: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  buttonDelete: {
    borderRadius: 5,
    height: 28,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  controls: {
    marginTop: 15,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 10,
    borderBottomWidth: 1,
  },
  contentRecipent: {
    marginTop: 15,
    maxHeight: 210,
  },
  recipentItem: {
    height: 55,
    justifyContent: 'center',
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 10,
    borderBottomWidth: 1,
    marginBottom: 10,
    paddingLeft: 15,
  },
  contentEvent: {
    marginTop: 20,
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 100,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  paddingHorizontal: {
    paddingHorizontal: 20,
  },
  separator: {
    marginTop: 5,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentMessage: {
    marginTop: '5%',
  },
  contentLine: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)',
  },
  input: { flex: 1, height: 80, marginLeft: 15 },
  inputMultiline: {
    height: 95,
    paddingTop: Platform.OS === 'ios' ? 10 : 5,
    paddingBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  contenButtonSend: { marginBottom: '5%', marginHorizontal: '8%' },
  emptyList: { height: 45, justifyContent: 'center', alignItems: 'center' },
});
const Layout = ({
  goBack,
  setState,
  state,
  handleCreateFastAnswer,
  handleUpdateFastAnswer,
  handleDeleteFastAnswer,
}) => {
  const { subject, content, aliasName, edit } = state;
  const [cursorPosition, setCursorPosition] = useState(null);

  const onEmoji = (emoji) => {
    const formatText = cursorPosition
      ? [content.slice(0, cursorPosition), emoji, content.slice(cursorPosition)].join('')
      : content.concat(emoji);
    setState('content', formatText);
    setCursorPosition(null);
  };

  const onSelectionChange = ({ nativeEvent: { selection } }) => {
    setCursorPosition(selection.start);
  };

  const input = ({ multiline = false, title, placeholder, value, attrName, updateMasterState }) => {
    return (
      <View style={[styles.paddingHorizontal]}>
        <View style={[styles.row, styles.separator, styles.contentLine]}>
          <Text>{title}: </Text>
          {!multiline && (
            <TextInput
              style={styles.input}
              multiline={multiline}
              placeholder={placeholder}
              value={value}
              onChangeText={(text) => updateMasterState(attrName, text)}
              placeholderTextColor="#909090"
            />
          )}
        </View>
        {multiline && (
          <View style={[styles.contentLine, styles.inputMultiline]}>
            <Emoji onEmoji={onEmoji}>
              <TextInput
                textAlignVertical="top"
                style={styles.input}
                multiline={multiline}
                onSelectionChange={onSelectionChange}
                placeholder={placeholder}
                placeholderTextColor="#909090"
                value={value}
                onChangeText={(text) => updateMasterState(attrName, text)}
              />
            </Emoji>
          </View>
        )}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.createFastAnswer')} onPress={goBack} />
      <ScrollView contentContainerStyle={{ paddingTop: '5%' }}>
        {input({
          multiline: false,
          title: 'Alias  de la respuesta rapida',
          placeholder: 'Escriba el Alias',
          value: aliasName,
          attrName: 'aliasName',
          updateMasterState: setState,
        })}
        {input({
          multiline: false,
          title: 'Asunto',
          placeholder: '¿De que trata este mensaje?',
          value: subject,
          attrName: 'subject',
          updateMasterState: setState,
        })}
        {input({
          multiline: true,
          title: 'Cuerpo',
          placeholder: 'Escriba su mensaje acá',
          value: content,
          attrName: 'content',
          updateMasterState: setState,
        })}
      </ScrollView>
      <View style={styles.contenButtonSend}>
        <Button
          title={edit ? 'Editar repuesta rápida' : 'Crear respuesta rápida'}
          onPress={edit ? handleUpdateFastAnswer : handleCreateFastAnswer}
        />
      </View>
      {edit && (
        <View style={styles.contenButtonSend}>
          <Button
            title="Eliminar respuesta rápida"
            onPress={handleDeleteFastAnswer}
            color={Colors.DANGER}
          />
        </View>
      )}
    </View>
  );
};

export default Layout;
