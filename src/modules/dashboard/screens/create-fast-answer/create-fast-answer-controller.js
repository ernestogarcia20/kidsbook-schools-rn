import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {
  createFastAnswerAction,
  deleteFastAnswerAction,
  editFastAnswerAction,
} from '@dashboard/actions/fast-answers.actions';
import CreateFastAnswerLayout from './components/create-fast-answer-layout';

class CreateFastAnswerController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const oldFastAnswer = {
      idFastAnswer: getParam('idFastAnswer', null),
      subject: getParam('subject', ''),
      content: getParam('content', ''),
      aliasName: getParam('aliasName', ''),
    };
    this.state = {
      oldFastAnswer,
      idFastAnswer: getParam('idFastAnswer', null),
      subject: getParam('subject', ''),
      content: getParam('content', ''),
      aliasName: getParam('aliasName', ''),
      edit: getParam('edit', false),
    };
  }

  handleCreateFastAnswer = async () => {
    const { subject, content, aliasName } = this.state;
    const { user, _createFastAnswerAction } = this.props;
    if (!subject || !content || !aliasName) {
      return;
    }
    try {
      const data = {
        school_id: user.schoolId,
        content,
        subject,
        name: aliasName,
      };
      await _createFastAnswerAction(data);
      this.goBack();
    } catch (e) {}
  };

  handleUpdateFastAnswer = async () => {
    const { subject, content, aliasName, idFastAnswer, oldFastAnswer } = this.state;
    const { _editFastAnswerAction, fastAnswerList } = this.props;
    if (!subject || !content || !aliasName) {
      return;
    }
    if (
      subject === oldFastAnswer.subject &&
      content === oldFastAnswer.content &&
      aliasName === oldFastAnswer.aliasName
    ) {
      return;
    }
    try {
      const data = {
        id: idFastAnswer,
        content,
        subject,
        name: aliasName,
      };
      await _editFastAnswerAction(data, fastAnswerList);
      this.goBack();
    } catch (e) {}
  };

  handleDeleteFastAnswer = async () => {
    const { idFastAnswer } = this.state;
    const { _deleteFastAnswerAction, fastAnswerList } = this.props;
    if (!idFastAnswer) {
      return;
    }
    try {
      await _deleteFastAnswerAction(idFastAnswer, fastAnswerList);
      this.goBack();
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <CreateFastAnswerLayout
          goBack={this.goBack}
          handleCreateFastAnswer={this.handleCreateFastAnswer}
          handleUpdateFastAnswer={this.handleUpdateFastAnswer}
          handleDeleteFastAnswer={this.handleDeleteFastAnswer}
          user={user}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, fastAnswers: { fastAnswerList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  fastAnswerList,
});

export default connect(mapStateToProps, {
  _createFastAnswerAction: createFastAnswerAction,
  _deleteFastAnswerAction: deleteFastAnswerAction,
  _editFastAnswerAction: editFastAnswerAction,
})(CreateFastAnswerController);
