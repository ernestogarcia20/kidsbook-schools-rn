import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { DESIGN_REPORTS } from '@utils/constants/navigates';
import ImagePicker from 'react-native-image-picker';
import I18n from 'react-native-redux-i18n';
import { updateSchoolAction } from '@modules/dashboard/actions/dashboard.actions';
import SchoolOptionsLayout from './components/school-options-layout';

class SchoolOptionsController extends BaseComponent {
  constructor(props) {
    super(props);
    const { user } = props;
    this.state = {
      schoolName: user?.school.name || '',
      schoolNumber: user?.school.phone || '',
      schoolAddress: user?.school.address || '',
      schoolDescription: user?.school.description || '',
      profileImage: user?.school.profile_image || null,
    };
  }

  componentDidUpdate(prevProps) {
    const { user } = this.props;
    if (user?.school !== prevProps?.user?.school) {
      this.updateForm();
    }
  }

  updateForm = () => {
    const { user } = this.props;
    if (!user) {
      return;
    }
    this.setState({
      schoolName: user?.school?.name || '',
      schoolNumber: user?.school?.phone || '',
      schoolAddress: user?.school?.address || '',
      schoolDescription: user?.school?.description || '',
      profileImage: user?.school?.profile_image || null,
    });
  };

  handleChangeImageProfile = () => {
    const options = {
      title: I18n.t('optionCamera.selectOption'),
      cancelButtonTitle: I18n.t('optionCamera.cancel'),
      customButtons: [{ name: 'camera', title: I18n.t('optionCamera.camera') }],
      takePhotoButtonTitle: '',
      chooseFromLibraryButtonTitle: I18n.t('optionCamera.gallery'),
      quality: 1.0,
      mediaType: 'photo',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: false,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        ImagePicker.launchCamera(
          {
            mediaType: 'photo',
            includeBase64: false,
            maxHeight: 500,
            maxWidth: 500,
          },
          (response2) => {
            this.setState({ updateImage: `data:image/jpg;base64,${response2.data}` });
          }
        );
      }
      if (!response.data) {
        return;
      }
      this.setState({ updateImage: `data:image/jpg;base64,${response.data}` });
    });
  };

  validateForm = () => {
    const { user } = this.props;
    const { updateImage, schoolName, schoolNumber, schoolAddress, schoolDescription } = this.state;
    if (
      !updateImage &&
      user?.school?.name === schoolName &&
      user?.school?.phone === schoolNumber &&
      user?.school?.address === schoolAddress &&
      user?.school?.description === schoolDescription
    ) {
      return false;
    }
    return true;
  };

  handleUpdateSchoolData = async () => {
    const { _updateSchoolAction, user } = this.props;
    const {
      updateImage,
      schoolName,
      schoolNumber,
      schoolAddress,
      schoolDescription,
      profileImage,
    } = this.state;
    if (!this.validateForm()) {
      return;
    }
    let data = {
      address: schoolAddress,
      name: schoolName,
      phone: schoolNumber,
      description: schoolDescription,
      city: user.school.city,
      profile_image: profileImage,
      profile: null,
      assigned_to: user.school.id,
      website: user.school.website,
    };
    if (updateImage) {
      data = { ...data, profile_image: updateImage };
    }
    try {
      await _updateSchoolAction(user.schoolId, data, user);
    } catch (e) {}
  };

  handleGoToDesignReport = () => {
    this.navigate(DESIGN_REPORTS);
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <SchoolOptionsLayout
          openDrawer={this.openDrawer}
          setState={this.handleChange}
          handleChangeImageProfile={this.handleChangeImageProfile}
          handleUpdateSchoolData={this.handleUpdateSchoolData}
          handleGoToDesignReport={this.handleGoToDesignReport}
          user={this.props.user}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, { _updateSchoolAction: updateSchoolAction })(
  SchoolOptionsController
);
