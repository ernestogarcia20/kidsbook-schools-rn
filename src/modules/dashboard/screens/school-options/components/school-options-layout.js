import React from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity, Dimensions } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import FastImage from '@components/FastImage';
import Text from '@components/Text';
import Icon from 'react-native-vector-icons/Feather';
import Colors from '@assets/colors';
import CustomIcon from '@components/Icon';
import TextField from '@components/text-input-md';
import NoAvatar from '@images/no-avatar.jpg';
import Button from '@components/Button';
import { getImageById } from '@utils/functions/globals';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  avatar: {
    width: width / 2,
    height: width / 2,
    zIndex: -1,
    borderWidth: 2,
    borderColor: Colors.GREEN,
    borderRadius: 200,
  },
  borderAvatar: {
    position: 'absolute',
    width: width / 1.9,
    height: width / 1.9,
    zIndex: 1,
  },
  contentAvatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    marginBottom: '5%',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  marginText: { marginRight: 5 },
  contentInput: { paddingHorizontal: '5%' },
  contentContainerStyle: { paddingBottom: 20 },
  buttonTranslate: {
    justifyContent: 'space-between',
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    marginHorizontal: '5%',
    borderBottomColor: 'rgba(0,0,0,0.2)',
    paddingLeft: 15,
    paddingRight: 10,
    marginTop: 15,
  },
  schoolData: {
    height: 60,
    justifyContent: 'center',
    borderBottomWidth: 1,
    marginHorizontal: '5%',
    borderBottomColor: 'rgba(0,0,0,0.2)',
    marginTop: 11,
  },
  buttonAction: {
    minHeight: 50,
    paddingVertical: '5%',
    marginHorizontal: '5%',
    borderBottomWidth: 1,
    marginTop: 5,
    borderBottomColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 15,
  },
  iconAction: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    marginRight: '8%',
  },
});

const Layout = ({
  openDrawer,
  handleChangeImageProfile,
  user,
  setState,
  state,
  handleUpdateSchoolData,
  handleGoToDesignReport,
}) => {
  const { schoolName, schoolNumber, schoolAddress, schoolDescription, updateImage } = state;
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.schoolOptions')} onPress={openDrawer} menu />
      <ScrollView contentContainerStyle={styles.contentContainerStyle}>
        <TouchableOpacity style={styles.contentAvatar} onPress={handleChangeImageProfile}>
          <FastImage
            source={
              updateImage
                ? { uri: updateImage }
                : user?.school?.profile_image
                ? { uri: getImageById(user?.school?.profile_image, user?.token) }
                : NoAvatar
            }
            style={styles.avatar}
          />
        </TouchableOpacity>
        <View style={styles.row}>
          <Text style={styles.marginText}>{I18n.t('pressToChange')}</Text>
          <Icon name="arrow-up" size={19} color={Colors.BLACK} />
        </View>

        <View style={styles.schoolData}>
          <Text weight="SemiBold">{I18n.t('schoolData')}</Text>
        </View>
        <View style={styles.contentInput}>
          <TextField
            label={I18n.t('schoolName')}
            highlightColor={Colors.GREEN}
            value={schoolName}
            onChangeText={(text) => setState('schoolName', text)}
          />
        </View>
        <View style={styles.contentInput}>
          <TextField
            label={I18n.t('schoolNumber')}
            highlightColor={Colors.GREEN}
            value={schoolNumber}
            onChangeText={(text) => setState('schoolNumber', text)}
          />
        </View>
        <View style={styles.contentInput}>
          <TextField
            label={I18n.t('schoolAddress')}
            highlightColor={Colors.GREEN}
            value={schoolAddress}
            onChangeText={(text) => setState('schoolAddress', text)}
          />
        </View>
        <View style={styles.contentInput}>
          <TextField
            label={I18n.t('schoolDescription')}
            highlightColor={Colors.GREEN}
            value={schoolDescription}
            onChangeText={(text) => setState('schoolDescription', text)}
          />
        </View>
        <View style={{ paddingHorizontal: '25%', marginTop: '8%' }}>
          <Button title={I18n.t('buttons.update')} onPress={handleUpdateSchoolData} />
        </View>
        <View style={styles.schoolData}>
          <Text weight="SemiBold">{I18n.t('actions')}</Text>
        </View>
        <TouchableOpacity
          style={[styles.rowLarge, styles.buttonAction]}
          onPress={handleGoToDesignReport}
        >
          <View style={styles.row}>
            <View style={styles.iconAction}>
              <CustomIcon name="kb_reports" />
            </View>
            <Text>{I18n.t('designReport')}</Text>
          </View>
          <Icon name="chevron-right" size={24} />
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Layout;
