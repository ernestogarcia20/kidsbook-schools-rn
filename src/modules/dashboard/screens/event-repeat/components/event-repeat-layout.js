import React from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import Icon from 'react-native-vector-icons/Feather';
import I18n from 'react-native-redux-i18n';
import colors from '@assets/colors';
import Button from '@components/Button';
import DateTimeInput from '@components/DateTimeInput';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainerStyle: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  renderItem: {
    flexDirection: 'row',
    borderBottomColor: '#A0A0A0',
    borderBottomWidth: 1,
    justifyContent: 'space-between',
    paddingVertical: 15,
    marginBottom: 5,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
  buttonSelected: {
    backgroundColor: colors.GREEN,
    justifyContent: 'center',
    alignItems: 'center',
    width: 25,
    height: 25,
    borderRadius: 100,
  },
});

const Layout = ({ goBack, state, handleSelection, onPressSave, onSaveDate }) => {
  const { optionList, perDayObj, startDate } = state;
  const renderItem = ({ item, index }) => {
    const { text, modal, checked, disableMode } = item;
    let iconColor = colors.BLACK;
    if (!modal && !checked) iconColor = '#BBBBBB';
    else if (!modal && checked) iconColor = colors.WHITE;
    return (
      <>
        <View style={[styles.renderItem]}>
          <Text align="left">
            {text}
            {perDayObj && modal && (
              <Text fontSize={13} weight="Medium" color="rgba(0,0,0,0.5)">
                {'   '}({perDayObj?.joins})
              </Text>
            )}
          </Text>
          <TouchableOpacity
            onPress={() => handleSelection(item)}
            style={!checked ? { width: 25, height: 25 } : styles.buttonSelected}
            hitSlop={{ left: 30, right: 30, top: 20, bottom: 20 }}
          >
            <Icon
              name={modal ? 'chevron-right' : 'check'}
              size={modal ? 24 : checked ? 18 : 22}
              color={iconColor}
            />
          </TouchableOpacity>
        </View>

        {((checked && index !== 0) || (perDayObj && index === optionList.length - 1)) && (
          <DateTimeInput
            customPicker
            disableMode={disableMode}
            startDate={startDate}
            perDayObj={perDayObj}
            onSave={(dateSelected) => onSaveDate(disableMode, dateSelected)}
          />
        )}
      </>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.repeat')} onPress={goBack} />
      <FlatList
        data={optionList}
        renderItem={renderItem}
        contentContainerStyle={styles.contentContainerStyle}
        extraData={optionList}
        keyExtractor={(item, index) => item + index}
      />
      <View style={{ marginHorizontal: '10%', marginBottom: 25 }}>
        <Button title="Seleccionar" onPress={onPressSave} />
      </View>
    </View>
  );
};

export default Layout;
