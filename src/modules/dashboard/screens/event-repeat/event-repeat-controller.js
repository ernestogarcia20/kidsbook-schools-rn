import React from 'react';
import { connect } from 'react-redux';
import I18n from 'react-native-redux-i18n';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import moment from 'moment';
import EventRepeatLayout from './components/event-repeat-layout';

class EventRepeatController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const startDate = getParam('startDate', null);
    this.state = {
      optionList: [
        {
          text: I18n.t('eventOptions.never'),
          key: 'never',
          modal: false,
        },
        {
          text: I18n.t('eventOptions.allWeeks'),
          key: 'allWeeks',
          modal: false,
          disableMode: 'weeks',
        },
        {
          text: I18n.t('eventOptions.biweekly'),
          key: 'biweekly',
          disableMode: 'twoWeeks',
          modal: false,
        },
        {
          text: I18n.t('eventOptions.eachMonth'),
          disableMode: 'month',
          key: 'eachMonth',
          modal: false,
        },
        {
          text: I18n.t('eventOptions.perDay'),
          key: 'perDay',
          disableMode: 'perDay',
          modal: true,
        },
      ],
      perDayObj: null,
      dateSelected: null,
      repeatOption: null,
      startDate: moment(startDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
    };
  }

  handleSelection = (item) => {
    const { modal } = item;
    const { optionList } = this.state;
    const { dispatch, user } = this.props;
    if (modal) {
      this.showModalConfirm(
        {
          visible: true,
          title: I18n.t('modalsText.selectDays'),
          subTitle: I18n.t('modalsText.dayOrDays'),
          data: I18n.t('listDays'),
          multiSelection: true,
          onPressAccept: this.modalSelected,
        },
        dispatch
      );
      return;
    }

    const newList = optionList.map((x) => {
      if (x.text === item.text) {
        return { ...x, checked: !x.checked };
      }
      return { ...x, checked: false };
    });
    this.setState({ optionList: newList, repeatOption: item });
  };

  modalSelected = (response) => {
    const { optionList } = this.state;
    const perDayObj = { joins: response.map((x) => x.text).join(', '), allList: response };
    const newOptionList = optionList.map((x) => {
      return { ...x, checked: false };
    });
    this.setState({ perDayObj, optionList: newOptionList, dateSelected: null, repeatOption: null });
  };

  onPressSave = () => {
    const {
      navigation: { state },
    } = this.props;
    const { perDayObj, repeatOption, dateSelected } = this.state;
    if (!repeatOption && !perDayObj) {
      return;
    }
    const newObj = {
      text: repeatOption ? repeatOption.text : perDayObj.joins,
      objComplete: repeatOption || perDayObj,
      key: repeatOption ? repeatOption.key : 'perDay',
      dateSelected,
    };
    state.params.handleSelection(newObj);
    this.goBack();
  };

  onSaveDate = (key, date) => {
    if (!date) {
      return;
    }

    this.setState({ dateSelected: date });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <EventRepeatLayout
          goBack={this.goBack}
          onPressSave={this.onPressSave}
          onSaveDate={this.onSaveDate}
          handleSelection={this.handleSelection}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps)(EventRepeatController);
