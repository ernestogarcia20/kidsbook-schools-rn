import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import { logOut } from '@auth/actions/auth.action';
import {
  getDashboardAction,
  getEventCalendarAction,
  getBirthdayListAction,
} from '@dashboard/actions/dashboard.actions';
import { getClassroomAction } from '@dashboard/actions/classroom.actions';
import { getTeacherActivitiesAction } from '@dashboard/actions/activities.actions';
import BaseComponent from '@components/BaseComponent';
import {
  SIGN_IN,
  CHILDREN,
  TEACHERS,
  PARENTS,
  CLASSROOM,
  MESSAGES,
  ACTIVITIES,
  ACTIVITY_DETAILS,
  CLASSROOM_TEACHER,
  BIRTHDAY,
} from '@utils/constants/navigates';
import Colors from '@assets/colors';
import DashboardLayout from './components/dashboard-layout';

class DashboardController extends BaseComponent {
  constructor(props) {
    super(props);
    const { user } = props;
    const tags = !user.isTeacher
      ? [
          {
            label: 'children',
            icon: 'kb_babyBoy',
            count: -1,
            color: Colors.BLUELIGHT,
            page: CHILDREN,
          },
          {
            label: 'parents',
            icon: 'kb_parents',
            count: -1,
            color: Colors.BLUE,
            page: PARENTS,
          },
          {
            label: 'teachers',
            icon: 'kb_teachers',
            count: -1,
            color: Colors.PURPLE,
            page: TEACHERS,
          },
          {
            label: 'classrooms',
            icon: 'kb_school',
            count: -1,
            color: Colors.RED,
            page: CLASSROOM,
          },
          {
            label: 'activities',
            icon: 'kb_actividades',
            count: -1,
            color: Colors.ORANGE,
            page: ACTIVITIES,
          },
          {
            label: 'messages',
            icon: 'kb_messages',
            count: -1,
            color: Colors.YELLOW,
            page: MESSAGES,
          },
        ]
      : [
          {
            label: 'messages',
            icon: 'kb_messages',
            count: -1,
            color: '#FD9C15',
            page: MESSAGES,
          },
          {
            label: 'birthday',
            icon: 'kidsbook_birthday',
            count: -1,
            color: '#F9EAD9',
            page: BIRTHDAY,
          },
        ];
    this.state = {
      tags,
    };
  }

  async componentDidMount() {
    const {
      _getDashboardAction,
      _getEventCalendarAction,
      _getBirthdayListAction,
      _getTeacherActivitiesAction,
      user,
    } = this.props;
    const { tags } = this.state;
    let activities = [];
    try {
      const response = await _getDashboardAction(user.schoolId);
      await _getEventCalendarAction(user.schoolId);
      if (user.isTeacher) {
        activities = await _getTeacherActivitiesAction(user.schoolId);
        const birthdayList = await _getBirthdayListAction(user.schoolId);
        tags[0].count = response.tagValues[3];
        tags[1].count = birthdayList.length || 0;
      } else {
        const response = await _getDashboardAction(user.schoolId);
        tags[0].count = response.tagValues[0];
        tags[3].count = response.tagValues[1];
        tags[1].count = response.tagValues[2];
        tags[5].count = response.tagValues[3];
      }
      this.setState({ tags, activities });
    } catch (e) {}
  }

  handleLogOut = () => {
    const { _logOut } = this.props;
    _logOut();
    this.navigate(SIGN_IN);
  };

  handleGoToPage = (item) => {
    if (!item.page) {
      return;
    }
    this.navigate(item.page);
  };

  handleGoToActivityDetail = (activity) => {
    if (!activity) {
      return;
    }
    this.navigate(ACTIVITY_DETAILS, { activity });
  };

  handleGoToClassroom = (classroom) => {
    if (!classroom) {
      return;
    }
    this.navigate(CLASSROOM_TEACHER, { classroom });
  };

  render() {
    const { events, user, classroomList, birthdayList } = this.props;
    return (
      <Container showBackground>
        <DashboardLayout
          openDrawer={this.openDrawer}
          state={this.state}
          user={user}
          classroomList={classroomList}
          handleLogOut={this.handleLogOut}
          birthdayList={birthdayList}
          handleGoToClassroom={this.handleGoToClassroom}
          handleGoToActivityDetail={this.handleGoToActivityDetail}
          handleGoToPage={this.handleGoToPage}
          events={events}
        />
      </Container>
    );
  }
}

const mapStateToProps = (
  { calendar: { events, birthdayList }, classroom: { classroomList }, user },
  store
) => ({
  dispatch: store.navigation.dispatch,
  user,
  events,
  classroomList,
  birthdayList,
});

export default connect(mapStateToProps, {
  _logOut: logOut,
  _getDashboardAction: getDashboardAction,
  _getEventCalendarAction: getEventCalendarAction,
  _getClassroomAction: getClassroomAction,
  _getTeacherActivitiesAction: getTeacherActivitiesAction,
  _getBirthdayListAction: getBirthdayListAction,
})(DashboardController);
