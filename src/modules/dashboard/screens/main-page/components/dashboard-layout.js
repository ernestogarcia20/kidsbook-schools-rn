import React from 'react';
import { View, StyleSheet, FlatList, TouchableOpacity, Dimensions } from 'react-native';
import Text from '@components/Text';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import IconSvgs from '@components/Icon';
import CalendarMin from '@components/CalendarMin';
import { Styles } from '@assets/Styles';
import Activity from '@components/Activity';

const { width } = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  renderItem: {
    width: width / 3.5,
    height: width / 3.5,
    borderRadius: 10,
    margin: 5,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: 'space-between',
  },
  renderItemTeacher: {
    height: 55,
    marginVertical: 5,
    shadowColor: Colors.BLACK,
    width: '100%',
    paddingHorizontal: '5%',
  },
  count: { height: 25, alignItems: 'flex-end', marginRight: 15, marginTop: 5 },
  contentIcon: { flex: 0.7, justifyContent: 'center', alignItems: 'center' },
  contentCalendarMin: { marginTop: '10%', alignItems: 'center' },
});

const Layout = ({
  handleGoToPage,
  openDrawer,
  state,
  events,
  user,
  classroomList,
  handleGoToActivityDetail,
  handleGoToClassroom,
}) => {
  const { tags, activities } = state;

  const renderItem = ({ item, index }) => {
    const { label, color, icon, count } = item;
    return (
      <TouchableOpacity
        style={[styles.renderItem, { backgroundColor: color }]}
        onPress={() => handleGoToPage(item)}
      >
        <View style={styles.count}>
          {count > 0 && (
            <Text color={Colors.WHITE} fontSize={18} weight="Medium">
              {String(count).padStart(2, 0)}
            </Text>
          )}
        </View>
        <View style={styles.contentIcon}>
          <IconSvgs newKey={`${label}-${index}`} name={icon} width={45} heigth={45} />
        </View>
        <Text color={Colors.WHITE} style={{ marginVertical: 10 }} weight="Medium">
          {I18n.t(`menu_list.${label}`)}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderItemTeacher = ({ item, index }) => {
    const { label, color, icon, count } = item;
    return (
      <TouchableOpacity style={[styles.renderItemTeacher]} onPress={() => handleGoToPage(item)}>
        <View
          style={[
            {
              backgroundColor: 'white',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
              borderRadius: 5,
            },
            Styles.rowLarge,
          ]}
        >
          <View style={[Styles.row]}>
            <View
              style={[
                Styles.center,
                {
                  backgroundColor: color,
                  borderTopLeftRadius: 5,
                  borderBottomLeftRadius: 5,
                  width: 75,
                  marginRight: 20,
                },
              ]}
            >
              <View style={[{ width: 55, height: 55 }, Styles.center]}>
                <IconSvgs newKey={`${label}-${index}`} name={icon} width={45} heigth={45} />
              </View>
            </View>
            <Text color={Colors.BLACK} style={{ marginVertical: 10 }} weight="Medium">
              {I18n.t(`menu_list.${label}`)}
            </Text>
          </View>
          <View style={{ marginRight: 20 }}>
            {count > 0 && (
              <Text color={Colors.BLACK} fontSize={18} weight="Medium">
                {String(count).padStart(2, 0)}
              </Text>
            )}
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const ListFooterComponent = () => {
    return (
      <View style={styles.contentCalendarMin}>
        <CalendarMin events={events} />
        {activities?.map((activity, index) => {
          return (
            <View key={index} style={{ width: '100%' }}>
              <Activity user={user} activity={activity} onPress={handleGoToActivityDetail} />
            </View>
          );
        })}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.mainPage')} onPress={openDrawer} menu />

      <FlatList
        key={user?.isTeacher ? 't' : 'm'}
        data={tags}
        contentContainerStyle={
          user?.isTeacher ? { paddingTop: '5%' } : { alignItems: 'center', paddingTop: '10%' }
        }
        extraData={tags}
        numColumns={user?.isTeacher ? 1 : 3}
        renderItem={user?.isTeacher ? renderItemTeacher : renderItem}
        ListFooterComponent={ListFooterComponent}
        keyExtractor={(item, index) => item + index}
      />
    </View>
  );
};

export default Layout;
