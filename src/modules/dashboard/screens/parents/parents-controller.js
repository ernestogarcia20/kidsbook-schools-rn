import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import { getParentsAction, deleteParentsAction } from '@dashboard/actions/parents.actions';
import BaseComponent from '@components/BaseComponent';
import {
  CHILDREN_DETAIL,
  VERIFICATE_CLIENT,
  ADD_PARENT,
  DETAIL_PARENT,
} from '@utils/constants/navigates';
import I18n from 'react-native-redux-i18n';
import ParentsLayout from './components/parents-layout';

class ParentsController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      editList: false,
      selectAll: false,
      parentList: props.parentList,
    };
  }

  componentDidMount() {
    this.init();
  }

  componentDidUpdate(prevProps) {
    const { parentList } = this.props;
    if (prevProps.parentList !== parentList) {
      this.setListInState();
    }
  }

  setListInState = () => {
    const { parentList } = this.props;
    parentList.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
    this.setState({ parentList });
  };

  init = async () => {
    const { _getParentsAction, parentList, user } = this.props;
    try {
      const response = await _getParentsAction(user.schoolId, parentList.length === 0);
      console.log(JSON.stringify(response));
    } catch (e) {}
  };

  updateListTeachers = async () => {
    const { _getParentsAction, user } = this.props;
    try {
      await _getParentsAction(user.schoolId, false);
    } catch (e) {}
  };

  onPressAdd = () => {
    this.navigate(ADD_PARENT, { refreshList: this.updateListTeachers });
    /* this.navigate(CHILDREN_DETAIL, {
      edit: false,
      updateListChild: this.updateListChild,
    }); */
  };

  onPressEdit = () => {
    const { editList } = this.state;
    this.setState({ editList: !editList });
  };

  handleSelectAll = () => {
    const { selectAll, parentList } = this.state;
    const newList = parentList.map((child) => {
      return { ...child, checked: !selectAll };
    });
    this.setState({ selectAll: !selectAll, parentList: newList });
  };

  handleSelectedItem = (item) => {
    const { parentList } = this.state;
    const index = parentList.findIndex((it) => it === item);
    if (index < 0) {
      return;
    }
    let child = parentList[index];
    child = { ...child, checked: !child.checked };
    parentList[index] = child;
    this.setState({ parentList });
  };

  handleDelete = async (parent = null, multipleDelete = false) => {
    const { dispatch } = this.props;

    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t(
          multipleDelete ? 'modalsText.deleteMultipleParent' : 'modalsText.deleteParent'
        ),
        onPressAccept: () => this.deleteParent(parent, multipleDelete),
      },
      dispatch
    );
  };

  deleteParent = async (parent = null, multipleDelete = false) => {
    const { parentList } = this.state;
    const { _deleteParentsAction, dispatch } = this.props;
    if (parentList.length === 0) {
      return;
    }
    const filter = parentList.filter((ch) => ch.checked);
    const ids = [];
    // alert(JSON.stringify(parent.pivot.id))
    // return;
    if (parent) {
      ids.push(parent.pivot.id);
    }
    const deleteIds = parent ? ids : filter.map((ch) => ch.pivot.id);
    try {
      await _deleteParentsAction(deleteIds);
      if (!multipleDelete) {
        this.showModalConfirm(
          {
            visible: true,
            title: I18n.t('modalsText.qualify'),
            onPressAccept: () => this.goToQualify(parent),
          },
          dispatch
        );
      }
      const newParentList = parentList.filter((c) => deleteIds.indexOf(c.pivot.id) === -1);
      this.setState({ parentList: newParentList });
    } catch (e) {}
  };

  goToQualify = (parent) => {
    this.navigate(VERIFICATE_CLIENT, { client: parent });
  };

  handleDetailParent = (parent) => {
    this.navigate(DETAIL_PARENT, { parent });
  };

  render() {
    const { parentList, user } = this.props;
    return (
      <Container>
        <ParentsLayout
          openDrawer={this.openDrawer}
          user={user}
          parentList={parentList}
          state={this.state}
          onPressAdd={this.onPressAdd}
          onPressEdit={this.onPressEdit}
          handleDelete={this.handleDelete}
          handleVerify={this.goToQualify}
          handleSelectedItem={this.handleSelectedItem}
          handleSelectAll={this.handleSelectAll}
          handleDetailParent={this.handleDetailParent}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, parents: { parentList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  parentList,
});

export default connect(mapStateToProps, {
  _getParentsAction: getParentsAction,
  _deleteParentsAction: deleteParentsAction,
})(ParentsController);
