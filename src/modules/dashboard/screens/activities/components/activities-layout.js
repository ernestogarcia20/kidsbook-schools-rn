import React from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions, FlatList } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Search from '@components/Search';
import Text from '@components/Text';
import Activity from '@components/Activity';
import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Feather';
import { Styles } from '@assets/Styles';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabActive: {
    minWidth: width / 3,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREEN,
    borderRadius: 10,
    flexDirection: 'row',
  },
  tabInactive: {
    minWidth: width / 3,
    height: 35,
    backgroundColor: '#EDEDED',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  tabs: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: '3%',
    borderBottomColor: 'rgba(0,0,0,0.1)',
    borderBottomWidth: 0.9,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentActivity: {
    marginTop: 15,
    backgroundColor: Colors.WHITE,
    borderRadius: 10,
    padding: 15,
    marginHorizontal: '5%',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
  },
  imgProfile: {
    width: 25,
    height: 25,
    borderRadius: 100,
    backgroundColor: '#EAEAEA',
    marginRight: -7,
  },
  contentLine: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    marginLeft: 10,
    minHeight: 50,
  },
  contentFilter: {
    height: 40,
    marginTop: 13,
    marginHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0, 0.2)',
  },
});

const Layout = ({
  openDrawer,
  state,
  handleTabFilter,
  goToCreateActivity,
  user,
  loadMoreActivities,
  handleGoToActivityDetail,
  showModalSelection,
}) => {
  const { listButtonTabs, buttonFilter, activities, filterActivity } = state;

  const renderItem = ({ item }) => {
    return <Activity activity={item} onPress={handleGoToActivityDetail} user={user} />;
  };

  return (
    <View style={styles.container}>
      <Header
        title={I18n.t('menu_list.activities')}
        menu
        onPress={openDrawer}
        add
        onPressAdd={goToCreateActivity}
      />
      <Search hasBackground />
      <View style={[Styles.row, styles.contentFilter]}>
        <View>
          <Text fontSize={15} weight="Medium">
            {I18n.t('buttons.filter')}
          </Text>
        </View>
        <View>
          <TouchableOpacity
            style={[
              styles.row,
              {
                marginLeft: 10,
                marginBottom: 0,
              },
            ]}
            onPress={() => showModalSelection('relationship')}
          >
            <Text align="left">{filterActivity}</Text>
            <Icon
              name="triangle"
              size={12}
              style={{ transform: [{ rotate: '60deg' }], marginLeft: 10, marginBottom: 7 }}
            />
          </TouchableOpacity>
        </View>
      </View>
      <FlatList
        data={activities}
        extraData={activities}
        renderItem={renderItem}
        contentContainerStyle={{ paddingBottom: 20, paddingTop: 17 }}
        keyExtractor={(item, index) => item + index}
        disableVirtualization
        onEndReachedThreshold={6}
        onEndReached={loadMoreActivities}
        nestedScrollEnabled
      />
    </View>
  );
};

export default Layout;
