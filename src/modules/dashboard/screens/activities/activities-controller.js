import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { CREATE_ACTIVITY, ACTIVITY_DETAILS } from '@utils/constants/navigates';
import {
  getAllActivitiesAction,
  getTeacherActivitiesAction,
} from '@dashboard/actions/activities.actions';
import I18n from 'react-native-redux-i18n';
import ActivitiesLayout from './components/activities-layout';
import { ACTIVITY_TODAY } from '@utils/constants/globals';

class ActivitiesController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      listButtonTabs: [{ key: 'matter', label: 'Materias' }],
      buttonFilter: '',
      activities: [],
      endLoadActivities: false,
      loadingMore: false,
      filterActivity: I18n.t(`listActivityFilter.${ACTIVITY_TODAY}`),
      page: 1,
    };
  }

  componentDidMount() {
    this.loadActivities();
  }

  showModalSelection = () => {
    const { dispatch, user } = this.props;
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t('modalsText.selectedFilter'),
        data: I18n.t('listActivityFilterString'),
        onPressAccept: (item) => {},
      },
      dispatch
    );
  };

  loadActivities = async (loader = true) => {
    const { _getAllActivitiesAction, _getTeacherActivitiesAction, user } = this.props;
    const { page, activities: activitiesState } = this.state;

    this.setState({ loadingMore: page > 0 });
    try {
      const activities = await (user.isTeacher
        ? _getTeacherActivitiesAction
        : _getAllActivitiesAction)(user.schoolId, page, loader);
      let paginate = page;
      let endPaginate = false;
      if (activities.length === 0) {
        paginate = 0;
        endPaginate = true;
      } else {
        paginate += 1;
      }
      this.setState({
        activities: page > 1 ? activitiesState.concat(activities) : activities,
        page: paginate,
        endLoadActivities: endPaginate,
        loadingMore: false,
      });
    } catch (e) {}
  };

  loadMoreActivities = () => {
    const { endLoadActivities, loadingMore } = this.state;
    if (endLoadActivities || loadingMore) {
      return;
    }
    this.loadActivities();
  };

  loadNewActivities = (loader = false) => {
    this.setState({ page: 1, endLoadActivities: false, loadingMore: false }, () => {
      this.loadActivities(loader);
    });
  };

  handleTabFilter = (key) => {
    this.setState({ buttonFilter: key === this.state.buttonFilter ? '' : key });
  };

  goToCreateActivity = () => {
    this.navigate(CREATE_ACTIVITY, { loadActivities: this.loadNewActivities });
  };

  handleGoToActivityDetail = (activity) => {
    this.navigate(ACTIVITY_DETAILS, { activity, loadActivities: this.loadNewActivities });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <ActivitiesLayout
          goBack={this.goBack}
          handleTabFilter={this.handleTabFilter}
          goToCreateActivity={this.goToCreateActivity}
          loadMoreActivities={this.loadMoreActivities}
          handleGoToActivityDetail={this.handleGoToActivityDetail}
          openDrawer={this.openDrawer}
          showModalSelection={this.showModalSelection}
          setState={this.handleChange}
          state={this.state}
          user={this.props.user}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _getAllActivitiesAction: getAllActivitiesAction,
  _getTeacherActivitiesAction: getTeacherActivitiesAction,
})(ActivitiesController);
