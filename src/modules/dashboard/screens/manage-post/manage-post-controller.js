import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import ManagePostLayout from './components/manage-post-layout';

class ManagePostController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <ManagePostLayout goBack={this.goBack} setState={this.handleChange} state={this.state} />
      </Container>
    );
  }
}

const mapStateToProps = ({user}, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps)(ManagePostController);
