import React from 'react';
import { View, StyleSheet, FlatList, Dimensions, TouchableOpacity } from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import I18n from 'react-native-redux-i18n';
import IconSvgs from '@components/Icon';
import { Styles } from '@assets/Styles';
import TemplateItem from '@components/TemplateItem';

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  contentIcon: {
    height: 80,
    width: 80,
    marginBottom: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Layout = ({ goBack, birthdayList, loading, handleSendMessage }) => {
  const renderItem = ({ item, index }) => {
    return (
      <View>
        <TemplateItem index={index} absence childTemplate={false} item={item} />
        <View
          style={{
            width: 30,
            height: 30,
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: '5%',
            position: 'absolute',
            right: 0,
            top: '30%',
            bottom: 0,
          }}
        >
          <TouchableOpacity
            style={{
              width: 30,
              height: 30,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => handleSendMessage(item)}
          >
            <IconSvgs name="kb_messages" width={30} heigth={30} />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  const ListEmptyComponent = () => {
    if (loading?.isLoading) {
      return <View />;
    }
    return (
      <View style={[Styles.center, { height: height * 0.7 }]}>
        <View style={styles.contentIcon}>
          <IconSvgs newKey="1" name="kidsbook_birthday" width={80} heigth={80} />
        </View>
        <Text fontSize={18}>{I18n.t('withoutBirthday')}</Text>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.birthday')} onPress={goBack} />
      <FlatList
        data={birthdayList}
        renderItem={renderItem}
        ListEmptyComponent={ListEmptyComponent}
        keyExtractor={(item, index) => item + index}
      />
    </View>
  );
};

export default Layout;
