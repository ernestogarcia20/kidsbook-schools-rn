import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { getBirthdayListAction } from '@dashboard/actions/dashboard.actions';
import { CREATE_MESSAGE } from '@utils/constants/navigates';
import BirthdayLayout from './components/birthday-layout';

class BirthdayController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const { _getBirthdayListAction, user } = this.props;

    try {
      await _getBirthdayListAction(user.schoolId);
    } catch (e) {}
  }

  handleSendMessage = () => {
    this.navigate(CREATE_MESSAGE);
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { birthdayList, loading } = this.props;
    return (
      <Container>
        <BirthdayLayout
          goBack={this.goBack}
          loading={loading}
          birthdayList={birthdayList}
          setState={this.handleChange}
          handleSendMessage={this.handleSendMessage}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = (
  { user, calendar: { birthdayList }, loading: { propLoading } },
  store
) => ({
  dispatch: store.navigation.dispatch,
  user,
  birthdayList,
  loading: propLoading,
});

export default connect(mapStateToProps, { _getBirthdayListAction: getBirthdayListAction })(
  BirthdayController
);
