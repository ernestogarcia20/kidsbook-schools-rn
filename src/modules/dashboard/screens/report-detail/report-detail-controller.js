import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {
  getNamedReportFieldsAction,
  answerReportAction,
  getReportDetailAction,
} from '@dashboard/actions/report.actions';
import loadingAction from '@store/actions/loading.action';
import I18n from 'react-native-redux-i18n';
import moment from 'moment';
import ReportDetailLayout from './components/report-detail-layout';

class ReportDetailController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const report = getParam('report', null);
    const isHistory = getParam('isHistory', false);
    this.state = {
      report,
      allFields: [],
      stickerList: JSON.parse(report?.sticker_ids),
      buttonFilter: 'simple',
      isHistory,
    };
  }

  componentDidMount() {
    this.detailReport();
  }

  detailReport = async () => {
    const { _getReportDetailAction } = this.props;
    const { report } = this.state;

    try {
      const data = await _getReportDetailAction(report.id);
      this.setState({ report: data }, this.reportFields);
    } catch (e) {}
  };

  reportFields = async () => {
    const { _getNamedReportFieldsAction, user, dispatch } = this.props;
    const { isHistory } = this.state;

    try {
      const data = await _getNamedReportFieldsAction(user.schoolId, true);
      this.procesingAllFields(data);
    } catch (e) {}
  };

  procesingAllFields = (data = []) => {
    const o = [];
    data.forEach((field) => {
      field.usedChildren = [];
      if (this.isUsed(field.name) && this.compliesWithConditions(field)) {
        if (field.hasOwnProperty('children')) {
          const co = this.procesingAllFields(field.children);
          field.usedChildren = co;
        }
        o.push(field);
      }
    });
    this.setState({ allFields: o });
    return o;
  };

  compliesWithConditions(field) {
    const { report } = this.state;
    if (field.conditions.includes('Doesnt Use Bottle') && report.child.use_bottle) {
      return false;
    }

    if (field.conditions.includes('Uses Bottle') && !report.child.use_bottle) {
      return false;
    }

    if (field.conditions.includes('Doesnt Use Diaper') && report.child.use_diaper) {
      return false;
    }

    if (field.conditions.includes('Uses Diaper') && !report.child.use_diaper) {
      return false;
    }

    return true;
  }

  isUsed = (fieldName) => {
    const { report } = this.state;
    const data = JSON.parse(report.data) || {};
    return data.hasOwnProperty(fieldName);
  };

  getValue = (field) => {
    const { report } = this.state;
    const data = JSON.parse(report.data) || {};
    switch (field.type) {
      case 'Checkbox':
        return data[field.name] ? 'Si' : 'No';
      case 'Range':
        return field.values[data[field.name]];
      case 'Number':
      case 'Time':
        return data[field.name];
      case 'Date':
        return this.moment(data[field.name]);
      default:
        console.log(field);
        break;
    }
  };

  moment = (date, autoformat = true, moveUtc = true) => {
    let r = moment(date);

    if (moveUtc) {
      r = moment(date).add(-18000, 'seconds');
    }

    if (autoformat) {
      r = r.format('MM/DD/YYYY HH:mm');
    }

    return r;
  };

  handleReplyReport = async (message) => {
    if (!message) {
      return;
    }
    const { report } = this.state;
    const { _answerReportAction } = this.props;
    const data = {
      report_id: report.id,
      subject: I18n.t('reportAnswer'),
      content: message,
    };
    try {
      await _answerReportAction(data);
      this.goBack();
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <ReportDetailLayout
          user={user}
          goBack={this.goBack}
          setState={this.handleChange}
          getValue={this.getValue}
          state={this.state}
          handleReplyReport={this.handleReplyReport}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _getNamedReportFieldsAction: getNamedReportFieldsAction,
  _answerReportAction: answerReportAction,
  _getReportDetailAction: getReportDetailAction,
})(ReportDetailController);
