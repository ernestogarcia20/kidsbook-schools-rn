import React, { useEffect, useState } from 'react';

import { View, StyleSheet } from 'react-native';
import Text from '@components/Text';
import { Styles } from '@assets/Styles';
import { SvgCss } from 'react-native-svg';
import loadLocalResource from 'react-native-local-resource';
import { svg } from '@shared/icons/import-svg-file';
import normalize from 'react-native-normalize';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@assets/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  renderItemRow: { flex: 1 / 3, marginBottom: 5 },
  renderItem: { flex: 1, marginBottom: 5 },
  contentIcon: {
    height: normalize(60),
    width: normalize(60),
    marginVertical: 6,
  },
  itemDetailed: {
    paddingVertical: 8,

    marginBottom: 10,
  },
  contentInfoIcon: {
    width: normalize(20),
    height: normalize(20),
    backgroundColor: colors.YELLOW,
    borderRadius: 50,
    marginHorizontal: 5,
  },
  contentBoxObs: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: colors.WHITE,
    marginBottom: 10,
    marginHorizontal: '5%',
    borderRadius: 5,
  },
  contentChild: {
    backgroundColor: '#B19858',
    paddingVertical: 1,
    paddingHorizontal: 10,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
  },
  contentRenderChild: { flex: 1, paddingHorizontal: 20 },
});

const renderFields = ({
  field,
  getValue,
  index,
  simpleMode = true,
  report = {},
  totalNumber = 0,
}) => {
  const [newSvg, setNewSvg] = useState(null);
  const [isObs, setIsObs] = useState(false);
  const [obsText, setObs] = useState(null);

  const findResource = () => {
    loadLocalResource(svg[field.icon_name]).then((result) => {
      setNewSvg(result);
    });
  };

  useEffect(() => {
    hasObs();
    findResource();
  }, []);

  const hasObs = () => {
    const obsData = JSON.parse(report?.obs_data || {}) || {};
    const obsResult = obsData.hasOwnProperty(field?.name) && obsData[field.name];
    setObs(obsResult ? obsData[field.name] : null);
    setIsObs(obsResult);
  };

  const SimpleRender = () => {
    return (
      <>
        <Text weight="SemiBold">{field.name}</Text>
        <View style={styles.contentIcon}>
          {newSvg && <SvgCss xml={newSvg} width={60} height={60} />}
        </View>
        <Text fontSize={13}>{getValue(field)}</Text>
        {(field?.usedChildren.length > 0 || isObs) && (
          <View style={[Styles.center, Styles.row, styles.contentChild]}>
            {field?.usedChildren.length > 0 && (
              <Text color={colors.WHITE}>{field.usedChildren.length}</Text>
            )}
            <Icon name={!isObs ? 'plus' : 'alert-circle'} color={colors.WHITE} />
          </View>
        )}
      </>
    );
  };

  const DetailedRender = () => {
    return (
      <>
        <View
          style={[
            Styles.rowLarge,
            {
              backgroundColor: index % 2 ? '#F0F1A6' : 'white',
            },
            styles.itemDetailed,
          ]}
        >
          <View style={[Styles.center, { flex: 0.35 }]}>
            <Text weight="SemiBold">{field.name}</Text>
            <View style={styles.contentIcon}>
              {newSvg && <SvgCss xml={newSvg} width={60} height={60} />}
            </View>
            <Text fontSize={13}>{getValue(field)}</Text>
          </View>
          <View style={[{ flex: 0.7 }, Styles.center]}>
            {field.usedChildren.length === 0 ? (
              <Text>No hay mas info</Text>
            ) : (
              <View style={[Styles.rowLarge]}>
                {field.usedChildren.map((childField, index2) => {
                  return (
                    <View key={index2} style={[Styles.center, styles.contentRenderChild]}>
                      <Text weight="SemiBold">{childField.name}</Text>
                      {newSvg && <SvgCss xml={newSvg} width={60} height={60} />}
                      <Text fontSize={13}>{getValue(childField)}</Text>
                    </View>
                  );
                })}
              </View>
            )}
          </View>
        </View>
        {isObs && obsText ? (
          <View>
            <View style={[Styles.row, styles.contentBoxObs, Styles.shadow]}>
              <View style={[Styles.center, styles.contentInfoIcon]}>
                <Icon name="alert-circle" color={colors.WHITE} />
              </View>
              <Text fontSize={13}>{obsText}</Text>
            </View>
          </View>
        ) : null}
      </>
    );
  };

  return (
    <View
      style={[
        simpleMode
          ? totalNumber % 3
            ? { ...styles.renderItemRow, flex: totalNumber % 1 ? 1 / 2 : 1 }
            : styles.renderItemRow
          : styles.renderItem,
        simpleMode ? Styles.center : {},
      ]}
      key={index}
    >
      {simpleMode && SimpleRender()}
      {!simpleMode && DetailedRender(index)}
    </View>
  );
};

export default renderFields;
