import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  FlatList,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import ItemMessageAnswer from '@components/ItemMessageAnswer';
import Header from '@components/Header';
import FastImage from '@components/FastImage';
import PaperImage from '@images/pattern.png';
import CustomIcon from '@components/Icon';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import colors from '@assets/colors';
import { getImageById } from '@utils/functions/globals';
import { Styles } from '@assets/Styles';
import { SvgCssUri } from 'react-native-svg';
import InputMessage from '@components/InputMessage';
import moment from 'moment';
import RenderItemField from './render-fields';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileImage: {
    height: width / 2,
    width: width / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: colors.WHITE,
  },
  borderImage: {
    height: width / 1.9,
    width: width / 1.9,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: colors.GREEN,
    borderWidth: 4,
    borderColor: colors.GREEN,
  },
  content: {
    paddingVertical: 15,
  },
  row: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  rowLarge: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  contentProfileChild: {
    flex: 0.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentTextInfo: {
    marginTop: 10,
    maxWidth: '75%',
  },
  contentIcon: {
    height: 55,
    width: 55,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 0,
  },
  buttonSimple: {
    width: 100,
    height: 30,
    borderTopStartRadius: 5,
    borderBottomLeftRadius: 5,
    borderWidth: 1,
    borderColor: colors.GREEN,
  },
  buttonDetail: {
    width: 100,
    height: 30,
    borderWidth: 1,
    borderColor: colors.GREEN,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  line: {
    height: 1,
    backgroundColor: 'rgba(0,0,0,0.15)',
    marginTop: 6,
    marginBottom: 10,
  },
  box: {
    backgroundColor: colors.WHITE,
    borderRadius: 10,
    paddingVertical: 15,
    paddingHorizontal: 15,
    marginTop: 20,
  },
});

const Layout = ({ goBack, user, state, setState, getValue, handleReplyReport }) => {
  const { report, stickerList, allFields, buttonFilter } = state;
  const [scrollView, setScrollView] = useState(null);
  const [autoScroll, activeAutoScroll] = useState(false);
  const urlImage = report ? getImageById(report?.child?.avatar, user?.token) : null;
  // alert(JSON.stringify(report?.koala_sticker));

  useEffect(() => {
    if (autoScroll) {
      onContentSizeChange();
    }
  }, [autoScroll]);

  const onContentSizeChange = () => {
    if (!scrollView || !autoScroll) {
      return;
    }
    scrollView.scrollToEnd({ animated: true });
  };

  const renderFields = ({ item: field, index }) => {
    return (
      <RenderItemField
        getValue={getValue}
        field={field}
        index={index}
        report={report}
        totalNumber={allFields.length}
        simpleMode={buttonFilter === 'simple'}
      />
    );
  };
  return (
    <FastImage style={styles.container} source={PaperImage}>
      <Header title={I18n.t('menu_list.reportDetail')} onPress={goBack} />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === 'android' ? undefined : 'position'}
        enabled
        keyboardVerticalOffset={45}
      >
        <ScrollView
          contentContainerStyle={styles.content}
          ref={(ref) => {
            setScrollView(ref);
          }}
          onContentSizeChange={onContentSizeChange}
        >
          <View style={{ paddingHorizontal: '5%' }}>
            <View style={[styles.rowLarge, { alignItems: 'flex-start' }]}>
              <View style={styles.contentProfileChild}>
                <View style={styles.borderImage}>
                  <FastImage style={styles.profileImage} source={{ uri: urlImage }} />
                </View>
                <View style={styles.contentTextInfo}>
                  <Text weight="Medium" fontSize={19}>
                    {report?.child?.name}
                  </Text>
                  <Text fontSize={16}>{report?.date}</Text>
                </View>
              </View>
              <View style={{ flex: 0.25, alignItems: 'center' }}>
                {stickerList.map((id, index) => {
                  const urlSticker = getImageById(id, user?.authData?.apiToken);
                  return (
                    <View key={index} style={{ justifyContent: 'flex-start', marginBottom: 15 }}>
                      <FastImage source={{ uri: urlSticker }} style={{ width: 65, height: 65 }} />
                    </View>
                  );
                })}
              </View>
            </View>
            <View style={[styles.rowLarge, { marginTop: 25, alignItems: 'flex-start' }]}>
              <View style={[styles.center, { flex: 0.5 }]}>
                <Text weight="SemiBold">Salón</Text>
                <View style={styles.contentIcon}>
                  <CustomIcon name="kb_abc" width={43} heigth={43} />
                </View>
                <Text>{report?.classroom?.name}</Text>
              </View>
              <View style={[styles.center, { flex: 1 }]}>
                <Text weight="SemiBold">Hoy estuve: </Text>
                <View style={[styles.contentIcon, { marginTop: 5, marginBottom: 7 }]}>
                  <FastImage
                    source={{
                      uri: getImageById(report?.mood?.image_id, user?.token),
                    }}
                    style={{ height: 50, width: 50 }}
                  />
                </View>
                <Text>{report?.mood_text}</Text>
              </View>
              <View style={[styles.center, { flex: 0.5 }]}>
                <Text weight="SemiBold">Salón</Text>
                <View style={styles.contentIcon}>
                  <FastImage
                    source={{
                      uri: getImageById(report?.teacher?.avatar, user?.token, 64),
                    }}
                    style={{ height: 50, width: 50, borderRadius: 100 }}
                  />
                </View>
                <Text>{report?.teacher?.name}</Text>
              </View>
            </View>
            {allFields.length > 0 && (
              <View
                style={[
                  styles.box,
                  Styles.shadow,
                  { paddingHorizontal: buttonFilter !== 'simple' ? 0 : 15 },
                ]}
              >
                <View style={[Styles.center, Styles.row]}>
                  <TouchableOpacity
                    style={[
                      {
                        backgroundColor: buttonFilter === 'simple' ? colors.GREEN : colors.WHITE,
                      },
                      styles.buttonSimple,
                      Styles.center,
                    ]}
                    onPress={() => setState('buttonFilter', 'simple')}
                  >
                    <Text color={buttonFilter === 'simple' ? colors.WHITE : colors.GREEN}>
                      Simple
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[
                      {
                        backgroundColor: buttonFilter !== 'simple' ? colors.GREEN : colors.WHITE,
                      },
                      styles.buttonDetail,
                      Styles.center,
                    ]}
                    onPress={() => setState('buttonFilter', 'detail')}
                  >
                    <Text color={buttonFilter !== 'simple' ? colors.WHITE : colors.GREEN}>
                      Detallado
                    </Text>
                  </TouchableOpacity>
                </View>
                <FlatList
                  style={{ marginTop: 15 }}
                  key={buttonFilter}
                  data={allFields}
                  renderItem={renderFields}
                  numColumns={buttonFilter === 'simple' ? 3 : 1}
                  scrollEnabled={false}
                  keyExtractor={(item, index) => item + index}
                />
              </View>
            )}
            {report.koala_sticker && (
              <View style={[styles.box, Styles.shadow]}>
                <View style={Styles.row}>
                  <View style={{ marginRight: 25 }}>
                    <SvgCssUri
                      width={100}
                      height={115}
                      uri={getImageById(report?.koala_sticker?.sticker_id, user?.token)}
                    />
                    <Text>{report.koala_sticker.name}</Text>
                  </View>
                  <View style={{ flex: 1, height: '100%' }}>
                    <Text fontSize={13} color="#8D8D8D">
                      Observaciones
                    </Text>
                    <View style={styles.line} />
                    <Text fontSize={13} align={report.observation_text ? 'left' : 'center'}>
                      {report.observation_text || 'No hay observaciones hoy'}
                    </Text>
                  </View>
                </View>
                <View>
                  <View style={{ marginTop: 15 }}>
                    <Text fontSize={13} color="#8D8D8D">
                      Asignaciones
                    </Text>
                    <View style={styles.line} />
                    <View style={{ paddingHorizontal: 15, paddingVertical: 5 }}>
                      <Text align="left">{report?.assignments_text || 'No hubo asignaciones'}</Text>
                    </View>
                  </View>
                </View>
              </View>
            )}
            <View style={{ backgroundColor: colors.WHITE, flex: 1 }}>
              {report.messages != null &&
                report.messages.length > 0 &&
                report.messages.map((item, index) => {
                  const urlImageMessage = getImageById(
                    item.user.client.avatar,
                    user?.authData?.apiToken
                  );
                  return (
                    <View key={index}>
                      <View
                        style={{
                          paddingBottom: 15,
                          backgroundColor: colors.WHITE,
                          marginTop: 15,
                          borderBottomWidth: 2,
                          borderColor: 'transparent',
                          shadowColor: colors.BLACK,
                          shadowOffset: {
                            width: 0,
                            height: 2,
                          },
                          shadowOpacity: 0.25,
                          shadowRadius: 3.84,
                          elevation: 5,
                          borderRadius: 10,
                        }}
                      >
                        <View
                          style={[
                            styles.rowLarge,
                            {
                              height: 55,
                              borderTopLeftRadius: !item.sent ? 0 : 10,
                              borderTopRightRadius: 10,
                              backgroundColor: item.new ? 'white' : '#f4f4f4',
                              flex: 1,
                              paddingHorizontal: 10,
                              paddingLeft: '6%',
                            },
                          ]}
                        >
                          <View style={[styles.row]}>
                            <FastImage
                              source={{ uri: urlImageMessage }}
                              style={{
                                width: 40,
                                height: 40,
                                borderRadius: 100,
                                backgroundColor: '#DADADA',
                                marginRight: 15,
                              }}
                            />
                            <Text weight="Medium" fontSize={13} color={colors.BLACK}>
                              {item.sent ? I18n.t('you') : item.user.name}
                            </Text>
                          </View>
                        </View>
                        <View style={{ paddingHorizontal: '7%', paddingTop: '5%' }}>
                          <Text formatter align="auto" fontSize={13} color={colors.BLACK}>
                            <Text formatter align="auto" fontSize={13} color={colors.GRAY}>
                              {item.subject}: {'\n'}
                            </Text>
                            {item.content}
                          </Text>
                        </View>
                        <View style={{ marginTop: 5, marginRight: '6%' }}>
                          <Text align="right" color="#868686" weight="Medium" fontSize={11}>
                            {moment(item.created_at).format('DD/MM/YYYY HH:mm').toString()}
                          </Text>
                        </View>
                      </View>
                    </View>
                  );
                })}
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </FastImage>
  );
};

export default Layout;
