import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import I18n from 'react-native-redux-i18n';
import { sendMessageAction, sendFileForMessageAction } from '@dashboard/actions/messages.action';
import { LIST_GLOBAL, CREATE_FAST_ANSWER, GLOBAL_LIST_MESSAGE } from '@utils/constants/navigates';
import { showMessage } from 'react-native-flash-message';
import { MESSAGE_PARENTS } from '@utils/constants/globals';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';
import moment from 'moment';
import { fontMaker } from '@components/typografy';
import { uniqueArray, setStore } from '@utils/functions/globals';
import { SET_FAST_ANSWER } from '@utils/constants/reducers';
import { getEventCalendarAction } from '@modules/dashboard/actions/dashboard.actions';
import CreateMessageLayout from './components/create-message-layout';

class CreateMessageController extends BaseComponent {
  constructor(props) {
    super(props);
    const { user } = this.props;
    this.state = {
      recipientsButtons: [
        {
          id: 1,
          text: I18n.t('recipentsButton.childByclassroom'),
          key: 'childByclassroom',
        },
        {
          id: 2,
          text: user.isTeacher
            ? I18n.t('recipentsButton.addDirectors')
            : I18n.t('recipentsButton.addTeachers'),
          key: user.isTeacher ? 'directors' : 'teacher',
        },
        {
          id: 3,
          text: I18n.t('recipentsButton.addChildren'),
          key: 'children',
        },
        {
          id: 4,
          text: I18n.t('recipentsButton.addParent'),
          key: 'parents',
        },
      ],
      recipientList: [],
      isEvent: false,
      event: null,
      content: '',
      subject: '',
      aliasName: '',
      attachList: [],
      isAlias: false,
    };
  }

  handleSaveEvent = (data) => {
    const startDate = moment(data.startDate, 'DD/MM/YYYY').format('YYYY-MM-DD');
    if (!data || !data?.repeatObj || !data?.repeatObj?.dateSelected) {
      this.setState({ event: { startDate, arrayDates: [] } });
      return;
    }
    const {
      repeatObj: { dateSelected, key },
    } = data;
    const resultUniqueArray = uniqueArray(dateSelected.arrayDates);
    const newEventRepeat = resultUniqueArray
      .map((d) => {
        if (d > dateSelected.date || d === data.startDate) {
          return null;
        }
        return d;
      })
      .filter((x) => x);
    const newDataEvent = {
      startDate,
      arrayDates: newEventRepeat,
      endDate: dateSelected.date,
      key,
    };
    this.setState({ event: newDataEvent });
  };

  deleteRecipent = (recipent) => {
    const { recipientList } = this.state;
    if (recipent) {
      const index = recipientList.findIndex((x) => x.id === recipent.id);
      recipientList.splice(index, 1);
    }
    this.setState({ recipientList: recipent ? recipientList : [] });
  };

  handleSelectedRecipent = (listSelected, key) => {
    const { recipientList } = this.state;
    this.setState({
      recipientList: recipientList.length === 0 ? listSelected : recipientList.concat(listSelected),
      type: key === 'children' || key === 'childByclassroom' ? MESSAGE_PARENTS : '',
    });
  };

  handleGotoRecipent = (recipent) => {
    const { recipientList } = this.state;
    this.navigate(LIST_GLOBAL, {
      key: recipent.key,
      handleSelected: this.handleSelectedRecipent,
      ignore: recipientList,
      childList: recipientList,
    });
  };

  handleAddFastAnswer = () => {
    this.navigate(CREATE_FAST_ANSWER);
  };

  handleSelectedFastAnswer = (fastAnswer) => {
    const { fastAnswerSelected } = this.state;
    const { content, subject, name } = fastAnswer;
    this.setState({
      content: fastAnswerSelected?.id === fastAnswer?.id ? '' : content,
      subject: fastAnswerSelected?.id === fastAnswer?.id ? '' : subject,
      fastAnswerSelected: fastAnswerSelected?.id === fastAnswer?.id ? null : fastAnswer,
      isAlias: fastAnswerSelected !== fastAnswer,
      aliasName: fastAnswerSelected?.id === fastAnswer?.id ? '' : name,
    });
  };

  handleEditFastAnswer = (fastAnswer) => {
    this.navigate(CREATE_FAST_ANSWER, {
      edit: true,
      idFastAnswer: fastAnswer.id,
      content: fastAnswer.content,
      subject: fastAnswer.subject,
      aliasName: fastAnswer.name,
    });
  };

  handleCamera = () => {
    const { attachList } = this.state;
    const options = {
      quality: 1.0,
      mediaType: 'photo',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: false,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      if (!response.data) {
        return;
      }

      const filename = response.uri.split('/').pop();
      const type = filename.split('.').pop();
      const name = filename.replace(`.${type}`, '').substr(0, 8);
      const attach = `data:image/jpg;base64,${response.data}`;
      attachList.push({
        image: attach,
        type: 'image',
        fileData: { name: `${name}.${type}`, uri: response.uri, uriApi: attach },
      });
      this.setState({ attachList });
    });
  };

  handleFolder = async () => {
    const { attachList } = this.state;
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.allFiles],
        // There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      // Printing the log realted to the file
      const mBytes = Math.round(res.size / (1024 * 1024));
      if (mBytes > 150000) {
        showMessage({
          message: I18n.t(`validation.error/file`),
          type: 'default',
          titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
        });
        // alert(mBytes);
        return;
      }
      const fileExt = res.name.split('.').pop().toLowerCase();
      const filename = res.uri.split('/').pop();
      const name =
        filename.replace(`.${fileExt}`, '').length > 8
          ? filename.replace(`.${fileExt}`, '').substr(0, 8)
          : filename.replace(`.${fileExt}`, '');

      const find = attachList.filter((x) => x.fileNameLarge === filename);
      if (find.length > 0) {
        return;
      }

      const file64 = await RNFS.readFile(decodeURI(res.uri), 'base64');
      if (fileExt === 'png' || fileExt === 'jpg') {
        attachList.push({
          image: res.uri,
          type: 'image',
          fileType: fileExt,
          fileNameLarge: filename,
          fileData: {
            ...res,
            name: `${name}.${fileExt}`,
            uriApi: `data:image/${fileExt};base64,${file64}`,
          },
        });
        this.setState({ attachList });
        return;
      }
      attachList.push({
        image: res.uri,
        type: 'file',
        fileType: fileExt,
        fileNameLarge: filename,
        fileData: {
          ...res,
          name: `${name}.${fileExt}`,
          uriApi: `data:${fileExt};base64,${file64}`,
        },
      });
      this.setState({ attachList });
      // Setting the state to show single file attributes 60mb
      // setSingleFile(res);
    } catch (err) {
      // setSingleFile(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        // alert('Canceled');
      } else {
        // For Unknown Error
        // alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  handleDeleteAttach = (attach) => {
    const { attachList } = this.state;
    const index = attachList.findIndex((a) => a === attach);
    attachList.splice(index, 1);
    this.setState({ attachList });
  };

  handleSendMessage = async () => {
    const { subject, content, event, recipientList, type, attachList, isEvent } = this.state;
    const {
      _sendMessageAction,
      _sendFileForMessageAction,
      _getEventCalendarAction,
      user,
      navigation: { getParam },
    } = this.props;

    if (recipientList.length === 0) {
      showMessage({
        message: I18n.t(`validation.error/recipents`),
        type: 'danger',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }
    if (!subject || !content) {
      showMessage({
        message: I18n.t(`validation.error/${!subject ? 'subject' : 'content'}`),
        type: 'danger',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }
    const loadMessages = getParam('loadMessages', () => {});
    const recipients = JSON.stringify(recipientList.map((x) => x.id));

    try {
      const { data } = await _sendMessageAction({
        is_event: isEvent,
        type,
        content,
        date: isEvent ? event.startDate : '',
        time: '',
        subject,
        recipients,
        event_repeat: isEvent ? JSON.stringify(event.arrayDates) : '[]',
        school_id: user.schoolId,
        is_announcement: false,
      });
      if (attachList.length > 0) {
        attachList.forEach(async (file) => {
          const attachment = {
            name: file.fileData.name,
            imageData: null,
            extension: file.fileType,
            size: file.fileData.size,
            file: file.fileData.uriApi,
          };
          await _sendFileForMessageAction({ attachment, message_id: data.id });
        });
      }
      loadMessages();
      this.goBack();
      if (isEvent) {
        await _getEventCalendarAction(user.schoolId);
      }
    } catch (e) {}
  };

  updateList = (recipientList) => {
    this.setState({ recipientList });
  };

  selectedFastAnswer = (fastAnswer) => {
    const { dispatch } = this.props;
    this.handleSelectedFastAnswer(fastAnswer);
    const { fastAnswerList } = this.props;
    const index = fastAnswerList.findIndex((x) => x.id === fastAnswer.id);
    if (index > -1) {
      fastAnswerList.splice(0, 0, ...fastAnswerList.splice(index, 1));
    }

    dispatch(setStore(SET_FAST_ANSWER, fastAnswerList));
  };

  handleDetailAllList = (isFastAnswer = false) => {
    const { recipientList } = this.state;
    const { fastAnswerList } = this.props;
    this.navigate(GLOBAL_LIST_MESSAGE, {
      list: isFastAnswer ? fastAnswerList : recipientList,
      key: isFastAnswer ? 'fastAnswer' : 'recipients',
      selectedFastAnswer: this.selectedFastAnswer,
    });
  };

  handleGoToOpenFile = (attach) => {
    const decUrl = decodeURI(attach.fileData.uri);
    FileViewer.open(decUrl);
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user, fastAnswerList } = this.props;
    return (
      <Container>
        <CreateMessageLayout
          deleteRecipent={this.deleteRecipent}
          user={user}
          fastAnswerList={fastAnswerList}
          handleSelectedFastAnswer={this.handleSelectedFastAnswer}
          handleEditFastAnswer={this.handleEditFastAnswer}
          goBack={this.goBack}
          setState={this.handleChange}
          handleGotoRecipent={this.handleGotoRecipent}
          handleSaveEvent={this.handleSaveEvent}
          handleSendMessage={this.handleSendMessage}
          handleGoToOpenFile={this.handleGoToOpenFile}
          handleDeleteAttach={this.handleDeleteAttach}
          handleDetailAllList={this.handleDetailAllList}
          handleCamera={this.handleCamera}
          handleFolder={this.handleFolder}
          handleAddFastAnswer={this.handleAddFastAnswer}
          navigation={this.props.navigation}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, fastAnswers: { fastAnswerList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  fastAnswerList,
});

export default connect(mapStateToProps, {
  _sendMessageAction: sendMessageAction,
  _sendFileForMessageAction: sendFileForMessageAction,
  _getEventCalendarAction: getEventCalendarAction,
})(CreateMessageController);
