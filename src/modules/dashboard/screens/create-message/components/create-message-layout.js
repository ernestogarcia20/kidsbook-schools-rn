import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Switch,
  Animated,
  TextInput,
  Keyboard,
  Platform,
} from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/Feather';
import Colors from '@assets/colors';
import FastImage from '@components/FastImage';
import Event from '@components/Event';
import KeyboardAvoidingView from '@components/KeyboardAvoidingView';
import Emoji from '@components/Emoji';
import Button from '@components/Button';
import { getImageById } from '@utils/functions/globals';

const { width } = Dimensions.get('window');

const hitSlop = { top: 25, bottom: 15, right: 25 };
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentRecipients: {
    paddingBottom: 10,
  },
  contentContainerStyle: {
    paddingVertical: '5%',
  },
  recipientButton: {
    height: 30,
    width: '48%',
    backgroundColor: Colors.GREEN,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5,
    marginBottom: 10,
  },
  contentButtons: {
    flex: 1,
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  buttonDelete: {
    borderRadius: 5,
    height: 28,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  controls: {
    marginTop: 15,
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 10,
    borderBottomWidth: 1,
  },
  contentRecipent: {
    marginTop: 15,
    maxHeight: 210,
  },
  recipentItem: {
    height: 55,
    justifyContent: 'center',
    borderBottomColor: 'rgba(0,0,0,0.1)',
    paddingBottom: 10,
    borderBottomWidth: 1,
    marginBottom: 10,
    paddingLeft: 15,
  },
  contentEvent: {
    marginTop: 20,
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 100,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
  paddingHorizontal: {
    paddingHorizontal: 20,
  },
  separator: {
    marginTop: 5,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentMessage: {
    marginTop: '5%',
  },
  contentLine: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)',
  },
  input: { flex: 1, minHeight: 80, maxHeight: 190, marginLeft: 15 },
  inputMultiline: {
    height: null,
    minHeight: 90,
    paddingTop: Platform.OS === 'ios' ? 10 : 5,
    paddingBottom: Platform.OS === 'ios' ? 10 : 0,
  },
  contenButtonSend: { marginTop: '10%', marginHorizontal: '8%' },
  emptyList: { height: 45, justifyContent: 'center', alignItems: 'center' },
  buttonDeleteAttach: {
    position: 'absolute',
    top: -10,
    right: 15,
    width: 20,
    height: 20,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.DANGER,
  },
});
const Layout = ({
  goBack,
  state,
  deleteRecipent,
  setState,
  navigation,
  handleSaveEvent,
  handleGotoRecipent,
  handleAddFastAnswer,
  user,
  fastAnswerList,
  handleSelectedFastAnswer,
  handleEditFastAnswer,
  handleSendMessage,
  handleCamera,
  handleFolder,
  handleDeleteAttach,
  handleDetailAllList,
  handleGoToOpenFile,
}) => {
  const {
    recipientsButtons,
    recipientList,
    isEvent,
    subject,
    content,
    aliasName,
    fastAnswerSelected,
    attachList,
    isAlias,
  } = state;
  const [scrollView, setScrollView] = useState(null);
  const [cursorPosition, setCursorPosition] = useState(null);
  const [editFastAnswer, setEditFastAnswer] = useState(null);
  const scrolled = () => {
    if (!scrollView) {
      return;
    }
    setTimeout(() => {
      scrollView.scrollToEnd({ animated: true, index: 0 });
    }, 1);
  };
  const onEmoji = (emoji) => {
    const formatText = cursorPosition
      ? [content.slice(0, cursorPosition), emoji, content.slice(cursorPosition)].join('')
      : content.concat(emoji);
    setState('content', formatText);
    setCursorPosition(null);
  };
  const onSelectionChange = ({ nativeEvent: { selection } }) => {
    setCursorPosition(selection.start);
  };
  const recipientComponent = () => {
    return (
      <View>
        <View style={[styles.rowLarge, styles.controls]}>
          <Text align="left" color="#9A9A9A" weight="Medium">
            {I18n.t('actions')}:{' '}
          </Text>
          <TouchableOpacity
            style={[
              styles.buttonDelete,
              {
                backgroundColor: Colors.DANGER,
              },
            ]}
            onPress={() => deleteRecipent(null)}
          >
            <Text color={Colors.WHITE} fontSize={13}>
              Quitar
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.contentRecipent}>
          {recipientList.map((recipent, index) => {
            const { name } = recipent;
            const urlImage = getImageById(recipent.avatar, user?.authData?.apiToken);
            if (index > 2) {
              return <></>;
            }
            return (
              <View style={styles.recipentItem} key={index}>
                <View style={styles.rowLarge}>
                  <View style={styles.row}>
                    <FastImage source={{ uri: urlImage }} style={styles.imageProfile} />
                    <View style={{ marginLeft: 20 }}>
                      <Text align="left" fontSize={14} weight="Medium">
                        {name}
                      </Text>
                    </View>
                  </View>
                  <TouchableOpacity
                    style={styles.buttonDelete}
                    onPress={() => deleteRecipent(recipent)}
                  >
                    <Icon name="x" size={22} color={Colors.RED} />
                  </TouchableOpacity>
                </View>
              </View>
            );
          })}
        </View>
        {recipientList.length > 3 && (
          <TouchableOpacity
            style={{ alignItems: 'flex-end', marginBottom: 15 }}
            onPress={() => handleDetailAllList()}
          >
            <Text weight="SemiBold" color={Colors.PRIMARYTEXTCOLOR}>
              Ver todos +{recipientList.length - 3}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  const input = ({ multiline = false, title, placeholder, value, attrName, updateMasterState }) => {
    return (
      <View style={[styles.paddingHorizontal]}>
        <View style={[styles.row, styles.separator, styles.contentLine]}>
          <Text>{title}: </Text>
          {!multiline && (
            <TextInput
              style={styles.input}
              multiline={multiline}
              placeholder={placeholder}
              value={value}
              onChangeText={(text) => updateMasterState(attrName, text)}
              placeholderTextColor="#909090"
            />
          )}
        </View>
        {multiline && (
          <View style={[styles.contentLine, styles.inputMultiline]}>
            <Emoji onEmoji={onEmoji}>
              <TextInput
                textAlignVertical="top"
                style={styles.input}
                multiline={multiline}
                onSelectionChange={onSelectionChange}
                onFocus={scrolled}
                placeholder={placeholder}
                placeholderTextColor="#909090"
                value={value}
                onChangeText={(text) => updateMasterState(attrName, text)}
              />
            </Emoji>
          </View>
        )}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.createMessage')} onPress={goBack} />
      <View style={{ flex: 1 }}>
        <KeyboardAvoidingView>
          <ScrollView
            ref={(ref) => {
              setScrollView(ref);
            }}
            keyboardShouldPersistTaps="always"
            contentContainerStyle={styles.contentContainerStyle}
          >
            <View style={styles.paddingHorizontal}>
              <View style={styles.contentRecipients}>
                <Text align="left" color="#9A9A9A" weight="Medium">
                  Destinatarios
                </Text>
              </View>
              <View style={[styles.row, styles.contentButtons]}>
                {recipientsButtons?.map((recipient, index) => {
                  return (
                    <TouchableOpacity
                      style={[
                        styles.recipientButton,
                        { marginRight: index % 2 ? 0 : 5, marginLeft: index % 2 ? 5 : 0 },
                      ]}
                      key={index}
                      onPress={() => handleGotoRecipent(recipient)}
                    >
                      <Text color={Colors.WHITE} weight="Medium" fontSize={12}>
                        {recipient.text}
                      </Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
              {recipientList.length > 0 && recipientComponent()}
              <View style={{ marginTop: recipientList.length > 0 ? 0 : 15 }}>
                <Event
                  value={isEvent}
                  updateMasterState={setState}
                  attrName="isEvent"
                  onSave={handleSaveEvent}
                  navigation={navigation}
                />
              </View>
            </View>
            <View style={styles.separator}>
              <Header
                min
                title="Respuesta Rápida"
                height={40}
                add
                edit={fastAnswerList.length > 0}
                onPressEdit={() => setEditFastAnswer(!editFastAnswer)}
                hiddenBackground
                onPressAdd={handleAddFastAnswer}
              />
              <View style={{ justifyContent: 'center' }}>
                {editFastAnswer && fastAnswerList.length > 0 && (
                  <View
                    style={{
                      marginTop: 10,
                      paddingHorizontal: 15,
                    }}
                  >
                    <Text align="left" color="#707070" fontSize={13} weight="Medium">
                      Seleccione uno para editar
                    </Text>
                  </View>
                )}
                {fastAnswerList.length > 0 ? (
                  <View
                    style={{
                      height: 55,
                      flexDirection: 'row',
                      marginTop: editFastAnswer ? 4 : 0,
                      paddingHorizontal: 15,
                      alignItems: 'center',
                    }}
                  >
                    {fastAnswerList.map((fastAnswer, index) => {
                      if (index > 2) {
                        return <></>;
                      }
                      return (
                        <TouchableOpacity
                          key={index}
                          style={{
                            height: 30,
                            paddingHorizontal: 15,
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexDirection: 'row',
                            marginRight: 10,
                            flex: 1 / 3,
                            backgroundColor: editFastAnswer
                              ? '#BBBBBB'
                              : fastAnswerSelected?.id === fastAnswer?.id
                              ? Colors.ORANGE
                              : '#464646',
                            borderRadius: 5,
                          }}
                          onPress={() => {
                            setEditFastAnswer(false);
                            if (editFastAnswer) {
                              handleEditFastAnswer(fastAnswer);
                              return;
                            }
                            handleSelectedFastAnswer(fastAnswer);
                          }}
                        >
                          <Text fontSize={13} weight="SemiBold" color={Colors.WHITE}>
                            {fastAnswer.name.length > 8
                              ? `${fastAnswer.name.substr(0, 8)}...`
                              : fastAnswer.name}
                          </Text>
                        </TouchableOpacity>
                      );
                    })}
                  </View>
                ) : (
                  <View style={styles.emptyList}>
                    <Text>Lista vacía</Text>
                  </View>
                )}
                {fastAnswerList.length > 3 && (
                  <TouchableOpacity
                    onPress={() => handleDetailAllList(true)}
                    style={{ alignItems: 'flex-end', marginBottom: 15, marginRight: '6%' }}
                  >
                    <Text weight="SemiBold" color={Colors.PRIMARYTEXTCOLOR}>
                      Ver todos +{fastAnswerList.length - 3}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </View>
            <View style={styles.separator}>
              <Header min title="Mensaje" height={40} hiddenBackground color="#56ACDE" />
              {attachList.length > 0 && (
                <View
                  style={[
                    styles.contentMessage,
                    styles.paddingHorizontal,
                    styles.row,
                    { flex: 1, flexWrap: 'wrap', marginTop: '4%' },
                  ]}
                >
                  {attachList.map((attach, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        onPress={() => handleGoToOpenFile(attach)}
                        style={{
                          minHeight: 90,
                          width: width / 3.4,
                          marginTop: 10,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        {attach.type === 'image' ? (
                          <>
                            <FastImage
                              source={{ uri: attach.image }}
                              resizeMode="stretch"
                              style={{ height: 90, width: 70, borderRadius: 5 }}
                            />
                            <Text>{attach.fileData.name}</Text>
                          </>
                        ) : (
                          <>
                            <View
                              style={{ height: 90, width: 70, backgroundColor: 'rgba(0,0,0,0.1)' }}
                            />
                            <Text>{attach.fileData.name}</Text>
                          </>
                        )}
                        <TouchableOpacity
                          style={styles.buttonDeleteAttach}
                          onPress={() => handleDeleteAttach(attach)}
                        >
                          <Icon name="x" size={15} color="white" />
                        </TouchableOpacity>
                      </TouchableOpacity>
                    );
                  })}
                </View>
              )}
              <View
                style={[
                  styles.contentMessage,
                  styles.paddingHorizontal,
                  { marginTop: attachList.length > 0 ? 0 : '5%' },
                ]}
              >
                <View style={[styles.rowLarge, styles.contentLine]}>
                  <View style={styles.row}>
                    <View style={styles.center}>
                      <CustomIcon name="attach" width={25} heigth={25} />
                    </View>
                    <Text>Agregar adjunto</Text>
                  </View>
                  <View style={[styles.row, { paddingRight: 15 }]}>
                    <TouchableOpacity
                      style={{ marginRight: 15 }}
                      hitSlop={hitSlop}
                      onPress={handleCamera}
                    >
                      <Icon name="instagram" size={24} />
                    </TouchableOpacity>
                    <TouchableOpacity hitSlop={hitSlop} onPress={handleFolder}>
                      <Icon name="folder" size={24} />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
            {input({
              multiline: false,
              title: 'Asunto',
              placeholder: '¿De que trata este mensaje?',
              value: subject,
              attrName: 'subject',
              updateMasterState: setState,
            })}
            {input({
              multiline: true,
              title: 'Cuerpo',
              placeholder: 'Escriba su mensaje acá',
              value: content,
              attrName: 'content',
              updateMasterState: setState,
            })}
            {isAlias &&
              input({
                multiline: false,
                title: 'Alias  de la respuesta rapida',
                placeholder: 'Escriba el Alias',
                value: aliasName,
                attrName: 'aliasName',
                updateMasterState: setState,
              })}
            {!fastAnswerSelected && (
              <View style={styles.paddingHorizontal}>
                <View style={[styles.rowLarge, styles.contentLine, styles.separator]}>
                  <Text>Agregar como respuesta rapida</Text>
                  <Switch
                    trackColor={{ false: '#767577', true: 'rgba(0, 105, 107, 0.3)' }}
                    thumbColor={isAlias ? '#00696B' : '#f4f3f4'}
                    style={{ marginRight: 10 }}
                    onValueChange={() => {
                      setState('isAlias', !isAlias);
                      if (!isAlias) {
                        scrolled();
                      }
                    }}
                    value={isAlias}
                  />
                </View>
              </View>
            )}
            <View style={styles.contenButtonSend}>
              <Button title="Enviar mensaje" onPress={handleSendMessage} />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    </View>
  );
};

export default Layout;
