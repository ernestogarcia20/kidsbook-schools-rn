import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { getReportAction } from '@dashboard/actions/report.actions';
import { REPORT_DETAIL, CREATE_REPORT, LIST_GLOBAL } from '@utils/constants/navigates';
import ReportsLayout from './components/reports-layout';

class ReportsController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      page: 0,
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    const { _getReportAction, user } = this.props;
    const { page } = this.state;

    try {
      await _getReportAction(user.schoolId, page);
    } catch (e) {}
  };

  onPressAddReport = () => {
    this.navigate(LIST_GLOBAL, { navigate: CREATE_REPORT });
  };

  handleGoToDetail = (item) => {
    this.navigate(REPORT_DETAIL, { report: item });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user, reportList } = this.props;
    return (
      <Container>
        <ReportsLayout
          openDrawer={this.openDrawer}
          setState={this.handleChange}
          handleGoToDetail={this.handleGoToDetail}
          onPressAddReport={this.onPressAddReport}
          reportList={reportList}
          user={user}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, reports: { reportList } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  reportList,
});

export default connect(mapStateToProps, { _getReportAction: getReportAction })(ReportsController);
