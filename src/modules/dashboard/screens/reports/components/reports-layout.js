import React from 'react';
import { View, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import Colors from '@assets/colors';
import Header from '@components/Header';
import { getImageById } from '@utils/functions/globals';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/Feather';
import I18n from 'react-native-redux-i18n';
import Background from '@images/bg1.png';
import FastImage from '@components/FastImage';
import Search from '@components/Search';
import CheckBox from '@components/CheckBox';
import moment from 'moment';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
  },
  tabs: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tabActive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3,
    borderBottomColor: Colors.GREEN,
  },
  tabInactive: {
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentSearch: { padding: '7%' },
  paddingIcon: { paddingRight: 7 },
  center: { justifyContent: 'center', alignItems: 'center' },
  dateText: { position: 'absolute', paddingTop: 3 },
  contentIcons: {
    flexDirection: 'row',
    height: 25,
    marginTop: 5,
    alignItems: 'center',
  },
  buttonIconMargin: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.WHITE,
    marginLeft: 10,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  buttonIcon: {
    height: 27,
    width: 27,
    borderRadius: 5,
    backgroundColor: Colors.BLUELIGHT,
    shadowColor: Colors.BLACK,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  contentActionsEdit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonTrash: {
    width: 30,
    height: 30,
    borderRadius: 5,
    backgroundColor: Colors.RED,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: '2%',
  },
  contentSelectedAll: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '3.5%',
    marginTop: '7%',
    alignItems: 'center',
  },
  contentEdit: { paddingHorizontal: '5%', paddingTop: '5%' },

  renderItem: {
    height: 115,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contentInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: '5%',
  },
  contentSent: {
    height: 20,
    marginTop: 5,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREEN,
    width: '50%',
  },
});

const Layout = ({ openDrawer, user, onPressAddReport, reportList, state, handleGoToDetail }) => {
  const { editList, selectAll } = state;
  const renderItem = ({ item, index }) => {
    const { avatar, child, teacher, date, sent_at } = item;
    const urlImage = getImageById(child.avatar, user?.authData?.apiToken);
    return (
      <View
        style={[
          styles.renderItem,
          {
            backgroundColor: index % 2 ? '#EBEBEB' : '',
          },
        ]}
      >
        <View style={styles.contentInfo}>
          <FastImage
            source={{ uri: urlImage }}
            style={{ width: 50, height: 50, borderRadius: 100, backgroundColor: 'rgba(0,0,0,0.1)' }}
          />
          <View style={{ marginLeft: 15 }}>
            <Text align="left" fontSize={12} weight="SemiBold">
              {child?.name}
            </Text>
            <Text align="left" fontSize={12}>
              Enviado por: {teacher?.name}
            </Text>
            <View style={styles.contentIcons}>
              <View style={styles.paddingIcon}>
                <View style={styles.center}>
                  <CustomIcon name="kb_calendar_blank" width={20} heigth={20} />
                </View>
              </View>
              <Text fontSize={12} weight="SemiBold">
                {date}
              </Text>
            </View>
            <View style={styles.contentSent}>
              <Text align="left" color={Colors.WHITE} fontSize={11}>
                Enviado
              </Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            handleGoToDetail(item);
          }}
          style={{ marginRight: 20 }}
          hitSlop={{ top: 20, left: 30, bottom: 15, right: 20 }}
        >
          <Icon name="chevron-right" size={28} />
        </TouchableOpacity>
      </View>
    );
  };
  const ListHeaderComponent = () => {
    return (
      <View>
        <Search hasBackground />
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={I18n.t('menu_list.reports')}
        onPress={openDrawer}
        menu
        add
        onPressAdd={onPressAddReport}
      />
      <FlatList
        data={reportList || []}
        extraData={reportList}
        renderItem={renderItem}
        ListHeaderComponent={ListHeaderComponent}
        keyExtractor={(item, index) => item + index}
      />
    </View>
  );
};

export default Layout;
