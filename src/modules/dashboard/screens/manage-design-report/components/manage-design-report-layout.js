import React, { useEffect, useState } from 'react';
import { View, StyleSheet, FlatList, ScrollView } from 'react-native';
import Header from '@components/Header';
import TextField from '@components/text-input-md';
import I18n from 'react-native-redux-i18n';
import colors from '@assets/colors';
import Button from '@components/Button';
import { Styles } from '@assets/Styles';
import RenderField from './render-fields';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentInput: { marginHorizontal: '5%', marginBottom: '4%', marginTop: 5 },
});

const Layout = ({ goBack, state, fields, setState, handleSetDefault, handleSaveTemplate }) => {
  const { template, templateName, fieldsSelected } = state;

  useEffect(() => {
    if (template?.fields) {
      let parsed = JSON.parse(template?.fields);
      parsed = typeof parsed === 'string' ? JSON.parse(parsed) : parsed;
      parsed.forEach((fId) => {
        fieldsSelected[fId] = true;
      });
      setState('fieldsSelected', fieldsSelected);
    }
    return () => {
      setState('fieldsSelected', {});
    };
  }, [template]);

  const handleField = (field) => {
    const fieldSelected = fieldsSelected[field.id];
    fieldsSelected[field.id] = !fieldSelected;
    setState('fieldsSelected', { ...fieldsSelected });
  };
  const renderItem = ({ item }) => {
    return (
      <>
        <RenderField field={item} fieldsSelected={fieldsSelected} onPressField={handleField} />
        {fieldsSelected[item.id] &&
          item?.children?.map((childField) => {
            return (
              <RenderField
                field={childField}
                fieldsSelected={fieldsSelected}
                onPressField={handleField}
              />
            );
          })}
      </>
    );
  };
  const onchange = (text) => setState('templateName', text);
  const ListHeaderComponent = () => {
    return (
      <View style={styles.contentInput}>
        <TextField
          label={I18n.t('templateName')}
          highlightColor={colors.GREEN}
          value={templateName}
          onChangeText={onchange}
        />
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={I18n.t(template?.fields ? 'menu_list.editTemplate' : 'menu_list.createTemplate')}
        onPress={goBack}
      />
      <ScrollView contentContainerStyle={{ paddingBottom: 20 }}>
        {ListHeaderComponent()}
        {fields?.map((field, index) => {
          return <View key={index}>{renderItem({ item: field, index })}</View>;
        })}
      </ScrollView>
      <View style={Styles.rowLarge}>
        <Button
          title="Marcar por defecto"
          showGrass={false}
          style={{ flex: 1, marginHorizontal: 5, borderRadius: 20 }}
          onPress={handleSetDefault}
        />
        <Button
          title={template?.fields ? 'Salvar' : 'Crear'}
          showGrass={false}
          color="#E28A38"
          onPress={handleSaveTemplate}
          style={{ flex: 1, marginHorizontal: 5, borderRadius: 20 }}
        />
      </View>
    </View>
  );
};

export default Layout;
