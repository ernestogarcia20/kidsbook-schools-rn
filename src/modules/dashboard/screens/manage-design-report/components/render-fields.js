import React, { useEffect, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, Dimensions, Switch } from 'react-native';
import Slider from '@react-native-community/slider';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Text from '@components/Text';
import CustomIcon from '@components/Icon';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Styles } from '@assets/Styles';
import FastImage from '@components/FastImage';
import { getImageById } from '@utils/functions/globals';
import colors from '@assets/colors';
import { SvgCss } from 'react-native-svg';
import loadLocalResource from 'react-native-local-resource';
import { svg } from '@shared/icons/import-svg-file';
import CheckBox from '@components/CheckBox';

const { width } = Dimensions.get('screen');

const styles = StyleSheet.create({
  contentTextField: {
    flex: 1,
    minHeight: 54,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.15)',
    paddingRight: '6%',
  },
});

const renderFileds = ({
  checkShow,
  field,
  fieldValues,
  fieldsSelected,
  onValueChange = () => {},
  onPressField = () => {},
}) => {
  const [newSvg, setNewSvg] = useState(null);

  useEffect(() => {
    loadLocalResource(svg[field.icon_name]).then((result) => {
      setNewSvg(result);
    });
  }, []);

  const breakfastText = () => {
    try {
      return field.values[fieldValues[field.name]];
    } catch (e) {
      return '';
    }
  };
  return (
    <View style={[{ width, minHeight: 54 }]}>
      <View style={[Styles.rowLarge, { width }]}>
        <View style={[Styles.center, { width: width * 0.2 }]}>
          {newSvg && <SvgCss xml={newSvg} width={35} height={35} />}
        </View>
        <View style={[styles.contentTextField, Styles.rowLarge]}>
          <Text fontSize={13} align="left">
            {field.name}
          </Text>
          <CheckBox onPress={() => onPressField(field)} check={fieldsSelected[field.id]} />
        </View>
      </View>
    </View>
  );
};

export default renderFileds;
