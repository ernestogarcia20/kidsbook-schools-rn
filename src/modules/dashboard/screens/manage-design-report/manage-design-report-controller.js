import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {
  getReportFieldsAction,
  saveReportTemplateAction,
} from '@modules/dashboard/actions/report.actions';
import { updateUserAction } from '@modules/auth/actions/auth.action';
import ManageDesignReportLayout from './components/manage-design-report-layout';

class ManageDesignReportController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const template = getParam('template', {});
    this.state = {
      template,
      templateName: template?.name,
      fieldsSelected: {},
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    const { _getReportFieldsAction, fields } = this.props;
    try {
      await _getReportFieldsAction(fields?.length === 0);
    } catch (e) {}
  };

  handleSaveTemplate = () => {
    this.saveReportTemplate();
  };

  handleSetDefault = () => {
    this.saveReportTemplate(true);
  };

  saveReportTemplate = async (defaultTemplate = false) => {
    const {
      _saveReportTemplateAction,
      _updateUserAction,
      user,
      navigation: { getParam },
    } = this.props;
    const { template, fieldsSelected, templateName } = this.state;
    const updateList = getParam('updateList', () => {});

    if (Object.keys(fieldsSelected).length === 0 || !templateName) {
      return;
    }
    template.name = templateName;
    const fields = [];
    Object.keys(fieldsSelected).forEach((id) => {
      if (fieldsSelected[id]) {
        fields.push(Number.parseInt(id));
      }
    });
    template.fields = JSON.stringify(fields);
    template.is_default = defaultTemplate;
    try {
      const data = await _saveReportTemplateAction(user.schoolId, template);
      const newUser = {
        ...user,
        school: {
          ...user.school,
          default_report_id: defaultTemplate ? data?.template?.id : user.school.default_report_id,
        },
      };
      _updateUserAction(newUser);
      updateList();
      this.goBack();
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <ManageDesignReportLayout
          goBack={this.goBack}
          fields={this.props.fields}
          handleSaveTemplate={this.handleSaveTemplate}
          handleSetDefault={this.handleSetDefault}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user, reports: { fields } }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
  fields,
});

export default connect(mapStateToProps, {
  _getReportFieldsAction: getReportFieldsAction,
  _saveReportTemplateAction: saveReportTemplateAction,
  _updateUserAction: updateUserAction,
})(ManageDesignReportController);
