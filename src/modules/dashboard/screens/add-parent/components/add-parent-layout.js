import React from 'react';
import { View, StyleSheet, Dimensions, TouchableOpacity, ScrollView } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';
import Colors from '@assets/colors';
import GrassImage from '@images/grass.png';
import FastImage from '@components/FastImage';
import Text from '@components/Text';
import Icon from 'react-native-vector-icons/Feather';
import TextField from '@components/text-input-md';
import Button from '@components/Button';
import PhoneInput from '@components/PhoneComponent/InputMaskPhone';
import IconSvg from '@components/Icon';
import { getImageById } from '@utils/functions/globals';
import colors from '@assets/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  grass: {
    marginTop: 27,
    backgroundColor: Colors.GREEN,
    height: 75,
    alignItems: 'center',
    flexDirection: 'row',
  },
  circleIcon: {
    height: 30,
    width: 30,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3E4D5F',
    marginHorizontal: 15,
  },
  contentInput: {
    flex: 1,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: Colors.GREEN,
    borderBottomWidth: 1.5,
    paddingHorizontal: 5,
    alignItems: 'center',
    marginVertical: 5,
  },
  form: {
    marginHorizontal: '6%',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rowOnly: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentTitleFamily: {
    height: 50,
    justifyContent: 'center',
    borderBottomWidth: 1,
    marginTop: 15,
    paddingHorizontal: 5,
    borderColor: 'rgba(0,0,0,0.2)',
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 100,
    backgroundColor: '#DADADA',
    marginRight: 15,
  },
  contentChild: {
    marginTop: 15,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 10,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 3,
    zIndex: 2,
  },
});

const Layout = ({
  goBack,
  setState,
  handleFindParent,
  handleAddParent,
  state,
  onChangeTextPhone,
  user,
  showModalSelection,
  handleSendInvitation,
  handleGoToChildList,
  handleDeleteChild,
  handleDeleteChilds,
}) => {
  const {
    showNewParent,
    name,
    lastName,
    email,
    gender,
    relationship,
    phone,
    childs,
    city,
    address,
    child,
    otherRelationship,
    showResendEmail,
    parent,
    inParent,
  } = state;
  return (
    <View style={styles.container}>
      <Header title={I18n.t('menu_list.addParent')} onPress={goBack} />
      <ScrollView keyboardShouldPersistTaps="handled">
        <FastImage source={GrassImage} style={styles.grass}>
          <View style={styles.circleIcon}>
            <Text weight="Bold" fontSize={16} color={Colors.WHITE}>
              !
            </Text>
          </View>
          <View style={{ flex: 1, marginRight: 15 }}>
            <Text color={Colors.WHITE} align="justify" fontSize={13}>
              {I18n.t('messageAddParent.invited')}
            </Text>
          </View>
        </FastImage>
        <View style={styles.form}>
          {!showNewParent ? (
            <>
              <View style={{ marginTop: '5%' }}>
                <TextField
                  label={I18n.t('emailParent')}
                  highlightColor={Colors.GREEN}
                  value={email}
                  keyboardType="email-address"
                  onChangeText={(text) => setState('email', text)}
                />
              </View>
            </>
          ) : (
            <View>
              <View style={styles.row}>
                <TextField
                  label={I18n.t('firstName')}
                  highlightColor={Colors.GREEN}
                  value={name}
                  wrapperStyle={{ flex: 1, marginRight: 10 }}
                  onChangeText={(text) => setState('name', text)}
                />
                <TextField
                  label={I18n.t('lastName')}
                  highlightColor={Colors.GREEN}
                  value={lastName}
                  wrapperStyle={{ flex: 1, marginLeft: 10 }}
                  onChangeText={(text) => setState('lastName', text)}
                />
              </View>
              <View style={styles.row}>
                <TouchableOpacity
                  style={[
                    styles.contentInput,
                    {
                      borderBottomColor: gender ? Colors.GREEN : '#E0E0E0',
                      marginRight: 10,
                      marginTop: 10,
                      marginBottom: 0,
                    },
                  ]}
                  onPress={() => showModalSelection('gender')}
                >
                  <Text align="left" color={gender ? colors.BLACK : '#9D9D9D'} weight="Medium">
                    {gender ? gender.text : I18n.t('gender')}
                  </Text>
                  <Icon name="triangle" size={12} style={{ transform: [{ rotate: '60deg' }] }} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[
                    styles.contentInput,
                    {
                      borderBottomColor: relationship ? Colors.GREEN : '#E0E0E0',
                      marginLeft: 10,
                      marginTop: 10,
                      marginBottom: 0,
                    },
                  ]}
                  onPress={() => showModalSelection('relationship')}
                >
                  <Text
                    align="left"
                    color={relationship ? colors.BLACK : '#9D9D9D'}
                    weight="Medium"
                  >
                    {relationship ? relationship.text : I18n.t('relationship')}
                  </Text>
                  <Icon name="triangle" size={12} style={{ transform: [{ rotate: '60deg' }] }} />
                </TouchableOpacity>
              </View>
              {relationship && relationship.value === 'Other' && (
                <View>
                  <TextField
                    label={I18n.t('otherRelationship')}
                    highlightColor={Colors.GREEN}
                    value={otherRelationship}
                    onChangeText={(text) => setState('otherRelationship', text)}
                  />
                </View>
              )}
              <View style={{ marginTop: 10, marginBottom: 5 }}>
                <PhoneInput
                  placeholder={I18n.t('writePhone')}
                  highlightColor={Colors.GREEN}
                  value={phone}
                  onChangeText={onChangeTextPhone}
                  attrName="phone"
                />
              </View>
              {/* <View style={styles.row}>
              <TouchableOpacity
                style={[styles.contentInput, { marginRight: 10, marginBottom: 0 }]}
                onPress={showModalRelationship}
              >
                <Text align="left" color="#9D9D9D" weight="Medium">
                  {country || I18n.t('country')}
                </Text>
                <Icon name="triangle" size={12} style={{ transform: [{ rotate: '60deg' }] }} />
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.contentInput, { marginLeft: 10, marginBottom: 0 }]}
                onPress={showModalRelationship}
              >
                <Text align="left" color="#9D9D9D" weight="Medium">
                  {city || I18n.t('city')}
                </Text>
                <Icon name="triangle" size={12} style={{ transform: [{ rotate: '60deg' }] }} />
              </TouchableOpacity>
        </View> */}
              <TextField
                label={I18n.t('emailParent')}
                highlightColor={Colors.GREEN}
                value={email}
                editable={false}
                keyboardType="email-address"
                onChangeText={(text) => setState('email', text)}
              />
              <TextField
                label={I18n.t('address')}
                highlightColor={Colors.GREEN}
                value={address}
                onChangeText={(text) => setState('address', text)}
              />

              <View style={styles.contentTitleFamily}>
                <Text align="left" color="#A8A8A8">
                  {I18n.t('children')}
                </Text>
              </View>
              <View style={[styles.contentChild]}>
                {childs.length > 0 &&
                  !child &&
                  childs.map((_child, index) => {
                    return (
                      <View style={[styles.row, { flex: 1, marginBottom: 10 }]} key={index}>
                        <View style={styles.rowOnly}>
                          <FastImage
                            source={{ uri: getImageById(_child?.avatar, user.token) }}
                            style={styles.imageProfile}
                          />
                          <Text align="left" weight="Medium" fontSize={12}>
                            {_child.name}
                          </Text>
                        </View>
                        {inParent && (
                          <TouchableOpacity onPress={() => handleDeleteChilds(_child)}>
                            <Icon name="x" size={24} color={Colors.RED} />
                          </TouchableOpacity>
                        )}
                      </View>
                    );
                  })}
                {child && childs.length === 0 && (
                  <>
                    <View style={[styles.row, { flex: 1 }]}>
                      <View style={styles.rowOnly}>
                        <FastImage
                          source={{ uri: getImageById(child?.avatar, user.token) }}
                          style={styles.imageProfile}
                        />
                        <Text align="left" weight="Medium" fontSize={12}>
                          {child.name}
                        </Text>
                      </View>
                      {inParent && (
                        <TouchableOpacity onPress={handleDeleteChild}>
                          <Icon name="x" size={24} color={Colors.RED} />
                        </TouchableOpacity>
                      )}
                    </View>
                  </>
                )}
                {!child && childs.length === 0 && (
                  <TouchableOpacity style={[styles.row]} onPress={handleGoToChildList}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          width: 45,
                          height: 45,
                          marginRight: '10%',
                        }}
                      >
                        <IconSvg name="kb_babyBoy" width={45} heigth={45} />
                      </View>
                      <View>
                        <Text weight="Medium" fontSize={13}>
                          {I18n.t('buttons.addChild')}
                        </Text>
                      </View>
                    </View>
                    <View>
                      <Icon name="chevron-right" size={24} />
                    </View>
                  </TouchableOpacity>
                )}
              </View>
            </View>
          )}
        </View>
        <View style={{ marginHorizontal: '25%', marginTop: '10%' }}>
          <Button
            title={I18n.t(`buttons.${showNewParent ? 'inviteParent' : 'next'}`)}
            onPress={showNewParent ? handleAddParent : handleFindParent}
          />
        </View>
        {showResendEmail && !showNewParent && (
          <View style={{ marginTop: '8%', marginHorizontal: '6%' }}>
            <View
              style={[
                {
                  height: 60,
                  borderRadius: 5,
                  backgroundColor: Colors.GREEN,
                  justifyContent: 'center',
                  paddingHorizontal: 15,
                },
                styles.shadow,
              ]}
            >
              <Text align="left" weight="Medium" fontSize={13} color={Colors.WHITE}>
                {I18n.t(`validation.success/parentAlready`)}
              </Text>
            </View>
            <View
              style={[
                {
                  marginVertical: 10,
                  paddingHorizontal: 15,
                  borderRadius: 5,
                  backgroundColor: Colors.WHITE,
                },
                styles.shadow,
              ]}
            >
              <View
                style={{
                  flexDirection: 'row',
                  paddingTop: 15,
                  alignItems: 'center',
                  backgroundColor: Colors.WHITE,
                  marginVertical: 10,
                  paddingHorizontal: 15,
                }}
              >
                <FastImage
                  source={{ uri: getImageById(parent?.avatar, user.token) }}
                  style={styles.imageProfile}
                />
                <Text align="left" weight="Medium" fontSize={12}>
                  {parent?.name}
                </Text>
              </View>
              <View
                style={{
                  marginTop: 5,
                  height: 30,
                  paddingHorizontal: 15,
                  marginBottom: 10,
                  flexDirection: 'row',
                }}
              >
                <Icon name="mail" size={20} style={{ marginRight: 10 }} />
                <Text align="left" weight="Medium" fontSize={14}>
                  {parent?.email}
                </Text>
              </View>
              <View style={{ marginHorizontal: '25%', marginBottom: '5%' }}>
                <Button title={I18n.t(`buttons.inviteParent`)} onPress={handleSendInvitation} />
              </View>
            </View>
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default Layout;
