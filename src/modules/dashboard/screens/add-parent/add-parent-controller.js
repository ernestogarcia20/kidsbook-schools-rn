import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import {
  findParentAction,
  inviteParentAction,
  inviteExistingParentAction,
} from '@dashboard/actions/parents.actions';
import { validateEmail } from '@utils/functions/globals';
import I18n from 'react-native-redux-i18n';
import { showMessage } from 'react-native-flash-message';
import { LIST_GLOBAL } from '@utils/constants/navigates';
import { fontMaker } from '@components/typografy';
import AddParentLayout from './components/add-parent-layout';

class AddParentController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const child = getParam('child', null);
    this.state = {
      email: '',
      child,
      inParent: !child,
      showNewParent: false,
      showResendEmail: false,
      name: '',
      lastName: '',
      gender: null,
      relationship: null,
      phone: '',
      country: '',
      city: '',
      address: '',
      otherRelationship: '',
      childs: [],
    };
  }

  handleFindParent = async () => {
    const { _findParentAction, user } = this.props;
    const { email } = this.state;

    if (!validateEmail(email)) {
      showMessage({
        message: I18n.t(`validation.error/emailInvalid`),
        type: 'default',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
        position: 'top',
      });
      return;
    }

    try {
      const { client } = await _findParentAction(user.schoolId, { email });
      if (client) {
        this.setState({ parent: client, showResendEmail: true });
        return;
      }
      this.setState({ parent: null, showNewParent: true });
    } catch (e) {}
  };

  handleAddParent = async () => {
    const {
      user,
      _inviteParentAction,
      navigation: { state },
    } = this.props;
    const {
      name,
      lastName,
      phone,
      address,
      relationship,
      email,
      child,
      otherRelationship,
      childs,
    } = this.state;
    if (!name) {
      showMessage({
        message: I18n.t(`validation.error/parentNameRequire`),
        type: 'default',
        titleStyle: { ...fontMaker('SemiBold'), fontSize: 16 },
      });
      return;
    }

    let obj = {
      school_id: user.schoolId,
      name: `${name} ${lastName}`,
      telephone: phone,
      address,
      email,
      relationship: relationship?.value || '',
      relationship_other: otherRelationship,
    };
    if (child || childs.length > 0)
      obj = { ...obj, child_id: childs.length > 0 ? childs.map((c) => c.id)[0] : child?.id };
    try {
      const { connection } = await _inviteParentAction(obj);
      if (state.params.refreshList) {
        let objRelationship = {
          id: connection?.client_id,
          client_id: connection?.client_id,
          relationship_other: obj.relationship_other,
          relationship: obj.relationship,
          updated_at: connection?.updated_at,
          created_at: connection?.created_at,
        };
        if (child) objRelationship = { ...objRelationship, children_id: child.id };
        state.params.refreshList(connection?.client_id, objRelationship);
      }
      this.goBack();
    } catch (e) {}
  };

  onChangeTextPhone = ({ dialCode, unmaskedPhoneNumber, phoneNumber, isVerified }) => {
    this.setState({ phone: `${dialCode} ${phoneNumber}`, dialCode });
  };

  showModalSelection = (key) => {
    const { dispatch } = this.props;
    this.setState({ modalSelected: key });
    this.showModalConfirm(
      {
        visible: true,
        title: I18n.t(key !== 'relationship' ? 'gender' : 'relationship'),
        data: I18n.t(key !== 'relationship' ? 'listGender' : 'listRelationshipString'),
        onPressAccept: (item) => this.selectionModal(item),
      },
      dispatch
    );
  };

  handleSendInvitation = async () => {
    const {
      user,
      _inviteExistingParentAction,
      navigation: { state },
    } = this.props;
    const { parent, child } = this.state;

    let obj = {
      school_id: user.schoolId,
      client_id: parent.id,
      type: 'Parent',
    };
    if (child) obj = { ...obj, child_id: child.id };
    try {
      await _inviteExistingParentAction(obj);
      if (state.params.refreshList) {
        state.params.refreshList();
      }
      this.goBack();
    } catch (e) {}
  };

  selectionModal = (item) => {
    const { modalSelected } = this.state;

    if (modalSelected === 'relationship') {
      return this.setState({ relationship: item });
    }
    if (modalSelected === 'gender') {
      return this.setState({ gender: item });
    }
  };

  handleSelectedChild = (childs) => {
    if (childs.length === 0) {
      return;
    }
    this.setState({ childs: [childs[0]] });
  };

  handleGoToChildList = () => {
    this.navigate(LIST_GLOBAL, {
      key: 'children',
      handleSelected: this.handleSelectedChild,
    });
  };

  handleDeleteChild = () => {
    this.setState({ child: null });
  };

  handleDeleteChilds = (child) => {
    const { childs } = this.state;
    const index = childs.findIndex((c) => c.id === child.id);
    if (index > -1) {
      childs.splice(index, 1);
    }
    this.setState({ childs: [...childs] });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    const { user } = this.props;
    return (
      <Container>
        <AddParentLayout
          setState={this.handleChange}
          state={this.state}
          user={user}
          handleFindParent={this.handleFindParent}
          handleAddParent={this.handleAddParent}
          handleGoToChildList={this.handleGoToChildList}
          showModalSelection={this.showModalSelection}
          goBack={this.goBack}
          handleDeleteChild={this.handleDeleteChild}
          handleDeleteChilds={this.handleDeleteChilds}
          onChangeTextPhone={this.onChangeTextPhone}
          handleSendInvitation={this.handleSendInvitation}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, {
  _findParentAction: findParentAction,
  _inviteParentAction: inviteParentAction,
  _inviteExistingParentAction: inviteExistingParentAction,
})(AddParentController);
