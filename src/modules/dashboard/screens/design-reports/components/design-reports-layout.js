import React from 'react';
import { View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import Header from '@components/Header';
import Text from '@components/Text';
import Icon from 'react-native-vector-icons/Feather';

import I18n from 'react-native-redux-i18n';
import { color } from 'react-native-reanimated';
import colors from '@assets/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  section: {
    height: 50,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: 50,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.2)',
  },
  content: {
    backgroundColor: colors.WHITE,
    flex: 1,
    marginVertical: 15,
    marginHorizontal: 15,
    borderRadius: 10,
  },
  contentContainerStyle: {
    paddingHorizontal: '5%',
    paddingVertical: 15,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowLarge: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  circleDefault: {
    width: 20,
    height: 20,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.GREEN,
    marginRight: 10,
  },
});

const Layout = ({ goBack, handleAddNewTemplate, state, handleGoToDetailTemplate, user }) => {
  const { templates } = state;
  const isDefault = (template) => {
    return (
      user?.school?.default_report_id === template?.id ||
      (template?.id === 1 && user?.school?.default_report_id == null)
    );
  };
  return (
    <View style={styles.container}>
      <Header
        title={I18n.t('menu_list.reportTemplates')}
        onPress={goBack}
        add
        onPressAdd={handleAddNewTemplate}
      />
      <View style={styles.content}>
        <ScrollView contentContainerStyle={styles.contentContainerStyle}>
          <View>
            <View style={styles.section}>
              <Text weight="SemiBold">Plantillas de Sistema</Text>
            </View>
            {templates?.generalTemplates?.map((template) => {
              const onPress = () => handleGoToDetailTemplate(template);
              return (
                <TouchableOpacity style={styles.item} onPress={onPress} activeOpacity={0.7}>
                  <View style={[styles.row, { flex: 0.8 }]}>
                    {isDefault(template) && (
                      <View style={styles.circleDefault}>
                        <Icon name="check" color="white" />
                      </View>
                    )}
                    <Text align="left">{template.name}</Text>
                  </View>
                  <Icon name="chevron-right" size={24} />
                </TouchableOpacity>
              );
            })}
          </View>
          <View>
            <View style={styles.section}>
              <Text weight="SemiBold">Plantillas personalizadas</Text>
            </View>
            {templates?.templates?.map((template) => {
              const onPress = () => handleGoToDetailTemplate(template);
              return (
                <TouchableOpacity style={styles.item} onPress={onPress} activeOpacity={0.7}>
                  <View style={[styles.row, { flex: 0.8 }]}>
                    {isDefault(template) && (
                      <View style={styles.circleDefault}>
                        <Icon name="check" color="white" />
                      </View>
                    )}
                    <Text align="left">{template.name}</Text>
                  </View>
                  <Icon name="chevron-right" size={24} />
                </TouchableOpacity>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Layout;
