import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { MANAGE_DESIGN_REPORT } from '@utils/constants/navigates';
import { getReportTemplatesAction } from '@modules/dashboard/actions/report.actions';
import DesignReportsLayout from './components/design-reports-layout';

class DesignReportsController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      templates: {},
    };
  }

  componentDidMount() {
    this.init();
  }

  init = async () => {
    const { _getReportTemplatesAction, user } = this.props;
    const { templates } = this.state;

    try {
      const data = await _getReportTemplatesAction(
        user.schoolId,
        Object.keys(templates).length === 0
      );
      this.setState({ templates: data });
    } catch (e) {}
  };

  handleGoToDetailTemplate = (template) => {
    this.navigate(MANAGE_DESIGN_REPORT, { edit: true, template });
  };

  handleAddNewTemplate = () => {
    this.navigate(MANAGE_DESIGN_REPORT, { updateList: this.init });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container showBackground>
        <DesignReportsLayout
          goBack={this.goBack}
          user={this.props.user}
          handleAddNewTemplate={this.handleAddNewTemplate}
          handleGoToDetailTemplate={this.handleGoToDetailTemplate}
          setState={this.handleChange}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps, { _getReportTemplatesAction: getReportTemplatesAction })(
  DesignReportsController
);
