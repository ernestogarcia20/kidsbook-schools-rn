import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { SIGN_IN, DASHBOARD } from '@utils/constants/navigates';

class Authenticator extends Component {
  constructor(props) {
    super(props);
    this.bootstrapValidation();
  }

  bootstrapValidation = () => {
    const {
      navigation: { navigate },
      user,
    } = this.props;
    // navigate(user ? 'App' : 'Login'); todo Eliminar
    navigate(user ? DASHBOARD : SIGN_IN);
  };

  render = () => <View />;
}
const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(Authenticator);
