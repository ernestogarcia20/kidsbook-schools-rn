import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { NOTIFICATION_PAGE } from '@utils/constants/navigates';
import RequestAccessLayout from './components/request-access-layout';

class RequestAccessController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
  }

  handleRequestAccess = () => {
    this.navigate(NOTIFICATION_PAGE, { isRequest: true });
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <RequestAccessLayout
          setState={this.handleChange}
          goBack={this.goBack}
          handleRequestAccess={this.handleRequestAccess}
          handleGoToTranslate={this.handleGoToTranslate}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ i18n }, store) => ({
  dispatch: store.navigation.dispatch,
  i18n,
});

export default connect(mapStateToProps)(RequestAccessController);
