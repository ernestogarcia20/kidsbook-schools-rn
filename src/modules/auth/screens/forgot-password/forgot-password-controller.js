import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { forgotPasswordAction } from '@auth/actions/auth.action';
import { NOTIFICATION_PAGE } from '@utils/constants/navigates';
import { validateEmail } from '@utils/functions/globals';
import I18n from 'react-native-redux-i18n';
import { showMessage } from 'react-native-flash-message';
import ForgotPasswordLayout from './components/forgot-password-layout';

class ForgotPasswordController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: 'edgarciav@outlook.com',
    };
  }

  handleRecoverPassword = async () => {
    const { _forgotPasswordAction } = this.props;
    const { email } = this.state;
    if (!email) {
      return;
    }
    if (!validateEmail(email)) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.email/invalid`),
        type: 'danger',
      });
      return;
    }
    try {
      await _forgotPasswordAction(email);
      this.navigate(NOTIFICATION_PAGE);
    } catch (e) {}
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <ForgotPasswordLayout
          setState={this.handleChange}
          goBack={this.goBack}
          handleRecoverPassword={this.handleRecoverPassword}
          handleGoToTranslate={this.handleGoToTranslate}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ i18n }, store) => ({
  dispatch: store.navigation.dispatch,
  i18n,
});

export default connect(mapStateToProps, { _forgotPasswordAction: forgotPasswordAction })(
  ForgotPasswordController
);
