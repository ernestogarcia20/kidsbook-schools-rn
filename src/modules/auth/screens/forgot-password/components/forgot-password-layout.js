import React from 'react';
import { View, StyleSheet, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import FastImage from '@components/FastImage';
import IconSrc from '@images/icon.png';
import Background from '@images/bg1.png';
import IconSvg from '@components/Icon';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Text from '@components/Text';
import Button from '@components/Button';
import Input from '@components/InputAuth';
import Colors from '@assets/colors';
import I18n from 'react-native-redux-i18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 0.45,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: -1,
  },
  buttonTranslate: {
    position: 'absolute',
    top: 20,
    left: 20,
    width: 60,
    height: 60,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.WHITE,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
  },
  contentForm: {
    flex: 0.55,
    backgroundColor: Colors.WHITE,
    borderTopEndRadius: 40,
    borderTopStartRadius: 40,
    zIndex: 1,
    paddingVertical: '10%',
    paddingHorizontal: '10%',
  },
  footer: {
    paddingHorizontal: '10%',
    paddingBottom: '10%',
  },
  KeyboardAvoidingView: { flex: 1 },
  contentRule: { paddingTop: '10%' },
  contentTitle: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonBack: {
    marginRight: 15,
    marginBottom: 5,
  },
});

const Layout = ({ setState, state, handleGoToTranslate, handleRecoverPassword, goBack }) => {
  const { email } = state;
  return (
    <View style={styles.container}>
      <KeyboardAvoidingView style={styles.KeyboardAvoidingView} behavior="padding">
        <FastImage style={styles.header} source={Background}>
          <TouchableOpacity style={styles.buttonTranslate} onPress={handleGoToTranslate}>
            <IconSvg name="kb_translation" height={40} width={40} />
          </TouchableOpacity>
          <FastImage source={IconSrc} style={{ width: 190, height: 190 }} />
        </FastImage>
        <View style={styles.contentForm}>
          <View style={styles.contentTitle}>
            <TouchableOpacity
              style={styles.buttonBack}
              onPress={goBack}
              hitSlop={{ top: 15, left: 15, bottom: 15, right: 15 }}
            >
              <Icon name="chevron-left" size={22} color={Colors.BLACK} />
            </TouchableOpacity>
            <Text fontSize={24} align="left" weight="Light">
              {I18n.t('recoverPassword')}
            </Text>
          </View>
          <View style={styles.contentRule}>
            <Text fontSize={13} align="justify" weight="Medium">
              {I18n.t('ruleForgotPassword')}
            </Text>
          </View>
          <View>
            <Input
              updateMasterState={setState}
              attrName="email"
              value={email}
              placeholder={I18n.t('email')}
              keyboardType="email-address"
              style={{ marginVertical: '10%' }}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
      <View style={styles.footer}>
        <Button title={I18n.t('recoverPassword')} onPress={handleRecoverPassword} />
      </View>
    </View>
  );
};

export default Layout;
