import React from 'react';
import { View, StyleSheet, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';
import FastImage from '@components/FastImage';
import IconSrc from '@images/icon.png';
import Background from '@images/bg1.png';
import IconSvg from '@components/Icon';
import Text from '@components/Text';
import Button from '@components/Button';
import Input from '@components/InputAuth';
import Colors from '@assets/colors';
import I18n from 'react-native-redux-i18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 0.45,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: -1,
  },
  buttonTranslate: {
    position: 'absolute',
    top: 20,
    left: 20,
    width: 60,
    height: 60,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.WHITE,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
  },
  contentWelcomeText: {
    height: 40,
    backgroundColor: Colors.WHITE,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '8%',
    borderRadius: 10,
    borderTopColor: Colors.GREEN,
    borderTopWidth: 5,
    transform: [{ rotate: '2deg' }],
  },
  contentForm: {
    flex: 0.65,
    backgroundColor: Colors.WHITE,
    borderTopEndRadius: 40,
    borderTopStartRadius: 40,
    zIndex: 1,
    paddingVertical: '10%',
    paddingHorizontal: '15%',
  },
  buttonForgotPassword: { alignItems: 'flex-end', marginBottom: '15%' },
  footer: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '10%',
  },
  KeyboardAvoidingView: { flex: 1 },
});

const Layout = ({
  setState,
  state,
  handleGoToTranslate,
  handleGoToForgotPassword,
  handleLogIn,
  handleGoToRequestAccess,
}) => {
  const { email, password } = state;
  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        style={styles.KeyboardAvoidingView}
        behavior={Platform.OS === 'android' ? undefined : 'padding'}
      >
        <FastImage style={styles.header} source={Background}>
          <TouchableOpacity style={styles.buttonTranslate} onPress={handleGoToTranslate}>
            <IconSvg name="kb_translation" height={40} width={40} />
          </TouchableOpacity>
          <FastImage source={IconSrc} style={{ width: 160, height: 160 }} />
          <View style={styles.contentWelcomeText}>
            <Text fontSize={16}>{I18n.t('welcome')}</Text>
          </View>
        </FastImage>
        <View style={styles.contentForm}>
          <Text fontSize={24} align="left" weight="Light">
            {I18n.t('logIn')}
          </Text>
          <View>
            <Input
              updateMasterState={setState}
              attrName="email"
              value={email}
              placeholder={I18n.t('email')}
              keyboardType="email-address"
              style={{ marginTop: '10%' }}
            />
            <Input
              updateMasterState={setState}
              attrName="password"
              value={password}
              placeholder={I18n.t('password')}
              isPassword
              style={{ marginVertical: '10%' }}
            />
          </View>
          <View style={styles.buttonForgotPassword}>
            <TouchableOpacity
              onPress={handleGoToForgotPassword}
              hitSlop={{ top: 15, left: 15, bottom: 15, right: 15 }}
            >
              <Text weight="Medium">{I18n.t('forgotPassword')}</Text>
            </TouchableOpacity>
          </View>
          <Button title={I18n.t('logIn')} onPress={handleLogIn} />
        </View>
      </KeyboardAvoidingView>
      {/* <View style={styles.footer}>
        <Text weight="Light">{I18n.t('dontAccess')}</Text>
        <TouchableOpacity
          onPress={handleGoToRequestAccess}
          hitSlop={{ top: 15, left: 15, bottom: 15, right: 15 }}
        >
          <Text weight="Medium" color={Colors.GREEN}>
            {I18n.t('requestAccess')}
          </Text>
        </TouchableOpacity>
      </View> */}
    </View>
  );
};

export default Layout;
