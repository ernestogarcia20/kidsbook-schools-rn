import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { FORGOT_PASSWORD, REQUEST_ACCESS, DASHBOARD } from '@utils/constants/navigates';
import { logInAction } from '@auth/actions/auth.action';
import { validateEmail } from '@utils/functions/globals';
import { showMessage } from 'react-native-flash-message';
import I18n from 'react-native-redux-i18n';
import SignInLayout from './components/sign-in-layout';

class SignInController extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: 'kidsbookweb@gmail.com',
      // email: 'ernesto@kidsbooknet.com',
      password: 'kidsbook',
    };
  }

  handleGoToForgotPassword = () => {
    this.navigate(FORGOT_PASSWORD);
  };

  handleLogIn = async () => {
    const { _logInAction } = this.props;
    const { email, password } = this.state;

    if (!email || !password) {
      return;
    }
    if (!validateEmail(email)) {
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.email/invalid`),
        type: 'danger',
      });
      return;
    }
    try {
      await _logInAction(email, password);
      this.navigate(DASHBOARD);
    } catch (e) {}
  };

  handleGoToRequestAccess = () => {
    this.navigate(REQUEST_ACCESS);
  };

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <SignInLayout
          setState={this.handleChange}
          handleGoToForgotPassword={this.handleGoToForgotPassword}
          handleGoToTranslate={this.handleGoToTranslate}
          handleLogIn={this.handleLogIn}
          handleGoToRequestAccess={this.handleGoToRequestAccess}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ i18n }, store) => ({
  dispatch: store.navigation.dispatch,
  i18n,
});

export default connect(mapStateToProps, { _logInAction: logInAction })(SignInController);
