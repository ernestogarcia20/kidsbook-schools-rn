import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import { SIGN_IN } from '@utils/constants/navigates';
import NotificationPageLayout from './components/notification-page-layout';

class NotificationPageController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const isRequest = getParam('isRequest', false);
    this.state = {
      isRequest,
    };
  }

  handleChange = (name, value) => this.setState({ [name]: value });

  handleGoToLogin = () => {
    this.navigate(SIGN_IN);
  };

  render() {
    return (
      <Container>
        <NotificationPageLayout
          setState={this.handleChange}
          handleGoToLogin={this.handleGoToLogin}
          handleGoToTranslate={this.handleGoToTranslate}
          state={this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({ i18n }, store) => ({
  dispatch: store.navigation.dispatch,
  i18n,
});

export default connect(mapStateToProps)(NotificationPageController);
