import React from 'react';
import { View, StyleSheet, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import FastImage from '@components/FastImage';
import IconSrc from '@images/icon.png';
import Background from '@images/bg1.png';
import IconSvg from '@components/Icon';
import Icon from 'react-native-vector-icons/Feather';
import Text from '@components/Text';
import Button from '@components/Button';
import Colors from '@assets/colors';
import I18n from 'react-native-redux-i18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 0.45,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: -1,
  },
  buttonTranslate: {
    position: 'absolute',
    top: 20,
    left: 20,
    width: 60,
    height: 60,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.WHITE,

    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    zIndex: 2,
  },
  contentForm: {
    flex: 0.55,
    backgroundColor: Colors.WHITE,
    borderTopEndRadius: 40,
    borderTopStartRadius: 40,
    zIndex: 1,
    paddingVertical: '10%',
    paddingHorizontal: '10%',
  },
  footer: {
    paddingHorizontal: '10%',
    paddingBottom: '10%',
  },
  KeyboardAvoidingView: { flex: 1 },
  contentRule: { paddingTop: '10%' },
  contentTitle: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonBack: {
    marginRight: 15,
    marginBottom: 5,
  },
  contentText: {
    marginBottom: '7%',
  },
  roundCheck: {
    width: 150,
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.GREENLIGHT,
    borderRadius: 100,
    marginBottom: '10%',
  },
});

const Layout = ({ handleGoToTranslate, handleGoToLogin, state }) => {
  const { isRequest } = state;
  return (
    <View style={styles.container}>
      <FastImage style={styles.header} source={Background}>
        <TouchableOpacity style={styles.buttonTranslate} onPress={handleGoToTranslate}>
          <IconSvg name="kb_translation" height={40} width={40} />
        </TouchableOpacity>
        <FastImage source={IconSrc} style={{ width: 190, height: 190 }} />
      </FastImage>
      <View style={styles.contentForm}>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <View style={styles.roundCheck}>
            <Icon name="check" size={90} color={Colors.WHITE} />
          </View>
          <Text fontSize={18} align="center" weight="Medium">
            {I18n.t(isRequest ? 'requestSent' : 'successRecoverPassword')}
          </Text>
        </View>
      </View>
      <View style={styles.footer}>
        <View style={styles.contentText}>
          <Text weight="Light">{I18n.t(isRequest ? 'checkEmailSchool' : 'checkEmail')}</Text>
        </View>
        <Button title={I18n.t('goToHome')} onPress={handleGoToLogin} />
      </View>
    </View>
  );
};

export default Layout;
