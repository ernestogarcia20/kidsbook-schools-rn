/* eslint-disable prettier/prettier */
import API from '@api/services';

export const logInService = (data) => {
    return API.post('/auth/login', data);
}

export const forgotPassword = async (data) => {
    return API.post('/auth/passwordReset', data);
}

export const SignUp = async () => {
    return API.post('/auth/login');
}