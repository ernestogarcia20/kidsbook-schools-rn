import { SET_USER, REMOVE_ALL } from '@utils/constants/reducers';
import { setStore } from '@utils/functions/globals';
import loadingAction from '@store/actions/loading.action';
import { showMessage } from 'react-native-flash-message';
import I18n from 'react-native-redux-i18n';
import { logInService, forgotPassword } from '@auth/services/user-service';
import { getFastAnswerAction } from '@dashboard/actions/fast-answers.actions';
import { Platform } from 'react-native';
import API, { BASE_API } from '@api/services';

const isTeacher = (data) => {
  const access =
    data.appData.schoolData.schools[0].access !== undefined
      ? data.appData.schoolData.schools[0].access
      : '';
  return access === 'Teacher';
};

export const updateUserAction = (newUser) => {
  return async (dispatch) => {
    dispatch(setStore(SET_USER, newUser));
  };
};

export const logInAction = (email, password, fcmToken) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'logIn'));
    try {
      const { data } = await logInService({
        fcm_token: fcmToken,
        platform: Platform.OS,
        password,
        email,
      });
      dispatch(getFastAnswerAction(data.appData.schoolData.schools[0].id));
      API.setAuthorization(data.appData.authData.apiToken);
      dispatch(
        setStore(SET_USER, {
          ...data.appData,
          imageProfile: `${BASE_API}/file/${data.appData.schoolData.schools[0].profile_image}?api-key=${data.appData.authData.apiToken}`,
          isTeacher: isTeacher(data),
          schoolId: data.appData.schoolData.schools[0].id,
          school: data.appData.schoolData.schools[0],
          token: data.appData.authData.apiToken,
          id: data.appData.authData.client_id,
        })
      );

      dispatch(loadingAction(false));
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
    /*
    dispatch(setStore(SET_LANGUAGE_LIST, languageList || []));
    dispatch(setStore(SET_SELECTED_LANGUAGE, languageList.find((x) => x.selected) || null)); */
  };
};

export const forgotPasswordAction = (email) => {
  return async (dispatch) => {
    dispatch(loadingAction(true, 'loading'));
    try {
      const { success } = await forgotPassword({
        email,
      });
      dispatch(loadingAction(false));
      Promise.resolve(success);
    } catch (e) {
      dispatch(loadingAction(false));
      showMessage({
        message: I18n.t('alert.error'),
        description: I18n.t(`validation.${e.code}`),
        type: 'danger',
      });
      throw e;
    }
  };
};

export const logOut = () => {
  return async (dispatch) => {
    dispatch(setStore(REMOVE_ALL));
    /* dispatch(setLocale(locale));
      const languageList = I18n.t('languageList');
      dispatch(setStore(SET_LANGUAGE_LIST, languageList || []));
      dispatch(setStore(SET_SELECTED_LANGUAGE, languageList.find((x) => x.selected) || null)); */
  };
};
