import React from 'react';
import { View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import Header from '@components/Header';
import I18n from 'react-native-redux-i18n';

import Colors from '@assets/colors';
import Icon from 'react-native-vector-icons/Feather';
// import Pdf from 'react-native-pdf';
import FastImage from '@components/FastImage';

const { width, height } = Dimensions.get('window');

const hitSlop = { top: 15, left: 10, right: 10, bottom: 15 };

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BLACK,
  },
  goBack: {
    marginHorizontal: 15,
    marginVertical: '5%',
    maxWidth: 50,
  },
  pdf: {
    flex: 1,
    width,
    height,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.9,
    height: height * 0.9,
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Layout = ({ goBack, state }) => {
  const { attach } = state;
  const renderFile = () => {
    if (attach?.fileType === 'pdf' || attach?.fileType === 'gdoc') {
      const decUrl = decodeURI(attach.fileData.uri);
      // return <Pdf source={{ uri: decUrl, cache: true }} style={styles.pdf} />;
    }
    if (attach?.fileType === 'png' || attach?.fileType === 'jpg') {
      return (
        <View style={styles.content}>
          <FastImage source={{ uri: attach.image }} style={styles.image} />
        </View>
      );
    }
    return <></>;
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.goBack} hitSlop={hitSlop} onPress={goBack}>
        <Icon name="arrow-left" color="white" size={30} />
      </TouchableOpacity>
      {renderFile()}
    </View>
  );
};

export default Layout;
