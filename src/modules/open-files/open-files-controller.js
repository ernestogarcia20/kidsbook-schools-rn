import React from 'react';
import { connect } from 'react-redux';
import Container from '@components/Container';
import BaseComponent from '@components/BaseComponent';
import OpenFilesLayout from './components/open-files-layout';

class OpenFilesController extends BaseComponent {
  constructor(props) {
    super(props);
    const {
      navigation: { getParam },
    } = props;
    const attach = getParam('attach', null);
    this.state = {
      attach,
    };
  }

  handleChange = (name, value) => this.setState({ [name]: value });

  render() {
    return (
      <Container>
        <OpenFilesLayout goBack={this.goBack} setState={this.handleChange} state={this.state} />
      </Container>
    );
  }
}

const mapStateToProps = ({ user }, store) => ({
  dispatch: store.navigation.dispatch,
  user,
});

export default connect(mapStateToProps)(OpenFilesController);
