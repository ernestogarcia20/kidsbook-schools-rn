/**
 * GENDER
 */

export const FEMALE_GENDER = 'Female';
export const MALE_GENDER = 'Male';

/**
 * MESSAGES
 */

export const MESSAGE_PARENTS = 'Message';
export const MESSAGE_ALL = 'All';
export const MESSAGE_INTERNAL = 'Internal';
export const MESSAGE_RECEIVED = 'received';
export const MESSAGE_SENT = 'sent';
export const MESSAGE_ALL_SCHOOL = 'allschool';


/**
 * ACTIVITIES
 */

export const ACTIVITY_TODAY = 'today';
export const ACTIVITY_NEXT_EXPIRE = 'nextExpire';
export const ACTIVITY_DELIVERED = 'delivered';
