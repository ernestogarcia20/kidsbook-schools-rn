/**
 * AUTH
 */
export const SIGN_IN = 'Login';
export const DRAWER = 'Drawer';
export const NOTIFICATION_PAGE = 'NotificationPage';
export const FORGOT_PASSWORD = 'ForgotPassword';
export const REQUEST_ACCESS = 'RequestAccess';

export const LANGUAGE = 'Language';

/**
 * DASHBOARD
 */

export const DASHBOARD = 'Dashboard';
export const CHILDREN = 'Children';
export const CHILDREN_DETAIL = 'ChildrenDetail';
export const TEACHERS = 'Teachers';
export const PARENTS = 'Parents';
export const CLASSROOM = 'Classroom';
export const SOCIAL_NETWORK = 'SocialNetwork';
export const MANAGE_POST = 'ManagePost';
export const DETAIL_TEACHER = 'DetailTeacher';
export const DETAIL_PARENT = 'DetailParent';
export const CREATE_SOCIAL_NETWORK = 'CreateSocialNetwork';
export const DETAIL_SOCIAL_NETWORK = 'DetailSocialNetwork';
export const SETTINGS = 'Settings';
export const REPORTS = 'Reports';
export const CALENDAR = 'Calendar';
export const ADD_PARENTS_TO_CHILD = 'AddParentsToChild';
export const ADD_PARENT = 'AddParent';
export const VERIFICATE_CLIENT = 'VerificateClient';
export const VERIFICATION_CENTER = 'VerificationCenter';
export const SEARCH_VERIFY_CLIENT = 'SearchVerifyClient';
export const LIST_GLOBAL = 'ListGlobal';
export const ADD_TEACHER = 'AddTeacher';
export const MESSAGES = 'Messages';
export const ADD_EDIT_CLASSROOM = 'AddEditClassroom';
export const MESSAGE_DETAIL = 'MessageDetail';
export const CREATE_MESSAGE = 'CreateMessage';
export const CLASSROOM_TEACHER = 'ClassroomTeacher';
export const EVENT_REPEAT = 'EventRepeat';
export const REPORT_DETAIL = 'ReportDetail';
export const CREATE_FAST_ANSWER = 'CreateFastAnswer';
export const GLOBAL_LIST_MESSAGE = 'GlobalListMessage';
export const OPEN_FILES = 'OpenFiles';
export const ACTIVITIES = 'Activities';
export const CREATE_ACTIVITY = 'CreateActivity';
export const ACTIVITY_DETAILS = 'ActivityDetails';
export const ATTENDANCE = 'Attendance';
export const CREATE_REPORT = 'CreateReport';
export const BIRTHDAY = 'Birthday';
export const SCHOOL_OPTIONS = 'SchoolOptions';
export const DESIGN_REPORTS = 'DesignReports';
export const MANAGE_DESIGN_REPORT = 'ManageDesignReport';
