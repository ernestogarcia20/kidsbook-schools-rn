export const REMOVE_ALL = 'REMOVE_ALL';

/**
 * USER
 */

export const SET_USER = 'SET_USER';
export const REMOVE_USER = 'REMOVE_USER';

/**
 * LANGUAGE
 */

export const SET_SELECTED_LANGUAGE = 'SET_SELECTED_LANGUAGE';
export const SET_LANGUAGE_LIST = 'SET_LANGUAGE_LIST';

/**
 * CHILDREN
 */

export const SET_CHILDREN_LIST = 'SET_CHILDREN_LIST';
export const SET_CHILDREN_PENDING_LIST = 'SET_CHILDREN_PENDING_LIST';

/**
 * TEACHERS
 */

export const SET_TEACHER_LIST = 'SET_TEACHER_LIST';

/**
 * PARENTS
 */

export const SET_PARENT_LIST = 'SET_PARENT_LIST';
export const SET_PARENT_PENDING_LIST = 'SET_PARENT_PENDING_LIST';

/**
 * aCTIVITIES
 */

export const SET_ACTIVITIES = 'SET_ACTIVITIES';
export const REMOVE_ACTIVITY = 'REMOVE_ACTIVITY';
export const UPDATE_ACTIVITY = 'UPDATE_ACTIVITY';
export const INSERT_ACTIVITY = 'INSERT_ACTIVITY';

/**
 * PARENTS
 */

export const SET_CLASSROOM_LIST = 'SET_CLASSROOM_LIST';

/**
 * FAST ANSWER
 */

export const SET_FAST_ANSWER = 'SET_FAST_ANSWER';
export const REMOVE_FAST_ANSWER = 'REMOVE_FAST_ANSWER';

/**
 * CALENDAR
 */

export const SET_EVENT_CALENDAR = 'SET_EVENT_CALENDAR';

/**
 * CALENDAR
 */

export const SET_BIRTHDAY_LIST = 'SET_BIRTHDAY_LIST';

/**
 * SOCIAL
 */

export const SET_POSTS = 'SET_POSTS';
export const LIKE_POST = 'LIKE_POST';
export const REMOVE_POST = 'REMOVE_POST';

/**
 * REPORTS
 */

export const SET_REPORT_LIST = 'SET_REPORT_LIST';
export const SET_FIELDS = 'SET_FIELDS';

/**
 * LOADING
 */

export const SET_LOADING = 'SET_LOADING';

/**
 * LOADING
 */

export const SET_MODAL_CONFIRM = 'SET_MODAL_CONFIRM';
