import Colors from '@assets/colors';
import API, { BASE_API } from '@api/services';
import I18n from 'react-native-redux-i18n';
import moment from 'moment';

export function pickTextColorBasedOnBgColorSimple(bgColor) {
  const color = bgColor.charAt(0) === '#' ? bgColor.substring(1, 7) : bgColor;
  const r = parseInt(color.substring(0, 2), 16); // hexToR
  const g = parseInt(color.substring(2, 4), 16); // hexToG
  const b = parseInt(color.substring(4, 6), 16); // hexToB
  return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? Colors.BLACK : Colors.WHITE;
}

export function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

export const getInitials = (string) => {
  const names = string.split(' ');
  let initials = names[0].substring(0, 1).toUpperCase();

  if (names.length > 1) {
    initials += names[names.length - 1].substring(0, 1).toUpperCase();
  }
  return initials;
};

export const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export const uniqueArray = (array = []) => {
  const newArray = array.filter((item, index) => {
    const itemString = JSON.stringify(item);
    return (
      index ===
      array.findIndex((obj) => {
        return JSON.stringify(obj) === itemString;
      })
    );
  });
  return newArray;
};

export const setStore = (ACCION, data) => {
  return { type: ACCION, payload: data };
};

export const getImageById = (id, token, size = null) => {
  if (size == null) {
    return `${BASE_API}/file/${id}?api-key=${token}`;
  }
  return `${BASE_API}/file/${id}?size=${size}&api-key=${token}`;
};

export const getImagePost = (fileName, token, size = null) => {
  /* if (size == null) {
    return `${BASE_API}/file/${id}?api-key=${token}`;
  } */
  console.log(`${BASE_API}/social/getImage/${fileName}?api-key=${token}`);
  return `${BASE_API}/social/getImage/${fileName}?api-key=${token}`;
};

export const getImageActivity = (fileName, token, size = null) => {
  /* if (size == null) {
    return `${BASE_API}/file/${id}?api-key=${token}`;
  } */
  return `${BASE_API}/activities/getImage/${fileName}?api-key=${token}`;
};

export const excerpt = (message) => {
  if (isEmptyOrSpaces(message)) return I18n.t('noMessage');
  if (message.length < 140) return message;

  return `${message.substr(0, 137)}...`;
};

export const calculeAge = (string) => {
  const age = moment().diff(string, 'years');
  if (age > 0) {
    return age;
  }

  return `${moment().diff(string, 'months')}m`;
};

export const validateRelationship = {
  Father: 'Father',
  Mother: 'Mother',
  Uncle: 'Uncle',
  Brother: 'Brother',
  Sister: 'Sister',
  Other: 'Other',
  Teacher: 'Teacher',
  Parent: 'Parent',
};

export const getEmojis = () => {
  const EMOJIS =
    '😀 😃 😄 😁 😆 😅 😂 🤣 😊 😇 🙂 🙃 😉 😌 😍 😘 😗 😙 😚 😋 😜 😝 😛 🤑 🤗 🤓 😎 🤡 🤠 😏 😒 😞 😔 😟 😕 🙁' +
    ' ☹️ 😣 😖 😫 😩 😤 😠 😡 😶 😐 😑 😯 😦 😧 😮 😲 😵 😳 😱 😨 😰 😢 😥 🤤 😭 😓 😪 😴 🙄 🤔 🤥 😬 🤐 🤢 🤧 😷 🤒 🤕 😈 👿' +
    ' 👹 👺 💩 👻 💀 ☠️ 👽 👾 🤖 🎃 😺 😸 😹 😻 😼 😽 🙀 😿 😾 👐 🙌 👏 🙏 🤝 👍 👎 👊 ✊ 🤛 🤜 🤞 ✌️ 🤘 👌 👈 👉 👆 👇 ☝️ ✋ 🤚' +
    ' 🖐 🖖 👋 🤙 💪 🖕 ✍️ 🤳 💅 🖖 💄 💋 👄 👅 👂 👃 👣 👁 👀 🗣 👤 👥 👶 👦 👧 👨 👩 👱‍♀️ 👱 👴 👵 👲 👳‍♀️ 👳 👮‍♀️ 👮 👷‍♀️ 👷' +
    ' 💂‍♀️ 💂 🕵️‍♀️ 🕵️ 👩‍⚕️ 👨‍⚕️ 👩‍🌾 👨‍🌾 👩‍🍳 👨‍🍳 👩‍🎓 👨‍🎓 👩‍🎤 👨‍🎤 👩‍🏫 👨‍🏫 👩‍🏭 👨‍🏭 👩‍💻 👨‍💻 👩‍💼 👨‍💼 👩‍🔧 👨‍🔧 👩‍🔬 👨‍🔬' +
    ' 👩‍🎨 👨‍🎨 👩‍🚒 👨‍🚒 👩‍✈️ 👨‍✈️ 👩‍🚀 👨‍🚀 👩‍⚖️ 👨‍⚖️ 🤶 🎅 👸 🤴 👰 🤵 👼 🤰 🙇‍♀️ 🙇 💁 💁‍♂️ 🙅 🙅‍♂️ 🙆 🙆‍♂️ 🙋 🙋‍♂️ 🤦‍♀️ 🤦‍♂️ 🤷‍♀' +
    '️ 🤷‍♂️ 🙎 🙎‍♂️ 🙍 🙍‍♂️ 💇 💇‍♂️ 💆 💆‍♂️ 🕴 💃 🕺 👯 👯‍♂️ 🚶‍♀️ 🚶 🏃‍♀️ 🏃 👫 👭 👬 💑 👩‍❤️‍👩 👨‍❤️‍👨 💏 👩‍❤️‍💋‍👩 👨‍❤️‍💋‍👨 👪 👨‍👩‍👧' +
    ' 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👩‍👩‍👦 👩‍👩‍👧 👩‍👩‍👧‍👦 👩‍👩‍👦‍👦 👩‍👩‍👧‍👧 👨‍👨‍👦 👨‍👨‍👧 👨‍👨‍👧‍👦 👨‍👨‍👦‍👦 👨‍👨‍👧‍👧 👩‍👦 👩‍👧' +
    ' 👩‍👧‍👦 👩‍👦‍👦 👩‍👧‍👧 👨‍👦 👨‍👧 👨‍👧‍👦 👨‍👦‍👦 👨‍👧‍👧 👚 👕 👖 👔 👗 👙 👘 👠 👡 👢 👞 👟 👒 🎩 🎓 👑 ⛑ 🎒 👝 👛 👜 💼 👓' +
    ' 🕶 🌂 ☂️';

  const EmojiArr = EMOJIS.split(' ');

  return EmojiArr;
};
