import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as RNLocalize from 'react-native-localize';
import { setLanguageAction } from '@store/actions/language.action';
import FlashMessage from 'react-native-flash-message';

/* import messaging from '@react-native-firebase/messaging';
import { Notifications } from 'react-native-notifications';
import { requestNotifications } from 'react-native-permissions';
import { callToast } from '@components/Toast'; */
import Container from '@components/Container';
import ModalConfirm from '@components/ModalConfirm';

class GlobalTask extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    const { selectedLanguage } = this.props;
    if (selectedLanguage) {
      this.handleLocaleChange(selectedLanguage.locale);
    } else this.handleLocaleChange();
  }

  handleLocaleChange = (locale = null) => {
    const { _setLanguageAction } = this.props;
    const locales = RNLocalize.getLocales();

    if (Array.isArray(locales)) {
      _setLanguageAction(locale || locales[0].languageTag);
    }
  };

  /* componentDidMount() {
    this.requestUserPermission();
    /** CallBack notificacion en FOREGROUND 
    messaging().onMessage(this.onMessageReceived);
    Notifications.events().registerNotificationOpened(this.onNotificationOpenedApp);
  }

  componentDidUpdate(prevProps) {
    const { alert } = this.props;
    if (alert !== prevProps.alert && alert) {
      this.setAlert();
    }
  }

  setAlert = () => {
    const { alert } = this.props;
    const { body } = alert;
    callToast(
      {
        isrender: true,
        type: 'danger',
        backgroundType: 'none',
        content: body,
      },
      (r) => this.setState({ toastData: r }),
      3000
    );
  };

  onMessageReceived = async (remoteMessage) => {
    const { notificacion } = remoteMessage;
    const { data } = remoteMessage;
    this.showNotification(notificacion || data);
  };

  onNotificationOpenedApp = async (notification, completion) => {
    try {
      const { payload } = notification.payload;
      // const {action, params} = payload;
      return completion();
    } catch (e) {
      completion();
    }
  };

  showNotification = (data) => {
    const { title, body } = data;
    Notifications.postLocalNotification({
      title,
      body,
      badge: 1,
      payload: data,
    });
  };

  requestUserPermission = async () => {
    const authStatusPermission = await requestNotifications(['alert', 'sound']);
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (!enabled || !authStatusPermission) {
      return;
    }
    this.getFcmToken();
  };
*/
  /**
   * GET THE FCM TOKEN TO RECEIVE NOTIFICATIONS
   
  getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      // console.warn('Your Firebase Token is:', fcmToken);
    }
  }; */

  render() {
    const { children, loading } = this.props;
    return (
      <Container padding={false} propLoading={loading}>
        <FlashMessage duration={4000} position="bottom" />
        {children}
        <ModalConfirm />
      </Container>
    );
  }
}

const mapStateToProps = ({
  alert,
  user,
  loading: { propLoading },
  language: { selectedLanguage },
}) => ({
  alert,
  user,
  selectedLanguage,
  loading: propLoading,
});

export default connect(mapStateToProps, { _setLanguageAction: setLanguageAction })(GlobalTask);
